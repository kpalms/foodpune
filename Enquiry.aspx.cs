﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Enquiry : System.Web.UI.Page
{
    BL_Enquiry bl_enquiry = new BL_Enquiry();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
         if (!IsPostBack)
        {
            try
            {

                common.bindCity(DdlselectCity);
            }
            catch
            {

            }
        }
    }
    protected void ButSend_Click(object sender, EventArgs e)
    {
        SetFill();
        bl_enquiry.Mode = "Insert";
        bl_enquiry.insertdata(bl_enquiry);
        Clear();

       
    }
    private void SetFill()
    {
        bl_enquiry.Date = common.GetDate();
        bl_enquiry.Name = txtname.Text;
        bl_enquiry.Email = txtemail.Text;
        bl_enquiry.Phone = txtphone.Text;
        bl_enquiry.Mobile = Txtmobileno.Text;
        bl_enquiry.RestaurantName = Txtrestaurant.Text;
        bl_enquiry.CityId = Convert.ToInt32(DdlselectCity.SelectedItem.Value);
        bl_enquiry.AreaId = Convert.ToInt32(ddlselectarea.SelectedItem.Value);
        bl_enquiry.Comments = txtcomments.Text;
    }


    private void Clear()
    {
        txtname.Text="";
        txtemail.Text="";
        Txtmobileno.Text="";
        Txtrestaurant.Text="";
        Txtrestaurant.Text="";
        txtcomments.Text="";
        txtphone.Text = "";
    }
    protected void ddlselectarea_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
    protected void ButClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void DdlselectCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cityid = Convert.ToInt32(DdlselectCity.SelectedItem.Value);
        common.bindAreabycity(ddlselectarea,cityid);
    }
}