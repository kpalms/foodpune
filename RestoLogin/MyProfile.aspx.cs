﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Services;
using System.Configuration;
public partial class MyProfile : System.Web.UI.Page
{
    static int restoid;
    string Category = "";
    BL_Restaurants bl_restaurant = new BL_Restaurants();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindAllRestaurantDetails();
            restoid = Convert.ToInt32(Session["RestaurantId"]);
            rptreview.DataSource = GetReviewData(1);
            rptreview.DataBind();
        }
    }

    BL_RestoLogin bl_restologin = new BL_RestoLogin();

    private void BindAllRestaurantDetails()
    {
        bl_restologin.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        // bl_restaurant.RestaurantId = 1;
        DataSet ds = bl_restologin.SelectAllRestaurantDetails(bl_restologin);

        rptcategory.DataSource = ds.Tables[0];
        rptcategory.DataBind();

        rptitem.DataSource = ds.Tables[1];
        rptitem.DataBind();

        //lstresto.DataSource = ds.Tables[2];
        //lstresto.DataBind();

        lblrestonamehead.Text = ds.Tables[2].Rows[0]["RestaurantName"].ToString();
        Restoimage.ImageUrl = "../"+ds.Tables[2].Rows[0]["Image"];
        lblRestaurantName.Text = ds.Tables[2].Rows[0]["RestaurantName"].ToString();
        lblDescription.Text = ds.Tables[2].Rows[0]["Description"].ToString();
        lblAddress.Text = ds.Tables[2].Rows[0]["Address"].ToString();





       // rptreview.DataSource = ds.Tables[3];
       // rptreview.DataBind();

        lstdeliverylocation.DataSource = ds.Tables[4];
        lstdeliverylocation.DataBind();

        rptpaymenttype.DataSource = ds.Tables[5];
        rptpaymenttype.DataBind();

        rptrestocat.DataSource = ds.Tables[6];
        rptrestocat.DataBind();

        rptrestocathead.DataSource = ds.Tables[6];
        rptrestocathead.DataBind();

        //rptrestocondtions.DataSource = ds.Tables[7];
        //rptrestocondtions.DataBind();

        lblMinimumOrders.Text = ds.Tables[7].Rows[0]["MinimumOrders"].ToString();
        lblDeliveryCharges.Text = ds.Tables[7].Rows[0]["DeliveryCharges"].ToString();
        lblPickupTime.Text = ds.Tables[7].Rows[0]["PickupTime"].ToString();
        lblDeliveryTime.Text = ds.Tables[7].Rows[0]["DeliveryTime"].ToString();
        lblMinimumOrders.Text = ds.Tables[7].Rows[0]["MinimumOrders"].ToString();

    }


    protected void rptitem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Control divHeader = e.Item.FindControl("dvCategory");

        if (divHeader != null)
        {
            Label lblCategory = (Label)divHeader.FindControl("lblCategory");
            if (lblCategory != null)
            {
                if (Category == lblCategory.Text)
                {
                    divHeader.Visible = false;
                }
                else
                {
                    Category = lblCategory.Text;
                }
            }
        }
    }


    //protected void rptitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{


    //    Label lblitemid = e.Item.FindControl("lblitemid") as Label;
    //    Label lblItem = e.Item.FindControl("lblItem") as Label;
    //    Label lblPrice = e.Item.FindControl("lblPrice") as Label;


    //    string id = lblitemid.Text;
    //    string name = lblItem.Text;
    //    string price = lblPrice.Text;


    //}


    [WebMethod]
    public static string GetReview(int pageIndex)
    {
        return GetReviewData(pageIndex).GetXml();
    }

    public static DataSet GetReviewData(int pageIndex)
    {
        BL_Restaurants bl_restaurant = new BL_Restaurants();

        DataSet ds = bl_restaurant.GetReviewData(bl_restaurant, pageIndex, restoid);
        return ds;
    }

}