﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecentOdr.aspx.cs" Inherits="RestoLogin_RecentOdr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
                <asp:Repeater ID="rptrecntorder" runat="server" onitemdatabound="rptrecntorder_ItemDataBound" >
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar">
                
                <img src="img/user.png" height="50" width="50"/></a>
                  
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name "><asp:Label ID="lblname"  runat="server" Text='<%#Eval("Name") %>'></asp:Label></a>
<span class="time date"><a href="OrdersDetails.aspx?Orderid=<%#Eval("OrderId") %>">View Details</a></span>

                    </div>

<div class="text comments">
 <asp:Label ID="lblDiscountId" runat="server" Text='<%#Eval("DiscountId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblBillAmount" runat="server" Text='<%#Eval("BillAmount") %>' Visible="false"></asp:Label>
<asp:Label ID="lbltotal" runat="server" Text='' ></asp:Label>
<asp:Label ID="lblDeliveryCharges" runat="server" Text='<%#Eval("DeliveryCharges") %>' Visible="false"></asp:Label>
</div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
</html>
