﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{

    BL_RestoLogin bl_restologin = new BL_RestoLogin();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnlogin_Click(object sender, EventArgs e)
    {
        SetFill();
        string RestaurantId = bl_restologin.OwnerLogin(bl_restologin);
        if (RestaurantId != "")
        {
            Session["RestaurantId"] = RestaurantId;
            Clear();
            lblInvalid.Visible = false;
            bl_restologin.Status = 1;
            bl_restologin.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
            bl_restologin.OwnerStatus(bl_restologin); 
            LoggedInRestuarentSessionID.AddSessionID(new LoggedInUser() { Name = RestaurantId, SessionID = Session.SessionID });
            Response.Redirect("~/RestoLogin/Chat.aspx");
           //Response.Redirect("~/RestoLogin/Home.html");
           // Response.Redirect("~/RestoLogin/demohome.html");
        }
        else {
            lblInvalid.Visible = true;
            lblInvalid.Text = "Invalid email and password";
        }
    }

    private void SetFill()
    {
        bl_restologin.UserName = txtusername.Text;
        bl_restologin.Password = txtpassword.Text;
    }

    private void Clear()
    {
        txtpassword.Text = "";
        txtusername.Text = "";
    }
}