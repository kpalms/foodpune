﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="OrdersDetails.aspx.cs" Inherits="Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script type = "text/javascript">
     function PrintPanel() {
         var panel = document.getElementById("<%=pnlContents.ClientID %>");
         var printWindow = window.open('', '', 'height=400,width=800');
         printWindow.document.write('<html><head><title>DIV Contents</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
    </script>


    <base target="main">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 
<br />
<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li> >> 
        <li><a href="#">Order Details</a></li>

    </ul>
</div>
<a href="RecentOrder.aspx">Back</a>

<div  class="widget">
<div class="widget-content">



<asp:Panel id="pnlContents" runat = "server">
<b   style="font-size:larger">Food Pune</b>

Customer Details

Name:<asp:Label ID="lblcustname" runat="server" ></asp:Label>
Delivery  Status :<asp:Label ID="lblDeliveryStatus" runat="server" ></asp:Label>
DateTime :<asp:Label ID="lbldate" runat="server" Text="Label"></asp:Label>
Delivery DateTime :<asp:Label ID="lbldeliverydatetime" runat="server" ></asp:Label>
Address Details :<asp:Label ID="lbladdressdetails" runat="server" ></asp:Label>



<asp:Repeater ID="rptcustomer" runat="server" onitemdatabound="rptcustomer_ItemDataBound" 
         >
<ItemTemplate>

<div id="dvcustomer" runat="server">

<asp:Repeater ID="rptOrders" runat="server" >
<ItemTemplate>
<asp:Label ID="lblitem" runat="server" Text='<%# Eval("Item") %>'></asp:Label><br />
<asp:Label ID="lblqty" runat="server" Text='<%# Eval("Qty") %>'></asp:Label><br />
<asp:Label ID="lblprice" runat="server" Text='<%# Eval("Price") %>'></asp:Label><br />
<asp:Label ID="Label1" runat="server" Visible="false" Text='<%# Eval("UserId") %>'></asp:Label>
</ItemTemplate>
</asp:Repeater>
</div>
</ItemTemplate>
</asp:Repeater>


SubTotal : <asp:Label ID="lblsubtotal" runat="server" Text="Label"></asp:Label>
<br />
<asp:Label ID="lbldiscount" runat="server"></asp:Label>
<br />
Delivery Charges : <asp:Label ID="lbldeliverycharges" runat="server" Text="Label"></asp:Label>
<hr />
Final Total :<asp:Label ID="lblfinaltotal" runat="server" Text="Label"></asp:Label>
<br />

    
</asp:Panel>

<asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick = "return PrintPanel();" />


</div>
</div>

</div>
</div>
</div>
</div>
</div>









</asp:Content>

