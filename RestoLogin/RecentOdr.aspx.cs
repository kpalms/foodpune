﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Data;
using System.Data.SqlClient;


public partial class RestoLogin_RecentOdr : System.Web.UI.Page
{

    BL_Order bl_order = new BL_Order();


    protected void Page_Load(object sender, EventArgs e)
    {
        bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        DataTable dt = bl_order.SelectRecentOrder(bl_order);
        rptrecntorder.DataSource = dt;
        rptrecntorder.DataBind();
    }

    protected void rptrecntorder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //int i;
        //decimal discount = 0;
        //Label lbldiscount = (Label)e.Item.FindControl("lblComments");
        //bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        //DataTable dt = bl_order.SelectRecentOrder(bl_order);

        //for (i = 0; i < dt.Rows.Count; i++)
        //{
        //    if ("" != dt.Rows[i]["DiscountType"].ToString())
        //    {
        //        if ("0" == dt.Rows[i]["DiscountType"].ToString())
        //        {
        //            discount = Convert.ToDecimal(Convert.ToDecimal(dt.Rows[i]["DiscountAmount"]) * Convert.ToDecimal(dt.Rows[i]["BillAmount"]) / 100);

        //        }
        //        else
        //        {
        //            discount = Convert.ToDecimal(dt.Rows[i]["DiscountAmount"].ToString());
        //        }
        //    }

        //    if (discount > 0)
        //    {
        //        lbldiscount.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[i]["BillAmount"]) + Convert.ToDecimal(dt.Rows[i]["DeliveryCharges"]) - discount);
        //    }
        //    else
        //    {
        //        lbldiscount.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[i]["BillAmount"]) + Convert.ToDecimal(dt.Rows[i]["DeliveryCharges"]));
        //    }

        //}


        Label lblDiscountId = (Label)e.Item.FindControl("lblDiscountId");
        Label lblBillAmount = (Label)e.Item.FindControl("lblBillAmount");
        Label lblDeliveryCharges = (Label)e.Item.FindControl("lblDeliveryCharges");
        Label lbltotal = (Label)e.Item.FindControl("lbltotal");
        decimal totol;
        if (lblDiscountId.Text != "0")
        {
            bl_order.DiscountId = Convert.ToInt32(lblDiscountId.Text);
            DataTable dt = bl_order.GetDiscountById(bl_order);
            if (dt.Rows[0]["DiscountType"].ToString() == "0")
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - (Convert.ToDecimal(lblBillAmount.Text) * Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) / 100) + Convert.ToDecimal(lblDeliveryCharges.Text);


            }
            else
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) + Convert.ToDecimal(lblDeliveryCharges.Text);
            }
        }
        else
        {
            totol = Convert.ToDecimal(lblBillAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text);
        }
        lbltotal.Text = Convert.ToString(totol);

    }
}