﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Menu : System.Web.UI.Page
{
    string Category = "";
    BL_RestoLogin bl_restologin = new BL_RestoLogin();
    BL_MenuSubItem bl_menusubitem = new BL_MenuSubItem();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMenues();
        }
       
    }

    private void BindMenues()
    {
        bl_restologin.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        DataTable dt = bl_restologin.SelectOwnerCatItem(bl_restologin);

        rptitem.DataSource = dt;
        rptitem.DataBind();
    }

    protected void rptitem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptmenusubitem = e.Item.FindControl("rptmenusubitem") as Repeater;
        Label lblCustomisation = e.Item.FindControl("lblCustomisation") as Label;
        Label lblitemid = e.Item.FindControl("lblitemid") as Label;
        Label lblPrice = e.Item.FindControl("lblPrice") as Label;
       LinkButton lnksubitemactive = e.Item.FindControl("lnksubitemactive") as LinkButton;

        Control divHeader = e.Item.FindControl("dvCategory");

        if (divHeader != null)
        {
            Label lblCategory = (Label)divHeader.FindControl("lblCategory");
            if (lblCategory != null)
            {
                if (Category == lblCategory.Text)
                {
                    divHeader.Visible = false;
                }
                else
                {
                    Category = lblCategory.Text;
                }
            }
        }


        if (lblCustomisation.Text == "1")
        {
            rptmenusubitem.Visible = true;
            lblPrice.Visible = false;
           // lnksubitemactive.Visible = false;
          
            BL_MenuSubItem bl_MenuSubItem = new BL_MenuSubItem();
            bl_MenuSubItem.ItemId = Convert.ToInt32(lblitemid.Text);
            DataTable dt = bl_MenuSubItem.SelectOwnerMenuSubItemByItemId(bl_MenuSubItem);
            rptmenusubitem.DataSource = dt;
            rptmenusubitem.DataBind();
        }
    }
    protected void rptitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        try
        {
            string lblitemid = e.CommandArgument.ToString();
            bl_restologin.ItemId = Convert.ToInt32(lblitemid);
            LinkButton lnkactive = e.Item.FindControl("lnkactive") as LinkButton;

                if (lnkactive.Text == "Active")
                {
                    bl_restologin.Active = 1;


                }
                else
                {
                    bl_restologin.Active = 0;
                }

                bl_restologin.OwnerItemActiveDeactive(bl_restologin);
            BindMenues();
        }
        catch
        {

        }
    }


    protected void rptmenusubitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            string MenuSubItemId = e.CommandArgument.ToString();
            bl_menusubitem.MenuSubItemId = Convert.ToInt32(MenuSubItemId);
            LinkButton lnkactive = e.Item.FindControl("lnksubitemactive") as LinkButton;

            if (lnkactive.Text == "Active")
            {
                bl_menusubitem.Active = 1;


            }
            else
            {
                bl_menusubitem.Active = 0;
            }

            bl_menusubitem.AddOwnerMenuSubItem(bl_menusubitem);
            BindMenues();
        }
        catch
        {

        }
    }
}