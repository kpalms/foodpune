﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Orders : System.Web.UI.Page
{
    string customer = "";
    BL_Order bl_order = new BL_Order();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetOrderDetails();
    }




    private void GetOrderDetails()
    {
        if (!string.IsNullOrEmpty ( Request.QueryString["OrderId"]))
        {
            bl_order.OrderId = Convert.ToInt32(Request.QueryString["OrderId"].ToString());
            DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
            rptcustomer.DataSource = ds.Tables[0];
            rptcustomer.DataBind();
        }

    }

    protected void rptcustomer_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        decimal price = 0;
        string discount = "";
        Control divHeader = e.Item.FindControl("dvcustomer");

        if (divHeader != null)
        {
            Label lblname = (Label)divHeader.FindControl("lblname");
            //if (lblname != null)
            //{
                //if (customer == lblname.Text)
                //{
                //    divHeader.Visible = false;
                //}
                //else
                //{
                    //customer = lblname.Text;
                    Repeater rptOrders = e.Item.FindControl("rptOrders") as Repeater;
                    bl_order.OrderId = Convert.ToInt32(Request.QueryString["OrderId"].ToString());
                    DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                         price = price + Convert.ToDecimal(Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString()));
                    }


                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        if ("0" == ds.Tables[1].Rows[0]["DiscountType"].ToString())
                        {
                            discount = Convert.ToString(Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountAmount"]) * price / 100);
                            lbldiscount.Text = "Discount " + ds.Tables[1].Rows[0]["DiscountAmount"] + "%" + " In : Rs." + discount;

                        }
                        else
                        {
                            discount = ds.Tables[1].Rows[0]["DiscountAmount"].ToString();
                            lbldiscount.Text = "Discount :  Rs." + discount + ".00";
                        }
                    }
                    lblsubtotal.Text = " Rs. " + Convert.ToString(price);
                    decimal deliverycharges = Convert.ToDecimal(ds.Tables[0].Rows[0]["DeliveryCharges"]);
                    lbldeliverycharges.Text = " Rs." + deliverycharges;
                    if (discount == "")
                    {
                        lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges);
                    }
                    else
                    {
                        lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges - Convert.ToDecimal(discount));
                    }


                    rptOrders.DataSource = ds.Tables[0];
                    rptOrders.DataBind();


                    lblcustname.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                    lbladdressdetails.Text = ds.Tables[2].Rows[0]["Addres"].ToString();
                    lblDeliveryStatus.Text = ds.Tables[2].Rows[0]["DeliveryType"].ToString();
                    lbldeliverydatetime.Text = ds.Tables[2].Rows[0]["DeliveryTime"].ToString();







               // }
            //}
        }
    }







    




}