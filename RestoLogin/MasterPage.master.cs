﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    BL_RestoLogin bl_restologin = new BL_RestoLogin();
    BL_Order bl_order = new BL_Order();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["RestaurantId"] != null)
        {
            lnkhome.Visible = true;
            lnkmenu.Visible=true;
            lnkmyprofile.Visible=true;
            lnkorders.Visible=true;
            lnklogout.Visible = true;
            lnkorderhistory.Visible = true;
            lnkneworder.Visible = true;
            lnkLaterOrder.Visible = true;
            bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"]);
            bl_order.Date = common.GetDate();
            string count=  bl_order.GetNewOrderCount(bl_order);
            lblnewordercount.Text = count;
        }
        else
        {
            lnkhome.Visible = false;
            lnkneworder.Visible = false;
            lnkmenu.Visible = false;
            lnkLaterOrder.Visible = false;
            lnkmyprofile.Visible = false;
            lnkorders.Visible = false;
            lnklogout.Visible = false;
            lnkorderhistory.Visible = false;
            //Response.Redirect("~/RestoLogin/Default.aspx");
        }
    }
     protected void lnklogout_Click(object sender, EventArgs e)
    {
        bl_restologin.Status = 0;
        bl_restologin.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        bl_restologin.OwnerStatus(bl_restologin);
        Session.Abandon();
        Response.Redirect("~/RestoLogin/Default.aspx");
    }
}
