﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="Menu.aspx.cs" Inherits="Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="css/pages/Menu.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 
<br /> 
<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li> >> 
        <li><a href="#">Menu Setting</a></li>

    </ul>
</div>

<br />

<div  class="widget">
<div class="widget-content">



<span class="span6" >

<asp:Repeater ID="rptitem" runat="server" onitemdatabound="rptitem_ItemDataBound" onitemcommand="rptitem_ItemCommand">
<ItemTemplate > 
<div id="dvCategory" runat="server" class="menuItemCatrow ">
    <div class="menuItemCatHead">
        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
    </div>
    <div class="span6">
         <img src='../<%#Eval("Image") %>' alt='<%#Eval("Image") %>' >
    </div>

</div>

<div>

</div>

<div class="span6" >

<span class="span3 menuItemName" >
<asp:Label ID="lblitemid" runat="server" Text='<%#Eval("ItemId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblCustomisation" runat="server" Text='<%#Eval("Customisation") %>' Visible="false"></asp:Label>
<asp:Label ID="lblItem" CssClass="item_name" runat="server" Text='<%#Eval("Item") %>'></asp:Label>
<asp:Label ID="lblitemitemdescripation" CssClass="item_name" runat="server" Text='<%#Eval("ItemDescription") %>'></asp:Label>
</span>

<div class="span1 menuItemPrice" >
<asp:Label ID="lblPrice" CssClass="item_price" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
</div>

<span class="span1 addtocartbut" >
<%--<asp:ImageButton ID="imgadd" runat="server" ImageUrl="~/img/add.png"/>--%>
  <asp:LinkButton ID="lnkactive" runat="server" Text='<%#Eval("Action") %>' CommandArgument='<%#Bind("ItemId") %>'>LinkButton</asp:LinkButton>
</span>

 <span class="span6" >
    <asp:Repeater ID="rptmenusubitem" runat="server" Visible="false"  onitemcommand="rptmenusubitem_ItemCommand">
    <ItemTemplate>
        <asp:Label ID="lblMenuSubItemId" runat="server" Text='<%#Eval("MenuSubItemId") %>' Visible="false"></asp:Label>
        <span class="span3 menuItemName" ><asp:Label ID="lblName" CssClass="item_name" Font-Size="Small" runat="server" Text='<%#Eval("Name") %>'></asp:Label></span>
        <div class="span1 menuItemPrice" style="width:9%"><asp:Label ID="lblPrice" Font-Size="Small" CssClass="item_name" runat="server" Text='<%#Eval("Price") %>'></asp:Label></div>
        <span class="span1 addtocartbut" style="width:9%"><asp:LinkButton ID="lnksubitemactive" runat="server" Text='<%#Eval("Active") %>' CommandArgument='<%#Bind("MenuSubItemId") %>'>LinkButton</asp:LinkButton></span>
    </ItemTemplate>
    </asp:Repeater>
    </span>


</div>
</ItemTemplate>
</asp:Repeater>

</span>

</div>
</div>

</div>
</div>
</div>
</div>
</div>


<%--<asp:Repeater ID="rptitem" runat="server" onitemdatabound="rptitem_ItemDataBound" 
        onitemcommand="rptitem_ItemCommand">
<ItemTemplate > 


<div id="dvCategory" runat="server" >
    <div class="menuItem">
        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
    </div>
    <div>
        <img src='Images/MenuCategory/<%#Eval("ImageCat") %>' alt='<%#Eval("ImageCat") %>' height="30px" width="100px">
    </div>
</div>

<div>
<asp:Label ID="lblitemid" runat="server" Text='<%#Eval("ItemId") %>' Visible="false"></asp:Label>
 
<div >
<asp:Label ID="lblItem" CssClass="item_name" runat="server" Text='<%#Eval("Item") %>'></asp:Label>

<asp:Label ID="lblPrice" CssClass="item_price" runat="server" Text='<%#Eval("Price") %>'></asp:Label>

    <asp:LinkButton ID="lnkactive" runat="server" Text='<%#Eval("Action") %>' CommandArgument='<%#Bind("ItemId") %>'>LinkButton</asp:LinkButton>
</div>
</div>

</ItemTemplate>
</asp:Repeater>--%>




</asp:Content>

