﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="RecentOrder.aspx.cs" Inherits="RestoLogin_RecentOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="css/pages/RecentOrder.css"



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script type = "text/javascript">
     function PrintPanel() {
 
         var panel = document.getElementById("<%=pnlContents.ClientID %>");
         var printWindow = window.open('', '', 'Height=auto,width=400');
         printWindow.document.write('<html><head><title>Print Bill</title>');
         printWindow.document.write('</head><body >');
         printWindow.document.write(panel.innerHTML);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         setTimeout(function () {
             printWindow.print();
         }, 500);
         return false;
     }
    </script>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  
<br />
<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li> >> 
        <li><a href="#">Recent Order</a></li>

    </ul>
</div>
<br />

<div  class="widget">
<div class="widget-content">



<%--tab3--%>					
<div class="tab-pane " id="Review">
<div class="span10 review" >
<fieldset>

    <div class="widget">
            <div class="widget-header"> <i class="icon-file"></i>
              <h3> Recent Order</h3>
            </div>
           
            <div class="widget-content">
            
              <ul class="messages_layout">
              <span class="span4" >
              <div id="dvReviews">
              
               <asp:Repeater ID="rptrecntorder" runat="server" 
                      onitemdatabound="rptrecntorder_ItemDataBound" 
                      onitemcommand="rptrecntorder_ItemCommand" >
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar">
                
                <img src="img/user.png" height="50" width="50"/></a>
                  
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name "><asp:Label ID="lblname"  runat="server" Text='<%#Eval("Name") %>'></asp:Label></a>
<span class="time date">
<%--<a href="OrdersDetails.aspx?Orderid=<%#Eval("OrderId") %>">View Details</a>--%>
    <asp:LinkButton ID="lnkorderid" runat="server" CommandArgument='<%#Bind("OrderId") %>'>View Details</asp:LinkButton>
</span>

                    </div>

<div class="text comments">
 <asp:Label ID="lblDiscountId" runat="server" Text='<%#Eval("DiscountId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblBillAmount" runat="server" Text='<%#Eval("BillAmount") %>' Visible="false"></asp:Label>
<asp:Label ID="lbltotal" runat="server" Text='' ></asp:Label>
<asp:Label ID="lblDeliveryCharges" runat="server" Text='<%#Eval("DeliveryCharges") %>' Visible="false"></asp:Label>
</div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               </span>


<%--++++++++++ order Details +++++++++++++++++++--%>

                <span class="span5" >
                <asp:Panel id="pnlContents" runat = "server" >
<b   style="font-size:larger">Food Pune</b>

Customer Details
<div class="span4" >
<table border="1">
<tr>
<td valign="top">
<table>
<tr>
<td><span class="head">Name:</span></td>
<td><span class="infohead"><asp:Label ID="lblcustname" runat="server" ></asp:Label></span></td>
</tr>
<tr>
<td><span class="head">Delivery Status :</span></td>
<td><span class="infohead"><asp:Label ID="lblDeliveryStatus" runat="server" ></asp:Label></span></td>
</tr>
<tr>
<td><span class="head">Delivery DateTime :</span></td>
<td><span class="infohead"><asp:Label ID="lbldeliverydatetime" runat="server" ></asp:Label></span></td>
</tr>
</table>
</td>

<td valign="top">
<table>
<tr>
<td valign="top"><span class="head">Address Details :</span></td>
<td><span class="infohead">
<asp:Label ID="lblCompany" runat="server" ></asp:Label>
<asp:Label ID="lblFlatNo" runat="server" ></asp:Label>
<asp:Label ID="lblAppartment" runat="server" ></asp:Label>
<asp:Label ID="lblAddress" runat="server" ></asp:Label>
<asp:Label ID="lblLandmark" runat="server" ></asp:Label>
<asp:Label ID="lblAreaName" runat="server" ></asp:Label>
<asp:Label ID="lblCityName" runat="server" ></asp:Label>
<asp:Label ID="lblAlternateContactNo" runat="server" ></asp:Label>
</span></td>
</tr>
</table>
</td>


</tr>







</table>


</div>


<div class="span4" style="border:1px solid #000;margin-top:10px" >
<asp:Repeater ID="rptOrdersDetails" runat="server" >
<HeaderTemplate>
<table border="0" width="100%">
<tr>
<td>ItemName</td>
<td>Qty</td>
<td>Price</td>
</tr>
</HeaderTemplate>
<ItemTemplate>
<tr>
<td>
<span class="span1 menuItemName" >
<asp:Label ID="lblitem" runat="server" Text='<%# Eval("Item") %>'></asp:Label>
</span>
</td>
<td>
<div class="span1 menuItemPrice" >
<asp:Label ID="lblqty" runat="server" Text='<%# Eval("Qty") %>'></asp:Label>
</div>
</td>
<td>
<span class="span1 addtocartbut" >
<asp:Label ID="lblprice" runat="server" Text='<%# Eval("Price") %>'></asp:Label>
</span>
<asp:Label ID="Label1" runat="server" Visible="false" Text='<%# Eval("UserId") %>'></asp:Label>
</td>
</tr>

</ItemTemplate>

<FooterTemplate>
</table>
</FooterTemplate>
</asp:Repeater>
</div>


<div class="span4" style="margin-top:10px">
SubTotal : <asp:Label ID="lblsubtotal" runat="server" Text="Label"></asp:Label>
<br />
<asp:Label ID="lbldiscount" runat="server"></asp:Label>
<br />
Delivery Charges : <asp:Label ID="lbldeliverycharges" runat="server" Text="Label"></asp:Label>
<hr />
Final Total :<asp:Label ID="lblfinaltotal" runat="server" Text="Label"></asp:Label>
<br />
</div>

</asp:Panel>
<div class="span4" style="border:1px solid #000">
<asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick = "return PrintPanel();" />

</div>
                </span>

<%--++++++++++++++++++++++++++++++=--%>


              </ul>
             
            </div>
          
          </div>

</fieldset>


</div>
</div>









</div>
</div>


</div>
</div>
</div>
</div>
</div>




</asp:Content>

