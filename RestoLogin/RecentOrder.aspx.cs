﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class RestoLogin_RecentOrder : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
            DataTable dt = bl_order.SelectRecentOrder(bl_order);
            rptrecntorder.DataSource = dt;
            rptrecntorder.DataBind();
        }

    }

    protected void rptrecntorder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Label lblDiscountId = (Label)e.Item.FindControl("lblDiscountId");
        Label lblBillAmount = (Label)e.Item.FindControl("lblBillAmount");
        Label lblDeliveryCharges = (Label)e.Item.FindControl("lblDeliveryCharges");
         Label lbltotal = (Label)e.Item.FindControl("lbltotal");
        decimal totol;
        if (lblDiscountId.Text != "0")
        {
            bl_order.DiscountId = Convert.ToInt32(lblDiscountId.Text);
            DataTable dt=bl_order.GetDiscountById(bl_order);
            if (dt.Rows[0]["DiscountType"].ToString() == "0")
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) -(Convert.ToDecimal(lblBillAmount.Text) * Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) / 100) + Convert.ToDecimal(lblDeliveryCharges.Text);
                    
                    
            }
            else {
                totol = Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) + Convert.ToDecimal(lblDeliveryCharges.Text);
            }
        }
        else
        { 
             totol = Convert.ToDecimal(lblBillAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text);
        }
        lbltotal.Text=Convert.ToString(totol);

    }
    protected void rptrecntorder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        bl_order.OrderId = Convert.ToInt32(e.CommandArgument.ToString());
        DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
        rptOrdersDetails.DataSource = ds.Tables[0];
        rptOrdersDetails.DataBind();

        decimal price = 0;
        string discount = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                price = price + Convert.ToDecimal(Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString()) * Convert.ToDecimal(ds.Tables[0].Rows[i]["Qty"].ToString()));
            }


            if (ds.Tables[1].Rows.Count > 0)
            {
                if ("0" == ds.Tables[1].Rows[0]["DiscountType"].ToString())
                {
                    discount = Convert.ToString(Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountAmount"]) * price / 100);
                    lbldiscount.Text = "Discount " + ds.Tables[1].Rows[0]["DiscountAmount"] + "%" + " In : Rs." + discount;

                }
                else
                {
                    discount = ds.Tables[1].Rows[0]["DiscountAmount"].ToString();
                    lbldiscount.Text = "Discount :  Rs." + discount + ".00";
                }
            }
            lblsubtotal.Text = " Rs. " + Convert.ToString(price);
            decimal deliverycharges = Convert.ToDecimal(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            lbldeliverycharges.Text = " Rs." + deliverycharges;
            if (discount == "")
            {
                lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges);
            }
            else
            {
                lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges - Convert.ToDecimal(discount));
            }


        lblcustname.Text = ds.Tables[0].Rows[0]["Name"].ToString();
       
        lblDeliveryStatus.Text = ds.Tables[2].Rows[0]["DeliveryType"].ToString();
        lbldeliverydatetime.Text = ds.Tables[2].Rows[0]["DeliveryTime"].ToString();

        lblCompany.Text = ds.Tables[2].Rows[0]["Company"].ToString();
        lblFlatNo.Text = ds.Tables[2].Rows[0]["FlatNo"].ToString();
        lblAppartment.Text = ds.Tables[2].Rows[0]["Appartment"].ToString();
        lblAddress.Text = ds.Tables[2].Rows[0]["Address"].ToString();
        lblLandmark.Text = ds.Tables[2].Rows[0]["Landmark"].ToString();
        lblAreaName.Text = ds.Tables[2].Rows[0]["AreaName"].ToString();
        lblCityName.Text = ds.Tables[2].Rows[0]["CityName"].ToString();
        lblAlternateContactNo.Text = ds.Tables[2].Rows[0]["AlternateContactNo"].ToString();

    }

    //protected void rptcustomer_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
        //decimal price = 0;
        //string discount = "";
        //Control divHeader = e.Item.FindControl("dvcustomer");

        //if (divHeader != null)
        //{
        //    Label lblname = (Label)divHeader.FindControl("lblname");
        //    Repeater rptOrders = e.Item.FindControl("rptOrders") as Repeater;
        //    //bl_order.OrderId = Convert.ToInt32(Request.QueryString["OrderId"].ToString());
        //    bl_order.OrderId = Convert.ToInt32(48);
        //    DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
           
            
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        price = price + Convert.ToDecimal(Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString()));
        //    }


        //    if (ds.Tables[1].Rows.Count > 0)
        //    {
        //        if ("0" == ds.Tables[1].Rows[0]["DiscountType"].ToString())
        //        {
        //            discount = Convert.ToString(Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountAmount"]) * price / 100);
        //            lbldiscount.Text = "Discount " + ds.Tables[1].Rows[0]["DiscountAmount"] + "%" + " In : Rs." + discount;

        //        }
        //        else
        //        {
        //            discount = ds.Tables[1].Rows[0]["DiscountAmount"].ToString();
        //            lbldiscount.Text = "Discount :  Rs." + discount + ".00";
        //        }
        //    }
        //    lblsubtotal.Text = " Rs. " + Convert.ToString(price);
        //    decimal deliverycharges = Convert.ToDecimal(ds.Tables[0].Rows[0]["DeliveryCharges"]);
        //    lbldeliverycharges.Text = " Rs." + deliverycharges;
        //    if (discount == "")
        //    {
        //        lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges);
        //    }
        //    else
        //    {
        //        lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges - Convert.ToDecimal(discount));
        //    }


        //    rptOrders.DataSource = ds.Tables[0];
        //    rptOrders.DataBind();


        //    lblcustname.Text = ds.Tables[0].Rows[0]["Name"].ToString();
        //    lbladdressdetails.Text = ds.Tables[2].Rows[0]["Addres"].ToString();
        //    lblDeliveryStatus.Text = ds.Tables[2].Rows[0]["DeliveryType"].ToString();
        //    lbldeliverydatetime.Text = ds.Tables[2].Rows[0]["DeliveryTime"].ToString();




    //}

}