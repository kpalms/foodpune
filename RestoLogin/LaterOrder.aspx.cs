﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RestoLogin_NewOrder : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindneworder();
        }

    }


    protected void rptrecntorder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Label lblDiscountId = (Label)e.Item.FindControl("lblDiscountId");
        Label lblBillAmount = (Label)e.Item.FindControl("lblBillAmount");
        Label lblDeliveryCharges = (Label)e.Item.FindControl("lblDeliveryCharges");
        Label lbltotal = (Label)e.Item.FindControl("lbltotal");
        decimal totol;
        if (lblDiscountId.Text != "0")
        {
            bl_order.DiscountId = Convert.ToInt32(lblDiscountId.Text);
            DataTable dt = bl_order.GetDiscountById(bl_order);
            if (dt.Rows[0]["DiscountType"].ToString() == "0")
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - (Convert.ToDecimal(lblBillAmount.Text) * Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) / 100) + Convert.ToDecimal(lblDeliveryCharges.Text);


            }
            else
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) + Convert.ToDecimal(lblDeliveryCharges.Text);
            }
        }
        else
        {
            totol = Convert.ToDecimal(lblBillAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text);
        }
        lbltotal.Text = Convert.ToString(totol);

    }
    protected void rptrecntorder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "view")
        {
            bl_order.OrderId = Convert.ToInt32(e.CommandArgument.ToString());
            DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
            rptOrdersDetails.DataSource = ds.Tables[0];
            rptOrdersDetails.DataBind();

            decimal price = 0;
            string discount = "";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                price = price + Convert.ToDecimal(Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString()) * Convert.ToDecimal(ds.Tables[0].Rows[i]["Qty"].ToString()));
            }


            if (ds.Tables[1].Rows.Count > 0)
            {
                if ("0" == ds.Tables[1].Rows[0]["DiscountType"].ToString())
                {
                    discount = Convert.ToString(Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountAmount"]) * price / 100);
                    lbldiscount.Text = "Discount " + ds.Tables[1].Rows[0]["DiscountAmount"] + "%" + " In : Rs." + discount;

                }
                else
                {
                    discount = ds.Tables[1].Rows[0]["DiscountAmount"].ToString();
                    lbldiscount.Text = "Discount :  Rs." + discount + ".00";
                }
            }
            lblsubtotal.Text = " Rs. " + Convert.ToString(price);
            decimal deliverycharges = Convert.ToDecimal(ds.Tables[0].Rows[0]["DeliveryCharges"]);
            lbldeliverycharges.Text = " Rs." + deliverycharges;
            if (discount == "")
            {
                lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges);
            }
            else
            {
                lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges - Convert.ToDecimal(discount));
            }
        }


        if (e.CommandName == "ok")
        {
            bl_order.OrderId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_order.UpdateNewOrder(bl_order);
            bindneworder();
        }

    }


    private void bindneworder()
    {
        bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        DataTable dt = bl_order.SelectLaterOrderById(bl_order);
        rptrecntorder.DataSource = dt;
        rptrecntorder.DataBind();


    }
}