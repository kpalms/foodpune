﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="Chat.aspx.cs" Inherits="RestoLogin_Chat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="css/pages/chat.css" />

<%--<script type="text/javascript" src="../Search/jquery-1.11.3.min.js"></script>--%>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<style>


.black_overlay{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}

.white_content {
	display: none;
	position: absolute;
	top: 25%;
	left: 25%;
	width: 50%;
	height: 50%;
	padding: 16px;
	border: 16px solid orange;
	background-color: white;
	z-index:1002;
	overflow: auto;
}


</style>  

<div id="window" style="display:none" class="white_content">  
 
<p id="dlgMsg">You Got Order!</p>  
<div style="display:block" id="divButton">

<input type="button" id="btnaccept"  value="Accept" class="btn btn-success"/>
<input type="button" id="btnreject"  value="Reject" class="btn btn-success"/> 
</div>
<div style="display:none" id="divReject">
<input type="text"  id="txtRejectReason" style="height:100px;width:200px"  />
<input type="button" value="OK" id="btnrejectandclose" class="btn btn-success"/>
</div>
</div>
<div id="fade" class="black_overlay"></div>

<input id="show" type="button" value="Dont Post" onclick="OpenPopUp('mssdasdas')" />
<input type="button" id="btn" value="insert sale" onclick="SaveOrderDetails('a','b')"/>



    <script type="text/javascript">





        function OpenPopUp(message) {

            debugger
            var notifymessage = message;
            document.getElementById('dlgMsg').innerHTML = notifymessage.message;
            //            document.getElementById('window').show();
            document.getElementById('window').style.display = 'block';
            document.getElementById('fade').style.display = 'block'
            $(".my_audio").trigger('play');

            document.getElementById('btnaccept').onclick = function () {
            $(".my_audio").trigger('pause');
            document.getElementById('window').style.display = 'none';
            document.getElementById('fade').style.display = 'none'
            SaveOrderDetails('Accept', message.orderid);

            }


                document.getElementById('btnreject').onclick = function () {
                $(".my_audio").trigger('pause');
                document.getElementById('divReject').style.display = 'block';

                document.getElementById('btnrejectandclose').onclick = function () {
                document.getElementById('window').style.display = 'none';
                document.getElementById('divReject').style.display = 'none';
                var resion = document.getElementById('txtRejectReason').value;
                RejectOrderDetails('Reject', message.orderid, resion);
                }
            }
        }

//        function SaveOrderDetails(status, orderid) {


//            alert(status + '    ' + orderid);
//        }

   
        function SaveOrderDetails(status, orderid) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WebService.asmx/UpdateCall",
                data: "{'status':'"+status+"','orderid':'"+orderid+"'}",
                dataType: "json"
            });
        }

        function RejectOrderDetails(status, orderid, resion) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WebService.asmx/RejectCall",
                data: "{'status':'" + status + "','orderid':'" + orderid + "','resion':'" + resion + "'}",
                dataType: "json"
            });
        }

</script>



<table style="border:1px solid #000; width:100%;height:768px" height="768px">
<tr>
<td>



    <div style="display:none">
    <audio class="my_audio" controls preload="none">
    <source  src="../2.mp3" type="audio/mpeg"></source>
    </audio>
    </div>




    <script type="text/javascript">
        $(function () {
            //            debugger

            var IWannaChat = $.connection.OrderNotificationHub;

            IWannaChat.client.addMessage = function (message) {
                $('#listMessages').append('<li>' + message + '</li>');
                $(".my_audio").trigger('play');
            };

            IWannaChat.client.received = function (message) {
                               debugger
                $('#listMessages').append('<li>' + message.message + '</li>');
//                $(".my_audio").trigger('play');

                OpenPopUp(message);


            };

            $("#BraodcastMessge").click(function () {
                //                debugger
                IWannaChat.server.send($('#txtMessage').val());

            });

            $("#SendMessage").click(function () {
                //                debugger
                IWannaChat.server.send($('#txtSessionID').val(), $('#txtMessage').val());

            });

            //            debugger
            $.connection.hub.start();

            $(document).ready(function () {

                $(".my_audio").trigger('load');
            });



        });
    </script>


        <ul id="listMessages">
        </ul>

<div id="result1"></div>
<div style="display:none">
<asp:TextBox ID="txtSessionID" runat="server" Width="422px"></asp:TextBox>
</div>

</td>
</tr>
</table>
</asp:Content>

