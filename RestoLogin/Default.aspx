﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    



<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 

<div  class="widget">
<div class="widget-content">



<div class="account-container">
	
	<div class="content clearfix">
		
		
			<h1>Welcome</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>

                <asp:Label ID="lblInvalid" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
				
				<div class="field">
					<label for="username">Email Address</label>
                    <asp:TextBox ID="txtusername" runat="server" placeholder="Email Address" class="login username-field" required></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="enter email address" ControlToValidate="txtusername" 
                    ValidationGroup="login" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                    runat="server" ErrorMessage="Envalid email" ControlToValidate="txtusername" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ForeColor="Red" ValidationGroup="login"></asp:RegularExpressionValidator>
				</div> 
				
				<div class="field">
					<label for="password">Password:</label>
                 <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" required class="login password-field"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                ErrorMessage="Please enter mobile no" ControlToValidate="txtpassword" 
                ValidationGroup="login" ForeColor="Red"></asp:RequiredFieldValidator>
                </div> 
				
			</div> 
			
			<div class="login-actions">
				
        <asp:Button ID="btnlogin" runat="server" Text="Log In" 
        onclick="btnlogin_Click" class="button btn btn-success btn-large" ValidationGroup="login"/>

			</div> 
				
	</div> 
	
</div>


</div>
</div>


</div>
</div>
</div>
</div>
</div>


</asp:Content>

