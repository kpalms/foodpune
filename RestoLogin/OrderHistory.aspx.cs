﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class RestoLogin_OrderHistory : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();
   static DateTime date1, date;
   string price = "";
   Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        TextBox1.Text = common.GetDate().ToString("dd-MM-yyyy");
        TextBox2.Text = common.GetDate().ToString("dd-MM-yyyy");
       binddetails(Convert.ToDateTime(TextBox1.Text), Convert.ToDateTime(TextBox2.Text));
    }


    protected void rptorderhistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int i;
        decimal discount = 0;
        Label lbldiscount = (Label)e.Item.FindControl("lblComments");
        bl_order.FromDate = date;
        bl_order.ToDate = date1;
        bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        DataTable dt = bl_order.SelectOrderHistory(bl_order);

        for( i=0; i < dt.Rows.Count ;i++ )
        {

        if ("" != dt.Rows[i]["DiscountType"].ToString())
        {
            if ("0" == dt.Rows[i]["DiscountType"].ToString())
            {
                discount = Convert.ToDecimal(Convert.ToDecimal(dt.Rows[i]["DiscountAmount"]) * Convert.ToDecimal(dt.Rows[i]["BillAmount"]) / 100);
            }
            else
            {
                discount = Convert.ToDecimal(dt.Rows[i]["DiscountAmount"].ToString());
            }
        }

        if (discount > 0)
        {
            lbldiscount.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[i]["BillAmount"]) + Convert.ToDecimal(dt.Rows[i]["DeliveryCharges"]) - discount);
        }
        else
        {
            lbldiscount.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[i]["BillAmount"]) + Convert.ToDecimal(dt.Rows[i]["DeliveryCharges"]));
        }
        }

    }
    protected void calone_SelectionChanged(object sender, EventArgs e)
    {

    }
    protected void btnsee_Click(object sender, EventArgs e)
    {
        date =Convert.ToDateTime(TextBox1.Text);
        date1 = Convert.ToDateTime(TextBox2.Text);
        binddetails(date, date1);
    }

    private void binddetails(DateTime date, DateTime date1)
    {
         bl_order.FromDate = date;
        bl_order.ToDate = date1;
        bl_order.RestaurantId = Convert.ToInt32(Session["RestaurantId"].ToString());
        DataTable dt = bl_order.SelectOrderHistory(bl_order);
        rptorderhistory.DataSource = dt;
        rptorderhistory.DataBind();
    }
}