﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RestoLogin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderHistory.aspx.cs" Inherits="RestoLogin_OrderHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" href="Calender/style.css">
<script type="text/javascript" src="Calender/jquery.min.js"></script>
	<script type="text/javascript" src="Calender/jquery-ui.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
     <div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  
<br />
<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li> >> 
        <li><a href="#">Order History</a></li>

    </ul>
</div>

<br />

<div  class="widget">
<div class="widget-content">



<%--tab3--%>					
<div class="tab-pane " id="Review">
<div class="span10 review" >
<fieldset>

    <div class="widget">
            <div class="widget-header"> <i class="icon-file"></i>
              <h3> Order History</h3>
            </div>
           
            <div class="widget-content">
            
              <ul class="messages_layout">

              <div id="dvReviews">
              <%--<asp:Calendar ID="calone" runat="server" 
        onselectionchanged="calone_SelectionChanged"></asp:Calendar>
     <asp:Calendar ID="caltwo" runat="server"></asp:Calendar>--%>
                  <asp:TextBox ID="TextBox1" runat="server" class="datepicker" ></asp:TextBox>
            <asp:TextBox ID="TextBox2" runat="server" class="datepicker" ></asp:TextBox>
    <asp:Button ID="btnsee" runat="server" Text="Show" onclick="btnsee_Click" />


                <asp:Repeater ID="rptorderhistory" runat="server" onitemdatabound="rptorderhistory_ItemDataBound" >
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar">
                
                <img src="img/user.png" height="50" width="50"/></a>
                  
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name "><asp:Label ID="lblname"  runat="server" Text='<%#Eval("Name") %>'></asp:Label></a>
<span class="time date"><a href="OrdersDetails.aspx?Orderid=<%#Eval("OrderId") %>">View Details</a></span>

                    </div>

<div class="text comments"> <asp:Label ID="lblComments" runat="server" Text='<%#Eval("BillAmount") %>'></asp:Label></div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               
              </ul>
             
            </div>
          
          </div>

</fieldset>


</div>
</div>









</div>
</div>


</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
    });
</script>


</asp:Content>

