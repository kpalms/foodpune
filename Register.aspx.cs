﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    BL_UserDetails bl_userdetails = new BL_UserDetails();
    Common common = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        lnkresto.Text = Request.QueryString["rn"].ToString();
        lnkcityname.Text = "Restaurants in " + Request.QueryString["area"].ToString();
    }
    protected void btnLogIn_Click(object sender, EventArgs e)
    {
        bl_userdetails.Email = txtUserName.Text;
        bl_userdetails.Password = txtPassword.Text;
        DataTable dt = bl_userdetails.CheckUserLogin(bl_userdetails);
        if (dt.Rows.Count > 0)
        {
            Session["UserId"] = dt.Rows[0]["UserId"].ToString();
            Session["username"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
            Session["email"] = txtUserName.Text;
            //if (Request.QueryString["pg"].ToString() == "checkout")
            //{
            //Response.Redirect("CheckOut.aspx?pg=checkout&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
           // }
            Response.Redirect("CheckOut.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");


        }
        else {
            lblInvalid.Visible = true;
            lblInvalid.Text = "invalid?please try again";
        }
        
       // Response.Redirect("CheckOut.aspx?key=1&rn=abc&city=pune&area=katraj&ct=5&at=6");


    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bl_userdetails.Email=txtEmail.Text;
        DataTable dt = bl_userdetails.CheckUserEmailValidation(bl_userdetails);
        if (dt.Rows.Count == 0 )
        {
            setField();
        bl_userdetails.Mode = "Insert";
        int userid = bl_userdetails.InsertUserDetails(bl_userdetails);
        Session["UserId"] = Convert.ToString(userid);
        Session["username"] = txtFirstName.Text;
        Session["email"] = txtEmail.Text;

      



        //EmailSms email = new EmailSms();
        //string body = "<table><tr><td>User Name :</td><td>'" + txtEmail.Text + "'</td></tr><tr><td>Password :</td><td>'" + txtPassword.Text + "'</td></tr></table>";
        //email.SendMail("Welcome To Food Pune", body, txtEmail.Text);
        //Response.Redirect("CheckOut.aspx?key=1&rn=abc&city=pune&area=katraj&ct=5&at=6");
      //  Response.Redirect("CheckOut.aspx?pg=checkout&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
        Clear();
            Response.Redirect("CheckOut.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");


        }

        else
        {
            lblmessage.Text = "This Eamil Id Is Already Existing Please Login";
            Clear();
        }
        

    }

    public void setField()
    {
        bl_userdetails.Date = common.GetDate();
        bl_userdetails.FirstName = txtFirstName.Text;
        if (radiomale.Checked == true)
        {
            bl_userdetails.Gender = 0;
        }
        else
        {
            bl_userdetails.Gender = 1;
        }
        bl_userdetails.MiddleName = txtMiddleName.Text;
        bl_userdetails.LastName = txtLastName.Text;
        bl_userdetails.Mobile = txtMobile.Text;
        bl_userdetails.Email = txtEmail.Text;
        bl_userdetails.Password = txtRepeatPassword.Text;
    }

    public void Clear()
    {
        txtFirstName.Text = "";
        txtMiddleName.Text = "";
        txtLastName.Text = "";
        txtMobile.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        txtRepeatPassword.Text = "";
    }


    protected void lnkcityname_Click(object sender, EventArgs e)
    {
        Response.Redirect("Restaurants.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "");
    }


    protected void lnkresto_Click(object sender, EventArgs e)
    {
        Response.Redirect("Menu.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
    }

}