﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >



<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 


<%--<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Restaurants in <asp:Label ID="lblcity" runat="server" Text="Label"></asp:Label></a></li>
        <li><asp:Label ID="lblarea" runat="server" Text="Label"></asp:Label></li>
          <li><asp:Label ID="lblrestoname" runat="server" Text=''></asp:Label></li>
    </ul>
    <asp:Label ID="lblctid" runat="server" Text='' Visible="false"></asp:Label>
    <asp:Label ID="lblarid" runat="server" Text='' Visible="false"></asp:Label>
        <asp:Label ID="lblrestoid" runat="server" Text='' Visible="false"></asp:Label>

</div>--%>



<div  class="widget">
<div class="widget-content">

    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>

<div class="account-container register">
<div class="content clearfix">
<h1>Signup for Free Account</h1>			
<div class="login-fields">
<p>Create your free account:</p>

<div class="field">
<label for="firstname">First Name:</label>
<asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" class="textboxes" required></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
ErrorMessage="Please enter first name" ControlToValidate="txtFirstName" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
  
</div> 


<div class="field">
<label for="firstname">Middle Name:</label>
<asp:TextBox ID="txtMiddleName" runat="server" class="signup" placeholder="Middle Name"></asp:TextBox>
</div> 

<div class="field">
<label for="firstname">Last Name:</label>
<asp:TextBox ID="txtLastName" runat="server" class="signup" required placeholder="Last Name"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
ErrorMessage="Please enter last name" ControlToValidate="txtLastName" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
 
</div> 

<div class="field">
<label for="firstname">Mobile :</label>
<asp:TextBox ID="txtMobile" runat="server" class="signup" required placeholder="Mobile"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
ErrorMessage="Please enter mobile no" ForeColor="Red" ControlToValidate="txtMobile" 
ValidationGroup="new1" ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div> 

<div class="field">
<label for="firstname">Email:</label>
<asp:TextBox ID="txtEmail" runat="server" class="signup" required placeholder="Email"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
ErrorMessage="enter email address" ControlToValidate="txtEmail" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
runat="server" ErrorMessage="Envalid email" ControlToValidate="txtEmail" 
ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
                    
</div> 

<div class="field">
<label for="firstname">Password:</label>
<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="signup" required placeholder="Password"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
ErrorMessage="Please enter password" ControlToValidate="txtPassword" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>

</div> 

<div class="field">
<label for="firstname">Repeat Password:</label>
<asp:TextBox ID="txtRepeatPassword" runat="server" TextMode="Password" class="signup" required placeholder="Repeat Password"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" 
    ErrorMessage="password does not match" ControlToCompare="txtPassword" ControlToValidate="txtRepeatPassword"
    ForeColor="Red" ValidationGroup="new1"></asp:CompareValidator>
 </div> 
					
<%-- Male :   <asp:RadioButton ID="radiomale" runat="server" GroupName="Gender" />
Female :	 <asp:RadioButton ID="radiofemale" runat="server" GroupName="Gender"/>--%>
		 <asp:RadioButtonList ID="rbtgender" runat="server" RepeatDirection="Vertical">
      <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
     <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
      </asp:RadioButtonList>

        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" Display="Dynamic"
    ControlToValidate="rbtgender" ForeColor="Red" ErrorMessage="plese select Gender"
    ValidationGroup="new1"></asp:RequiredFieldValidator>		
</div>
<div class="login-actions">

<asp:Button ID="btnSubmit" runat="server" Text="Sign up"  class="button btn btn-success btn-large"
onclick="btnSubmit_Click"  ValidationGroup="new1"/>

</div>
</div> 
</div> 

</div>
</div>

</div>
</div>
</div>
</div>
</div>

</asp:Content>

