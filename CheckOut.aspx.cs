﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web.Mail;
using System.Web.SessionState;


public partial class CheckOut : System.Web.UI.Page
{
    decimal minimum=0, tota=0;
    BL_Order bl_Order = new BL_Order();
    BL_UserDetails bl_userdetails = new BL_UserDetails();
    BL_DeliveryAddress bl_deliveryaddress = new BL_DeliveryAddress();
    BL_CheckOut bl_checkout = new BL_CheckOut();
    BL_DiscountOffers bl_discountoffer = new BL_DiscountOffers();
    Common common = new Common();
    int rid, ai, ci;
    string ord;
    static int discountid = 0;


    DataTable Basket_DataTable = null;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["basket"] != null)
        {
            Basket_DataTable = (DataTable)Session["basket"];
            grdcart.DataSource = Basket_DataTable;
            GetTotal(Basket_DataTable);
            grdcart.DataBind();
        }
       
        if (!IsPostBack)
        {


            if (Session["UserId"] != null)
            {
                SelectMyAddress();
            }
            BindRestoDetails();
            if (tota < minimum)
            {
                Response.Redirect("Menu.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
            }
            
            createDate();

            if (Request.QueryString["or"].ToString() == "Preorder Now")
            {
                rbtassoonaspossible.Visible = false;
                rdodelivery.Checked = true;
                rbtlater.Checked = true;
                ddltime.Visible = true;
                ddldate.Visible = true;
            }
            else
            {
                ddldate.Visible = false;
                ddltime.Visible = false;
                rbtassoonaspossible.Checked = true;
                rdodelivery.Checked = true;
            }
            pnlselectaddress.Visible = false;
            rdonewaddress.Checked = true;
        }
    }
    //protected void lnkaddmessage_Click(object sender, EventArgs e)
    //{
        
    //    txtmessage.Visible = true;
    //}
    private void check()
    {
        if (rbtlater.Checked == true)
        {
            //ddltime.Visible = true;
            //ddldate.Visible = true;
        }
        //else
        //{
        //    ddldate.Visible = false;
        //    ddltime.Visible = false;
        //}
    }
    private void cleardeliveryaddress()
    {
        txtbuildingname.Text="";
        txtalternetcontact.Text="";
        txtlandmark.Text="";
    }
   
    private void BindRestoDetails()
    {
        lnkresto.Text = Request.QueryString["rn"].ToString();
        //lblarea.Text = Request.QueryString["area"].ToString();
        lnkcityname.Text = "Restaurants in " + Request.QueryString["area"].ToString();
        rid=Convert.ToInt32(Request.QueryString["key"].ToString());
        lblrestoid.Text = Request.QueryString["key"].ToString();
        ci = Convert.ToInt32(Request.QueryString["ct"].ToString());
        ai = Convert.ToInt32(Request.QueryString["at"].ToString());

       bl_checkout.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
       DataSet ds= bl_checkout.SelectcheckoutinfoByRestoId(bl_checkout);
       lblrestonamehead.Text = Convert.ToString(ds.Tables[0].Rows[0]["RestaurantName"]);
       Restoimage.ImageUrl =  ds.Tables[0].Rows[0]["Image"].ToString();
       lblrestoaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
       if (rdodelivery.Checked == true)
       {
           lbldeliverytext.Text = "Delivery";
           lbldeliverytime.Text= ds.Tables[1].Rows[0]["DeliveryTime"].ToString();
       }
       else
       {
           lbldeliverytext.Text = "PickUp";
           lbldeliverytime.Text = ds.Tables[1].Rows[0]["PickupTime"].ToString();
       }
       lblminimumorder.Text = Convert.ToString(ds.Tables[1].Rows[0]["MinimumOrders"]);
       minimum = Convert.ToDecimal(ds.Tables[1].Rows[0]["MinimumOrders"]);
       if (Session["basket"] != null)
       {

       if (ds.Tables[2].Rows[0]["DeliveryCharges"].ToString() == "")
       {
           lbldeliverycharges.Text = "Free";
       }
       else
       {
           lbldeliverycharges.Text = ds.Tables[2].Rows[0]["DeliveryCharges"].ToString();
           lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(lbltotal.Text) + Convert.ToDecimal(lbldeliverycharges.Text));
       }
        }
        else
        {
            lbldeliverycharges.Text = "00.00";
            lblfinaltotal.Text = "00.00";
            lbltotal.Text = "00.00";
            btncheckout.Enabled = false;
        }
        
      // lbldiscount.Text = ds.Tables[3].Rows[0]["DiscountAmount"].ToString();
       lbldiscountcoupn.Visible = false;
    }
    private void createDate()
    {
        int j = 1;
        string b = DateTime.Now.ToString("dd-MM-yyyy");
       // ddldate.Items.Insert(0, "Select a date");
        ddldate.Items.Insert(0, new ListItem("-- Select a date--", "0"));
        ddldate.Items.Insert(1, b);
        for (int i = 1; i <= 4; i++)
        {
            j = j + 1;
            string a = DateTime.Now.AddDays(i).ToString("dd-MM-yyyy");
            ddldate.Items.Insert(j, a);
        }  
    }

    protected void ddldate_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        ddltime.Items.Clear();
        if (ddldate.SelectedItem.Text == DateTime.Now.ToString("dd-MM-yyyy"))
        {

                ddltime.Items.Insert(0, "Select a time");
                string time;
                int min = 30;
                DateTime dd = Convert.ToDateTime(DateTime.Now.ToString("HH:mm"));

                for (int i = 1; i <= 24; i++)
                {


                    int r = 0;
                    int m = Convert.ToInt32(DateTime.Now.ToString("mm"));

                    if (m <= 30)
                    {
                        r = 30 - m;
                    }
                    else
                    {

                        r = 60 - m;

                    }
                    m = min + r;
                    time = dd.AddMinutes(m).ToString("HH:mm");
                    ddltime.Items.Insert(i, time);

                    if (time == "22:00")
                    {
                        break;
                    }
                    else
                    {
                        min = min + 30;
                    }
                }
        }
        else
        {
               // ddltime.Items.Insert(0, "Select a time");
            ddltime.Items.Insert(0, new ListItem("-- Select a time--", "0"));
                string time;
                int min = 30;
                DateTime dd = Convert.ToDateTime("09:30");
                for (int i = 1; i <= 25; i++)
                {
                    time = dd.AddMinutes(min).ToString("HH:mm");
                    ddltime.Items.Insert(i, time);
                    if (time == "22:00")
                    {
                        break;
                    }
                    else
                    {
                        min = min + 30;
                    }
              }
        }
    }
    protected void btncheckout_Click(object sender, EventArgs e)
    {
        try
        {

        if (Basket_DataTable!=null)
        {
            int DeliveryId;
            if (rdonewaddress.Checked)
            {
                SetDeliveryAddress();
                DeliveryId = bl_deliveryaddress.InsertDeliveryAddress(bl_deliveryaddress);
            }
            else {
                DeliveryId = Convert.ToInt32(rdoselectaddress.SelectedItem.Value);

            }
            bl_Order.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            bl_Order.Date = common.GetDate();
            bl_Order.UserId = Convert.ToInt32(Session["UserId"].ToString());
            if (rdodelivery.Checked == true)
            {
                bl_Order.DeliveryType = 0;
            }
            else
            {
                bl_Order.DeliveryType = 1;  
            }
            if (rbtassoonaspossible.Checked == true)
            {
                bl_Order.DeliveryTime = common.GetDate();
                bl_Order.OrderStatus = "PlaceOrder";
            }
            else
            {
                bl_Order.OrderStatus = "Later";
                DateTime dateonly=Convert.ToDateTime(ddldate.Text);
                DateTime timeonly = Convert.ToDateTime(ddltime.Text);
                bl_Order.DeliveryTime = dateonly.Add(timeonly.TimeOfDay);
            }

           
            //UpdateDiscountOccurance
            if (discountid > 0 && lnkcancel.Visible == true)
            {
                bl_Order.DiscountId = discountid;
                bl_Order.UpdateDiscountOffers(bl_Order);
            }
            else
            {
                bl_Order.DiscountId = 0;
            }
            bl_Order.DeliveryCharges = Convert.ToDecimal(lbldeliverycharges.Text);
            bl_Order.DeliveryId = DeliveryId;
            bl_Order.BillNo = 0;
            int orderid = bl_Order.Orders(bl_Order);

            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            {
                bl_Order.OrderId = orderid;
                bl_Order.DeliveryId = DeliveryId;
                bl_Order.Item = Basket_DataTable.Rows[i]["itemname"].ToString();
                bl_Order.Qty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]);
                bl_Order.Price = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"]);
                bl_Order.OrderDetails(bl_Order);
            }
            Session["basket"] = null;

            //send email

            //EmailSms email = new EmailSms();
            //string body = "<table><tr><td>User Name :</td><td>'" + txtEmail.Text + "'</td></tr><tr><td>Password :</td><td>'" + txtPassword.Text + "'</td></tr></table>";
            //email.SendMail("Welcome To Food Pune", body, Session["email"].ToString());





            //notification
            string restaurentAddress = LoggedInRestuarentSessionID.GetSessionIDByRestuarentLoginName(lblrestoid.Text);
            string message = "You got new order from " + Session["username"].ToString() + "<a href='OrdersDetails.aspx?Orderid=" + orderid + "'>View Details <a/>";
            OrderNotificationHub.SendOrderNotification(restaurentAddress, message, orderid);
            cleardeliveryaddress();

        }
        Response.Redirect("ThankYou.aspx");
        }
        catch (Exception ex)
        {

            throw ex;
        }
    
    }

    private void SetDeliveryAddress()
    {
        bl_deliveryaddress.Appartment = txtbuildingname.Text;
        bl_deliveryaddress.AlternateContactNo = txtalternetcontact.Text;
        bl_deliveryaddress.Landmark = txtlandmark.Text;
        bl_deliveryaddress.AreaId = Convert.ToInt32(Request.QueryString["at"].ToString());
        bl_deliveryaddress.CityId = Convert.ToInt32(Request.QueryString["ct"].ToString());
        bl_deliveryaddress.FlatNo = "";
        //bl_deliveryaddress.DeliveryInstruction = "";
        bl_deliveryaddress.DeliveryId = 0;
        bl_deliveryaddress.Date = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy"));
        bl_deliveryaddress.Company = "";
        bl_deliveryaddress.Address = "";
        bl_deliveryaddress.UserId =Convert.ToInt32(Session["UserId"]);
        bl_deliveryaddress.DeliveryNote = txtmessage.Text;
        bl_deliveryaddress.Mode = "Insert";
    }


    protected void rbtlater_CheckedChanged(object sender, EventArgs e)
    {
        ddldate.Visible = true;
        ddltime.Visible = true;
            
    }
    protected void rbtassoonaspossible_CheckedChanged(object sender, EventArgs e)
    {
        ddldate.Visible = false;
        ddltime.Visible = false;
    }
    private void GetTotal(DataTable dt)
    {
        decimal tot = 0;
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tot = tot + Convert.ToDecimal(dt.Rows[i]["Total"].ToString()) * Convert.ToDecimal(dt.Rows[i]["Qty"].ToString());
            }
            if (tot >= Convert.ToDecimal(lblminimumorder.Text))
            {
                btncheckout.Enabled = true;
            }
            else
            {
                btncheckout.Enabled = false;
            }
            tota = tot;
            lbltotal.Text = Convert.ToString(tot);
            BindRestoDetails();
        }
        else
        {
            lbldeliverycharges.Text = "00.00";
            lblfinaltotal.Text = "00.00";
            lbltotal.Text = "00.00";
            btncheckout.Enabled = false;
            Session["basket"] = null;
        }
    }

    private void GetFinalTotal()
    {
        int total, deliverycharges,finaltotal;
        total = Convert.ToInt32(lbltotal.Text);
        deliverycharges = Convert.ToInt32(lbldeliverycharges.Text);
        finaltotal = total + deliverycharges;
    }
    protected void grdcart_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        Label lblid = (Label)row.FindControl("lblid");
        Label lblitemname = (Label)row.FindControl("lblitemname");
        Label lblQty = (Label)row.FindControl("lblQty");
        Label lblTotal = (Label)row.FindControl("lblTotal");

        if (e.CommandName.ToString() == "plus")
        {
            string id = lblid.Text;
            string name = lblitemname.Text;
            string price = Convert.ToString(Convert.ToDecimal(lblTotal.Text) / Convert.ToInt32(lblQty.Text));
            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            {
                if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text)
                {
                    int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) + 1;
                    Basket_DataTable.Rows[i]["Qty"] = newqty;
                    Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString());
                }
            }
            grdcart.DataSource = Basket_DataTable;
            grdcart.DataBind();
            GetTotal(Basket_DataTable);
            Session["basket"] = Basket_DataTable;
        }
        if (e.CommandName.ToString() == "minus")
        {
            string id = lblid.Text;
            string name = lblitemname.Text;
            string price = Convert.ToString(Convert.ToDecimal(lblTotal.Text) / Convert.ToInt32(lblQty.Text));
            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            {
                if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text && Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"].ToString()) > 1)
                {
                    int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) - 1;
                    Basket_DataTable.Rows[i]["Qty"] = newqty;
                    Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString());
                    lbltotal.Text = Convert.ToString( Convert.ToDecimal(lbltotal.Text) - Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString()));
                }
                else
                {
                    if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text)
                        Basket_DataTable.Rows.Remove(Basket_DataTable.Rows[i]);
                }
            }
            grdcart.DataSource = Basket_DataTable;
            grdcart.DataBind();
            GetTotal(Basket_DataTable);
            Session["basket"] = Basket_DataTable;
        }
    }


    protected void lnkcityname_Click(object sender, EventArgs e)
    {
        Response.Redirect("Restaurants.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "");
    }


    protected void lnkresto_Click(object sender, EventArgs e)
    {
        Response.Redirect("Menu.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
    }

    protected void lnkcontinuetocart_Click(object sender, EventArgs e)
    {
        Response.Redirect("Menu.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
       if (Convert.ToDecimal(lblminimumorder.Text) <= Convert.ToDecimal(lbltotal.Text))
        {
            if (txtvouchercode.Text != "")
            {
                bl_checkout.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
                bl_checkout.Code = txtvouchercode.Text;

                DataTable dt = bl_checkout.checkcoupcode(bl_checkout);
                lblmessage.Visible = true;
                if (dt.Rows.Count > 0)
                {
                    if ("3" != dt.Rows[0]["DiscountOccurance"].ToString())
                    {


                    DateTime currentdate=common.GetDate();
                    if (currentdate >= Convert.ToDateTime(dt.Rows[0]["DiscountStart"].ToString()) && currentdate <= Convert.ToDateTime(dt.Rows[0]["DiscountEnd"].ToString()))
                    {

                        if ("0" == dt.Rows[0]["DiscountOccurance"].ToString() || "1" == dt.Rows[0]["DiscountOccurance"].ToString())
                        {
                            discountid = Convert.ToInt32(dt.Rows[0]["DiscountId"].ToString());
                            string subtotal;
                            if ("0" == dt.Rows[0]["DiscountType"].ToString())
                            {
                                lbldiscountcoupn.Visible = true;
                                lbldiscountcoupn.Text = "Discount -" + Convert.ToString(Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]) * Convert.ToDecimal(lbltotal.Text) / 100);
                                subtotal = Convert.ToString(Convert.ToDecimal(lbltotal.Text) - (Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]) * Convert.ToDecimal(lbltotal.Text) / 100));
                                lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(subtotal) + Convert.ToDecimal(lbldeliverycharges.Text));
                                lblmessage.Text = "successfully Apply";
                                lnkcancel.Visible = true;
                                lblmessage.ForeColor = Color.Green;
                                txtvouchercode.Text = dt.Rows[0]["Code"].ToString();
                                txtvouchercode.BorderColor = Color.Green;
                            }
                            else
                            {
                                lbldiscountcoupn.Text = "Discount -" + dt.Rows[0]["DiscountAmount"].ToString();
                                lbldiscountcoupn.Visible = true;
                                subtotal = Convert.ToString(Convert.ToDecimal(lbltotal.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]));
                                lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(subtotal) + Convert.ToDecimal(lbldeliverycharges.Text));
                                lblmessage.Text = "successfully Apply !";
                                lnkcancel.Visible = true;
                                lblmessage.ForeColor = Color.Green;
                                txtvouchercode.Text = dt.Rows[0]["Code"].ToString();
                                txtvouchercode.BorderColor = Color.Green;
                            }

                        }
                        else
                        {
                            lblmessage.Text = "This Voucher is used";
                            lblmessage.ForeColor = Color.Red;
                            txtvouchercode.BorderColor = Color.Red;

                        }

                    }
                    else
                    {
                        lblmessage.Text = "Voucher Code Expired";
                        lblmessage.ForeColor = Color.Red;
                        txtvouchercode.BorderColor = Color.Red;
                    }

                }
                else
                {

                    discountid = Convert.ToInt32(dt.Rows[0]["DiscountId"].ToString());
                    string subtotal;
                    if ("0" == dt.Rows[0]["DiscountType"].ToString())
                    {
                        lbldiscountcoupn.Visible = true;
                        lbldiscountcoupn.Text = "Discount -" + Convert.ToString(Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]) * Convert.ToDecimal(lbltotal.Text) / 100);
                        subtotal = Convert.ToString(Convert.ToDecimal(lbltotal.Text) - (Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]) * Convert.ToDecimal(lbltotal.Text) / 100));
                        lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(subtotal) + Convert.ToDecimal(lbldeliverycharges.Text));
                        lblmessage.Text = "successfully Apply";
                        lnkcancel.Visible = true;
                        lblmessage.ForeColor = Color.Green;
                        txtvouchercode.Text = dt.Rows[0]["Code"].ToString();
                        txtvouchercode.BorderColor = Color.Green;
                    }
                    else
                    {
                        lbldiscountcoupn.Text = "Discount -" + dt.Rows[0]["DiscountAmount"].ToString();
                        lbldiscountcoupn.Visible = true;
                        subtotal = Convert.ToString(Convert.ToDecimal(lbltotal.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"]));
                        lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(subtotal) + Convert.ToDecimal(lbldeliverycharges.Text));
                        lblmessage.Text = "successfully Apply !";
                        lnkcancel.Visible = true;
                        lblmessage.ForeColor = Color.Green;
                        txtvouchercode.Text = dt.Rows[0]["Code"].ToString();
                        txtvouchercode.BorderColor = Color.Green;
                    }  
                }
                }
                else
                {
                    lblmessage.Text = "This voucher is not valid";
                    lblmessage.ForeColor = Color.Red;
                    txtvouchercode.BorderColor = Color.Red;
                    lbldiscountcoupn.Visible = false;
                    lblfinaltotal.Text = Convert.ToString(Convert.ToDecimal(lbltotal.Text) + Convert.ToDecimal(lbldeliverycharges.Text));


                }
            }
            else
            {
                lblmessage.Text = "Enter voucher Code";
                lblmessage.ForeColor = Color.Red;
            }
           
        }
        else
        {
            lblmessage.Text = "Minimum Order less than Your Order";
            lblmessage.ForeColor = Color.Red;
        }

}

    protected void lnkcancel_Click(object sender, EventArgs e)
    {
        lblmessage.Visible = false;
        txtvouchercode.Text = "";
        txtvouchercode.BorderColor = Color.Gray;
        lnkcancel.Visible = false;
    }

    protected void rdoaddress_CheckedChanged(object sender, EventArgs e)
    {
        pnlnewaddress.Visible = false;
        pnlselectaddress.Visible = true;
    }
    protected void rdonewaddress_CheckedChanged(object sender, EventArgs e)
    {
        pnlselectaddress.Visible = false;
        pnlnewaddress.Visible = true;
    }
    public void SelectMyAddress()
    {
        bl_deliveryaddress.UserId = Convert.ToInt32(Session["UserId"].ToString());
        DataTable dt = bl_deliveryaddress.SelectAddress(bl_deliveryaddress);
        rdoselectaddress.DataSource = dt;
        if (dt.Rows.Count > 0)
        {
            lblmsgdeliveryaddress.Visible = false;
            rdoselectaddress.DataTextField = "Addresss";
            rdoselectaddress.DataValueField = "DeliveryId";
            rdoselectaddress.DataBind();
        }
        else
        {
            pnldeliveryaddress.Visible = false;
            pnlnewaddress.Visible = true;
           // lblmsgdeliveryaddress.Visible = true;
        }


    }

}