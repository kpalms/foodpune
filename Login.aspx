﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 

<div  class="widget">
<div class="widget-content">



<div class="account-container">
	
	<div class="content clearfix">
		
		
			<h1>Welcome</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>

                <asp:Label ID="lblInvalid" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
				
				<div class="field">
					<label for="username">Username</label>
					<asp:TextBox ID="txtUserName" runat="server" placeholder="Enter Email Id" class="login username-field" required></asp:TextBox><br/>
                   
				</div> 
				
				<div class="field">
					<label for="password">Password:</label>
					
				<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Enter Password" required class="login password-field"></asp:TextBox><br/>

                </div> 
				
			</div> 
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<a href="User/ForgotPassword.aspx?pg=user">Forgot Password</a> &nbsp&nbsp&nbsp&nbsp 
				</span>
                <span class="login-checkbox">
					<a href="SignUp.aspx">New Registration</a>
				</span>

    <asp:Button ID="btnLogIn" runat="server" Text="Log In" 
    onclick="btnLogIn_Click" class="button btn btn-success btn-large"/>
			</div> 
				
	</div> 
	
</div>


</div>
</div>


</div>
</div>
</div>
</div>
</div>


</asp:Content>

