﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Restaurantsold.aspx.cs" Inherits="Restaurants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link rel="Stylesheet" type="text/css" href="css/pages/Restaurant.css" />

<link href="Search/jquerysctipttop.css" rel="stylesheet" type="text/css">

<%--<script src="Search/jquery-1.11.3.min.js"></script>
<script src="Search/mv-autocomplete.js" type="text/javascript"></script>
<link href="Search/mv-autocomplete.css" rel="stylesheet" type="text/css" />--%>



<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
--%>


<script type="text/javascript">
    function Go() {

        window.location = "Restaurants.aspx?ct=" + hdnSelectedCity.value + '&city=' + txtCity.value + '&at=' + hdnSelectedCityArea.value + '&area=' + txtArea.value;

    }
</script> 


<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript">
    var pageIndex = 0;
    var pageCount;
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            GetRecords();
        }
    });
    $(function () {
        GetRecords();
    });
    function GetRecords() {
        pageIndex++;
        if (pageIndex == 1 || pageIndex <= pageCount) {
            $("#loader").show();
            $.ajax({
                type: "POST",
                url: "DemoRestaurant.aspx/GetImages",
                data: '{pageIndex: ' + pageIndex + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        }
    }
    function OnSuccess(response) {
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
        var images = xml.find("Images");
        var repeatColumns = parseInt("<%=rptrestaurant.RepeatColumns == 0 ? 1 : rptrestaurant.RepeatColumns %>");
        var rowCount = Math.ceil(images.length / repeatColumns);
        var j = 0;
        images.each(function () {
            var image = $(this);
            var row = $("[id*=rptrestaurant] .item:last").closest("tr");
            if ($(".is_used[value='1']", row).length == repeatColumns) {
                row = $("[id*=rptrestaurant] tr").eq(0).clone();
                $(".is_used", row).val("0");
                $(".image", row).attr("src", "");
                $(".button", row).attr("href", "");
                $(".loader", row).remove();
                $("[id*=rptrestaurant]").append(row);
                j = 0;
            } else {
                row = $("[id*=rptrestaurant] .item:last").closest("tr");
            }
            var cell = $(".item", row).eq(j);
            $(".name", cell).html(image.find("RestaurantName").text());

            $(".MinimumOrders", cell).html(image.find("MinimumOrders").text());
            $(".DeliveryCharges", cell).html(image.find("DeliveryCharges").text());
            $(".PickupTime", cell).html(image.find("PickupTime").text());
            $(".DeliveryTime", cell).html(image.find("DeliveryTime").text());

            $(".RestaurantId", cell).html(image.find("RestaurantId").text());
            $(".RestaurantName", cell).html(image.find("RestaurantName").text());
//            // $(".CityName", cell).html(image.find("CityName").text());
//           $(".AreaName", cell).html(image.find("AreaName").text());
//            $(".CityId", cell).html(image.find("CityId").text());
//            $(".AreaId", cell).html(image.find("AreaId").text());
            //$(".Orders", cell).html(image.find("Orders").text());


            $(".CityName", cell).html(image.find("CityName").text());
            $(".AreaName", cell).html(image.find("AreaName").text());
            $(".CityId", cell).html(image.find("CityId").text());
            $(".AreaId", cell).html(image.find("AreaId").text());
            $(".Orders", cell).html(image.find("Orders").text());

            $(".button", cell).attr("href", image.find("Image").text());

            $(".is_used", cell).attr("value", "1");
            var img = $(".image", cell);
            var loader = $("<img class = 'loader' src = 'loading.gif' />");
            img.after(loader);
            img.hide();
            img.attr("src", image.find("Image").text());
            img.load(function () {
                $(this).parent().find(".loader").remove();
                $(this).fadeIn();
            });
            j++;
        });
        $("[id*=rptrestaurant] .is_used[value='0']").closest(".item").remove();
    }
</script>





</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
    
<%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>--%>

 
<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />


<div class="subnavbar2">
  <div class="subnavbar-inner2">
    <div class="container2">
      <div class="middlebox">
     
      <input id="txtCity" type="text" class="textboxes" placeholder="Enter Your City..." data-object />
       <input id="txtArea" type="text" class="textboxes" placeholder="Enter your Area..." data-object />
       <input type="button" value="Find Restaurant" id="btnCheckID" onclick="Go()" class="button btn btn-success btn-large"/>
       
        </div>
    


</div> 
<div class="widget-content">
</div>
  </div>
</div>






<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  


<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="Default.aspx">Home</a></li>
        <li><a href="#">
            <asp:LinkButton ID="lnkcityname" runat="server" onclick="lnkcityname_Click"> </asp:LinkButton> </a></li>
        
          <li><asp:Label ID="Label3" runat="server" Text=''></asp:Label></li>
    </ul>
</div>



<div  class="widget">
<div class="widget-content">

                  <div class="span3" >
                       
                            <div class="controls">
                            <div class="input-append" style="margin:0px">
                            <asp:TextBox ID="txtSearch" runat="server"  class="span2 m-wrap" ></asp:TextBox> 
                            <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn" onclick="btnsearch_Click" />
                            </div>
                            </div>

                            <b>Filter Restaurants</b><br/>
                            <asp:CheckBoxList ID="cboRestoFacilities" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="cboRestoFacilities_SelectedIndexChanged" CssClass="chkrestoCat">
                            </asp:CheckBoxList>



                            <br/>
                            
                            <asp:DropDownList ID="ddlSorting" runat="server" Enabled="True" 
                            Font-Bold="True" AppendDataBoundItems="True" 
                            onselectedindexchanged="ddlSorting_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="1">Popularity</asp:ListItem>
                            <asp:ListItem Value="2">Ratings</asp:ListItem>
                            <asp:ListItem Value="3">Minimum Orders</asp:ListItem>
                            <asp:ListItem Value="4">Delivery Fee</asp:ListItem>
                            <asp:ListItem Value="5">Fastest Delivery</asp:ListItem>
                            </asp:DropDownList>
                            <br/>
    
                            <b>Cuisines</b>
                            <asp:CheckBoxList ID="cboRestoCategory" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="cboRestoCategory_SelectedIndexChanged" 
                            RepeatDirection="Vertical" RepeatLayout="Table"  CssClass="chkrestoCat">
                            </asp:CheckBoxList>



                   </div>
                  

<b><asp:Label ID="lblrestoname" runat="server" Text='<%#Eval("RestaurantName")  %>'></asp:Label> 

                  <div class="span8" >




              <div id="dvRestaurants">

        <asp:DataList ID="rptrestaurant" runat="server" RepeatLayout="Table" RepeatColumns="1"
        CellPadding="2" CellSpacing="20">
        <ItemTemplate>
          <div class="restopanal span8">
            <table class="item" cellpadding="0" cellspacing="0" border="0">
                <tr>

                    <td align="center" class="body">
                     <div class="ownerimage span2 ">
                    <img class="image" src='<%# Eval("Image") %>' alt="" />
                    </div>
                    </td>

                    <td align="center" class="header">
                        <div class="span3">
                        <span class="name ownertitle restaurantname">
                           <span class="ownertitle restaurantname"> <%# Eval("RestaurantName")%></span>
                         </span>
  
                        <span class="infohead">Rattings:</span> 450

                       MinimumOrders :<span class="MinimumOrders"><%#Eval("MinimumOrders")  %></span>
                       DeliveryCharges: <span class="DeliveryCharges"><%#Eval("DeliveryCharges")%></span>
                        PickupTime:<span class="PickupTime"><%#Eval("PickupTime")%></span>
                       DeliveryTime: <span class="DeliveryTime"><%#Eval("DeliveryTime")%></span>
                 
                         </div>
                    </td>

                    <td>
                    <a href="Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>&or=<%#Eval("Orders")%>" class="btn btn-success Orders">
                    </td>

                </tr>

<tr>
<td>

</td>
</tr>






              <input type="hidden" class="is_used" value="0" />
            </table>
</div>
        </ItemTemplate>
    </asp:DataList>



                      <asp:Label ID="lblemty" runat="server" Text="Oops, no restaurants found with your current filter settings!" Visible="false"></asp:Label>
    
    <img id="loader" alt="" src="loading.gif" style="display: none" />
                     
 </div>  
                       
                                                          

                  </div>
                  

               </div>
              </div>


<%--         <a href="Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>" class="btn btn-success">Menu Card</a>      
--%>


</div>
</div>
</div>
</div>
</div>
        </b>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>

<script>

    var countries = "Manoj";


    $('#txtCity').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyCity',
        loading_html: 'Loading...',
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
        }
    });

    $('#txtArea').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyArea',
        loading_html: 'Loading...',
        post_data: {
            cityid: function () { return document.getElementById('hdnSelectedCity').value; }
        },
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
        }
    });


</script>


    </b>


</asp:Content>

