﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="css/pages/Register.css" rel="stylesheet" type="text/css">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   




<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 

<div class="row">&nbsp</div>


<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
       <li><a href="#"><asp:LinkButton ID="lnkcityname" runat="server" onclick="lnkcityname_Click"></asp:LinkButton></a></li>
          <li><a href="#">
              <asp:LinkButton ID="lnkresto" runat="server" OnClick="lnkresto_Click"></asp:LinkButton></a></li>
    </ul>
    <asp:Label ID="lblctid" runat="server" Text='' Visible="false"></asp:Label>
    <asp:Label ID="lblarid" runat="server" Text='' Visible="false"></asp:Label>
        <asp:Label ID="lblrestoid" runat="server" Text='' Visible="false"></asp:Label>

</div>





<div  class="widget">
<div class="widget-content">

    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>

	<!-- start registration -->
	<div class="registration">
		<div class="registration_left">
		<h2>new user? <span> create an account </span></h2>
		<!-- [if IE] 
		    < link rel='stylesheet' type='text/css' href='ie.css'/>  
		 [endif] -->  
		  
		<!-- [if lt IE 7]>  
		    < link rel='stylesheet' type='text/css' href='ie6.css'/>  
		<! [endif] -->  

		 <div class="registration_form ">
	



				<div>
					<label>
						
                        <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" class="textboxes"  ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="Please enter first name" ControlToValidate="txtFirstName" 
                        ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </label>
				</div>
				<div>
					<label>
						
                        <asp:TextBox ID="txtMiddleName" runat="server" class="signup" placeholder="Middle Name" ></asp:TextBox>
                       
					</label>
				</div>
				<div>
					<label>
						
                        <asp:TextBox ID="txtLastName" runat="server" class="signup"  placeholder="Last Name" ValidationGroup="new1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ErrorMessage="Please enter last name" ControlToValidate="txtLastName" 
                        ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </label>
				</div>
                <div>
					<label>
						
                        <asp:TextBox ID="txtMobile" runat="server" class="signup"  placeholder="Mobile" ValidationGroup="new1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ErrorMessage="Please enter mobile no" ControlToValidate="txtMobile" 
                        ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no"   ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile" ValidationGroup="new1" ForeColor="Red"></asp:RegularExpressionValidator>
                    </label>
				</div>
                <div>
					<label>
                        <asp:TextBox ID="txtEmail" runat="server" class="signup"  placeholder="Email"  ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="enter email address" ControlToValidate="txtEmail" 
                        ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                        runat="server" ErrorMessage="Envalid email" ControlToValidate="txtEmail" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
                    
                    </label>
				</div>
                <div>
					<label>
					
                        <asp:TextBox ID="txtpasswordsign" runat="server"  class="signup"  placeholder="Password" ValidationGroup="new1" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ErrorMessage="Please enter password" ControlToValidate="txtpasswordsign" 
                        ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
					</label>
				</div>
                <div>
					<label>
						<%--<input placeholder="email address:" type="email" tabindex="3" required>--%>
                        <asp:TextBox ID="txtRepeatPassword" runat="server"  class="signup"  placeholder="Repeat Password" ValidationGroup="new1" TextMode="Password" data-match="password" ></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ErrorMessage="password does not match" ControlToCompare="txtpasswordsign" ControlToValidate="txtRepeatPassword"
                        ForeColor="Red" ValidationGroup="new1"></asp:CompareValidator>
                    </label>
				</div>

				<div class="sky-form">
					<div class="sky_form1">
						<ul>
							<li><label class="radio left">
                                <asp:RadioButton ID="radiomale" runat="server" GroupName="Gender"/><i></i>Male</label></li>
							<li><label class="radio"> <asp:RadioButton ID="radiofemale" runat="server" GroupName="Gender"/><i></i>Female</label></li>
							<div class="clearfix"></div>
						</ul>
					</div>
				</div>
					
                    
         
                    
                    
                    	

				<div>
                    <asp:Button ID="btnSubmit" runat="server" Text="Sign up"  class="button btn btn-primary btn-large" onclick="btnSubmit_Click" ValidationGroup="new1" />
				</div>
<%--				<div class="sky-form">
					<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>i agree to shoppe.com &nbsp;<a class="terms" href="#"> terms of service</a> </label>
				&nbsp;</div>--%>
            

	
		</div>
	</div>
	<div class="registration_left form-group">
		<h2>existing user</h2>
		 <div class="registration_form">
	
     
				<div>
					<label>
				
					<asp:TextBox ID="txtUserName" runat="server" placeholder="email" class="login username-field" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ErrorMessage="please enter email address" ControlToValidate="txtUserName" 
                        ValidationGroup="login" ForeColor="Red"></asp:RequiredFieldValidator>
                                           <asp:RegularExpressionValidator ID="RegularExpressionValidator3" 
                        runat="server" ErrorMessage="Envalid email" ControlToValidate="txtUserName" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ForeColor="Red" ValidationGroup="login"></asp:RegularExpressionValidator>
                    </label>
				</div>
				<div>
					<label>
						
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"  class="login password-field"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ErrorMessage="Please enter password" ControlToValidate="txtPassword" 
                        ValidationGroup="login" ForeColor="Red"></asp:RequiredFieldValidator>
                    </label>
				</div>						
				<div>
                   <asp:Label ID="lblInvalid" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                   <asp:Button ID="btnLogIn" runat="server" Text="Log In" onclick="btnLogIn_Click" class="button btn btn-success btn-large" ValidationGroup="login"/>
				
            

                </div>
				<div class="forget">
					<a href="User/ForgotPassword.aspx?pg=user">forgot your password</a>
				</div>
               
	
			
			</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!-- end registration -->







</div>
</div>




</div>
</div>
</div>
</div>
</div>







</asp:Content>