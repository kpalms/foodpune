﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Restaurants.aspx.cs" Inherits="Restaurants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link rel="Stylesheet" type="text/css" href="css/pages/Restaurant.css" />

<%--<link href="Search/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="Search/jquery-1.11.3.min.js"></script>
<script src="Search/mv-autocomplete.js" type="text/javascript"></script>
<link href="Search/mv-autocomplete.css" rel="stylesheet" type="text/css" />



<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
--%>

<script type="text/javascript">
    function Go() {
        window.location = "Restaurants.aspx?ct=" + hdnSelectedCity.value + '&city=' + txtCity.value + '&at=' + hdnSelectedCityArea.value + '&area=' + txtArea.value;
    }
</script> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
    
<%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>--%>

 
<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />


<div class="subnavbar2">
  <div class="subnavbar-inner2">
    <div class="container2">
      <div class="middlebox">
     
      <input id="txtCity" type="text" class="textboxes" placeholder="Enter Your City..." data-object />
       <input id="txtArea" type="text" class="textboxes" placeholder="Enter your Area..." data-object />
       <input type="button" value="Find Restaurant" id="btnCheckID" disabled="disabled" onclick="Go()" class="button btn btn-success btn-large"/>
       
        </div>
    


</div> 
<div class="widget-content">
</div>
  </div>
</div>






<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  


<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="Default.aspx">Home</a></li>
        <li><a href="#">
            <asp:LinkButton ID="lnkcityname" runat="server" onclick="lnkcityname_Click"> </asp:LinkButton> </a></li>
        
          <li><asp:Label ID="Label3" runat="server" Text=''></asp:Label></li>
    </ul>
</div>

<div  class="widget">
<div class="widget-content">

                  <div class="span3" >
                       
                            <div class="controls">
                            <div class="input-append" style="margin:0px">
                            <asp:TextBox ID="txtSearch" runat="server"  class="span2 m-wrap" ></asp:TextBox> 
                            <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn" onclick="btnsearch_Click" />
                            </div>
                            </div>

                            <b>Filter Restaurants</b><br/>
                            <asp:CheckBoxList ID="cboRestoFacilities" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="cboRestoFacilities_SelectedIndexChanged" CssClass="chkrestoCat">
                            </asp:CheckBoxList>



                            <br/>
                            
                            <asp:DropDownList ID="ddlSorting" runat="server" Enabled="True" 
                            Font-Bold="True" AppendDataBoundItems="True" 
                            onselectedindexchanged="ddlSorting_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="1">Popularity</asp:ListItem>
                            <asp:ListItem Value="2">Ratings</asp:ListItem>
                            <asp:ListItem Value="3">Minimum Orders</asp:ListItem>
                            <asp:ListItem Value="4">Delivery Fee</asp:ListItem>
                            <asp:ListItem Value="5">Fastest Delivery</asp:ListItem>
                            </asp:DropDownList>
                            <br/>
    
                            <b>Cuisines</b>
                            <asp:CheckBoxList ID="cboRestoCategory" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="cboRestoCategory_SelectedIndexChanged" 
                            RepeatDirection="Vertical" RepeatLayout="Table"  CssClass="chkrestoCat">
                            </asp:CheckBoxList>



                   </div>
                  

<b><asp:Label ID="lblrestoname" runat="server" Text='<%#Eval("RestaurantName")  %>'></asp:Label> 

                  <div class="span8" >




              <div id="dvRestaurants">

                        <asp:Repeater ID="rptrestaurant" runat="server" onitemdatabound="rptrestaurant_ItemDataBound" OnItemCommand="rptrestaurant_ItemCommand">

                        <ItemTemplate>
                        
                      
<table border="0">
<tr>
<td>

                        

                        <div class="restopanal span8">

                        <table border="0" cellpadding="2" cellspacing="2" width="100%">
                        <tr>
                        

                        <td>
                        <div class="ownerimage span2 ">
                        <img class="image" src='<%# Eval("Image") %>' alt=""  height="120" width="120"/>

                        </div>
                        </td>

                        <td>
                        <div class="span3">

                        <asp:Label ID="lblrestoid" runat="server" Text='<%#Eval("RestaurantId")  %>' CssClass="restaurantid" Visible="false"></asp:Label>
                        <asp:Label ID="lblrestoname" runat="server" Text='<%#Eval("RestaurantName")  %>'  CssClass="ownertitle restaurantname"></asp:Label>

                        <ul>
                        <li>
                        <span class="infohead">Rattings:</span> 450
                        </li>
                        <li>
                        <span class="infohead">MinimumOrders :</span><asp:Label ID="lblMinimumOrders" runat="server" Text='<%#Eval("MinimumOrders")  %>' CssClass="minimumOrders"></asp:Label><br />
                        </li>
                        <li>
                        <span class="infohead">DeliveryCharges :</span>  <asp:Label ID="lblDeliveryCharges" runat="server" Text='<%#Eval("DeliveryCharges")  %>' CssClass="deliveryCharges"></asp:Label><br />
                        </li>
                        <li>
                        <span class="infohead">PickupTime :</span><asp:Label ID="lblPickupTime" runat="server" Text='<%#Eval("PickupTime")  %>' CssClass="pickupTime"></asp:Label> Min<br />
                        </li>
                        <li>
                        <span class="infohead">DeliveryTime :</span><asp:Label ID="lblDeliveryTime" runat="server" Text='<%#Eval("DeliveryTime")  %>' CssClass="deliveryTime"></asp:Label> Min<br />
                        </li>
                        </ul>

                        <asp:ImageButton ID="imgfave" runat="server"  height="10px" width="10px" CommandArgument='<%# Eval("RestaurantId") %>' Enabled="false" /> 
                        <asp:Label ID="lblfavcount" runat="server" Text="0"></asp:Label><br/>
                        </div>
                        </td>

                        <td>
                        <div class="menubutton span2">
                        <asp:Label ID="lbldiscount" runat="server"></asp:Label><br />
                         <asp:Label ID="lblcoupncode" runat="server"></asp:Label>
                        <a href="Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>&or=<%#Eval("Orders")%>" class="btn btn-success">
                        <asp:Label ID="lblcartbutton" runat="server" Text='<%#Eval("Orders")  %>'></asp:Label>
                        </a>
                        </div>
                        </td>

                         </tr>
                        </table>
                        </div>

</td>
</tr>
</table>

                        </ItemTemplate>
                       
                

        
                     

                        </asp:Repeater> 

		<input id="txtHidden" style="width: 28px" type="hidden" value="0"
		runat="server" />
		<hr>
		<asp:LinkButton ID="lnkBtnPrev" runat="server" Font-Underline="False"
		OnClick="lnkBtnPrev_Click" Font-Bold="True"><< Prev </asp:LinkButton>
		&nbsp;&nbsp;
		<asp:LinkButton ID="lnkBtnNext" runat="server" Font-Underline="False"
		OnClick="lnkBtnNext_Click" Font-Bold="True">Next >></asp:LinkButton>
       <asp:Label ID="Label1" runat="server" Text="Oops, no restaurants found with your current filter settings!" Visible="false"></asp:Label>
    

                      <asp:Label ID="lblemty" runat="server" Text="Oops, no restaurants found with your current filter settings!" Visible="false"></asp:Label>
    
    <img id="loader" alt="" src="loading.gif" style="display: none" />
                     
 </div>  
                       
                                                          

                  </div>
                  

               </div>
              </div>


<%--         <a href="Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>" class="btn btn-success">Menu Card</a>      
--%>


</div>
</div>
</div>
</div>
</div>
        </b>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>

<script>

    var countries = "Manoj";


    $('#txtCity').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyCity',
        loading_html: 'Loading...',
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
        }
    });

    $('#txtArea').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyArea',
        loading_html: 'Loading...',
        post_data: {
            cityid: function () { return document.getElementById('hdnSelectedCity').value; }
        },
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
            document.getElementById('btnCheckID').disabled = false
        }
    });


</script>


    </b>

</asp:Content>

