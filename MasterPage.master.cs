﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class MasterPage : System.Web.UI.MasterPage
{
    BL_UserDetails bl_userdetails = new BL_UserDetails();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            lblLogin.Text = "Log In Or Sign Up";
            lnkLogIn.Visible = true;
            lnkSignUp.Visible = true;
            lnkchangepassword.Visible = false;
            lnkproile.Visible = false;
            lnkMyActivity.Visible = false;
            lnkmyaddress.Visible = false;
            lnkreview.Visible = false;
            lnkfavorites.Visible = false;
            lnkLogOut.Visible = false;
            lnkchangepassword.Visible = false;
        }
        else
        {
            bl_userdetails.UserId = Convert.ToInt16(Session["UserId"].ToString());
            lblLogin.Text = Session["username"].ToString();
            lnkLogIn.Visible = false;
            lnkSignUp.Visible = false;
            lnkchangepassword.Visible = true;
            lnkproile.Visible = true;
            lnkMyActivity.Visible = true;
            lnkmyaddress.Visible = true;
            lnkreview.Visible = true;
            lnkfavorites.Visible = true;
            lnkLogOut.Visible = true;
            lnkchangepassword.Visible = true;
        }

       

    }

    protected void lnkLogIn_Click(object sender, EventArgs e)
    {
       Response.Redirect("~/Login.aspx");

    }
    protected void lnkSignUp_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/SignUp.aspx");
    }

    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        //Session.Abandon();
        Session.Remove("UserId");
        Session["basket"] = null;
        lnkLogIn.Visible = true;
        lnkSignUp.Visible = true;
        lnkMyActivity.Visible = false;
        lnkLogOut.Visible = false;
        Response.Redirect("~/Default.aspx");

    }
    protected void lnkhome_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["pg"]))
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            Response.Redirect("../Default.aspx");
        }
    }
}
