﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class SignUp : System.Web.UI.Page
{
    BL_UserDetails bl_userdetails = new BL_UserDetails();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bl_userdetails.Email=txtEmail.Text;
        DataTable dt = bl_userdetails.CheckUserEmailValidation(bl_userdetails);
        if (dt.Rows.Count == 0)
        {
        setField();
        bl_userdetails.Mode = "Insert";
        int userid =bl_userdetails.InsertUserDetails(bl_userdetails);
        Session["UserId"] = Convert.ToString(userid);
        Session["username"] = txtFirstName.Text;
        Session["email"] = txtEmail.Text;

        Clear();




        //EmailSms email = new EmailSms();
        //string body = "<table><tr><td>User Name :</td><td>'" + txtEmail.Text + "'</td></tr><tr><td>Password :</td><td>'" + txtPassword.Text + "'</td></tr></table>";
        //email.SendMail("Welcome To Food Pune", body, txtEmail.Text);


        Clear();
         Response.Redirect("Default.aspx");

       // Response.Redirect("Default.aspx");


        }
        else
        {
            lblmessage.Text = "This Eamil Id Is Already Existing Please Login";
            Clear();
        }
        

    }

    public void setField()
    {
        Common common = new Common();
        bl_userdetails.Date = common.GetDate();
        bl_userdetails.FirstName = txtFirstName.Text;
        bl_userdetails.MiddleName = txtMiddleName.Text;
        bl_userdetails.LastName = txtLastName.Text;
       // if (radiomale.Checked == true)
        if (rbtgender.SelectedItem.Value == "Male")
        {
            bl_userdetails.Gender = 0;
        }
        else
        {
            bl_userdetails.Gender = 1;
        }
        bl_userdetails.Mobile = txtMobile.Text;
        bl_userdetails.Email = txtEmail.Text;
        bl_userdetails.Password = txtPassword.Text;
    }

    public void Clear()
    {
        txtFirstName.Text = "";
        txtMiddleName.Text = "";
        txtLastName.Text = "";
        txtMobile.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        txtRepeatPassword.Text = "";
        rbtgender.ClearSelection();
    }

}