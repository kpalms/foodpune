﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <title></title>

<link href="css/pages/Default.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function Go() {

            window.location = "Restaurants.aspx?ct=" + hdnSelectedCity.value + '&city=' + txtCity.value + '&at=' + hdnSelectedCityArea.value + '&area=' + txtArea.value;
        }
    </script> 
  
                
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />




    <div class="intro-header">
        <div class="col-lg-12">
            <div class="intro-message">
                <h1>
                    Celebrations Never End With Food Pune</h1>
                <h3>
                    Order food online from the best restaurants</h3>
                <hr class="intro-divider" />
      
                <ul class="list-inline intro-social-buttons">
                    <li>
                         <input id="txtCity"  type="text"  placeholder="Enter Your City..."  class="textboxes" required/>
                    </li>
                    <li>
                        <input id="txtArea"  type="text"  placeholder="Enter your Area..."  class="textboxes" />
                     </li>
                    <li>
                      <input type="button" value="Find Restaurant" id="btnCheckID" disabled="disabled" onclick="Go()" class="button btn btn-success btn-large"/>
                    </li>
                </ul>
     
            </div>
        </div>
    </div>


    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Awesome Features</h2>
                <p class="text-center wow fadeInDown">Depending on where you live, a delicious meal can be yours just by opening your web browser.<br /> There are a number of web services that let you log in, choose a restaurant in your area, and order delivery with a couple of clicks</p>
            </div>
          </div>
    </section>



    <div class="container">

      <div class="row">
   
    <div class="grid_4">
      <div class="cwrap">
        <div class="circle c1">
          <div class="info">
            <div class="title">Best Cuisine </div>
             <br>
            <a href="#"></a> </div>
        </div>
      </div>
    </div>
    <div class="grid_4">
      <div class="cwrap">
        <div class="circle c2">
          <div class="info">
            <div class="title">Great service</div>
           <br>
            <a href="#"></a> </div>
        </div>
      </div>
    </div>
    <div class="grid_4">
      <div class="cwrap">
        <div class="circle c3">
          <div class="info">
            <div class="title">Special Offer</div>
            <br>
            <a href="#"></a> </div>
        </div>
      </div>
    </div>
 </div>

     </div>



    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Hungry?</h2>
                <p class="text-center wow fadeInDown">Good,we are here to serve you</p>
            </div>
          </div>
    </section>


<div class="bg-content">
  <div id="content">
    <div class="container">
    <div class="row">
   
     <h2 class="section-title text-center" style="color:White">Delicious food</h2>
    </div>
      <div class="row">

        <div class="clear"></div>
        <ul class="thumbnails thumbnails-1 list-services">
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/Cuisine1.jpg" alt="">
              <section> <a href="#" class="link-1">Biryani </a>
                <p>A biryani is usually heavy with spices and butter or ghee but in a pulao the rice is cooked in a seasoned broth and it can be made with vegetables, chicken or meat.</p>
             <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/cuisine2.jpg" alt="">
              <section> <a href="#" class="link-1">Burger </a>
                <p>The patties that are the essence of a veggie burger have existed in various Eurasian cuisines for millennia, including in the form of disc-shaped grilled or fried meatballs or as koftas</p>
            <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/cuisine3.jpg" alt="">
              <section> <a href="#" class="link-1">chinese </a>
                <p>Noodles are an essential ingredient and staple in Chinese cuisine. There is a great variety of Chinese noodles, which vary according to their region of production, ingredients, shape or width, and manner of preparation</p>
            <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/cuisine4.jpg" alt="">
              <section> <a href="#" class="link-1">South Indian </a>
                <p>The cuisine of South India is known for its light, low calorie appetizing dishes. The traditional food of South India is mainly rice based. </p>
             <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/cuisine5.jpg" alt="">
              <section> <a href="#" class="link-1">Punjabi </a>
                <p>The cuisine of Punjab has an enormous variety of mouth-watering vegetarian as well as non vegetarian dishes. The spice content ranges from minimal to pleasant to high</p>
             <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
          <li class="span4">
            <div class="thumbnail thumbnail-1"> <img src="img/cuisine6.jpg" alt="">
              <section> <a href="#" class="link-1">Seafood </a>
                <p>These delightful treats from the sea are key to a healthy diet. Low in calories, sodium, and cholesterol, protein-packed seafood provides vitamins and minerals and may reduce the risk of heart disease and lower blood pressure.</p>
            <%-- <a class="btn btn-success" href="#">Order Now</a>--%>
              </section>
            </div>
          </li>
        </ul>
      </div>


    </div>
  </div>
</div>



    <section id="cta2">
        <div class="container">
            <div class="text-center">
                <h2 class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">The food you want
from your favorite restaurants</h2>
                <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">Browse our ratings and reviews. Or make it easy on yourself and pick the place with the coolest name. Or the best coupons. Whichever.</p>
                <img class="img-responsive wow fadeIn" src="img/banner.png" alt="" data-wow-duration="300ms" data-wow-delay="300ms">
            </div>
        </div>
    </section>



   <div class="services s2" id="services">
   	  <div class="container">
   	  	<h2 class="service_head">easy step</h2>
   	   <div class="row text-center">
   	    <div class="col-md-3 service_grid">
   		  <i class="icon1"> </i>
   		  <h3 class="m_1"><a href="#">Search Restaurant</a></h3>
   		  <p class="m_2">Find restaurants near you from thousands of restaurants in pune.</p>
   		</div>
   		<div class="col-md-3 service_grid">
   		  <i class="icon2"> </i>
   		 <h3 class="m_1"><a href="#">Find the food</a></h3>
   		  <p class="m_2">Search for restaurants by menu item, location and price..</p>
   		</div>
   		<div class="col-md-3 service_grid">
   		  <i class="icon3"> </i>
   		  <h3 class="m_1"><a href="#">Pay secure online</a></h3>
   		  <p class="m_2">Your payment for this purchase is processed securely by food pune.</p>
   		</div>
   		<div class="col-md-3">
   		  <i class="icon4"> </i>
   		  <h3 class="m_1"><a href="#">Food Delivery</a></h3>
   		  <p class="m_2">Food Delivered.</p>
   		</div>
   	  </div>
   	  </div>
   	</div>


    <div class="main">
    <section class="content">
				<div id="owl">
					<div class="item"><img src="img/1.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/2.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/3.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/4.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/5.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/6.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/7.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/8.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/9.jpg" alt="" height="60" width="100"></div>
					<div class="item"><img src="img/10.jpg" alt="" height="60" width="100"></div>
				</div>
    </section>
    </div>
<%--+++++++++++++++++++++++++++++++++++++++++++++++++++++++--%>
        <script>

            var countries = "Manoj";


            $('#txtCity').mvAutocomplete({
                data: countries,
                container_class: 'results',
                result_class: 'result',
                url: 'WebService.asmx/GetMyCity',
                loading_html: 'Loading...',
                callback: function (el, selected) {
                    console.log('Click Back!');
                    document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
                    
                }
            });

            $('#txtArea').mvAutocomplete({
                data: countries,
                container_class: 'results',
                result_class: 'result',
                url: 'WebService.asmx/GetMyArea',
                loading_html: 'Loading...',
                post_data: {
                    cityid: function () { return document.getElementById('hdnSelectedCity').value; }
                },
                callback: function (el, selected) {
                    console.log('Click Back!');
                    document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
                    document.getElementById('btnCheckID').disabled = false
                }
            });


        </script>




</asp:Content>

