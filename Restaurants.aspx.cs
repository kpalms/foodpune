﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Services;
using System.Configuration;


public partial class Restaurants : System.Web.UI.Page
{

    BL_Restaurants bl_restaurants = new BL_Restaurants();
    BL_Favorites bl_favorites = new BL_Favorites();
    Common common = new Common();
    static int cityid, areaid;
    int totalCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {
                lnkcityname.Text = "Restaurants in " + Request.QueryString["area"].ToString();
               // lblarea.Text = Request.QueryString["area"].ToString();
                cityid = Convert.ToInt32(Request.QueryString["ct"].ToString());
                areaid = Convert.ToInt32(Request.QueryString["at"].ToString());
                GetRestaurantData();
                BindRestoCategory(bl_restaurants);
                BindRestoFacilities(bl_restaurants);
           }
           catch
            {

            }
        }
    }


    //private void CheckPrivate()
    //{
    //    bl_restaurants.UserId = Convert.ToInt32(Session["UserId"]);
    //}

    private void BindRestoCategory(BL_Restaurants bl_restaurants)
    {
        DataTable dt = bl_restaurants.BindRestoCategory(bl_restaurants);
        cboRestoCategory.DataSource = dt;
        cboRestoCategory.DataValueField = "RestoCategoryId";
        cboRestoCategory.DataTextField = "RestoCategory";
        cboRestoCategory.DataBind();
    }

    private void BindRestoFacilities(BL_Restaurants bl_restaurants)
    {
        DataTable dt = bl_restaurants.BindRestoFacilities(bl_restaurants);
        cboRestoFacilities.DataSource = dt;
        cboRestoFacilities.DataValueField = "RestoFacilityId";
        cboRestoFacilities.DataTextField = "Name";
        cboRestoFacilities.DataBind();
    }


    private void restocategory()
    {
        string getValue = "";
        foreach (ListItem item in cboRestoCategory.Items)
            {
                if (item.Selected == true)
                {
                    getValue = getValue + item.Value + ",";
                }
            }
            getValue = getValue.TrimEnd(',');
            bl_restaurants.Cuisines = getValue;
    }


    private void restfacility()
    {
        string getValue1 = "";
        foreach (ListItem item in cboRestoFacilities.Items)
        {
            if (item.Selected == true)
            {
                getValue1 = getValue1 + item.Value + ',';
            }
        }
        getValue1 = getValue1.TrimEnd(',');
        bl_restaurants.Facility = getValue1;

    }
    protected void cboRestoCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_restaurants.CityId = Convert.ToInt32(Request.QueryString["ct"].ToString());
        bl_restaurants.AreaId = Convert.ToInt32(Request.QueryString["at"].ToString());
      
        if (cboRestoFacilities.SelectedIndex != -1 && cboRestoCategory.SelectedIndex != -1)
        {
            bl_restaurants.Mode = "CatFaci";
            bl_restaurants.NameText = "";
            restocategory();
            restfacility();
            DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);

                rptrestaurant.DataSource = dt;
                rptrestaurant.DataBind();
        }
        else if (cboRestoFacilities.SelectedIndex != -1)
        {
            bl_restaurants.Mode = "Facility";
            bl_restaurants.NameText = "";
            bl_restaurants.Cuisines = "";
            restfacility();
                 DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
                rptrestaurant.DataSource = dt;
                rptrestaurant.DataBind();
           
        }
        else
        {
            bl_restaurants.Mode = "Cuisines";
            bl_restaurants.NameText = "";
            bl_restaurants.Facility = "";
            restocategory();
            DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
            if (dt.Rows.Count > 0)
            {
                rptrestaurant.DataSource = dt;
                rptrestaurant.DataBind();
            }
            else
            {
                if (cboRestoCategory.SelectedIndex == -1)
                {
                    GetRestaurantData();
                
                }
                else
                {
                    dt = null;
                    rptrestaurant.DataSource = dt;
                    rptrestaurant.DataBind();

                }
            }
        }
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bl_restaurants.CityId = Convert.ToInt32(Request.QueryString["ct"].ToString());
        bl_restaurants.AreaId = Convert.ToInt32(Request.QueryString["at"].ToString());
        bl_restaurants.NameText = txtSearch.Text;
            if (txtSearch.Text != "")
            {
                bl_restaurants.Mode = "Txt";
                bl_restaurants.Facility = "";
                bl_restaurants.Cuisines = "";
                DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
                if (dt.Rows.Count > 0)
                {
                    rptrestaurant.DataSource = dt;
                    rptrestaurant.DataBind();
                    lnkBtnNext.Visible = true;
                    lnkBtnPrev.Visible = true;
             
                }
                else
                {
                    rptrestaurant.DataSource = dt;
                    rptrestaurant.DataBind();
                    lnkBtnNext.Visible = false;
                    lnkBtnPrev.Visible = false;
                    lblemty.Visible = true;
                    
                }
            }
            else
            {
                //rptrestaurant.DataSource = GetRestaurantData(1);
                //rptrestaurant.DataBind();
                GetRestaurantData();
            } 
    }
    protected void ddlSorting_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSorting.SelectedItem.Text == "Minimum Orders")
        {
            bl_restaurants.ColName = "MinimumOrders";
        }
        if (ddlSorting.SelectedItem.Text == "Delivery Fee")
        {
            bl_restaurants.ColName = "DeliveryCharges";
        }
        if (ddlSorting.SelectedItem.Text == "Fastest Delivery")
        {
            bl_restaurants.ColName = "DeliveryTime";
        }
        bl_restaurants.CityId = Convert.ToInt32(Request.QueryString["ct"].ToString());
        bl_restaurants.AreaId = Convert.ToInt32(Request.QueryString["at"].ToString());
        DataTable dt = bl_restaurants.SearchRestaurantsBySortMode(bl_restaurants);
        rptrestaurant.DataSource = dt;
        rptrestaurant.DataBind();

    }
    protected void cboRestoFacilities_SelectedIndexChanged(object sender, EventArgs e)
    {
        bl_restaurants.CityId = Convert.ToInt32(Request.QueryString["ct"].ToString());
        bl_restaurants.AreaId = Convert.ToInt32(Request.QueryString["at"].ToString());
        if (cboRestoFacilities.SelectedIndex != -1 && cboRestoCategory.SelectedIndex != -1)
        {
            bl_restaurants.Mode = "CatFaci";
            bl_restaurants.NameText = "";
            restocategory();
            restfacility();
            DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
            if (dt.Rows.Count == 0)
            {
                lblemty.Visible = true;
                lnkBtnNext.Visible = false;
                lnkBtnPrev.Visible = false;
            }
            else
            {
                rptrestaurant.DataSource = dt;
                rptrestaurant.DataBind();
                lnkBtnNext.Visible = true;
                lnkBtnPrev.Visible = true;
            }

        }
        else if (cboRestoCategory.SelectedIndex != -1)
        {
            bl_restaurants.Mode = "Cuisines";
            bl_restaurants.NameText = "";
            bl_restaurants.Facility = "";
            restocategory();
            DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
            if (dt.Rows.Count == 0)
            {
                lblemty.Visible = true;
                lnkBtnNext.Visible = false;
                lnkBtnPrev.Visible = false;
            }
            else
            {
                rptrestaurant.DataSource = dt;
                rptrestaurant.DataBind();
                lblemty.Visible = false;
                lnkBtnNext.Visible = true;
                lnkBtnPrev.Visible = true;
            }

        }
        else
        {
            bl_restaurants.Mode = "Facility";
            bl_restaurants.NameText = "";
            bl_restaurants.Cuisines = "";
            restfacility();
            DataTable dt = bl_restaurants.FilterRestaurants(bl_restaurants);
            rptrestaurant.DataSource = dt;
            rptrestaurant.DataBind();
            if (dt.Rows.Count == 0)
            {
                lblemty.Visible = true;
                lnkBtnNext.Visible = false;
                lnkBtnPrev.Visible = false;
            }
            else
            {
                lblemty.Visible = false;
                lnkBtnNext.Visible = true;
                lnkBtnPrev.Visible = true;
            }
            //if (dt.Rows.Count > 0)
            //{
            //    rptrestaurant.DataSource = dt;
            //    rptrestaurant.DataBind();
            //}
            //else
            //{
            //    if (cboRestoFacilities.SelectedIndex == -1)
            //    {
            //        GetRestaurantData();

            //    }
            //    else
            //    {
            //        if (dt.Rows.Count == 0)
            //        {
            //            lblemty.Visible = true;
            //            lnkBtnNext.Visible = false;
            //            lnkBtnPrev.Visible = false;
            //        }
            //        else
            //        {
            //            rptrestaurant.DataSource = dt;
            //            rptrestaurant.DataBind();
            //            lnkBtnNext.Visible = true;
            //            lnkBtnPrev.Visible = true;
            //        }

            //    }
            //}
        }
    }


    protected void rptrestaurant_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lbldiscount = (Label)e.Item.FindControl("lbldiscount");
        Label lblrestoid = (Label)e.Item.FindControl("lblrestoid");
        Label lblfavcount = (Label)e.Item.FindControl("lblfavcount");
        ImageButton imgfave = (ImageButton)e.Item.FindControl("imgfave");
        Label lblcoupncode = (Label)e.Item.FindControl("lblcoupncode");

        bl_restaurants.UserId = Convert.ToInt32(Session["UserId"]);
        bl_restaurants.RestaurantId = Convert.ToInt32(lblrestoid.Text);
        DataSet ds = bl_restaurants.SelectRestoConditionbyId(bl_restaurants);

        if (ds.Tables[0].Rows.Count > 0)
        {
            lblfavcount.Text = ds.Tables[0].Rows[0][0].ToString();
        }
        if (ds.Tables[1].Rows.Count > 0)
        {
            lbldiscount.Text = ds.Tables[1].Rows[0]["DiscountAmount"].ToString() + ' ' + "Discount";
            lblcoupncode.Text = "Use Code " + ds.Tables[1].Rows[0]["Code"].ToString();
        }

        if (Session["UserId"] != null)
        {

            bl_favorites.RestaurantId = Convert.ToInt32(lblrestoid.Text);
            bl_favorites.UserId = Convert.ToInt32(Session["UserId"]);
            DataTable dtfavorites = bl_favorites.GetFavouriteByUserId(bl_favorites);

            if (dtfavorites.Rows.Count > 0)
            {

                imgfave.ImageUrl = "img/fave.png";
                imgfave.ToolTip = "Make Unfavourite";
                imgfave.Enabled = true;
                imgfave.CommandName = "UnFavorites";

            }
            else
            {
                imgfave.ImageUrl = "img/FavBlack.png";
                imgfave.ToolTip = "Make Favourite";
                imgfave.Enabled = true;
                imgfave.CommandName = "Favorites";
            }
        }
        else
        {
            imgfave.Attributes.Add("onmouseover", "this.style.cursor='pointer'");
            imgfave.ImageUrl = "img/FavBlack.png";
            imgfave.ToolTip = "Log in to save this as your favorite";
        }
    }

    protected void rptrestaurant_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Favorites")
        {
            bl_favorites.Date = common.GetDate();
            bl_favorites.UserId = Convert.ToInt32(Session["UserId"]);
            bl_favorites.RestaurantId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_favorites.InserFavourite(bl_favorites);
            GetRestaurantData();

        }
        if (e.CommandName.ToString() == "UnFavorites")
        {
            bl_favorites.UserId = Convert.ToInt32(Session["UserId"]);
            bl_favorites.RestaurantId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_favorites.DeleteFavourite(bl_favorites);
            GetRestaurantData();

        }
    }


    public void GetRestaurantData()
    {
        BL_Restaurants bl_restaurant = new BL_Restaurants();
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;

        DataSet ds = bl_restaurant.GetRestaurantData(bl_restaurant, val, cityid, areaid);
        totalCount = ds.Tables[0].Rows.Count;


        if (ds.Tables[0].Rows.Count == 0)
        {
            lblemty.Visible = true;
            lnkBtnNext.Visible = false;
            lnkBtnPrev.Visible = false;
        }
        else
        {
            rptrestaurant.DataSource = ds.Tables[0];
            rptrestaurant.DataBind();
            lnkBtnNext.Visible = true;
            lnkBtnPrev.Visible = true;
        }
        //if (val <= 0)
        //{
        //    lnkBtnPrev.Visible = false;
        //    lnkBtnNext.Visible = true;
        //}

        //if (val >= 5)
        //{
        //    lnkBtnPrev.Visible = true;
        //    lnkBtnNext.Visible = true;
        //}

        //if ((val + 5) >= totalCount)
        //{
        //    lnkBtnNext.Visible = false;
        //}
    }

    protected void lnkcityname_Click(object sender, EventArgs e)
    {
        Response.Redirect("Restaurants.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "");
    }

    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 5);
        GetRestaurantData();
    }
    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 5);
        GetRestaurantData();
    }
}