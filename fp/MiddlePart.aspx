﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MiddlePart.aspx.cs" Inherits="WF_MiddlePart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>


<div class="account-container register">
<div class="content clearfix">
<h1>Signup for Free Account</h1>			
<div class="login-fields">
<p>Create your free account:</p>

<div class="field">
<label for="firstname">First Name:</label>
<input type="text" id="firstname" name="firstname" value="" placeholder="First Name" class="login" required/>
</div> 

</div>
<div class="login-actions">
<button class="button btn btn-primary btn-large">Register</button>
</div>
</div> 
</div> 


</body>
</html>
