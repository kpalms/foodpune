﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class fp_ViewDeactiveRestaurant : System.Web.UI.Page
{
    BL_Restaurants bl_restaurants = new BL_Restaurants();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindDeactiveResto();
        }
    }

    private void BindDeactiveResto()
    {
        try
        {
            DataTable dt = bl_restaurants.SelectDeactiveRestaurantDetails(bl_restaurants);
            gvviewdeactiverestaurant.DataSource = dt;
            gvviewdeactiverestaurant.DataBind();
        }
        catch
        {

        }
    }
   
    protected void gvviewdeactiverestaurant_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
               bl_restaurants.RestaurantId = Convert.ToInt32(e.CommandArgument.ToString());
               bl_restaurants.Active = 1;
               bl_restaurants.DeleteRestaurant(bl_restaurants);
               BindDeactiveResto();
            
        }
        catch
        {

        }
    }
}