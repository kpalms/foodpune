﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Script.Serialization;

public partial class WF_demo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string Conec = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(Conec);
        con.Open();
        SqlDataAdapter da = new SqlDataAdapter(" select * from Area where CityId=5 ", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        DropDownList2.DataSource = dt;
        DropDownList2.DataValueField = "AreaId";
        DropDownList2.DataTextField = "AreaName";
        DropDownList2.DataBind();

    }


    [System.Web.Services.WebMethod]
    public static string LoadstudentDetails(string Regno)
    {
        string Conec = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(Conec);
        con.Open();
        SqlDataAdapter da = new SqlDataAdapter("select * from Area where CityId=5) ", con);
        DataTable st = new DataTable();
        da.Fill(st);
        List<StudentDetails> details = new List<StudentDetails>();
        foreach (DataRow dtrow in st.Rows)
        {
            StudentDetails obj = new StudentDetails();
            obj.StudentID = dtrow["AreaId"].ToString();
            obj.StudentName = dtrow["AreaName"].ToString();
            obj.Gender = dtrow["AreaName"].ToString();
            details.Add(obj);
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(details);
    }
}

public class StudentDetails
{
    public string StudentName { get; set; }
    public string Gender { get; set; }
    public string StudentID { get; set; }

}
