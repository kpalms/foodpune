﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_ViewRestaurants : System.Web.UI.Page
{
    BL_Restaurants bl_restaurants = new BL_Restaurants();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
         restaurantsBindGrid();
        }
    }

    private void restaurantsBindGrid()
    {
        try
        {
            DataTable dt = bl_restaurants.SelectRestaurants(bl_restaurants);
            gdvrestaurants.DataSource = dt;
            gdvrestaurants.DataBind();
        }
        catch
        {

        }

    }
    protected void gdvrestaurants_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            bl_restaurants.RestaurantId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_restaurants.Active = 0;
            bl_restaurants.DeleteRestaurant(bl_restaurants);
            restaurantsBindGrid();

        }
        catch
        {

        }
    }
}