﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WF_Coupen : System.Web.UI.Page
{
    static int id;
    BL_Coupen bl_coupen = new BL_Coupen();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCoupanGRD();
            btnUpdate.Enabled = false;
            int restoid = Convert.ToInt32(Request.QueryString["key"].ToString());
            common.SelectDiscountOffersNameId(ddlDiscount,restoid);
            lblrestoname.Text = Request.QueryString["rn"].ToString();
        }
    }
   
    public void BindCoupanGRD()
    {
        bl_coupen.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        DataTable dt = bl_coupen.BindCoupanGRD(bl_coupen);
        grdCoupen.DataSource = dt;
        grdCoupen.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        setField();
        bl_coupen.Mode = "Insert";
        bl_coupen.CoupenId = 0;
        bl_coupen.InsertCoupen(bl_coupen);
        BindCoupanGRD();
        Clear();
        string message = "Your details have been saved successfully.";
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "')};";
        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

    }
    public void setField()
    {
        bl_coupen.Code = txtCode.Text;
        bl_coupen.DiscountId = Convert.ToInt16(ddlDiscount.SelectedValue);
    }
    public void Clear()
    {
        txtCode.Text = "";
        ddlDiscount.SelectedIndex = 0;
        btnAdd.Enabled = true;
        btnUpdate.Enabled = false;
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        setField();
        bl_coupen.CoupenId = id;
        bl_coupen.Mode = "Update";
        bl_coupen.InsertCoupen(bl_coupen);
        BindCoupanGRD();
        Clear();
        string message = "Your details have been Updated successfully.";
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "')};";
        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
    }
    protected void coupan_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCoupanId = (Label)row.FindControl("lblCoupanId");
            Label lblCode = (Label)row.FindControl("lblCode");
            Label lblDiscountId = (Label)row.FindControl("lblDiscountId");
            if (e.CommandName == "EditRow")
            {
                id = Convert.ToInt16(e.CommandArgument.ToString());
               
                txtCode.Text = lblCode.Text;
                ddlDiscount.SelectedValue = (lblDiscountId.Text);
                btnAdd.Enabled = false;
                btnUpdate.Enabled = true;
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_coupen.Mode = "Delete";
                bl_coupen.CoupenId = Convert.ToInt16(e.CommandArgument.ToString());
                bl_coupen.Code = "";
                bl_coupen.InsertCoupen(bl_coupen);
                Clear();
                BindCoupanGRD();
            }
        }
        catch
        {
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}