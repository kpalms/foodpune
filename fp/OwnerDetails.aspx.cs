﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
public partial class WF_OwnerDetails : System.Web.UI.Page
{
    BL_Owner bl_owner = new BL_Owner();
    Common common = new Common();
    static int id;
    static string imagename;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            common.bindCity(ddlcity);
            BindOwner();
        }
        btnUpdate.Enabled = false;
        Image1.Visible = false;
    
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        setField();
        bl_owner.Mode = "Insert";
        if (imageuploder.HasFile)
        {
            string ext = Path.GetExtension(imageuploder.FileName);
            string maxid = common.ReturnOneValue("select max(OwnerId) from OwnerDetails");
            string filename = maxid + "" + ext;
            imageuploder.PostedFile.SaveAs(Server.MapPath("images/Owner/") + filename);
            bl_owner.Image = filename;
        }
        else
        {
            bl_owner.Image = "NoImage.jpg";
        }
        bl_owner.InsertOwner(bl_owner);
        BindOwner();
        clear();
        string message = "Your details have been saved successfully.";
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "')};";
        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

    }
    public void BindOwner()
    {
        DataTable dt = bl_owner.BindOwner();
        grdOwner.DataSource = dt;
        grdOwner.DataBind();
    }
    
    public void setField()
    {
        bl_owner.Date = common.GetDate();
        bl_owner.FirstName = txtFirstName.Text;
        bl_owner.MiddleName = txtMiddleName.Text;
        bl_owner.LastName = txtLastName.Text;
        bl_owner.PhoneNo = (txtPhoneNo.Text);
        bl_owner.Mobile1 = (txtMobile1.Text);
        bl_owner.Mobile2 = (txtMobile2.Text);
        bl_owner.Mobile3 = (txtMobile3.Text);
        bl_owner.Email = txtEmail.Text;
        bl_owner.CityId = Convert.ToInt32(ddlcity.SelectedValue);
        bl_owner.AreaId = Convert.ToInt32(ddlarea.SelectedValue);
        bl_owner.Address = txtAddress.Text;
        bl_owner.Password = txtPassword.Text;
    }
    
    public void clear()
    {
        txtFirstName.Text = "";
        txtMiddleName.Text = "";
        txtLastName.Text = "";
        txtPhoneNo.Text = "";
        txtMobile1.Text = "";
        txtMobile2.Text = "";
        txtMobile3.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        txtAddress.Text = "";
        common.bindCity(ddlcity);
        ddlarea.SelectedItem.Text = "";
       
        
    }

    protected void owner_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblOwnerId = (Label)row.FindControl("lblOwnerId");
            Label lblDate = (Label)row.FindControl("lblDate");
            Label lblFirstName = (Label)row.FindControl("lblFirstName");
            Label lblMiddleName = (Label)row.FindControl("lblMiddleName");
            Label lblLastName = (Label)row.FindControl("lblLastName");
            Label lblPhoneNo = (Label)row.FindControl("lblPhoneNo");
            Label lblMobile1 = (Label)row.FindControl("lblMobile1");
            Label lblMobile2 = (Label)row.FindControl("lblMobile2");
            Label lblMobile3 = (Label)row.FindControl("lblMobile3");
            Label lblCityId = (Label)row.FindControl("lblCityId");
            Label lblAreaId = (Label)row.FindControl("lblAreaId");
            Label lblEmail = (Label)row.FindControl("lblEmail");
            Label lblImage = (Label)row.FindControl("lblImage");
            Label lblPassword = (Label)row.FindControl("lblPassword");
            Label lblAddress = (Label)row.FindControl("lblAddress");
           
            
            if (e.CommandName == "EditRow")
            {
                txtFirstName.Text = lblFirstName.Text;
                txtMiddleName.Text = lblMiddleName.Text;
                txtLastName.Text = lblLastName.Text;
                txtPhoneNo.Text = lblPhoneNo.Text;
                txtMobile1.Text = lblMobile1.Text;
                txtMobile2.Text = lblMobile2.Text;
                txtMobile3.Text = lblMobile3.Text;
                txtEmail.Text = lblEmail.Text;
                ddlcity.SelectedValue = lblCityId.Text;
                common.bindAreabycity(ddlarea, Convert.ToInt32(lblCityId.Text));
                ddlarea.SelectedValue = lblAreaId.Text;
                Image1.Visible = true;
                Image1.ImageUrl = lblImage.Text;
                txtPassword.Text = lblPassword.Text;
                btnSubmit.Enabled = false;
                btnUpdate.Enabled = true;
                txtAddress.Text = lblAddress.Text;
                id = Convert.ToInt32(e.CommandArgument.ToString());
                string sub = lblImage.Text.Substring(13);
                imagename = sub;

            }

            if (e.CommandName == "DeleteRow")
            {
                bl_owner.Mode = "Delete";
                bl_owner.Date = common.GetDate();
                bl_owner.FirstName = "";
                bl_owner.MiddleName = "";
                bl_owner.LastName ="";
                bl_owner.PhoneNo = "";
                bl_owner.Mobile1 = "";
                bl_owner.Mobile2 = "";
                bl_owner.Mobile3 = "";
                bl_owner.Email = "";
                bl_owner.CityId = 0;
                bl_owner.AreaId = 0;
                bl_owner.Address = "";
                bl_owner.Image = "name";
                bl_owner.Password = "";
                bl_owner.OwnerId = Convert.ToInt32(e.CommandArgument.ToString());
                string sub = lblImage.Text.Substring(13);
                imagename = sub;
                if ("NoImage.jpg" != imagename)
                {
                    string imagefilepath = Server.MapPath(lblImage.Text);
                    File.Delete(imagefilepath);
                }

               
                bl_owner.InsertOwner(bl_owner);
                BindOwner();
                clear();
            }
        }
        catch
        {


        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string filename;
        setField();
        bl_owner.Mode = "Update";
        bl_owner.OwnerId = id;
        if (imageuploder.HasFile)
        {
            string ext = Path.GetExtension(imageuploder.FileName);

            if ("NoImage.jpg" == imagename)
            {
                string maxid = common.ReturnOneValue("select max(OwnerId) from OwnerDetails");
                 filename = maxid + "" + ext;
            }
            else
            {
                string fn = Path.GetFileNameWithoutExtension(imagename);
                 filename = fn + "" + ext;
                string imagefilepath = Server.MapPath("images/Owner/" + imagename);
                File.Delete(imagefilepath);
            }
            imageuploder.PostedFile.SaveAs(Server.MapPath("images/Owner/") + filename);
            bl_owner.Image = filename;
        }
        else
        {
            bl_owner.Image = imagename;
        }
        bl_owner.InsertOwner(bl_owner);
        BindOwner();
        Image1.Visible = false;
        clear();
        string message = "Your details have been Updated successfully.";
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "')};";
        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
    }

    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        common.bindAreabycity(ddlarea, Convert.ToInt32(ddlcity.SelectedValue));
    }
}