﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.Services;
using System.Configuration;
public partial class Menu : System.Web.UI.Page
{
    BL_Restaurants bl_restaurant = new BL_Restaurants();
    string Category = "";
   static string restname="";
   static int restoid;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
             BindAllRestaurantDetails();
             restoid = Convert.ToInt32(Request.QueryString["key"].ToString());
             rptreview.DataSource = GetReviewData(1);
             rptreview.DataBind();
        }
    }

    private void BindAllRestaurantDetails()
    {
        bl_restaurant.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        DataSet ds = bl_restaurant.SelectAllRestaurantDetails(bl_restaurant);
        rptcategory.DataSource = ds.Tables[0];
        rptcategory.DataBind();

        rptitem.DataSource = ds.Tables[1];
        rptitem.DataBind();


        lstresto.DataSource = ds.Tables[2];
        lstresto.DataBind();

        lblrestonamehead.Text = Convert.ToString(ds.Tables[2].Rows[0]["RestaurantName"]);
        restname = Convert.ToString(ds.Tables[2].Rows[0]["RestaurantName"]);

        string fullpath = ds.Tables[2].Rows[0]["Image"].ToString();
        string path = fullpath.Substring(3, fullpath.Length - 3); //remove "fp/"
        Restoimage.ImageUrl = path;

        //rptreview.DataSource = ds.Tables[3];
        //rptreview.DataBind();

        lstdeliverylocation.DataSource = ds.Tables[4];
        lstdeliverylocation.DataBind();

        rptpaymenttype.DataSource = ds.Tables[5];
        rptpaymenttype.DataBind();

        rptrestocat.DataSource = ds.Tables[6];
        rptrestocat.DataBind();

        rptrestocathead.DataSource = ds.Tables[6];
        rptrestocathead.DataBind();

        if (ds.Tables[7].Rows.Count > 0)
        {
            lblminimumorder.Text = "MinimumOrders : " + ds.Tables[7].Rows[0]["MinimumOrders"].ToString();
            lbldeliverycharges.Text = "DeliveryCharges : " + ds.Tables[7].Rows[0]["DeliveryCharges"].ToString();
            lblpickuptime.Text = "PickupTime   : " + ds.Tables[7].Rows[0]["PickupTime"].ToString() + " Min";
            lbldeliverytime.Text = "DeliveryTime   : " + ds.Tables[7].Rows[0]["DeliveryTime"].ToString() + " Min";
        }
    }

    protected void rptitem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Control divHeader = e.Item.FindControl("dvCategory");
        Repeater rptmenusubitem = e.Item.FindControl("rptmenusubitem") as Repeater;
        Label lblCustomisation = e.Item.FindControl("lblCustomisation") as Label;
        Label lblitemid = e.Item.FindControl("lblitemid") as Label;
        Label lblPrice = e.Item.FindControl("lblPrice") as Label;
        LinkButton lnkaddmenusubitem = e.Item.FindControl("lnkaddmenusubitem") as LinkButton;

        if (divHeader != null)
        {
            Label lblCategory = (Label)divHeader.FindControl("lblCategory");
            if (lblCategory != null)
            {
                if (Category == lblCategory.Text)
                {
                    divHeader.Visible = false;
                }
                else
                {
                    Category = lblCategory.Text;
                }
            }
        }

        if (lblCustomisation.Text == "1")
        {
            rptmenusubitem.Visible = true;
            lblPrice.Visible = false;
           
            lnkaddmenusubitem.Visible = true;
            BL_MenuSubItem bl_MenuSubItem = new BL_MenuSubItem();
            bl_MenuSubItem.ItemId = Convert.ToInt32(lblitemid.Text);
            DataTable dt = bl_MenuSubItem.SelectMenuSubItem(bl_MenuSubItem);
            rptmenusubitem.DataSource = dt;
            rptmenusubitem.DataBind();
        }
    }


    protected void lnkresto_Click(object sender, EventArgs e)
    {
        
        Response.Redirect("SelectRestoCategory.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }


    protected void lnkpaymenttype_Click(object sender, EventArgs e)
    {

        Response.Redirect("PaymentTypeEntry.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }

    protected void lnkrestocondition_Click(object sender, EventArgs e)
    {

        Response.Redirect("RestaruantConditions.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }


    protected void lnkdeliverylocation_Click(object sender, EventArgs e)
    {

        Response.Redirect("DeliveryLocation.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }

    protected void lnkdiscountoffer_Click(object sender, EventArgs e)
    {

        Response.Redirect("DiscountOffers.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }


    protected void lnkcoupens_Click(object sender, EventArgs e)
    {

        Response.Redirect("Coupen.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);
    }
    protected void lnkmenucategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuCategory.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);

    }
    protected void lnkmenuitem_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuItem.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);

    }

    protected void rptitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        Label lblitemid = e.Item.FindControl("lblitemid") as Label;
        Label lblItem = e.Item.FindControl("lblItem") as Label;
        Response.Redirect("MenuSubItem.aspx?itemid=" + lblitemid.Text + "&itemname=" + lblItem.Text + "&Key=" + Request.QueryString["key"].ToString());

    }


    protected void lnkeditresto_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditRestaurant.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + restname);

    }

    [WebMethod]
    public static string GetReview(int pageIndex)
    {
        return GetReviewData(pageIndex).GetXml();
    }

    public static DataSet GetReviewData(int pageIndex)
    {
        BL_Restaurants bl_restaurant = new BL_Restaurants();

        DataSet ds = bl_restaurant.GetReviewData(bl_restaurant, pageIndex, restoid);
        return ds;
    }
}

