﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="Restaurants.aspx.cs" Inherits="WF_Restaurants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../Search/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="../Search/jquery-1.11.3.min.js"></script>
<script src="../Search/mv-autocomplete.js" type="text/javascript"></script>
<link href="../Search/mv-autocomplete.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <input id="hdnSelectedCity" class="form-control" type="hidden" name="hdnSelectedCity" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" name="hdnSelectedCityArea"/>

<div class="account-container register">
<div class="content clearfix">
<h1 style="margin-left:300px">Add Restaurants Details</h1>			
<div class="login-fields">
<div class="span5" >
<div class="field">
 City :<br />
  <input id="txtCity" type="text" class="form-control" required="required" placeholder="Enter Your City..." data-object />
</div> 

<div class="field">
Restaurant Name :<br />
 <asp:TextBox  placeholder="Restaurant Name" ID="txtrestaurantname" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Address :<br />
<asp:TextBox ID="txtadd" placeholder="Restaurant Address" runat="server" TextMode="MultiLine" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Mobile No1 :<br />
<asp:TextBox ID="txtMobile1" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
ErrorMessage="Please enter mobile no" ForeColor="Red" ControlToValidate="txtMobile1" 
ValidationGroup="new1" ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile1" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div> 

<div class="field"> 
 Contact Person1  :<br /> 
 <asp:TextBox placeholder="Contact Person1" ID="txtcontactperson1" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Email Id:<br /> 
 <asp:TextBox ID="txtemail" placeholder="Email" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
ErrorMessage="enter email address" ControlToValidate="txtemail" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" 
runat="server" ErrorMessage="Envalid email" ControlToValidate="txtemail" 
ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
 
</div>

<div class="field">
Select Owner :<br />
<asp:DropDownList ID="ddlowner" runat="server"> </asp:DropDownList>
 <asp:RequiredFieldValidator ControlToValidate="ddlowner" ID="RequiredFieldValidator1"
ValidationGroup="new1" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Owner"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
</div> 



</div>


<div class="span5" >
<div class="field">
Area :<br />
<input id="txtArea" type="text" class="form-control" required="required" placeholder="Enter your Area..." data-object />
</div> 

<div class="field">
Description :<br />
<asp:TextBox ID="txtdes"  placeholder="Description" runat="server" TextMode="MultiLine" CssClass="textboxes"></asp:TextBox>
</div> 

<div class="field">
Phone Number :<br />
 <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="textboxes"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="please enter correct phone number"   ValidationExpression="\d{5}([- ]*)\d{6}" ControlToValidate="txtPhoneNo" ValidationGroup="new1" ForeColor="Red"></asp:RegularExpressionValidator>
</div> 

<div class="field">
Mobile No2 :<br />

<asp:TextBox ID="txtmobileno2" runat="server" CssClass="textboxes"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtmobileno2" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div>

<div class="field">
Contact Person2  :<br /> 
 <asp:TextBox  placeholder="Contact Person2"  ID="txtcontactperson2" runat="server" CssClass="textboxes"></asp:TextBox>
</div>
<div class="field">
Image : <br />
<asp:FileUpload ID="imageuploder" runat="server" /> 
</div>

<div class="field">
Password :<br />
  <asp:TextBox ID="txtpassword" placeholder="Password" runat="server" CssClass="textboxes" required="required" TextMode="Password"></asp:TextBox>
</div> 


</div>



</div>

</div>
</div>
<hr />
<b>Location  :</b>
     <asp:CheckBoxList ID="chklocation" RepeatDirection="Horizontal" RepeatColumns="10" runat="server">
    </asp:CheckBoxList>
     <asp:CustomValidator ID="CustomValidator1" ValidationGroup="new1" ErrorMessage="Please select at least one Location."
    ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
    <hr />
<b>Resturant Category :</b>
    <asp:CheckBoxList ID="lstchkcategory" RepeatDirection="Horizontal" RepeatColumns="10" runat="server">
    </asp:CheckBoxList>
    <asp:CustomValidator ID="CustomValidator2" ValidationGroup="new1" ErrorMessage="Please select at least one Resturant Category."
    ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList1" runat="server" />
    <hr />
    <hr />
<b>Payment Type :</b>
     <asp:CheckBoxList ID="lstchkpaymenttype" RepeatDirection="Horizontal" RepeatColumns="10" runat="server">
    </asp:CheckBoxList>
    <asp:CustomValidator ID="CustomValidator3" ValidationGroup="new1" ErrorMessage="Please select at least one Payment Type."
    ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList2" runat="server" />
    <hr />
<div class="login-actions">
 <asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" ValidationGroup="new1" class="button btn btn-primary btn-large"/>
    <asp:LinkButton ID="btnclear" OnClick="btnclear_Click" runat="server" class="button btn btn-primary btn-large">Clear</asp:LinkButton>
        
         </div>

<script type="text/javascript" language="javascript">

     var countries = "Manoj";


     $('#txtCity').mvAutocomplete({
         data: countries,
         container_class: 'results',
         result_class: 'result',
         url: '../WebService.asmx/GetMyCity',
         loading_html: 'Loading...',
         callback: function (el, selected) {
             console.log('Click Back!');
             document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
             
             GetLocation();
         }
     });

     $('#txtArea').mvAutocomplete({
         data: countries,
         container_class: 'results',
         result_class: 'result',
         url: '../WebService.asmx/GetMyArea',
         loading_html: 'Loading...',
         post_data: {
             cityid: function () { return document.getElementById('hdnSelectedCity').value; }
         },
         callback: function (el, selected) {
             console.log('Click Back!');
             document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
         }
     });



//     function GetLocation(cityid) {
//    
//        $.ajax({
//            type: "GET",
//            url: '../WebService.asmx/GetLocation?CityId=' + hdnSelectedCity.value,
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (response) {

//                alert(response);

//            },
//            failure: function (msg) {
//                alert(msg);
//            }

//        });
//    }


        </script>


<script type="text/javascript">
    function ValidateCheckBoxList(sender, args) {
        var checkBoxList = document.getElementById("<%=chklocation.ClientID %>");
        var checkboxes = checkBoxList.getElementsByTagName("input");
        var isValid = false;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                isValid = true;
                break;
            }
        }
        args.IsValid = isValid;

    }

    function ValidateCheckBoxList1(sender, args) {
        var checkBoxList = document.getElementById("<%=lstchkcategory.ClientID %>");
        var checkboxes = checkBoxList.getElementsByTagName("input");
        var isValid = false;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                isValid = true;
                break;
            }
        }
        args.IsValid = isValid;

    }

    function ValidateCheckBoxList2(sender, args) {
        var checkBoxList = document.getElementById("<%=lstchkpaymenttype.ClientID %>");
        var checkboxes = checkBoxList.getElementsByTagName("input");
        var isValid = false;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                isValid = true;
                break;
            }
        }
        args.IsValid = isValid;
    }
</script>


</asp:Content>


