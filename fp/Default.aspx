﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="WF_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>
<body>






	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				Admin Login				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="signup.html" class="">
							
						</a>
						
					</li>
					
					<li class="">						
						<a href="Default.aspx" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form runat="server">
		
			<h1>Welcome</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>

                <asp:Label ID="lblInvalid" runat="server" Text="" ForeColor="Red"></asp:Label>
				
				<div class="field">
					<label for="username">Email Address</label>
					<asp:TextBox ID="txtusername" runat="server" placeholder="Email Address" class="login username-field" required></asp:TextBox><br />
                    <%--<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />--%>
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<%--<input type="password" id="Password" name="password" value="" placeholder="Password" class="login password-field"/>--%>
				<asp:TextBox ID="txtpassword" runat="server" TextMode="Password" required class="login password-field"></asp:TextBox><br />

                </div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				

		<%--							
				<button class="button btn btn-success btn-large">Sign In</button>
				--%>
<asp:Button ID="btnLogIn" runat="server" Text="Log In" 
onclick="btnLogIn_Click" class="button btn btn-success btn-large"/>

			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->





<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>


 
</body>
</html>
