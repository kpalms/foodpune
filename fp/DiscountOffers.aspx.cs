﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_DiscountOffers : System.Web.UI.Page
{
    BL_DiscountOffers bl_discountoffer = new BL_DiscountOffers();
    static int id = 0;
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                DiscountOfferBindGrid();
                lblrestoname.Text = Request.QueryString["rn"].ToString();
            }
            catch
            {

            }
        }
        btnupdate.Enabled = false;
    }
    private void Setfill()
    {
        Common common = new Common();
        bl_discountoffer.Date = common.GetDate();
        bl_discountoffer.OfferName = txtoffername.Text;
        bl_discountoffer.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_discountoffer.DiscountType = Convert.ToInt32(ddldiscounttype.SelectedItem.Value);
        bl_discountoffer.DiscountStart = Convert.ToDateTime(txtstartdate.Text);
        bl_discountoffer.DiscountEnd = Convert.ToDateTime(txtenddate.Text);
        bl_discountoffer.DiscountOccurance = Convert.ToInt32(ddldiscountoccurance.SelectedItem.Value);
        bl_discountoffer.DiscountAmount = Convert.ToDecimal(txtdisamount.Text);
    }

    private void Clear()
   {
    txtstartdate.Text = "";
    txtenddate.Text = "";
    txtoffername.Text = "";
    txtdisamount.Text = "";
  }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            Setfill();
            bl_discountoffer.Mode = "Insert";
            bl_discountoffer.DiscountOffer(bl_discountoffer);
            DiscountOfferBindGrid();
            Clear();
        }
        catch(Exception ex)
        {
            throw (ex);
        }
    }
   

    private void DiscountOfferBindGrid()
    {
        try
        {
            bl_discountoffer.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_discountoffer.SelectDiscountOffer(bl_discountoffer);
            gdvdiscountoffer.DataSource = dt;
            gdvdiscountoffer.DataBind();
        }
        catch
        {

        }
    }

    protected void discountoffer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblRestaurantId = (Label)row.FindControl("lblRestaurantId");
            Label lblDiscountType = (Label)row.FindControl("lblDiscountType");
            Label lblDiscountStart = (Label)row.FindControl("lblDiscountStart");
            Label lblDiscountEnd = (Label)row.FindControl("lblDiscountEnd");
            Label lblDiscountOccurance = (Label)row.FindControl("lblDiscountOccurance");
            Label lblDiscountAmount = (Label)row.FindControl("lblDiscountAmount");
            Label lblOfferName = (Label)row.FindControl("lblOfferName");
          
            if (e.CommandName == "EditRow")
            {
                ddldiscounttype.SelectedItem.Text = lblDiscountType.Text;
                txtstartdate.Text = lblDiscountStart.Text;
                txtenddate.Text = lblDiscountEnd.Text;
                ddldiscountoccurance.SelectedItem.Text = lblDiscountOccurance.Text;
                txtdisamount.Text = lblDiscountAmount.Text;
                txtoffername.Text = lblOfferName.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_discountoffer.DiscountId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_discountoffer.DiscountType = 0;
                bl_discountoffer.RestaurantId = 0;
                bl_discountoffer.OfferName = "";
                bl_discountoffer.DiscountOccurance = 0;
                bl_discountoffer.DiscountStart =Convert.ToDateTime( DateTime.Now.ToString("dd/MM/yyyy"));
                bl_discountoffer.DiscountEnd = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
                bl_discountoffer.DiscountAmount = 0;
                bl_discountoffer.Mode = "Delete";
                bl_discountoffer.Date = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
                bl_discountoffer.DiscountOffer(bl_discountoffer);
                DiscountOfferBindGrid();
            }
        }
        catch
        {
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            Setfill();
            bl_discountoffer.Mode = "Update";
            bl_discountoffer.DiscountId = id;
            bl_discountoffer.DiscountOffer(bl_discountoffer);
            DiscountOfferBindGrid();
            Clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
        }
        catch
        {
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtdisamount.Text = "";
        txtenddate.Text = "";
        txtstartdate.Text = "";
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
    protected void rbtunlimited_CheckedChanged(object sender, EventArgs e)
    {
        txtenddate.Enabled = true;
        txtstartdate.Enabled = true;
        ddldiscountoccurance.Enabled = true;
    }
}