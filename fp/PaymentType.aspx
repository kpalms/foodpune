﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="PaymentType.aspx.cs" Inherits="WF_PaymentType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="account-container register">
<div class="content clearfix">
<h1>Payment Type</h1>			
<div class="login-fields">

<div class="field">
<asp:TextBox ID="txtpaymenttype" runat="server" CssClass="textboxes" required="required" placeholder="Payment Type" ></asp:TextBox>
</div> 

</div>
<div class="login-actions">

<asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" class="button btn btn-primary btn-large"/>
<asp:Button ID="btnupdate" runat="server" Text="Update" 
onclick="btnupdate_Click" class="button btn btn-primary btn-large"/>
<asp:Button ID="btnclear" runat="server" Text="Clear" 
onclick="btnclear_Click" class="button btn btn-primary btn-large"/>
</div>
</div> 
</div>




    <br />
     <div class="rounded_corners" >
    <asp:GridView ID="dgvpaymenttype" runat="server" onrowcommand="paymenttype_RowCommand" 
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>

        <Columns>
            <asp:TemplateField HeaderText="PaymentTypeId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PaymentTypeId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("PaymentTypeId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PaymentType">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PaymentType") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblpaymenttype" runat="server" Text='<%# Bind("PaymentType") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("PaymentTypeId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("PaymentTypeId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

