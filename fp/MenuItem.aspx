﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="MenuItem.aspx.cs" Inherits="WF_MenuItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="account-container register">
<div class="content clearfix">
<h1>Menu Item</h1>		
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
	
<div class="login-fields">

Select Category :<br /> 
    <asp:DropDownList ID="ddlcategory" runat="server">
    </asp:DropDownList>
     <asp:RequiredFieldValidator ControlToValidate="ddlcategory" ID="RequiredFieldValidator1"
ValidationGroup="new1" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Category"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
<div class="field">
Item :<br />
    <asp:TextBox ID="txtitem" runat="server" CssClass="textboxes" required="required" placeholder="Enter Item"></asp:TextBox>
</div> 

<div class="field">
Descripation : <br />
     <asp:TextBox ID="txtitemdescripation" runat="server" TextMode="MultiLine" CssClass="textboxes"  placeholder="Enter Item Description"></asp:TextBox>
</div> 


<div class="field">
Price :<br />
   <asp:TextBox ID="txtprice" runat="server" CssClass="textboxes" required="required" placeholder=" Price"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="((\d+)((\.\d{1,2})?))$"
ErrorMessage="Please enter valid integer or decimal number with 2 decimal places."
ControlToValidate="txtprice" ForeColor="Red" ValidationGroup="new1"/>
</div> 
<div class="field">
 Image :<asp:FileUpload ID="FileUploaderimage" runat="server" />
    <asp:Image ID="imgedit" runat="server" Visible="false" Width="100px" Height="100px"/>
  </div>
  <div class="field">
Type :<br />
      <asp:RadioButtonList ID="rbttype" runat="server" RepeatDirection="Vertical" >
      <asp:ListItem Text="Veg" Value="Veg"></asp:ListItem>
     <asp:ListItem Text="NonVeg" Value="NonVeg"></asp:ListItem>
      </asp:RadioButtonList>

      <asp:RequiredFieldValidator runat="server" ID="genderRequired" Display="Dynamic"
    ControlToValidate="rbttype" ForeColor="Red" ErrorMessage="plese select type"
    ValidationGroup="new1"></asp:RequiredFieldValidator>
     
    <%--<asp:RadioButton ID="rbtveg" runat="server"  Text="Veg" GroupName="type"/>
    <asp:RadioButton ID="rbtnonveg" runat="server"  Text="NonVeg" GroupName="type"/>--%>
    </div>
   <div class="field">
    Menu Sub Item :<br />
     <asp:RadioButtonList ID="rbtmenusubitem" runat="server" RepeatDirection="Vertical">
      <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
     <asp:ListItem Text="No" Value="No"></asp:ListItem>
      </asp:RadioButtonList>

        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" Display="Dynamic"
    ControlToValidate="rbtmenusubitem" ForeColor="Red" ErrorMessage="plese select Menu Sub Item"
    ValidationGroup="new1"></asp:RequiredFieldValidator>

       <%-- <asp:RadioButton ID="rbtcustmization" runat="server"  Text="Yes" GroupName="subitemname"/>
         <asp:RadioButton ID="rbtcustmization1" runat="server"  Text="No" GroupName="subitemname"/>--%>
</div>

</div>
<div class="login-actions">
 <asp:Button ID="btnsave" runat="server" Text="Save" ValidationGroup="new1" onclick="btnsave_Click" class="button btn btn-primary btn-large"/>
    <asp:Button ID="btnupdate" runat="server" Text="Update" ValidationGroup="new1" class="button btn btn-primary btn-large" onclick="btnupdate_Click" />
    <asp:LinkButton ID="btnclear" runat="server" Text="Clear" class="button btn btn-primary btn-large" onclick="btnclear_Click" ></asp:LinkButton>
    <asp:LinkButton ID="btnback" runat="server" Text="Go To Back"  class="button btn btn-warning btn-large" onclick="btnback_Click"></asp:LinkButton>
</div>
</div> 
</div> 


     <div class="rounded_corners" >
    <asp:GridView ID="gdvmenuitem" runat="server" onrowcommand="menuitem_RowCommand"
    
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />
        <alternatingrowstyle CssClass="AlternateRow"/>
        <Columns>
            <asp:TemplateField HeaderText="ItemId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ItemId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblItemId" runat="server" Text='<%# Bind("ItemId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CategoryId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CategoryId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCategoryId" runat="server" Text='<%# Bind("CategoryId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Item") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblItem" runat="server" Text='<%# Bind("Item") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ItemDescription">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ItemDescription") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblItemDescription" runat="server" Text='<%# Bind("ItemDescription") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Price">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPrice" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Image">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
                   
                </EditItemTemplate>
                <ItemTemplate>
                   <img src="<%#Eval("Image")%>" alt="" height="100px" width="100px">
                    <asp:Label ID="lblImage" runat="server" Text='<%# Bind("Image") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Type">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Customisation">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Customisation") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCustomisation" runat="server" Text='<%# Bind("Customisation") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblActive" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("ItemId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("ItemId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                    <asp:LinkButton ID="lnkActive" runat="server" CommandName="Active" Text='<%# Bind("Active") %>' CommandArgument='<%#Bind("ItemId") %>'>Active</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

