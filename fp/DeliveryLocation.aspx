﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="DeliveryLocation.aspx.cs" Inherits="WF_DeliveryLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
    
<div class="login-fields">
City Name : <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlCity_SelectedIndexChanged"></asp:DropDownList>
        
         <asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a City"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>

Area Name : <asp:DropDownList ID="ddlArea" runat="server" ></asp:DropDownList>
                
                <asp:RequiredFieldValidator ControlToValidate="ddlArea" ID="RequiredFieldValidator2"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Area"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>

        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
<div class="field">
</div> 

</div>
<div class="login-actions">

 <asp:Button ID="btnSave" ValidationGroup="new" runat="server" Text="Save" onclick="btnSave_Click" class="button btn btn-primary btn-large"/>
   
 <asp:Button ID="btnback" runat="server" Text="Go To Back" 
       class="button btn btn-warning btn-large" onclick="btnback_Click"/>
</div>
</div> 
</div> 

     <div class="rounded_corners" >
<asp:GridView ID="gdvdeliverylocation" runat="server" 
        onrowcommand="gdvdeliverylocation_RowCommand"
        
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>
        
    <Columns>
        <asp:TemplateField HeaderText="DeliveryId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("DeliveryId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDeliveryId" runat="server" Text='<%# Bind("DeliveryId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="RestaurantId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="RestaurantName" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblRestaurantName" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="CityId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("CityId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="CityName">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CityName") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="AreaId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AreaId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAreaId" runat="server" Text='<%# Bind("AreaId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="AreaName">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AreaName") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="RowDelete" CommandArgument='<%#Bind("DeliveryId")%>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    

</asp:GridView>
</div>
</asp:Content>

