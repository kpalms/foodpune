﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewOwnerDetails.aspx.cs" Inherits="WF_ViewOwnerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h3 style="text-align:center">Owner Details</h3>
<br />
<table width="100%" cellpadding="10px">
<tr>
<td><asp:Image ID="Image1" runat="server" Height="50" Width="50" /></td>
<td><b>Date :</b><asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label> </td>
</tr>

<tr>
<td><b>FullName :</b><asp:Label ID="lblFullName" runat="server" Text="Label"></asp:Label></td>
<td><b>Address :</b><asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label></td>
</tr>

<tr>
<td><b>PhoneNo :</b><asp:Label ID="lblPhoneNo" runat="server" Text="Label"></asp:Label></td>
<td><b>Mobile1 :</b><asp:Label ID="lblMobile1" runat="server" Text="Label"></asp:Label></td>
</tr>

<tr>
<td><b>Mobile2 :</b><asp:Label ID="lblMobile2" runat="server" Text="Label"></asp:Label></td>
<td><b>Mobile3 :</b><asp:Label ID="lblMobile3" runat="server" Text="Label"></asp:Label></td>
</tr>

<tr>
<td><b>Email :</b><asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label></td>
<td><b>Password :</b><asp:Label ID="lblPassword" runat="server" Text="Label"></asp:Label></td>
</tr>

<tr>
<td><b>CityName :</b><asp:Label ID="lblCityName" runat="server" Text="Label"></asp:Label></td>
<td><b>AreaName :</b><asp:Label ID="lblAreaName" runat="server" Text="Label"></asp:Label></td>
</tr>
</table>

 <%--  OwnerId :<asp:Label ID="lblOwnerId" runat="server" Text="Label" Visible="false"></asp:Label>
   <br />--%>
<hr />
<h3 style="text-align:center">Restaurants Details</h3>
<br />
 <div class="rounded_corners" >
<asp:GridView ID="gdvresto" runat="server" 

 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>


    <Columns>
     <asp:TemplateField HeaderText="Image">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
               <img src="<%#Eval("Image")%>" alt="" height="100px" width="100px">
<%--                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Image") %>'></asp:Label>
--%>            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="RestaurantId" Visible="false">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="RestaurantName">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Address">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <b>
                   <a href="RestaurantDetails.aspx?key=<%#Eval("RestaurantId")  %>">View Details</a>
                </b>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>

</asp:GridView>
</div>
</asp:Content>

