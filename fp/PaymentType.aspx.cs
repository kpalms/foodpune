﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_PaymentType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                paymenttypeBindGrid();
                
            }
            catch
            {

            }
        }
        btnupdate.Enabled = false;
    }


    static int id = 0;
    BL_PaymentType bl_paymenttype = new BL_PaymentType();

    private void paymenttypeBindGrid()
    {
        try
        {
            DataTable dt = bl_paymenttype.SelectPaymentType();
            dgvpaymenttype.DataSource = dt;
            dgvpaymenttype.DataBind();
        }
        catch
        {

        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bl_paymenttype.PaymentType= txtpaymenttype.Text;
            bl_paymenttype.Mode = "Insert";
            bl_paymenttype.PaymentTypes(bl_paymenttype);
            paymenttypeBindGrid();
            txtpaymenttype.Text = "";
        }
        catch
        {


        }
    }




    protected void paymenttype_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblpaymenttype = (Label)row.FindControl("lblpaymenttype");
            
            if (e.CommandName == "EditRow")
            {
                txtpaymenttype.Text = lblpaymenttype.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_paymenttype.PaymentTypeId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_paymenttype.PaymentType = "";
                bl_paymenttype.Mode = "Delete";
                bl_paymenttype.PaymentTypes(bl_paymenttype);
                paymenttypeBindGrid();
            }

          
        }
        catch
        {


        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {

            bl_paymenttype.PaymentTypeId = id;
            bl_paymenttype.PaymentType = txtpaymenttype.Text;
            bl_paymenttype.Mode = "Update";
            bl_paymenttype.PaymentTypes(bl_paymenttype);
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            txtpaymenttype.Text = "";
            paymenttypeBindGrid();
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtpaymenttype.Text = "";
    }
}