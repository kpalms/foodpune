﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class fp_SelectTodayOrderByDisaccept : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();
    Common common = new Common();
    static int restoid = 0;
    decimal finaltotal = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txttoday.Text = common.GetDate().ToString("dd-MM-yyyy");
            txtfrom.Text = common.GetDate().ToString("dd-MM-yyyy");
            Bindtodayorders(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
        }
    }

    protected void btnsee_Click(object sender, EventArgs e)
    {
        Bindtodayorders(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
    }

    private void Bindtodayorders(DateTime ToDate, DateTime FromDate)
    {
        bl_order.ToDate = ToDate;
        bl_order.FromDate = FromDate;
        DataSet ds = bl_order.SelectTodayOrderByDisaccept(bl_order);
        rpttodayorders.DataSource = ds.Tables[0];
        rpttodayorders.DataBind();
    }


    protected void rpttodayorders_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblRestaurantId = (Label)e.Item.FindControl("lblRestaurantId");
        Label lblRestaurantName = (Label)e.Item.FindControl("lblRestaurantName");
        Label lblOrders = (Label)e.Item.FindControl("lblOrders");
        Label lblAmount = (Label)e.Item.FindControl("lblAmount");
        Label lblDeliveryCharges = (Label)e.Item.FindControl("lblDeliveryCharges");

        //DataSet ds = bl_order.SelectTodayOrderAdmin(bl_order);

        //if (ds.Tables[0].Rows.Count > 0)
        //{
        //    if (ds.Tables[0].Rows[0]["RestaurantId"].ToString() != "")
        //    {
        //        restoid = ds.Tables[0].Rows[0]["RestaurantId"].ToString();

        //        if(restoid != )
        //    }
        //}


        //    if (restoid == lblRestaurantName.Text)
        //    {
        //        lblAmount.Text = Convert.ToString( Convert.ToDecimal(lblAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text));
        //    }
        //    else
        //    {
        //        restoid = lblRestaurantName.Text;
        //    }

        //    count = count + 1;

        //    lblOrders.Text =Convert.ToString(count);



        // finally Total Count
        //finaltotal = finaltotal + totol;

        //lblalltotal.Text = Convert.ToString(finaltotal);
    }




    protected void rpttodayorders_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        restoid = Convert.ToInt32(e.CommandArgument.ToString());
        Response.Redirect("ResturantOrderDisaccept.aspx?key=" + restoid + "&FromDate=" + txtfrom.Text + "&ToDate=" + txttoday.Text);
    }
}