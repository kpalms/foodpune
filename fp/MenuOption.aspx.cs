﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_MenuOption : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddlsubmenu();
            BindGridview();
            btnupdate.Enabled = false;
        }

    }
    BL_MenuItem bl_menuitem = new BL_MenuItem();
    static int id = 0;
    private void setfill()
    {

        bl_menuitem.MenuSubItemId = Convert.ToInt32(ddlsubname.SelectedItem.Value);
        bl_menuitem.Name = txtname.Text;
    }

    private void clear()
    {
        txtname.Text = "";
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        setfill();
        bl_menuitem.Active = 0;
        bl_menuitem.Mode = "Insert";
        bl_menuitem.MenuOptionId = 0;
        bl_menuitem.AddMenuOption(bl_menuitem);
        clear();
        BindGridview();
    }

    private void bindddlsubmenu()
    {
        DataTable dt = bl_menuitem.SelectMenuSubItemIdName(bl_menuitem);
        ddlsubname.DataSource = dt;
        ddlsubname.DataTextField = "Name";
        ddlsubname.DataValueField = "MenuSubItemId";
        ddlsubname.DataBind();

    }

    private void BindGridview()
    {
        DataTable dt = bl_menuitem.SelectMenuOption(bl_menuitem);
        dgvmenuoption.DataSource = dt;
        dgvmenuoption.DataBind();
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void dgvmenuoption_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblName = (Label)row.FindControl("lblName");
            Label lblsubname = (Label)row.FindControl("lblsubname");
            Label lblMenuSubItemId = (Label)row.FindControl("lblMenuSubItemId");

            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");



            if (e.CommandName == "EditRow")
            {
                // ddlitems.Text = lblItem.Text;
                // ddlitems.Text = lblItemId.Text;
                txtname.Text = lblName.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_menuitem.Name = "";
                bl_menuitem.Mode = "Delete";
                bl_menuitem.Active = 0;
                bl_menuitem.MenuSubItemId = 0;
                bl_menuitem.MenuOptionId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_menuitem.AddMenuOption(bl_menuitem);
                BindGridview();
            }
            if (e.CommandName == "Active")
            {
                if (lnkactive.Text == "Active")
                {
                    bl_menuitem.Name = "";
                    bl_menuitem.Mode = "Active";
                    bl_menuitem.Active = 0;
                    bl_menuitem.MenuSubItemId = 0;
                    bl_menuitem.MenuOptionId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menuitem.AddMenuOption(bl_menuitem);
                    BindGridview();
                }
                else
                {
                    bl_menuitem.Name = "";
                    bl_menuitem.Mode = "Active";
                    bl_menuitem.Active = 1;
                    bl_menuitem.MenuSubItemId = 0;
                    bl_menuitem.MenuOptionId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menuitem.AddMenuOption(bl_menuitem);
                    BindGridview();
                }
            }
        }
        catch
        {


        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_menuitem.MenuOptionId = id;
            bl_menuitem.Mode = "Update";
            bl_menuitem.AddMenuOption(bl_menuitem);
            BindGridview();
            clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
        }
        catch
        {


        }
    }
}