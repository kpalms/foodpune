﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;



public partial class WF_Area : System.Web.UI.Page
{
    static int id = 0;
    BL_Area bl_area = new BL_Area();
    BL_City bl_city = new BL_City();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                Common common = new Common();
                common.bindCity(ddlcity);
                AreaBindGrid();
            }
            catch
            {
            }
        }
        btnUpdate.Enabled = false;
    }
    private void AreaBindGrid()
    {
        try
        {
            DataTable dt = bl_area.SelectArea(bl_area);
            gdvarea.DataSource = dt;
            gdvarea.DataBind();
        }
        catch
        {
        }
    }
    private void SetFill()
    {
        bl_area.CityId = Convert.ToInt32(ddlcity.SelectedValue.ToString());
        bl_area.AreaName = txtareaname.Text;
        bl_area.Pincode = txtpincode.Text;
    }
    private void Clear()
    {
        txtareaname.Text = "";
        txtpincode.Text = "";
        ddlcity.SelectedIndex = 0;
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            SetFill();
            bl_area.Mode = "Insert";
            bl_area.Area(bl_area);
            AreaBindGrid();
            Clear();
        }
        catch
        {
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            SetFill();
            bl_area.AreaId = id;
            bl_area.Mode = "Update";
            bl_area.Area(bl_area);
            Clear();
            AreaBindGrid();
            btnUpdate.Enabled = false;
            btnsave.Enabled = true;
        }
        catch
        {
        }
    }



    protected void area_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCityName = (Label)row.FindControl("lblCityName");
            Label lblAreadId = (Label)row.FindControl("lblAreadId");
            Label lblAreaName = (Label)row.FindControl("lblAreaName");
            Label lblPincode = (Label)row.FindControl("lblPincode");
            Label lblActive = (Label)row.FindControl("lblActive");
            Label lblCityId = (Label)row.FindControl("lblCityId");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");
            if (e.CommandName == "EditRow")
            {
                ddlcity.Text = lblCityId.Text;
                txtareaname.Text = lblAreaName.Text;
                txtpincode.Text = lblPincode.Text;
                btnsave.Enabled = false;
                btnUpdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_area.AreaId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_area.AreaName = "";
                bl_area.CityId = 0;
                bl_area.Pincode = "0";
                bl_area.Mode = "Delete";

                bl_area.Area(bl_area);
                AreaBindGrid();
            }
            if (e.CommandName == "Active")
            {
                if (lnkactive.Text == "Active")
                {
                    bl_area.AreaName = "";
                    bl_area.Mode = "Active";
                    bl_area.Active = 1;
                    bl_area.CityId = 0;
                    bl_area.Pincode = "0";
                    bl_area.AreaId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_area.Area(bl_area);
                    AreaBindGrid();
                }
                else
                {
                    bl_area.AreaName = "";
                    bl_area.Mode = "Active";
                    bl_area.Active = 0;
                    bl_area.CityId = 0;
                    bl_area.Pincode = "0";
                    bl_area.AreaId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_area.Area(bl_area);
                    AreaBindGrid();
                }
            }
        }
        catch
        {


        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
}