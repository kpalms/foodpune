﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WF_Default : System.Web.UI.Page
{

    BL_AdminLogin bl_adminlogin = new BL_AdminLogin();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogIn_Click(object sender, EventArgs e)
    {
        SetFill();

       // Label lblusername=(Label)Master.FindControl("lblusername");
        //Label lblusername = (Label)Master.FindControl("lblusername");


        DataTable dt = bl_adminlogin.AdminLogin(bl_adminlogin);
        if (dt.Rows.Count > 0)
        {
            Session["LoginId"]= dt.Rows[0]["LoginId"].ToString();
            Session["AdminUserName"] = dt.Rows[0]["UserName"].ToString();
            Response.Redirect("Home.aspx");
        }
        else
        {
            lblInvalid.Text = "Invalid Credentials";
        }
        
        //if (Convert.ToInt16(dt.Rows[0]["LoginId"].ToString())==null)
        //{
        //    Session["LoginId"] = null;
        //    lblInvalid.Text = "Invalid Credentials";
        //}
        //else
        //{
        //    Session["LoginId"] = Convert.ToInt16(dt.Rows[0]["LoginId"].ToString());
        //    Response.Redirect("Home.aspx?");
        //    Clear();
        //}
    }

    private void SetFill()
    {
        bl_adminlogin.UserName = txtusername.Text;
        bl_adminlogin.Password = txtpassword.Text;
    }

    private void Clear()
    {
        txtpassword.Text = "";
        txtusername.Text = "";

    }

}