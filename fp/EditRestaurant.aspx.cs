﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class fp_EditRestaurant : System.Web.UI.Page
{
    Common common = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblrestoname.Text = Request.QueryString["rn"].ToString();
            common.bindCity(ddlcity);
            restaurantsBindGrid();
          

        }
    }
    BL_Restaurants bl_restaurants = new BL_Restaurants();

    static string imagename = "";
    private void restaurantsBindGrid()
    {
        try
        {
            bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_restaurants.SelectRestaurantDetailsByRestoId(bl_restaurants);
            txtrestaurantname.Text = dt.Rows[0]["RestaurantName"].ToString();
            txtdes.Text = dt.Rows[0]["Description"].ToString();
            txtadd.Text = dt.Rows[0]["Address"].ToString();
            txtphoneno.Text = dt.Rows[0]["PhoneNo"].ToString();
            txtmobileno1.Text = dt.Rows[0]["MobileNo1"].ToString();
            txtmobileno2.Text = dt.Rows[0]["MobileNo2"].ToString();
            txtcontactperson1.Text = dt.Rows[0]["ContactPerson1"].ToString();
            txtcontactperson2.Text = dt.Rows[0]["ContactPerson2"].ToString();
            txtemail.Text = dt.Rows[0]["Email"].ToString();
            imgresto.ImageUrl = dt.Rows[0]["Images"].ToString();
            ddlcity.SelectedValue =dt.Rows[0]["CityId"].ToString();
            common.bindAreabycity(ddlarea, Convert.ToInt32(dt.Rows[0]["CityId"]));
            ddlarea.SelectedValue = dt.Rows[0]["AreaId"].ToString();
            string fullpath = dt.Rows[0]["Images"].ToString();
            string path = fullpath.Substring(13, fullpath.Length - 13);
            imagename = path;
            
        }
        catch
        {

        }

    }


    private void setfill()
    {
        bl_restaurants.Date = common.GetDate();
        bl_restaurants.CityId = Convert.ToInt32( ddlcity.SelectedValue);
        bl_restaurants.AreaId = Convert.ToInt32(ddlarea.SelectedValue);
        bl_restaurants.RestaurantName = txtrestaurantname.Text;
        bl_restaurants.Description = txtdes.Text;
        bl_restaurants.Address = txtadd.Text;
        bl_restaurants.Phoneno = txtphoneno.Text;
        bl_restaurants.MobileNo1 = txtmobileno1.Text;
        bl_restaurants.MobileNo2 = txtmobileno2.Text;
        bl_restaurants.ContactPerson1 = txtcontactperson1.Text;
        bl_restaurants.ContactPerson2 = txtcontactperson2.Text;
        bl_restaurants.Email = txtemail.Text;
        bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_restaurants.OwnerId = 0;
        bl_restaurants.ResturantCategory = "";
        bl_restaurants.PaymentTypesList = "";
        bl_restaurants.LocationList = "";
        bl_restaurants.Password = "";
    }

    private void Clear()
    {
        txtmobileno2.Text = "";
        txtmobileno1.Text = "";
        txtemail.Text = "";
        txtphoneno.Text = "";
        txtrestaurantname.Text = ""; 
        txtdes.Text = "";
        txtcontactperson2.Text = "";
        txtcontactperson1.Text = "";
        txtadd.Text = "";
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            setfill();
            bl_restaurants.Mode = "Update";
            bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            if (imageuploder.HasFile)
            {
                string ext = Path.GetExtension(imageuploder.FileName);
                if ("NoImage.jpg" == imagename)
                {
                    string maxid = common.ReturnOneValue("select max(RestaurantId) from RestaurantDetails");
                    filename = maxid + "" + ext;
                }
                else
                {
                    string fn = Path.GetFileNameWithoutExtension(imagename);
                    filename = fn + "" + ext;
                    string imageFilePath = Server.MapPath("images/Resto/"+ imagename);
                    File.Delete(imageFilePath);
                }

                imageuploder.PostedFile.SaveAs(Server.MapPath("images/Resto/") + filename);
                bl_restaurants.Image = filename;
            }
            else
            {
                bl_restaurants.Image = imagename;
            }
            bl_restaurants.Restaurants(bl_restaurants);
            restaurantsBindGrid();

            string message = "Your Restaurant Details Updated successfully.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }
        catch
        {


        }
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }

    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        common.bindAreabycity(ddlarea,Convert.ToInt32( ddlcity.SelectedValue));
    }
}