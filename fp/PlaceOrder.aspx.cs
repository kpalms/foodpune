﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class fp_SelectTodayOrderByPlaceOrder : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();
    Common common = new Common();
    static int restoid = 0;
    static string restoname;
    decimal finaltotal = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txttoday.Text = common.GetDate().ToString("dd-MM-yyyy");
            txtfrom.Text = common.GetDate().ToString("dd-MM-yyyy");
            Bindtodayorders(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
        }
    }

    protected void btnsee_Click(object sender, EventArgs e)
    {
        Bindtodayorders(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
    }

    private void Bindtodayorders(DateTime ToDate, DateTime FromDate)
    {
        bl_order.ToDate = ToDate;
        bl_order.FromDate = FromDate;
        DataSet ds = bl_order.SelectTodayOrderByPlaceOrder(bl_order);
        //if (ds.Tables[0].Rows.Count > 0)
        //{
        //    decimal tot = 0;
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    { 
        //         tot= tot +  Convert.ToDecimal(ds.Tables[0].Rows[i]["Total"]);
        //         lblalltotal.Text = Convert.ToString(tot);
        //    }
        //}

        rpttodayorders.DataSource = ds.Tables[0];
        rpttodayorders.DataBind();
    }


    protected void rpttodayorders_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblRestaurantId = (Label)e.Item.FindControl("lblRestaurantId");
        Label lblRestaurantName = (Label)e.Item.FindControl("lblRestaurantName");
        Label lblOrders = (Label)e.Item.FindControl("lblOrders");
        Label lblAmount = (Label)e.Item.FindControl("lblAmount");
        Label lblDiscountId = (Label)e.Item.FindControl("lblDiscountId");

        //DataSet ds = bl_order.SelectTodayOrderAdmin(bl_order);

        //if (ds.Tables[0].Rows.Count > 0)
        //{
        //    if (ds.Tables[0].Rows[0]["RestaurantId"].ToString() != "")
        //    {
        //        restoid = ds.Tables[0].Rows[0]["RestaurantId"].ToString();

        //        if(restoid != )
        //    }
        //}


        //    if (restoid == lblRestaurantName.Text)
        //    {
        //        lblAmount.Text = Convert.ToString( Convert.ToDecimal(lblAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text));
        //    }
        //    else
        //    {
        //        restoid = lblRestaurantName.Text;
        //    }

        //    count = count + 1;

        //    lblOrders.Text =Convert.ToString(count);



        // finally Total Count
        //finaltotal = finaltotal + totol;

        //lblalltotal.Text = Convert.ToString(finaltotal);

        

        decimal totol;
        if (lblDiscountId.Text != "0")
        {
            bl_order.DiscountId = Convert.ToInt32(lblDiscountId.Text);
            DataTable dt = bl_order.GetDiscountById(bl_order);
            if (dt.Rows[0]["DiscountType"].ToString() == "0")
            {
                totol = Convert.ToDecimal(lblAmount.Text) - (Convert.ToDecimal(lblAmount.Text) * Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) / 100);
            }
            else
            {
                totol = Convert.ToDecimal(lblAmount.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString());
            }
        }
        else
        {
            totol = Convert.ToDecimal(lblAmount.Text);
        }

        finaltotal = finaltotal + totol;
        lblAmount.Text = Convert.ToString(totol);

        lblalltotal.Text = Convert.ToString(finaltotal);
    }




    protected void rpttodayorders_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        restoid = Convert.ToInt32(e.CommandArgument.ToString());
        Response.Redirect("ResturantOrderPlaceOrder.aspx?key=" + restoid + "&FromDate=" + txtfrom.Text + "&ToDate=" + txttoday.Text);
    }
}