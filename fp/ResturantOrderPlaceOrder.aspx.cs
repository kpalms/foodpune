﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class fp_SelectResturantOrderDetailsByPlaceOrder : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();
    Common common = new Common();
    static int orderid = 0;
    decimal finaltotal = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txttoday.Text = common.GetDate().ToString("dd-MM-yyyy");
            //txtfrom.Text = common.GetDate().ToString("dd-MM-yyyy");
            txttoday.Text = Request.QueryString["ToDate"].ToString();
            txtfrom.Text = Request.QueryString["FromDate"].ToString();
            SelectResturantOrderDetails(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
        }
    }

    protected void rptrecntorder_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Label lblDiscountId = (Label)e.Item.FindControl("lblDiscountId");
        Label lblBillAmount = (Label)e.Item.FindControl("lblBillAmount");
        Label lblDeliveryCharges = (Label)e.Item.FindControl("lblDeliveryCharges");
        Label lbltotal = (Label)e.Item.FindControl("lbltotal");
        decimal totol;
        if (lblDiscountId.Text != "0")
        {
            bl_order.DiscountId = Convert.ToInt32(lblDiscountId.Text);
            DataTable dt = bl_order.GetDiscountById(bl_order);
            if (dt.Rows[0]["DiscountType"].ToString() == "0")
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - (Convert.ToDecimal(lblBillAmount.Text) * Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) / 100) + Convert.ToDecimal(lblDeliveryCharges.Text);
            }
            else
            {
                totol = Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(dt.Rows[0]["DiscountAmount"].ToString()) + Convert.ToDecimal(lblDeliveryCharges.Text);
            }
        }
        else
        {
            totol = Convert.ToDecimal(lblBillAmount.Text) + Convert.ToDecimal(lblDeliveryCharges.Text);
        }
        finaltotal = finaltotal + totol;
        lbltotal.Text = Convert.ToString(totol);

        lblalltotal.Text = Convert.ToString(finaltotal);

    }

    protected void btnsee_Click(object sender, EventArgs e)
    {
        SelectResturantOrderDetails(Convert.ToDateTime(txttoday.Text), Convert.ToDateTime(txtfrom.Text));
    }


    private void SelectResturantOrderDetails(DateTime FromDate, DateTime ToDate)
    {
        bl_order.ToDate = FromDate;
        bl_order.FromDate = ToDate;
        bl_order.RestaurantId = Convert.ToInt32(Request.QueryString["Key"].ToString());
        DataTable dt = bl_order.SelectResturantOrderDetailsByPlaceOrder(bl_order);
        rptrecntorder.DataSource = dt;
        rptrecntorder.DataBind();
    }


    // 

    protected void rptrecntorder_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string str = e.CommandArgument.ToString();
        orderid = Convert.ToInt32(e.CommandArgument.ToString());
        Response.Redirect("OrdersDetails.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + txtfrom.Text + "&ToDate=" + txttoday.Text + "&orderid=" + orderid + "&orderstatus=PlaceOrder");
    }
}