﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="PaymentTypeEntry.aspx.cs" Inherits="WF_PaymentTypeEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
    
<div class="login-fields">
Select PaymentType :<asp:DropDownList ID="ddlPaymentType" runat="server">
    </asp:DropDownList>
    
     <asp:RequiredFieldValidator ControlToValidate="ddlPaymentType" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a PaymentType"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>

<asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>

</div>
<div class="login-actions">


 <asp:Button ID="btnSave" runat="server" ValidationGroup="new" Text="Save" onclick="btnSave_Click" class="button btn btn-primary btn-large"/>
    <asp:Button ID="btnback" runat="server" Text="Go To Back" 
       class="button btn btn-warning btn-large" onclick="btnback_Click"/>
</div>
</div> 
</div> 


     <div class="rounded_corners" >
    <asp:GridView ID="gdvpaymenttypeentry" runat="server" onrowcommand="gdvpaymenttypeentry_RowCommand"

 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>

   
        <Columns>
            <asp:TemplateField HeaderText="PaymentTEId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PaymentTEId") %>' Visible="false"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPaymentTEId" runat="server" Text='<%# Bind("PaymentTEId") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantName" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantName" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PaymentTypeId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PaymentTypeId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%# Bind("PaymentTypeId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PaymentType">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PaymentType") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPaymentType" runat="server" Text='<%# Bind("PaymentType") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("PaymentTEId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

