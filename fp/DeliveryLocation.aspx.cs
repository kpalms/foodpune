﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_DeliveryLocation : System.Web.UI.Page
{

    BL_DeliveryLocation bl_deliverylocation = new BL_DeliveryLocation();
    Common common = new Common();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            lblrestoname.Text = Request.QueryString["rn"].ToString();
            common.bindCity(ddlCity);
            
            BindGrid();

        }
    }

   


    private void setfill()
    {
        bl_deliverylocation.RestaurantId = Convert.ToInt32( Request.QueryString["key"].ToString());
        bl_deliverylocation.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_deliverylocation.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            int bindareaid = 0;
            int selectedareaid = Convert.ToInt32(ddlArea.SelectedValue);
            DataTable dt = bl_deliverylocation.SelectDeliveryLocation(bl_deliverylocation);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bindareaid = Convert.ToInt32(dt.Rows[i]["AreaId"]);
                    if (selectedareaid == bindareaid)
                    {
                        break;
                    }
                }
            }
            if (selectedareaid == bindareaid)
            {
                lblmsg.Text = "This Location is already Exiting";
                lblmsg.Visible = true;
            }
            else
            {
                setfill();
                bl_deliverylocation.Mode = "Insert";
                bl_deliverylocation.DeliveryLocation(bl_deliverylocation);
                BindGrid();
                lblmsg.Visible = false;
            }
        }
        catch
        {

        }
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.bindAreabycityAdmin(ddlArea, id);
    }

    private void BindGrid()
    {
        DataTable dt = bl_deliverylocation.SelectDeliveryLocation(bl_deliverylocation);
        gdvdeliverylocation.DataSource = dt;
        gdvdeliverylocation.DataBind();
    }
    protected void gdvdeliverylocation_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            if (e.CommandName == "RowDelete")
            {
                bl_deliverylocation.Mode = "Delete";
                bl_deliverylocation.RestaurantId = 0;
                bl_deliverylocation.AreaId = 0;
                bl_deliverylocation.CityId = 0;
                bl_deliverylocation.DeliveryId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_deliverylocation.DeliveryLocation(bl_deliverylocation);
                BindGrid();
                lblmsg.Visible = false;
            }  
        }
        catch
        {

        }
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
   
}