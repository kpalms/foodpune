﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="SelectRestoCategory.aspx.cs" Inherits="WF_SelectRestoCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
<div class="login-fields">
    <asp:DropDownList ID="ddlRestaurantCategory" runat="server" AutoPostBack="True">
    </asp:DropDownList>
         <asp:RequiredFieldValidator ControlToValidate="ddlRestaurantCategory" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a RestaurantCategory"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
     <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label><br/>
   <br/>
    
</div>
<div class="login-actions">
  <asp:Button ID="btnAdd" runat="server" Text="Add" ValidationGroup="new"
        class="button btn btn-primary btn-large" onclick="btnAdd_Click"/>
        <asp:Button ID="btnback" runat="server" Text="Go To Back" 
       class="button btn btn-warning btn-large" onclick="btnback_Click"/>
</div>
</div> 

    <asp:GridView ID="gdvselectrestocategory" runat="server" 
        AutoGenerateColumns="False" 
        onrowcommand="gdvselectrestocategory_RowCommand" >
        <Columns>
        <asp:TemplateField HeaderText="SelectRestoCatId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SelectRestoCatId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSelectRestoCatId" runat="server" Text='<%# Bind("SelectRestoCatId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
<%--            <asp:TemplateField HeaderText="RestaurantId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="RestoCategory">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RestoCategory") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestoCategory" runat="server" Text='<%# Bind("RestoCategory") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("SelectRestoCatId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</div> 






  

</asp:Content>

