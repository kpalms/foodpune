﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_PaymentTypeEntry : System.Web.UI.Page
{

    BL_PaymentTypeEntry bl_PaymentTypeEntry = new BL_PaymentTypeEntry();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                Common common = new Common();
                common.bindPaymentType(ddlPaymentType);
                PaymentTypeEnteryGridBind();
                lblrestoname.Text = Request.QueryString["rn"].ToString();
            }
            catch
            {

            }
        }
       
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlPaymentType.SelectedItem.Text == "-- Select PaymentType --")
        {
            lblmsg.Text = "please select payment type";
        }
        else
        {
            int bindpaymentid = 0;
            int selectpaymentid = Convert.ToInt32(ddlPaymentType.SelectedValue);
            bl_PaymentTypeEntry.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_PaymentTypeEntry.SelectPaymentTypeEntry(bl_PaymentTypeEntry);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bindpaymentid = Convert.ToInt32(dt.Rows[i]["PaymentTypeId"]);
                    if (selectpaymentid == bindpaymentid)
                    {
                        break;
                    }
                }
            }
            if (selectpaymentid == bindpaymentid)
            {
                lblmsg.Text = "This payment type is already Exiting";
            }
            else
            {
            SetFill();
            bl_PaymentTypeEntry.Mode = "Insert";
            bl_PaymentTypeEntry.PaymentTypeEntry(bl_PaymentTypeEntry);
            PaymentTypeEnteryGridBind();
            lblmsg.Visible = false;
            }
        }
    }




    private void SetFill()
    {
       bl_PaymentTypeEntry.PaymentTypeId= Convert.ToInt32(ddlPaymentType.SelectedItem.Value);
       bl_PaymentTypeEntry.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
    }


    private void PaymentTypeEnteryGridBind()
    {
        try
        {
            bl_PaymentTypeEntry.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_PaymentTypeEntry.SelectPaymentTypeEntry(bl_PaymentTypeEntry);
            gdvpaymenttypeentry.DataSource = dt;
            gdvpaymenttypeentry.DataBind();
        }
        catch
        {

        }

    }
    protected void gdvpaymenttypeentry_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);          
            if (e.CommandName == "DeleteRow")
            {
                bl_PaymentTypeEntry.PaymentTEId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_PaymentTypeEntry.PaymentTypeId = 0;
                bl_PaymentTypeEntry.RestaurantId = 0;
                bl_PaymentTypeEntry.Mode = "Delete";
                bl_PaymentTypeEntry.PaymentTypeEntry(bl_PaymentTypeEntry);
                PaymentTypeEnteryGridBind();
                lblmsg.Visible = false;
            }
        }
        catch
        {

        }
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}