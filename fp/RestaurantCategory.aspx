﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="RestaurantCategory.aspx.cs" Inherits="WF_RestaurantCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1>Restaurant Category</h1>			
<div class="login-fields">


<div class="field">
 <asp:TextBox ID="txtrestocategory" runat="server" CssClass="textboxes" required="required" placeholder="Restaurant Category"></asp:TextBox>

</div> 

</div>
<div class="login-actions">

 <asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click1" class="button btn btn-primary btn-large"/>
    <asp:Button ID="btnupdate" runat="server" Text="Update" class="button btn btn-primary btn-large"
        onclick="btnupdate_Click" />
        <asp:Button ID="btnclear" runat="server" Text="Clear" class="button btn btn-primary btn-large" 
        onclick="btnclear_Click" />

</div>
</div> 
</div> 



     <div class="rounded_corners" >
    <asp:GridView ID="dgvrestocategory" runat="server" onrowcommand="restocategory_RowCommand"
  AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>
    
      <Columns>
            <asp:TemplateField HeaderText="RestoCategoryId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RestoCategoryId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("RestoCategoryId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestoCategory">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RestoCategory") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblrestocategory" runat="server" Text='<%# Bind("RestoCategory") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("RestoCategoryId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("RestoCategoryId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

