﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class WF_ViewOwnerDetails : System.Web.UI.Page
{
    BL_Owner bl_owner = new BL_Owner();
    protected void Page_Load(object sender, EventArgs e)
    {
        OnwerDetails();
    }

    private void OnwerDetails()
    {
       bl_owner.OwnerId = Convert.ToInt32(Request.QueryString["key"].ToString());
       DataSet ds = bl_owner.SelectOwnerRestoDetailsById(bl_owner);
      // lblOwnerId.Text = ds.Tables[0].Rows[0]["OwnerId"].ToString();
       lblDate.Text = ds.Tables[0].Rows[0]["Date"].ToString();
       lblFullName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();
       lblPhoneNo.Text = ds.Tables[0].Rows[0]["PhoneNo"].ToString();
       lblMobile1.Text = ds.Tables[0].Rows[0]["Mobile1"].ToString();
       lblMobile2.Text = ds.Tables[0].Rows[0]["Mobile2"].ToString();
       lblMobile3.Text = ds.Tables[0].Rows[0]["Mobile3"].ToString();
       lblEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
       lblCityName.Text = ds.Tables[0].Rows[0]["CityName"].ToString();
       lblAreaName.Text = ds.Tables[0].Rows[0]["AreaName"].ToString();
       lblAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
       Image1.ImageUrl = ds.Tables[0].Rows[0]["Image"].ToString();
       lblPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();
       gdvresto.DataSource = ds.Tables[1];
       gdvresto.DataBind();
    }
}