﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_RestaurantCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                restorentcategoryBindGrid();

            }
            catch
            {
            }
        }
        btnupdate.Enabled = false;
    }
    static int id = 0;
    BL_RestaurantCategory bl_RestaurantCategory = new BL_RestaurantCategory();
    private void restorentcategoryBindGrid()
    {
        try
        {
            DataTable dt = bl_RestaurantCategory.SelectRestaurantCategory(bl_RestaurantCategory);
            dgvrestocategory.DataSource = dt;
            dgvrestocategory.DataBind();
        }
        catch
        {
        }
    }
    protected void restocategory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblrestocategory = (Label)row.FindControl("lblrestocategory");

            if (e.CommandName == "EditRow")
            {
                txtrestocategory.Text = lblrestocategory.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_RestaurantCategory.RestoCategoryId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_RestaurantCategory.RestoCategory = "";
                bl_RestaurantCategory.Mode = "Delete";
                bl_RestaurantCategory.RestaurantCategory(bl_RestaurantCategory);
                restorentcategoryBindGrid();
            }


        }
        catch
        {


        }
    }
   
        
   protected void  btnupdate_Click(object sender, EventArgs e)
{
    try
    {

        bl_RestaurantCategory.RestoCategoryId = id;
        bl_RestaurantCategory.RestoCategory = txtrestocategory.Text;
        bl_RestaurantCategory.Mode = "Update";
        bl_RestaurantCategory.RestaurantCategory(bl_RestaurantCategory);
        btnupdate.Enabled = false;
        btnsave.Enabled = true;
        txtrestocategory.Text = "";
        restorentcategoryBindGrid();
    }
    catch
    {


    }
}
        
    protected void btnsave_Click1(object sender, EventArgs e)
    {
        try
        {
            bl_RestaurantCategory.RestoCategory = txtrestocategory.Text;
            bl_RestaurantCategory.Mode = "Insert";
            bl_RestaurantCategory.RestaurantCategory(bl_RestaurantCategory);
            restorentcategoryBindGrid();
            txtrestocategory.Text = "";
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtrestocategory.Text = "";
    }
}