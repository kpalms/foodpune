﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ResturantOrderLater.aspx.cs" Inherits="fp_ResturantOrderLater" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
Order Later
<br />
From :  <asp:TextBox ID="txttoday" runat="server" class="datepicker" ></asp:TextBox>
    To :        <asp:TextBox ID="txtfrom" runat="server" class="datepicker" ></asp:TextBox>
    <asp:Button ID="btnsee" runat="server" Text="Show" onclick="btnsee_Click" />
  
        <div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  
<a href="OrderLater.aspx">Back</a>
<div  class="widget">
          <div class="widget-content">
            
              <ul class="messages_layout">
             
              <div id="dvReviews">
                <asp:Repeater ID="rptrecntorder" runat="server" 
                      onitemdatabound="rptrecntorder_ItemDataBound" 
                      onitemcommand="rptrecntorder_ItemCommand" >
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar">
                
                <img src="../img/user.png" height="50" width="50"/></a>
                  
                  <div class="message_wrap"> <span class="arrow"></span>
                  

<a class="name "><asp:Label ID="lblname"  runat="server" Text='<%#Eval("Name") %>'></asp:Label></a>
<%--<span class="time date"><a href="OrdersDetails.aspx?Orderid=<%#Eval("OrderId") %>">View Details</a></span>
--%>
 <asp:LinkButton ID="lnkorderid"  runat="server" CommandArgument='<%#Bind("OrderId")%>'>View Details</asp:LinkButton>
<div class="text comments">
 <asp:Label ID="lblDiscountId" runat="server" Text='<%#Eval("DiscountId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblBillAmount" runat="server" Text='<%#Eval("BillAmount") %>' Visible="false"></asp:Label>
<asp:Label ID="lbltotal" runat="server" Text='' ></asp:Label>
<asp:Label ID="lblDeliveryCharges" runat="server" Text='<%#Eval("DeliveryCharges") %>' Visible="false"></asp:Label>
</div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               


                        <h1 style="margin-left:700px">Total : <asp:Label ID="lblalltotal" runat="server"></asp:Label></h1>


              </ul>
             
            </div>
            </div>


</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
    });
</script>

</asp:Content>

