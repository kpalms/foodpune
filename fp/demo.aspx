﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="demo.aspx.cs" Inherits="WF_demo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head id="Head1" runat="server">
    <title></title>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
   <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script type="text/javascript">
        function Load_Details() {
      var Regno = document.getElementById('DropDownList2').value;
      $.ajax(
      {
         type: "POST",
         contentType: "application/json;charset=utf-8",
         url: "drop.aspx/LoadstudentDetails",
         data: JSON.stringify({ Regno: Regno }),
         dataType: "json",
         success: Succe,
         error: function (XMLHttpRequest, textStatus, errorThrown) {

             if (XMLHttpRequest.status == 0) {
                        alert(' Check Your Network.');
              } else if (XMLHttpRequest.status == 404) {
                        alert('Requested URL not found.');
              } else if (XMLHttpRequest.status == 500) {
                        alert('Internel Server Error.');
              } else {
              alert('Unknow Error.\n' + XMLHttpRequest.responseText);
                    }
                }
             });

            return false;
        }

        function Succe(result) {
            BindCheck(result);
        }
        function BindCheck(result) {

            var items = JSON.parse(result.d);
            CreateCheckBox(items);
        }
        function CreateCheckBox(checkboxlistItems) {
     
            var table = $('<table></table>');
            var counter = 0;
               $('.Chkclass').remove();
               $('#dvCheckBoxListControl').empty();
               $(checkboxlistItems).each(function ()                            table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
               type: 'checkbox', name: 'chklistitem', value: this.Value, id: 'chklistitem' + counter , class : 'Chkclass'
                })).append(
                $("<label class='Chkclass'>").attr({
                    for: 'chklistitem' + counter++
                }).text(this.AreaName))));
       
            });
               $(checkboxlistItems).each(function () {
               table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
             type: 'checkbox', name: 'chklistitem', value: this.Value, id: 'chklistitem' + counter , class : 'Chkclass'
                })).append(
                $("<label class='Chkclass'>").attr({
                    for: 'chklistitem' + counter++
                }).text(this.AreaId))));
              });

            $('#BindingDiv').append(table);
        }

    </script>
</head>
<body >
    <form id="form1" runat="server">
    <div align="center">
    <br />
    <br />
        <asp:DropDownList ID="DropDownList2" runat="server" onchange="return Load_Details();">
        </asp:DropDownList>
      <div id="BindingDiv">
     
      </div>
    </div>
    </form>
</body>

</html>
