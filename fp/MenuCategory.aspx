﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="MenuCategory.aspx.cs" Inherits="WF_MenuCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="account-container register">
<div class="content clearfix">
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			

<h3>Menu Category</h3>      

  

	
<div class="login-fields">


<div class="field">
 Menu Category :<br />
 <asp:TextBox ID="txtCategory" runat="server" CssClass="textboxes" required="required" placeholder="Category"></asp:TextBox>

</div> 

Image : 
<br />
    <asp:FileUpload ID="imagefileuploader" runat="server" />
    <br />
        <asp:Image ID="imgedit" runat="server" Visible="false" Width="50px" Height="50px"/>
    <br />

</div>
<div class="login-actions">

<asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" class="button btn btn-primary btn-large"/>
    <asp:Button ID="btnupdate" runat="server" Text="Update" class="button btn btn-primary btn-large"
        onclick="btnupdate_Click" />
        <asp:Button ID="btnclear" runat="server" Text="Clear" class="button btn btn-primary btn-large"
        onclick="btnclear_Click" />
        
    <asp:LinkButton ID="btnback" onclick="btnback_Click" class="button  btn btn-warning btn-large" runat="server">Go To Back</asp:LinkButton>
       <asp:LinkButton ID="lnkmenuitem" runat="server" onclick="lnkmenuitem_Click">Go To Menu Item</asp:LinkButton>

</div>
</div> 
</div> 




         <div class="rounded_corners" >
    <asp:GridView ID="gdvmenucategory" runat="server" onrowcommand="menucategory_RowCommand"
    
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>
    
    
     
        <Columns>
            <asp:TemplateField HeaderText="CategoryId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CategoryId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCategoryId" runat="server" Text='<%# Bind("CategoryId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="RestaurantName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantName" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Category">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Image">
                <EditItemTemplate>
                   <%-- <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>--%>
                </EditItemTemplate>
                <ItemTemplate>
                   <img src="<%#Eval("Image")%>" alt="" height="100px" width="100px">

                    <asp:Label ID="lblImage" runat="server" Text='<%# Bind("Image") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblActive" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("CategoryId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("CategoryId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                    <asp:LinkButton ID="lnkactive" runat="server" CommandName="Active" Text='<%#Bind("Active") %>' CommandArgument='<%#Bind("CategoryId") %>'>Active</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
   
</asp:Content>

