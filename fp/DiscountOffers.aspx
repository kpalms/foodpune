﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="DiscountOffers.aspx.cs" Inherits="WF_DiscountOffers" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" href="../RestoLogin/Calender/style.css">
<script type="text/javascript" src="../RestoLogin/Calender/jquery.min.js"></script>
	<script type="text/javascript" src="../RestoLogin/Calender/jquery-ui.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1>Discount Offers</h1>	
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
		
<div class="login-fields">
<%--Restaurant :<asp:DropDownList ID="ddlrestaurant" runat="server">
    </asp:DropDownList>
    <br />--%>
    Discount Occurance :
 <asp:DropDownList ID="ddldiscountoccurance" runat="server">
    <asp:ListItem Text="OneTime" Value="0" />
<asp:ListItem Text="Multiple" Value="1" />
<asp:ListItem Text="Unlimited" Value="3" />
    </asp:DropDownList>
 <div class="field">
     Offer Name :<asp:TextBox ID="txtoffername" runat="server" CssClass="textboxes" required="required" placeholder="Offer Name"></asp:TextBox>
</div> 

<div class="field">

Discount Type :
 <asp:DropDownList ID="ddldiscounttype" runat="server">
    <asp:ListItem Text="Percentage" Value="0" />
<asp:ListItem Text="Rupees" Value="1" />
    </asp:DropDownList>

</div> 

<div class="field">
Discount Start Date : <asp:TextBox ID="txtstartdate" class="datepicker" runat="server"  required="required" placeholder="Discount Start Date"></asp:TextBox>
</div> 

<div class="field">
Discount End Date :   <asp:TextBox ID="txtenddate" class="datepicker" runat="server"  required="required" placeholder="Discount End Date"></asp:TextBox>
</div> 



<div class="field">
  Discount Amount : <asp:TextBox ID="txtdisamount" runat="server" CssClass="textboxes" required="required" placeholder="Discount Amount"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="((\d+)((\.\d{1,2})?))$"
ErrorMessage="Please enter valid integer or decimal number with 2 decimal places."
ControlToValidate="txtdisamount" ForeColor="Red" ValidationGroup="new1"/>
</div> 

</div>
<div class="login-actions">
 <asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" class="button btn btn-primary btn-large" />
    <asp:Button ID="btnupdate" runat="server" Text="Update" class="button btn btn-primary btn-large"
        onclick="btnupdate_Click" />

    <asp:LinkButton ID="btnclear" runat="server" onclick="btnclear_Click" Text="Clear" class="button btn btn-primary btn-large"></asp:LinkButton>

    <asp:LinkButton ID="btnback" runat="server" class="button btn btn-warning btn-large" onclick="btnback_Click">Go To Back</asp:LinkButton>
</div>
</div> 
</div> 

  


         <div class="rounded_corners" >
    <asp:GridView ID="gdvdiscountoffer" runat="server" onrowcommand="discountoffer_RowCommand" 
    
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>
    
    
        <Columns>
            <asp:TemplateField HeaderText="Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="OfferName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("OfferName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblOfferName" runat="server" Text='<%# Bind("OfferName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountId" Visible=false>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DiscountId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountId" runat="server" Text='<%# Bind("DiscountId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantId" Visible=false>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantName" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountType">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("DiscountType") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountType" runat="server" Text='<%# Bind("DiscountType") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountStart">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("DiscountStart") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountStart" runat="server" Text='<%# Bind("DiscountStart") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountEnd">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("DiscountEnd") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountEnd" runat="server" Text='<%# Bind("DiscountEnd") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountOccurance">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" 
                        Text='<%# Bind("DiscountOccurance") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountOccurance" runat="server" Text='<%# Bind("DiscountOccurance") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DiscountAmount">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DiscountAmount") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDiscountAmount" runat="server" Text='<%# Bind("DiscountAmount") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("DiscountId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("DiscountId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
</asp:GridView>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
    });
</script>
</asp:Content>


