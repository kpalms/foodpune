﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_RestaruantConditions : System.Web.UI.Page
{
    BL_RestaurantCondition bl_restaurantconditions = new BL_RestaurantCondition();
    Common common = new Common();
    static int id=0;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        btnupdate.Enabled = false;
        if (!IsPostBack)
        {
            
            bindgrid();
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_restaurantconditions.Mode = "Insert";
        setfill();
        bl_restaurantconditions.RestoConditionId = 0;
        bl_restaurantconditions.InsertRestoConditions(bl_restaurantconditions);
        clear();
        bindgrid();
    }

    private void setfill()
    {
        bl_restaurantconditions.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_restaurantconditions.MinimumOrders = Convert.ToDecimal(txtminimumorder.Text);
        bl_restaurantconditions.DeliveryCharges = Convert.ToDecimal(txtdeliverycharges.Text);
        bl_restaurantconditions.PickupTime = Convert.ToInt32( txtpickuptime.Text);
        bl_restaurantconditions.DeliveryTime = Convert.ToInt32(txtpickuptime.Text);
       
    }

    private void clear()
    {
        txtpickuptime.Text = "";
        txtminimumorder.Text = "";
        txtdeliverytime.Text = "";
        txtdeliverycharges.Text = "";
       // btnsave.Enabled = true;
        btnupdate.Enabled = false;
     }

    private void bindgrid()
    {
        lblrestoname.Text = Request.QueryString["rn"].ToString();
        bl_restaurantconditions.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
       DataTable dt= bl_restaurantconditions.SelectRestoConditions(bl_restaurantconditions);
       gvrestoconditions.DataSource = dt;
       
       gvrestoconditions.DataBind();
       if (dt.Rows.Count > 0)
       {
           btnsave.Enabled = false;
       }
       else
       {
           btnsave.Enabled = true;
       }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        bl_restaurantconditions.Mode = "Update";
        setfill();
        bl_restaurantconditions.RestoConditionId =id;
        bl_restaurantconditions.InsertRestoConditions(bl_restaurantconditions);
        clear();
        bindgrid();
        btnsave.Enabled = false;
        btnupdate.Enabled = false;
    }
    protected void gvrestoconditions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblRestoConditionId = (Label)row.FindControl("lblRestoConditionId");
            Label lblRestaurantId = (Label)row.FindControl("lblRestaurantId");
            Label lblRestaurantName = (Label)row.FindControl("lblRestaurantName");
            Label lblMinimumOrders = (Label)row.FindControl("lblMinimumOrders");
            Label lblDeliveryCharges = (Label)row.FindControl("lblDeliveryCharges");
            Label lblPickupTime = (Label)row.FindControl("lblPickupTime");
            Label lblDeliveryTime = (Label)row.FindControl("lblDeliveryTime");



            if (e.CommandName == "EditRow")
            {
                ////ddlcity.Text = lblCityId.Text;
                txtdeliverycharges.Text = lblDeliveryCharges.Text;
                txtdeliverytime.Text = lblDeliveryTime.Text;
                txtminimumorder.Text = lblMinimumOrders.Text;
                txtpickuptime.Text = lblPickupTime.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_restaurantconditions.RestoConditionId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_restaurantconditions.RestaurantId = 0;
                bl_restaurantconditions.PickupTime = 0;
                bl_restaurantconditions.DeliveryTime = 0;
                bl_restaurantconditions.DeliveryCharges = 0;
                bl_restaurantconditions.MinimumOrders = 0;
                bl_restaurantconditions.Mode = "Delete";

                bl_restaurantconditions.InsertRestoConditions(bl_restaurantconditions);
                bindgrid();
                btnsave.Enabled = true;
                btnupdate.Enabled = false;
            }
            
        }
        catch
        {


        }
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}