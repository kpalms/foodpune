﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_DashBoard : System.Web.UI.Page
{

    BL_Order bl_order = new BL_Order();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAllOrderStatus();
        }
    }

    private void BindAllOrderStatus()
    {
        DataSet ds = bl_order.SelectAllOrderStatus(bl_order);

        dgvcomplete.DataSource = ds.Tables[0];
        dgvcomplete.DataBind();
       
        dgvprocessing.DataSource = ds.Tables[1];
        dgvprocessing.DataBind();
       
        dgvPending.DataSource = ds.Tables[2];
        dgvPending.DataBind();
        
        dgvcancel.DataSource = ds.Tables[3];
        dgvcancel.DataBind();

        
        dgvLater.DataSource=ds.Tables[4];
        dgvLater.DataBind();


        if (ds.Tables[5].Rows.Count > 0)
        {
            lblAccept.Text = ds.Tables[5].Rows[0]["Accept"].ToString();
        }
        else
        {
            lblAccept.Text = "0";
        }


         if (ds.Tables[5].Rows.Count > 0)
        {

        lblPlaceOrder.Text = ds.Tables[5].Rows[0]["PlaceOrder"].ToString();
        }
        else
        {
          lblPlaceOrder.Text = "0";
        }


        if (ds.Tables[5].Rows.Count > 0)
        {
        lblDisaccept.Text = ds.Tables[5].Rows[0]["Disaccept"].ToString();
        }
        else
        {
             lblDisaccept.Text = "0";
        }


         if (ds.Tables[5].Rows.Count > 0)
        {
          lblCancel.Text = ds.Tables[5].Rows[0]["Cancel"].ToString();
        }
         else
         {
               lblCancel.Text = "0";
         }


         if (ds.Tables[5].Rows.Count > 0)
         {
             lblLater.Text = ds.Tables[5].Rows[0]["Later"].ToString();
         }
         else
         {
             lblLater.Text = "0";
         }
    }
}