﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
public partial class WF_MenuCategory : System.Web.UI.Page
{
    Common common = new Common();
    static string imagename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                
               
                menucategoryBindGrid();
                lblrestoname.Text = Request.QueryString["rn"].ToString();
               // bl_PaymentTypeEntry.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());

            }
            catch
            {

            }
        }
        btnupdate.Enabled = false;
    }

    BL_MenuCategory bl_menucategory = new BL_MenuCategory();
    static int id = 0;
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            SetFill();
            bl_menucategory.Mode = "Insert";
            if (imagefileuploader.HasFile)
            {
                string ext = Path.GetExtension(imagefileuploader.FileName);
                string maxid = common.ReturnOneValue("select max(CategoryId) from MenuCategory");
                string filename = maxid + "" + ext;
                imagefileuploader.PostedFile.SaveAs(Server.MapPath("images/MenuCategory/") + filename);
                bl_menucategory.Image = filename;
            }
            else
            {
                bl_menucategory.Image = "NoImage.jpg";
            }
            bl_menucategory.MenuCategory(bl_menucategory);
            menucategoryBindGrid();
            Clear();
        }
        catch
        {


        }
    }

    private void SetFill()
    {

        bl_menucategory.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_menucategory.Category = txtCategory.Text;
        bl_menucategory.Active = 1;
    }

    private void Clear()
    {
        txtCategory.Text = "";
        //ddlrestaurant.Text = "";
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            SetFill();
            bl_menucategory.Mode = "Update";
            bl_menucategory.CategoryId = id;
            if (imagefileuploader.HasFile)
            {
                string ext = Path.GetExtension(imagefileuploader.FileName);
                if ("NoImage.jpg" == imagename)
                {
                    string maxid = common.ReturnOneValue("select max(CategoryId) from MenuCategory");
                     filename = maxid + "" + ext;
                }
                else
                {
                    string fn = Path.GetFileNameWithoutExtension(imagename);
                     filename = fn + "" + ext;
                    string imageFilePath = Server.MapPath("images/MenuCategory/" + imagename);
                    File.Delete(imageFilePath);
                }
                
                imagefileuploader.PostedFile.SaveAs(Server.MapPath("images/MenuCategory/") + filename);
                bl_menucategory.Image = filename;
            }
            else
            {
                bl_menucategory.Image = imagename;
            }
            bl_menucategory.MenuCategory(bl_menucategory);
            menucategoryBindGrid();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            Clear();
        }
        catch
        {


        }
    }


    private void menucategoryBindGrid()
    {
        try
        {
            bl_menucategory.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_menucategory.SelectMenuCategory(bl_menucategory);
            gdvmenucategory.DataSource = dt;
            gdvmenucategory.DataBind();
        }
        catch
        {

        }

    }



    protected void menucategory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
           // Label lblCategoryId = (Label)row.FindControl("lblCategoryId");
            Label lblRestaurantId = (Label)row.FindControl("lblRestaurantId");
            Label lblCategory = (Label)row.FindControl("lblCategory");
            Label lblImage = (Label)row.FindControl("lblImage");
            Label lblActive = (Label)row.FindControl("lblActive");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");
            if (e.CommandName == "EditRow")
            {
                txtCategory.Text = lblCategory.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                imgedit.Visible = true;
                string sub = lblImage.Text.Substring(20);
                imagename = sub;
               imgedit.ImageUrl = lblImage.Text;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_menucategory.CategoryId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_menucategory.Category = "";
                bl_menucategory.Active= 0;
                bl_menucategory.RestaurantId = 0;
                bl_menucategory.Image = "";
                bl_menucategory.Mode = "Delete";
                imgedit.Visible = false;
                bl_menucategory.MenuCategory(bl_menucategory);
                menucategoryBindGrid();
                string sub = lblImage.Text.Substring(20);
                imagename = sub;
                if ("NoImage.jpg" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImage.Text);
                    File.Delete(imageFilePath);
                }
                
                Clear();
            }


            if (e.CommandName == "Active")
            {
                if (lnkactive.Text == "Active")
                {
                    bl_menucategory.Category = "";
                    bl_menucategory.Mode = "Active";
                    bl_menucategory.Active = 1;
                    bl_menucategory.RestaurantId = 0;
                    bl_menucategory.Image = "";
                    bl_menucategory.CategoryId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menucategory.MenuCategory(bl_menucategory);
                    menucategoryBindGrid();
                }
                else
                {

                    bl_menucategory.Category = "";
                    bl_menucategory.Mode = "Active";
                    bl_menucategory.Active = 0;
                    bl_menucategory.RestaurantId = 0;
                    bl_menucategory.Image = "";
                    bl_menucategory.CategoryId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menucategory.MenuCategory(bl_menucategory);
                    menucategoryBindGrid();
                    Clear();
                }
            }
           
                
            
        }
        catch
        {


        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }


    protected void lnkmenuitem_Click(object sender, EventArgs e)
    {
        Response.Redirect("MenuItem.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString());

    }

}