﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="OwnerDetails.aspx.cs" Inherits="WF_OwnerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
	<div class="content clearfix">
<h1 style="margin-left:300px">Add Owner Details</h1>			
<div class="login-fields">
<div class="span5" >
<div class="field">
First Name : <br />
<asp:TextBox ID="txtFirstName" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div>
<div class="field">
LastName:<br />
<asp:TextBox ID="txtLastName" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div>
 <div class="field">
Mobile1:<br />
<asp:TextBox ID="txtMobile1" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
ErrorMessage="Please enter mobile no" ForeColor="Red" ControlToValidate="txtMobile1" 
ValidationGroup="new1" ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile1" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div> 

<div class="field">
Mobile3 : <br />
<asp:TextBox ID="txtMobile3" runat="server" CssClass="textboxes"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile3" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div>
<div class="field">
City:<br />
 <asp:DropDownList ID="ddlcity" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlcity_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  runat="server" ErrorMessage="please select city" InitialValue="0" Operator="Equel"  ControlToValidate="ddlcity" Display="Dynamic" ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
</div>
<div class="field">
Image:<br />
<asp:FileUpload ID="imageuploder" runat="server" />
    <asp:Image ID="Image1" runat="server" Width="20px" Height="20px"/>
</div>

<div class="field">
Password:<br />
<asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div>

</div>
<%--+++++++++++++++++++++++++--%>
<div class="span5" >
<div class="field">
MiddleName :<br />
<asp:TextBox ID="txtMiddleName" runat="server" CssClass="textboxes"></asp:TextBox>
</div>
<div class="field">
PhoneNo :<br />
<asp:TextBox ID="txtPhoneNo" runat="server" CssClass="textboxes"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="please enter correct phone number"   ValidationExpression="\d{5}([- ]*)\d{6}" ControlToValidate="txtPhoneNo" ValidationGroup="new1" ForeColor="Red"></asp:RegularExpressionValidator>

</div>
<div class="field">
Mobile2 :<br />
<asp:TextBox ID="txtMobile2" runat="server" CssClass="textboxes"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobile2" ValidationGroup="new1"></asp:RegularExpressionValidator>
  
</div> 
<div class="field">
Email: <br />
<asp:TextBox ID="txtEmail" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
ErrorMessage="enter email address" ControlToValidate="txtEmail" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" 
runat="server" ErrorMessage="Envalid email" ControlToValidate="txtEmail" 
ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
 
</div>
<div class="field">
Area:<br />
 <asp:DropDownList ID="ddlarea" runat="server">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  runat="server" ErrorMessage="please select Area" InitialValue="0" Operator="Equel"  ControlToValidate="ddlarea" Display="Dynamic" ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>

</div>

<div class="field">
Address:<br />
<asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div>

</div>
				
 			
			</div> <!-- /login-fields -->
		
			

		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

	<br />
    <br />
			<div class="login-actions">
				
					
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" style="margin-left:150px"  class="button btn btn-primary btn-large"
    onclick="btnSubmit_Click"  ValidationGroup="new1"/>
    <asp:Button ID="btnUpdate" runat="server" Text="Update" style="margin-left:50px" class="button btn btn-primary btn-large"
        onclick="btnUpdate_Click"  ValidationGroup="new1"/>
                <asp:LinkButton ID="btnClear"   onclick="btnClear_Click"  runat="server" style="margin-left:50px" class="button btn btn-primary btn-large" >Clear</asp:LinkButton>
        			</div> <!-- .actions -->


<br />
    <br />

     <div class="rounded_corners" >
    <asp:GridView ID="grdOwner" runat="server" OnRowCommand="owner_RowCommand"
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />
        <alternatingrowstyle CssClass="AlternateRow"/>
        <Columns>
            <asp:TemplateField HeaderText="OwnerId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtOwnerId" runat="server" Text='<%# Bind("OwnerId") %>'></asp:TextBox>
                </EditItemTemplate>

                <ItemTemplate>
                    <asp:Label ID="lblOwnerId" runat="server" Text='<%# Bind("OwnerId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtDate" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbldate" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FirstName">
                <EditItemTemplate>
                    <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="MiddleName">
                <EditItemTemplate>
                    <asp:TextBox ID="txtMiddleName" runat="server" Text='<%# Bind("MiddleName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMiddleName" runat="server" Text='<%# Bind("MiddleName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="LastName">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PhoneNo">
                <EditItemTemplate>
                    <asp:TextBox ID="txtPhoneNo" runat="server" Text='<%# Bind("PhoneNo") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Bind("PhoneNo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile1">
                <EditItemTemplate>
                    <asp:TextBox ID="txtMobile1" runat="server" Text='<%# Bind("Mobile1") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMobile1" runat="server" Text='<%# Bind("Mobile1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile2" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtMobile2" runat="server" Text='<%# Bind("Mobile2") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMobile2" runat="server" Text='<%# Bind("Mobile2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile3" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtMobile3" runat="server" Text='<%# Bind("Mobile3") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMobile3" runat="server" Text='<%# Bind("Mobile3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CityId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CityName">
                <EditItemTemplate>
                    <asp:TextBox ID="txtCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AreaName">
                <EditItemTemplate>
                    <asp:TextBox ID="txtAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AreaId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtAreaId" runat="server" Text='<%# Bind("AreaId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblAreaId" runat="server" Text='<%# Bind("AreaId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address">
                <EditItemTemplate>
                    <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("Address") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Image">
                <EditItemTemplate>
                    <asp:TextBox ID="txtImage" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image2" runat="server" ImageUrl='<%# Bind("Image") %>' Width="20" Height="20" />
                    <asp:Label ID="lblImage" runat="server" Text='<%# Bind("Image") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Password" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="txtPassword" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPassword" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("OwnerId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("OwnerId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
   
</asp:Content>

