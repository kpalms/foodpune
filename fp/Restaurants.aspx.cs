﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;


public partial class WF_Restaurants : System.Web.UI.Page
{
    BL_Restaurants bl_restaurants = new BL_Restaurants();
    BL_PaymentType bl_paymenttype = new BL_PaymentType();
    BL_RestaurantCategory bl_restaurantcategory = new BL_RestaurantCategory();
    static int id;
    static string imagename;
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                //common.bindCity(ddlcity.SelectedValue.ToString());
                BindChekboxList();
                common.bindOwner(ddlowner);
            }
            catch
            {

            }
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_restaurants.Mode = "Insert";
            bl_restaurants.RestaurantId = 0;
            String str = "";
             String paymenttype = "";
             String location = "";
            for (int i = 0; i <= lstchkcategory.Items.Count-1; i++)
            {
                if (lstchkcategory.Items[i].Selected)
                {
                    if (str == "")
                    {
                        str = lstchkcategory.Items[i].Value;
                    }
                    else
                    {
                        str += "," + lstchkcategory.Items[i].Value;

                    }
                }
            }

             for (int j = 0; j <= lstchkpaymenttype.Items.Count-1; j++)
            {
                if (lstchkpaymenttype.Items[j].Selected)
                {
                    if (paymenttype == "")
                    {
                        paymenttype = lstchkpaymenttype.Items[j].Value;
                    }
                    else
                    {
                        paymenttype += "," + lstchkpaymenttype.Items[j].Value;

                    }
                }
            }
            for (int k = 0; k <= chklocation.Items.Count - 1; k++)
            {
                if (chklocation.Items[k].Selected)
                {
                    if (location == "")
                    {
                        location = chklocation.Items[k].Value;
                    }
                    else
                    {
                        location += "," + chklocation.Items[k].Value;
                    }
                }
            }
            bl_restaurants.ResturantCategory = str + ",";
            bl_restaurants.PaymentTypesList = paymenttype + ",";
            bl_restaurants.LocationList = location + ",";
            if (imageuploder.HasFile)
            {
                string ext = Path.GetExtension(imageuploder.FileName);
                string maxid = common.ReturnOneValue("select max(RestaurantId) from RestaurantDetails");
                string filename = maxid + "" + ext;
                imageuploder.PostedFile.SaveAs(Server.MapPath("images/Resto/") + filename);
                bl_restaurants.Image = filename;
            }
            else
            {
                bl_restaurants.Image = "NoImage.jpg";
            }
                bl_restaurants.Restaurants(bl_restaurants);
                Clear();


                string message = "Your details have been saved successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }
        catch
        {

        }
    }

    private void BindChekboxList()
    {
        //restocategory
      DataTable dt=  bl_restaurantcategory.SelectRestaurantCategory(bl_restaurantcategory);
      lstchkcategory.DataSource = dt;
      lstchkcategory.DataValueField = "RestoCategoryId";
      lstchkcategory.DataTextField = "RestoCategory";
      lstchkcategory.DataBind();
        //payment type
      DataTable dtp = bl_paymenttype.SelectPaymentType();
      lstchkpaymenttype.DataSource = dtp;
      lstchkpaymenttype.DataValueField = "PaymentTypeId";
      lstchkpaymenttype.DataTextField = "PaymentType";
      lstchkpaymenttype.DataBind();
        //location
      BL_Area bl_area = new BL_Area();
      //bl_area.CityId = Convert.ToInt32(Request.Form["hdnSelectedCity"]); 
      bl_area.CityId = 1;
      DataTable dtloc = bl_area.SelectAreaByCityId(bl_area); 
      chklocation.DataSource = dtloc;
      chklocation.DataValueField = "AreaId";
      chklocation.DataTextField = "AreaName";
      chklocation.DataBind();  
    }

    private void setfill()
    {
        bl_restaurants.Date = common.GetDate();
        bl_restaurants.CityId = Convert.ToInt32(Request.Form["hdnSelectedCity"]);
        bl_restaurants.AreaId = Convert.ToInt32(Request.Form["hdnSelectedCityArea"]);
        bl_restaurants.RestaurantName = txtrestaurantname.Text;
        bl_restaurants.Description = txtdes.Text;
        bl_restaurants.Address = txtadd.Text;
        bl_restaurants.Phoneno = txtPhoneNo.Text;
        bl_restaurants.MobileNo1 = txtMobile1.Text;
        bl_restaurants.MobileNo2 = txtmobileno2.Text;
        bl_restaurants.ContactPerson1 = txtcontactperson1.Text;
        bl_restaurants.ContactPerson2 = txtcontactperson2.Text;
        bl_restaurants.Email = txtemail.Text;
        bl_restaurants.Password = txtpassword.Text;
        bl_restaurants.OwnerId = Convert.ToInt32(ddlowner.SelectedItem.Value);
        bl_restaurants.Active = 1;
       
    }

    private void Clear()
    {
        txtpassword.Text = "";
        txtmobileno2.Text = "";
        txtMobile1.Text = "";
        txtemail.Text = "";
        txtPhoneNo.Text = "";
        txtrestaurantname.Text = "";
        txtdes.Text = "";
        txtcontactperson2.Text = "";
        txtcontactperson1.Text = "";
        txtadd.Text = "";
        common.bindOwner(ddlowner);
        lstchkcategory.ClearSelection();
        lstchkpaymenttype.ClearSelection();
        chklocation.ClearSelection();
    }

    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }
   
}


