﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="City.aspx.cs" Inherits="WF_City" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
 <div class="account-container register">
<div class="content clearfix">
<h1 style="text-align:center">City</h1>			
<div class="login-fields">
<div class="field">
City : <br />
 <asp:TextBox ID="txtcity" runat="server" CssClass="textboxes" required="required" placeholder="City"></asp:TextBox>
</div> 

</div>
<div class="login-actions">
<asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" class="button btn btn-primary btn-large"/>
<asp:Button ID="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"  class="button btn btn-primary btn-large"/>
    <asp:LinkButton ID="btncancel" onclick="btncancel_Click" class="button btn btn-primary btn-large" runat="server">Clear</asp:LinkButton>
</div>
</div> 
</div> 
 
 


   <div class="rounded_corners" >
    
    <asp:GridView ID="grdCity" runat="server" onrowcommand="city_RowCommand" 

 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>

             
        <Columns>
            <asp:TemplateField HeaderText="CityId" Visible=false>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CityId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CityName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CityName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" Visible=false>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblActive" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("CityId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("CityId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                    <asp:LinkButton ID="lnkactive" runat="server" CommandName="Active" Text='<%#Bind("Active") %>' CommandArgument='<%#Bind("CityId") %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

