﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="Area.aspx.cs" Inherits="WF_Area" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="account-container register">
<div class="content clearfix">
<h1>Area</h1>			
<div class="login-fields">
<div class="field">
City:    <asp:DropDownList ID="ddlcity" runat="server" ValidationGroup="new">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlcity" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a City"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
</div> 

<div class="field">
Area :<asp:TextBox ID="txtareaname" runat="server" CssClass="textboxes" required="required" placeholder="Enter Your Area..." ></asp:TextBox>
</div> 

<div class="field">
Pincode :<asp:TextBox ID="txtpincode" runat="server" CssClass="textboxes" placeholder="Enter Your Pincode..."></asp:TextBox>
 <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txtpincode" runat="server" ErrorMessage="Only Numbers allowed" ValidationGroup="new" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
</div> 


</div>
<div class="login-actions">

<asp:Button ID="btnsave" runat="server" Text="Save" onclick="btnsave_Click" ValidationGroup="new" class="button btn btn-primary btn-large"/>
<asp:Button ID="btnUpdate" ValidationGroup="new" runat="server" Text="Update" 
onclick="btnUpdate_Click" class="button btn btn-primary btn-large"/>
<asp:Button ID="btnClear" runat="server" Text="Clear" 
onclick="btnClear_Click" class="button btn btn-primary btn-large"/>
</div>
</div> 
</div> 
         <div class="rounded_corners" >
        <asp:GridView ID="gdvarea" runat="server"  onrowcommand="area_RowCommand"
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />
        <alternatingrowstyle CssClass="AlternateRow"/>
            <Columns>
                <asp:TemplateField HeaderText="AreaId" Visible=false>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AreaId") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAreadId" runat="server" Text='<%# Bind("AreaId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="CityId" Visible=false>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CityId") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCityId" runat="server" Text='<%# Bind("CityId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CityName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CityName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AreaName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("AreaName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAreaName" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pincode">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Pincode") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblPincode" runat="server" Text='<%# Bind("Pincode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" Visible=false>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblActive" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("AreaId") %>'>Edit</asp:LinkButton>
                        <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("AreaId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                        <asp:LinkButton ID="lnkactive" runat="server" CommandName="Active" Text='<%#Bind("Active") %>' CommandArgument='<%#Bind("AreaId") %>'>Active</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
</asp:Content>

