﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_City : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                bindGrid();
            }
            catch
            {

            }
        }
        btnUpdate.Enabled = false;
    }


    static int id = 0;
    BL_City bl_city = new BL_City();
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bl_city.CityName = txtcity.Text;
        bl_city.Mode = "Insert";
        bl_city.Active = 1;
        bl_city.City(bl_city);
        bindGrid();
        txtcity.Text = "";
    }
    private void bindGrid()
    {
        try
        {
            bl_city.TextSearch = "";
            DataTable dt = bl_city.SelectCity(bl_city);
            grdCity.DataSource = dt;
            grdCity.DataBind();
        }
        catch
        {

        }

    }

    protected void city_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCityId = (Label)row.FindControl("lblCityId");
            Label lblCityName = (Label)row.FindControl("lblCityName");
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");
            if (e.CommandName == "EditRow")
            {
                txtcity.Text = lblCityName.Text;
                btnsave.Enabled = false;
                btnUpdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_city.CityName = "";
                bl_city.Mode = "Delete";
                bl_city.CityId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_city.City(bl_city);
                bindGrid();
            }
            if (e.CommandName == "Active")
            {
                if (lnkactive.Text == "Active")
                {
                    bl_city.CityName = "";
                    bl_city.Mode = "Active";
                    bl_city.Active = 1;
                    bl_city.CityId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_city.City(bl_city);
                    bindGrid();
                }
                else
                {
                    bl_city.CityName = "";
                    bl_city.Mode = "Active";
                    bl_city.Active = 0;
                    bl_city.CityId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_city.City(bl_city);
                    bindGrid();
                }
            }
        }
        catch
        {


        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        bl_city.CityName = txtcity.Text;
        bl_city.Mode = "Update";
        bl_city.CityId = id;
        bl_city.City(bl_city);
        bindGrid();
        txtcity.Text = "";
        btnUpdate.Enabled = false;
        btnsave.Enabled = true;
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtcity.Text = "";
    }
}