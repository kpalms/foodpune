﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="Coupen.aspx.cs" Inherits="WF_Coupen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="account-container register">
<div class="content clearfix">
<h1>Coupen</h1>		
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
	
<div class="login-fields">
    <asp:DropDownList ID="ddlDiscount" AutoPostBack="true" ValidationGroup="new" runat="server" >
    </asp:DropDownList>
     <asp:RequiredFieldValidator ControlToValidate="ddlDiscount" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Offer"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
    <br />
<div class="field">
    <asp:TextBox ID="txtCode" runat="server" CssClass="textboxes" required="required" placeholder="Coupen"></asp:TextBox>

<%--<input type="text" id="firstname" name="firstname" value=""  class="login" required/>
--%></div> 

</div>
<div class="login-actions">
<asp:Button ID="btnAdd" runat="server" ValidationGroup="new" Text="Add" onclick="btnAdd_Click" class="button btn btn-primary btn-large" />
<asp:Button ID="btnUpdate" runat="server" ValidationGroup="new" Text="Update" class="button btn btn-primary btn-large" onclick="btnUpdate_Click" />
<asp:LinkButton ID="btnclear" runat="server" Text="Clear" onclick="btnclear_Click" class="button btn btn-primary btn-large">Clear</asp:LinkButton>
<asp:LinkButton ID="btnback" runat="server" Text="Go To Back" class="button btn btn-warning btn-large" onclick="btnback_Click"></asp:LinkButton>
</div>
</div> 
</div> 


         <div class="rounded_corners" >
         <asp:GridView ID="grdCoupen" runat="server" 
        OnRowCommand="coupan_RowCommand"
        
 AutoGenerateColumns="False" Width="100%">
        <headerstyle CssClass="headers"/>
          <rowstyle  CssClass="RowCss"  />

        <alternatingrowstyle CssClass="AlternateRow"/>
        
        

             <Columns>
                 <asp:TemplateField HeaderText="CoupenId" Visible=false>
                     <EditItemTemplate>
                         <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CoupenId") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblCoupanId" runat="server" Text='<%# Bind("CoupanId") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="OfferName">
                     <EditItemTemplate>
                         <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("OfferName") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblOfferName" runat="server" Text='<%# Bind("OfferName") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Code">
                     <EditItemTemplate>
                         <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="DiscountId" Visible=false>
                     <EditItemTemplate>
                         <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("DiscountId") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDiscountId" runat="server" Text='<%# Bind("DiscountId") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 
                 <asp:TemplateField HeaderText="Action">
                  <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandName="EditRow" CommandArgument='<%#Bind("CoupanId") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="DeleteRow" CommandArgument='<%#Bind("CoupanId") %>' OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
                 </asp:TemplateField>
                 
             </Columns>
    </asp:GridView>
    </div>
</asp:Content>

