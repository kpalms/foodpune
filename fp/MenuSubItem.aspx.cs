﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class WF_MenuSubItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
           // bindddlitems();
            BindGridview();
             btnupdate.Enabled = false;
             lblitemname.Text = Request.QueryString["itemname"].ToString();
        }
       
    }
    BL_MenuItem bl_menuitem = new BL_MenuItem();
    static int id = 0;
    private void setfill()
    {

        bl_menuitem.ItemId = Convert.ToInt32(Request.QueryString["itemid"].ToString());
        bl_menuitem.Name = txtname.Text;
        bl_menuitem.Price = Convert.ToDecimal(txtprice.Text);
    }

    private void clear()
    {
        txtname.Text="";
        txtprice.Text = "";
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        setfill();
        bl_menuitem.Active = 1;
        bl_menuitem.Mode = "Insert";
        bl_menuitem.MenuSubItemId = 0;
        bl_menuitem.MenusubItem(bl_menuitem);
        clear();
        BindGridview();
        string message = "Your details have been saved successfully.";
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "')};";
        ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

    }

    //private void bindddlitems()
    //{
    //    DataTable dt = bl_menuitem.SelectItemIdItem(bl_menuitem);
    //    ddlitems.DataSource = dt;
    //    ddlitems.DataTextField = "Item";
    //    ddlitems.DataValueField = "ItemId";
    //    ddlitems.DataBind();
       
    //}

    private void BindGridview()
    {
        bl_menuitem.ItemId = Convert.ToInt32(Request.QueryString["itemid"].ToString());
        DataTable dt = bl_menuitem.SelectMenusubItem(bl_menuitem);
        dgvmenusubitem.DataSource = dt;
        dgvmenusubitem.DataBind();
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void dgvmenusubitem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblPrice = (Label)row.FindControl("lblPrice");
            Label lblName = (Label)row.FindControl("lblName");
            Label lblItem = (Label)row.FindControl("lblItem");
            Label lblItemId = (Label)row.FindControl("lblItemId");
            
            LinkButton lnkactive = (LinkButton)row.FindControl("lnkactive");



            if (e.CommandName == "EditRow")
            {
               // ddlitems.Text = lblItem.Text;
               // ddlitems.Text = lblItemId.Text;
                txtname.Text = lblName.Text;
                txtprice.Text = lblPrice.Text;
                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_menuitem.Name = "";
                bl_menuitem.Mode = "Delete";
                bl_menuitem.Active = 0;
                bl_menuitem.ItemId = 0;
                bl_menuitem.Price = 0;
                bl_menuitem.MenuSubItemId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_menuitem.MenusubItem(bl_menuitem);
                BindGridview();
            }
            if (e.CommandName == "Active")
            {
                if (lnkactive.Text == "Active")
                {
                    bl_menuitem.Name = "";
                    bl_menuitem.Mode = "Active";
                    bl_menuitem.Active = 1;
                    bl_menuitem.ItemId = 0;
                    bl_menuitem.Price = 0;
                    bl_menuitem.MenuSubItemId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menuitem.MenusubItem(bl_menuitem);
                    BindGridview();
                }
                else
                {
                    bl_menuitem.Name = "";
                    bl_menuitem.Mode = "Active";
                    bl_menuitem.Active = 0;
                    bl_menuitem.ItemId = 0;
                    bl_menuitem.Price = 0;
                    bl_menuitem.MenuSubItemId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_menuitem.MenusubItem(bl_menuitem);
                    BindGridview();
                }
            }
        }
        catch
        {


        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_menuitem.MenuSubItemId = id;
            bl_menuitem.Mode = "Update";
            bl_menuitem.MenusubItem(bl_menuitem);
            BindGridview();
            clear();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            string message = "Your details have been Updated successfully.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }
        catch
        {


        }
    }


    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}