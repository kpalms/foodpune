﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Configuration;

public partial class WF_ViewOwner : System.Web.UI.Page
{

    BL_Owner bl_owner = new BL_Owner();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindOwner();
        //if (!IsPostBack)
        //{
        //    grdOwner.DataSource = GetCustomersData(1);
        //    grdOwner.DataBind();
        //}

    }

    public void BindOwner()
    {
        DataTable dt = bl_owner.BindOwner();
        grdOwner.DataSource = dt;
        grdOwner.DataBind();
    }

   //[WebMethod]
   // public static string GetCustomers(int pageIndex)
   // {
   //     return GetCustomersData(pageIndex).GetXml();
   // }

   //public static DataSet GetCustomersData(int pageIndex)
   //{
   //    BL_Owner bl_owner = new BL_Owner();
   //    DataSet ds = bl_owner.BindOwner(bl_owner, pageIndex);
   //    return ds;
   //}


}