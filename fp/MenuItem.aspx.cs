﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
public partial class WF_MenuItem : System.Web.UI.Page
{
    Common common = new Common();
    static string imagename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                CategoryBindGrid();
                ItemBindGrid();
                lblrestoname.Text = Request.QueryString["rn"].ToString();
            }
            catch
            {

            }
        }
        btnupdate.Enabled = false;
        
    }

    BL_MenuItem bl_MenuItem = new BL_MenuItem();
    BL_MenuCategory bl_MenuCategory = new BL_MenuCategory();

    static int id = 0;
         
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            SetFill();
            bl_MenuItem.Mode = "Insert";
            if (FileUploaderimage.HasFile)
            {


                string ext = Path.GetExtension(FileUploaderimage.FileName);
                string maxid = common.ReturnOneValue("select max(ItemId) from MenuItem");
                string filename = maxid + "" + ext;
                FileUploaderimage.PostedFile.SaveAs(Server.MapPath("images/MenuItem/") + filename);
                bl_MenuItem.Image = filename;

            }
            else
            {
                bl_MenuItem.Image = "NoImage.jpg";
            }
            bl_MenuItem.MenuItem(bl_MenuItem);
            Clear();
            ItemBindGrid();
        }
        catch
        {


        }
    }

    private void SetFill()
    {
        bl_MenuItem.CategoryId = Convert.ToInt32(ddlcategory.SelectedValue);
        bl_MenuItem.Item = txtitem.Text;
        bl_MenuItem.ItemDescription = txtitemdescripation.Text;
        bl_MenuItem.Price = Convert.ToDecimal( txtprice.Text);

        bl_MenuItem.Active = 1;
        if (rbttype.SelectedItem.Value == "Veg")
        {
            bl_MenuItem.Type = 0;
        }
        else 
        {
            bl_MenuItem.Type = 1;
        }

         if (rbtmenusubitem.SelectedItem.Value == "Yes")
        {
            bl_MenuItem.Customisation = 1;
        }
        else
        {
            bl_MenuItem.Customisation = 0;
        }
    }

    private void Clear()
    {
        txtitem.Text = "";
        txtprice.Text = "";
        txtitemdescripation.Text = "";
        rbttype.ClearSelection();
        rbtmenusubitem.ClearSelection();
       // ddlcategory.Text = "";
       // rbtcustmization.Checked = false;
       // rbtnonveg.Checked = false;
        //rbtveg.Checked = false;
       // rbtcustmization1.Checked = false;
    }


    private void CategoryBindGrid()
    {
        try
        {
            bl_MenuCategory.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_MenuCategory.SelectMenuCategory(bl_MenuCategory);
            ddlcategory.DataSource = dt;
            ddlcategory.DataValueField = "CategoryId";
            ddlcategory.DataTextField = "Category";
            ddlcategory.DataBind();
           // ddlcategory.Items.Insert(0, "-- Select Category --");
            ddlcategory.Items.Insert(0, new ListItem("-- Select Category --", "0"));
            ddlcategory.SelectedIndex = 0;
        }
        catch
        {

        }

    }


    private void ItemBindGrid()
    {
        try
        {
            bl_MenuItem.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataTable dt = bl_MenuItem.SelectMenuItem(bl_MenuItem);
            gdvmenuitem.DataSource = dt;
            gdvmenuitem.DataBind();
        }
        catch
        {

        }

    }



    protected void menuitem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblCategoryId = (Label)row.FindControl("lblCategoryId");
            Label lblItem = (Label)row.FindControl("lblItem");
            Label lblPrice = (Label)row.FindControl("lblPrice");
            Label lblItemDescription = (Label)row.FindControl("lblItemDescription");
            Label lblImage = (Label)row.FindControl("lblImage");
            Label lblType = (Label)row.FindControl("lblType");
            Label lblCustomisation = (Label)row.FindControl("lblCustomisation");
            LinkButton lnkActive = (LinkButton)row.FindControl("lnkActive");
            if (e.CommandName == "EditRow")
            {
                txtitem.Text = lblItem.Text;
                ddlcategory.Text = lblCategoryId.Text;
                txtprice.Text = lblPrice.Text;
                txtitemdescripation.Text = lblItemDescription.Text;
                if ("Yes" == lblCustomisation.Text)
                {
                   // rbtcustmization.Checked = true;
                    rbtmenusubitem.SelectedValue = "Yes";
                }
                else
                {
                    rbtmenusubitem.SelectedValue = "No";
                    //rbtcustmization1.Checked = true;
                }


                if ("Veg" == lblType.Text)
                {
                    rbttype.SelectedValue = "Veg";
                    //rbtveg.Checked = true;
                }
                else
                {
                    rbttype.SelectedValue = "NonVeg";
                   // rbtnonveg.Checked = true;
                }

                btnsave.Enabled = false;
                btnupdate.Enabled = true;
                imgedit.Visible = true;
                string sub = lblImage.Text.Substring(16);
                imagename = sub;
                imgedit.ImageUrl =lblImage.Text;
                id = Convert.ToInt32(e.CommandArgument.ToString());
            }
            if (e.CommandName == "DeleteRow")
            {
                bl_MenuItem.ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                bl_MenuItem.Item = "";
                bl_MenuItem.CategoryId = 0;
                bl_MenuItem.Price = 0;
                bl_MenuItem.Image = "";
                bl_MenuItem.Customisation = 0;
                bl_MenuItem.ItemDescription = "";
                bl_MenuItem.Active = 0;
                bl_MenuItem.Mode = "Delete";
                bl_MenuItem.MenuItem(bl_MenuItem);
                ItemBindGrid();
                string sub = lblImage.Text.Substring(16);
                imagename = sub;
                if ("NoImage.jpg" != imagename)
                {
                    string imageFilePath = Server.MapPath(lblImage.Text);
                    File.Delete(imageFilePath);
                }
                Clear();
                imgedit.Visible = false;
            }

            if (e.CommandName == "Active")
            {
                if (lnkActive.Text == "Active")
                {
                    bl_MenuItem.ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_MenuItem.Item = "";
                    bl_MenuItem.CategoryId = 0;
                    bl_MenuItem.Price = 0;
                    bl_MenuItem.Image = "";
                    bl_MenuItem.Active = 1;
                    bl_MenuItem.Mode = "Active";
                    bl_MenuItem.MenuItem(bl_MenuItem);
                    ItemBindGrid();
                }
                else
                {
                    bl_MenuItem.ItemId = Convert.ToInt32(e.CommandArgument.ToString());
                    bl_MenuItem.Item = "";
                    bl_MenuItem.CategoryId = 0;
                    bl_MenuItem.Price = 0;
                    bl_MenuItem.Image = "";
                    bl_MenuItem.Active = 0;
                    bl_MenuItem.Mode = "Active";
                    bl_MenuItem.MenuItem(bl_MenuItem);
                    ItemBindGrid();
                }
            }
        }
        catch
        {
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string filename;
            SetFill();
            bl_MenuItem.Mode = "Update";
            bl_MenuItem.ItemId = id;
            if (FileUploaderimage.HasFile)
            {
                string ext = Path.GetExtension(FileUploaderimage.FileName);

                if ("NoImage.jpg" == imagename)
                {
                    string maxid = common.ReturnOneValue("select max(ItemId) from MenuItem");
                     filename = maxid + "" + ext;
                }
                else
                {
                    string fn = Path.GetFileNameWithoutExtension(imagename);
                    filename = fn + "" + ext;
                    string imagefilepath = Server.MapPath("images/MenuItem/" + imagename);
                    File.Delete(imagefilepath);
                }
                FileUploaderimage.PostedFile.SaveAs(Server.MapPath("images/MenuItem/") + filename);
                bl_MenuItem.Image = filename;
            }
            else
            {
                bl_MenuItem.Image = imagename;
            }
            bl_MenuItem.MenuItem(bl_MenuItem);
            Clear();
            ItemBindGrid();
            btnupdate.Enabled = false;
            btnsave.Enabled = true;
            imgedit.Visible = false;

        }
        catch
        {

        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}