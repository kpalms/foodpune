﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class fp_OrdersDetails : System.Web.UI.Page
{
    string customer = "";
    BL_Order bl_order = new BL_Order();
    protected void Page_Load(object sender, EventArgs e)
    {
        GetOrderDetails();
    }

    private void GetOrderDetails()
    {
        decimal price = 0;
        string discount = "";
        bl_order.OrderId = Convert.ToInt32(Request.QueryString["OrderId"].ToString());
        DataSet ds = bl_order.GetOrderDetailsByOrderId(bl_order);
        rptOrders.DataSource = ds.Tables[0];
        rptOrders.DataBind();
       
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            price = price + Convert.ToDecimal(Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString()) * Convert.ToDecimal(ds.Tables[0].Rows[i]["Qty"].ToString()));
        }


        if (ds.Tables[1].Rows.Count > 0)
        {
            if ("0" == ds.Tables[1].Rows[0]["DiscountType"].ToString())
            {
                discount = Convert.ToString(Convert.ToDecimal(ds.Tables[1].Rows[0]["DiscountAmount"]) * price / 100);
                lbldiscount.Text = "Discount " + ds.Tables[1].Rows[0]["DiscountAmount"] + "%" + " In : Rs." + discount;

            }
            else
            {
                discount = ds.Tables[1].Rows[0]["DiscountAmount"].ToString();
                lbldiscount.Text = "Discount :  Rs." + discount + ".00";
            }
        }
        lblsubtotal.Text = " Rs. " + Convert.ToString(price);
        decimal deliverycharges = Convert.ToDecimal(ds.Tables[0].Rows[0]["DeliveryCharges"]);
        lbldeliverycharges.Text = " Rs." + deliverycharges;
        if (discount == "")
        {
            lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges);
        }
        else
        {
            lblfinaltotal.Text = "Rs." + Convert.ToString(price + deliverycharges - Convert.ToDecimal(discount));
        }

        lblcustname.Text = ds.Tables[0].Rows[0]["Name"].ToString();
        // lbladdressdetails.Text = ds.Tables[2].Rows[0]["Addres"].ToString();
        lblDeliveryStatus.Text = ds.Tables[2].Rows[0]["DeliveryType"].ToString();
        lbldeliverydatetime.Text = ds.Tables[2].Rows[0]["DeliveryTime"].ToString();
    }



    protected void lnkback_Click(object sender, EventArgs e)
    {
        if ("Disaccept" == Request.QueryString["orderstatus"].ToString())
        {
            Response.Redirect("ResturantOrderDisaccept.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + Request.QueryString["FromDate"].ToString() + "&ToDate=" + Request.QueryString["ToDate"].ToString());
        }

        if ("PlaceOrder" == Request.QueryString["orderstatus"].ToString())
        {
            Response.Redirect("ResturantOrderPlaceOrder.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + Request.QueryString["FromDate"].ToString() + "&ToDate=" + Request.QueryString["ToDate"].ToString());
        }

        if ("Accept" == Request.QueryString["orderstatus"].ToString())
        {
            Response.Redirect("RestaurantOrderDetails.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + Request.QueryString["FromDate"].ToString() + "&ToDate=" + Request.QueryString["ToDate"].ToString());
        }

        if ("Cancel" == Request.QueryString["orderstatus"].ToString())
        {
            Response.Redirect("ResturantOrderCancel.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + Request.QueryString["FromDate"].ToString() + "&ToDate=" + Request.QueryString["ToDate"].ToString());
        }

        if ("Later" == Request.QueryString["orderstatus"].ToString())
        {
            Response.Redirect("ResturantOrderLater.aspx?key=" + Request.QueryString["Key"].ToString() + "&FromDate=" + Request.QueryString["FromDate"].ToString() + "&ToDate=" + Request.QueryString["ToDate"].ToString());
        }

        
    }


    }

  
   