﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="MenuSubItem.aspx.cs" Inherits="WF_MenuSubItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 Item Name :   <asp:Label ID="lblitemname" runat="server" Text="Label"></asp:Label>
    <br />
Sub Item Name :    <asp:TextBox ID="txtname" runat="server" required="required"></asp:TextBox><br />
  Price :  <asp:TextBox ID="txtprice" runat="server" required="required"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="((\d+)((\.\d{1,2})?))$"
ErrorMessage="Please enter valid integer or decimal number with 2 decimal places."
ControlToValidate="txtprice" ForeColor="Red" ValidationGroup="new1"/>
  <br />
    <asp:Button ID="btnsave" runat="server" ValidationGroup="new1" Text="Save" OnClick="btnsave_Click" />
    <asp:Button ID="btnupdate" runat="server" ValidationGroup="new1" Text="Update" OnClick="btnupdate_Click" />
  
    <asp:LinkButton ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click"></asp:LinkButton>
    <asp:LinkButton ID="btnback" runat="server" Text="Go To Back" class="button btn btn-warning btn-large" onclick="btnback_Click"></asp:LinkButton>
    <br />
    <asp:GridView ID="dgvmenusubitem" runat="server" AutoGenerateColumns="False" OnRowCommand="dgvmenusubitem_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="MenuSubItemId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("MenuSubItemId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("MenuSubItemId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ItemId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("ItemId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblItemId" runat="server" Text='<%# Bind("ItemId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Item") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblItem" runat="server" Text='<%# Bind("Item") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Price">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPrice" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkactive" runat="server" CommandName="Active" CommandArgument='<%# Bind("MenuSubItemId") %>' Text='<%# Bind("Active") %>'></asp:LinkButton>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Bind("MenuSubItemId") %>' CommandName="EditRow">Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%# Bind("MenuSubItemId") %>' CommandName="DeleteRow" OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>

