﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="RestaurantDetails.aspx.cs" Inherits="Menu"  %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="../css/pages/Menu.css" />



<script type="text/javascript">
    var pageIndex = 1;
    var pageCount;
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            GetRecords();
        }
    });
    function GetRecords() {
        pageIndex++;
        if (pageIndex == 2 || pageIndex <= pageCount) {
            $("#loader").show();
            $.ajax({
                type: "POST",
                url: "RestaurantDetails.aspx/GetReview",
                data: '{pageIndex: ' + pageIndex + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }
    }
    function OnSuccess(response) {
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
        var reviews = xml.find("dsname");
        reviews.each(function () {
            var customer = $(this);
            var table = $("#dvReviews table").eq(0).clone(true);
            $(".name", table).html(customer.find("Name").text());
            $(".date", table).html(customer.find("Date").text());
            $(".comments", table).html(customer.find("Comments").text());
            $(".reviewId", table).html(customer.find("ReviewId").text());
            $("#dvReviews").append(table).append("<br />");
        });
        $("#loader").hide();
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  


<div  class="widget">
<div class="widget-content">

<p>
<h1><asp:Image ID="Restoimage" runat="server" Width="100px" Height="100px"/><asp:Label ID="lblrestonamehead" runat="server" Text="Label"></asp:Label></h1>

<asp:Repeater ID="rptrestocathead" runat="server">
<ItemTemplate>
<asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>'></asp:Label>,
</ItemTemplate>
</asp:Repeater>
</p>	
</div> 
</div> 

 <asp:LinkButton ID="lnkmenucategory" runat="server" 
        onclick="lnkmenucategory_Click">Menu Category</asp:LinkButton>
  <asp:LinkButton ID="lnkmenuitem" runat="server" onclick="lnkmenuitem_Click">Menu Item</asp:LinkButton>

  <asp:LinkButton ID="lnkeditresto" runat="server" onclick="lnkeditresto_Click">Edit Resturant</asp:LinkButton>


                <div class="info-box">
                <div class="row-fluid stats-box">

  

						
<div class="tabbable">
<ul class="nav nav-tabs">
<li class="active">
<a href="#Menu" data-toggle="tab">Menu</a>
</li>
<li>
<a href="#Review" data-toggle="tab">Review</a>
</li>
<li>
<a href="#Info" data-toggle="tab">Info</a>
</li>
</ul>
						

<%--tabone	--%>					
<div class="tab-content">
<div class="tab-pane active" id="Menu">
<fieldset>

    <span class="span2" >
    <asp:Repeater ID="rptcategory" runat="server">
    <HeaderTemplate><ul  class="menucategory" ></HeaderTemplate>
        
        <ItemTemplate>
          <li>
        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
          </li>
        </ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
    </span>




<span class="span5" >
<asp:Repeater ID="rptitem" runat="server" onitemdatabound="rptitem_ItemDataBound" onitemcommand="rptitem_ItemCommand">
<ItemTemplate > 
<div id="dvCategory" runat="server" class="menuItemCatrow ">
    <div class="menuItemCatHead">
        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
    </div>
    <div>
        <img src='images/MenuCategory/<%#Eval("ImageCat") %>' alt='<%#Eval("ImageCat") %>' >
    </div>
</div>
<div>
</div>


<div class="simpleCart_shelfItem">
<div class="menuItemName">
<asp:Label ID="lblitemid" runat="server" Text='<%#Eval("ItemId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblItem" CssClass="item_name" runat="server" Text='<%#Eval("Item") %>'></asp:Label>
<asp:Label ID="lblCustomisation" runat="server" Text='<%#Eval("Customisation") %>' Visible="false"></asp:Label>
<asp:Label ID="lblitemitemdescripation" CssClass="item_name" runat="server" Text='<%#Eval("ItemDescription") %>'></asp:Label>
</div>
<div class="menuItemPrice">
<asp:Label ID="lblPrice" CssClass="item_price" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
</div>
</div>
 <span class="span6" >
    <asp:Repeater ID="rptmenusubitem" runat="server" Visible="false">
    <ItemTemplate>
        <asp:Label ID="lblMenuSubItemId" runat="server" Text='<%#Eval("MenuSubItemId") %>' Visible="false"></asp:Label>
        <span class="span3 menuItemName" ><asp:Label ID="lblName"  Font-Size="Small" CssClass="item_name" runat="server" Text='<%#Eval("Name") %>'></asp:Label></span>
        <div class="span1 menuItemPrice" style="width:9%"><asp:Label ID="lblPrice" Font-Size="Small" CssClass="item_name" runat="server" Text='<%#Eval("Price") %>'></asp:Label></div>  
    </ItemTemplate>
    </asp:Repeater>
    </span>
 <asp:LinkButton ID="lnkaddmenusubitem" runat="server" Visible="false">MenuSubItem</asp:LinkButton>
</ItemTemplate>
</asp:Repeater>
</span>
 </fieldset>
</div>


<%--tabtwo--%>					
<div class="tab-pane " id="Review">
<div class="span10 review" >
<fieldset>

     <div class="widget">
            <div class="widget-header"> <i class="icon-file"></i>
              <h3> Review</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            
              <ul class="messages_layout">
              <table>
              <tr><td>
              <div id="dvReviews">
                <asp:Repeater ID="rptreview" runat="server">
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar"><img src="../img/user.png"/></a>
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name reviewId"><asp:Label ID="lblreviewid"  runat="server" Text='<%#Eval("ReviewId") %>' Visible="false"></asp:Label></a>
<a class="name"><asp:Label ID="lblUserId" runat="server" Text='<%#Eval("Name") %>'></asp:Label></a> 
<span class="time date"> <asp:Label ID="lblDate" runat="server" Text='<%#Eval("Date") %>' Font-Size="Small"></asp:Label></span>
                    </div>

<div class="text comments"> <asp:Label ID="lblComments" runat="server" Text='<%#Eval("Comments") %>'></asp:Label></div>
                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               
                </td>
<td valign="bottom">
    <img id="loader" alt="" src="../loading.gif" style="display: none" />
</td>
</tr>
</table>
              </ul>
             
            </div>
            <!-- /widget-content --> 
          </div>

</fieldset>


</div>
</div>
<%--endtab2--%>	


<%--tab3--%>					
<div class="tab-pane" id="Info">
<div class="span10" >
<fieldset>




        <div class="widget">
            <div class="widget-header2"> <i class="icon-file"></i>
                <h3> 
                <asp:ListView ID="lstresto" runat="server">
                <ItemTemplate>
                <asp:Label ID="lblrestoname" runat="server" Text='<%#Eval("RestaurantName") %>'></asp:Label><br />
                <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label><br />
                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label><br />
                <asp:Label ID="lblPhoneNo" runat="server" Text='<%#Eval("PhoneNo") %>' Visible="false"></asp:Label><br />
                <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label><br />
                </ItemTemplate>
                </asp:ListView>
                </h3>
            </div>

            <div class="widget-content">
              <ul class="messages_layout">

                        
                        <li class="from_user left"> <a href="#" class="avatar"><img src="../img/cuisine.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info"> <a class="name">Cuisines</a>
                        </div>
                        <div class="text">
                        <asp:Repeater ID="rptrestocat" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>'>'></asp:Label>,
                        </ItemTemplate>
                        </asp:Repeater>
                            <asp:LinkButton ID="lnkresto" runat="server" OnClick="lnkresto_Click">Edit</asp:LinkButton>
                        </div>
                        </div>
                        </li>




                        <li class="from_user left  span10"> <a href="#" class="avatar"><img src="../img/condition.png"/></a>
                        <div class="message_wrap span8"> <span class="arrow"></span>
                        <div class="info"> <a class="name">Celeste Holm </a> <span class="time">1 Day ago</span>
                        </div>
                        <div class="text">
                            <asp:Label ID="lblminimumorder" runat="server"></asp:Label><br />
                             <asp:Label ID="lbldeliverycharges" runat="server"></asp:Label><br />
                              <asp:Label ID="lblpickuptime" runat="server"></asp:Label><br />
                               <asp:Label ID="lbldeliverytime" runat="server"></asp:Label>
                        <asp:LinkButton ID="lnkrestocondition" runat="server" OnClick="lnkrestocondition_Click">Edit</asp:LinkButton>

                        </div>
                        </div>
                        </li>

  
                        <li class="by_myself right"> <a href="#" class="avatar"><img src="../img/payment.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info"> 
                        <a class="name"> Payment Type </a> 
                        </div>
                        <div class="text">
                        <asp:Repeater ID="rptpaymenttype" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblPaymentTypeId" runat="server" Visible="false" Text='<%#Eval("PaymentTypeId") %>'>'></asp:Label>
                        <asp:Label ID="lblPaymentType" runat="server" Text='<%#Eval("PaymentType") %>'>'></asp:Label>,
                        </ItemTemplate>
                        </asp:Repeater>
                         <asp:LinkButton ID="lnkpaymenttype" runat="server" OnClick="lnkpaymenttype_Click">Edit</asp:LinkButton>

                        </div>
                        </div>
                        </li>

  

                        <li class="from_user left"><a href="#" class="avatar"><img src="../img/location.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info">
                        <a class="name">Delivery locations</a>
                        </div>
                        <div class="text">
                        <asp:ListView ID="lstdeliverylocation" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblAreaName" runat="server" Text='<%#Eval("AreaName") %>'></asp:Label>,
                        </ItemTemplate>
                        </asp:ListView>
                        <asp:LinkButton ID="lnkdeliverylocation" runat="server" OnClick="lnkdeliverylocation_Click">Edit</asp:LinkButton>

                        </div>
                        </div>
                        </li>

                          <li class="from_user left"> <a href="#" class="avatar"><img src="../img/discountoffer.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info"> <a class="name">Discount Offer</a>
                        </div>
                        <div class="text">
                        Discount Offers
                      <%--  <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>'>'></asp:Label>,
                        </ItemTemplate>
                        </asp:Repeater>--%>
                            <asp:LinkButton ID="lnkdiscountoffer" runat="server" OnClick="lnkdiscountoffer_Click">Edit</asp:LinkButton>
                        </div>
                        </div>
                        </li>



                        
                        <li class="from_user left  span10"> <a href="#" class="avatar"><img src="../img/coupencode.png"/></a>
                        <div class="message_wrap span8"> <span class="arrow"></span>
                        <div class="info"> <a class="name">Coupens </a> <span class="time"></span>
                        </div>
                        <div class="text">
                        Generate Coupens
                            <%--<asp:Label ID="Label2" runat="server"></asp:Label><br />
                             <asp:Label ID="Label3" runat="server"></asp:Label><br />
                              <asp:Label ID="Label4" runat="server"></asp:Label><br />
                               <asp:Label ID="Label5" runat="server"></asp:Label>--%>
                        <asp:LinkButton ID="lnkcoupens" runat="server" OnClick="lnkcoupens_Click">Edit</asp:LinkButton>

                        </div>
                        </div>
                        </li>

              </ul>
            </div>
          </div>

</fieldset>
</div>
</div>
<%--endtab3--%>	

							
</div>
</div>		  
	
   














	                  </div>
                  </div>
						

</div>
</div>
</div>
</div>
</div>


<script>

    var countries = "Manoj";


    $('#txtCity').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyCity',
        loading_html: 'Loading...',
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
        }
    });

    $('#txtArea').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyArea',
        loading_html: 'Loading...',
        post_data: {
            cityid: function () { return document.getElementById('hdnSelectedCity').value; }
        },
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
        }
    });


</script>


</asp:Content>

