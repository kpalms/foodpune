﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WF_AdminMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["LoginId"] == null)
        {
            Response.Redirect("Default.aspx");

        }
        else
        {
            lblusername.Text = Session["AdminUserName"].ToString();
        }
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
      
        Session.Abandon();
        Response.Redirect("Default.aspx?");
    }
}
