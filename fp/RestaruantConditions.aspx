﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="RestaruantConditions.aspx.cs" Inherits="WF_RestaruantConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			

    <br />
Minimum Order :<asp:TextBox ID="txtminimumorder" runat="server" required="required"></asp:TextBox>
 <asp:RegularExpressionValidator ID="Regex1" runat="server" ValidationExpression="((\d+)((\.\d{1,2})?))$"
ErrorMessage="Please enter valid integer or decimal number with 2 decimal places."
ControlToValidate="txtminimumorder" ForeColor="Red" ValidationGroup="new1"/>
<br />
Delivery Charges :<asp:TextBox ID="txtdeliverycharges" runat="server" required="required"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="((\d+)((\.\d{1,2})?))$"
ErrorMessage="Please enter valid integer or decimal number with 2 decimal places."
ControlToValidate="txtdeliverycharges" ForeColor="Red" ValidationGroup="new1"/>
<br />
PickUp Time :<asp:TextBox ID="txtpickuptime" runat="server" required="required"></asp:TextBox>
<asp:RegularExpressionValidator id="RegularExpressionValidator2" ControlToValidate="txtpickuptime"  ValidationGroup="new1" ValidationExpression="\d+" Display="Static" EnableClientScript="true" ForeColor="Red" ErrorMessage="Please enter numbers only" runat="server"/>
<br />
Delivery Time :<asp:TextBox ID="txtdeliverytime" runat="server" required="required"></asp:TextBox>
<asp:RegularExpressionValidator id="RegularExpressionValidator3" ControlToValidate="txtdeliverytime" ValidationGroup="new1" ValidationExpression="\d+" Display="Static" EnableClientScript="true" ForeColor="Red" ErrorMessage="Please enter numbers only" runat="server"/>

<br />
    <asp:Button ID="btnsave" runat="server" Text="Save" ValidationGroup="new1" onclick="btnsave_Click" />
        <asp:Button ID="btnupdate" runat="server" Text="Update" 
     ValidationGroup="new1"   onclick="btnupdate_Click" />
    <asp:LinkButton ID="btnclear" runat="server"  Text="Clear" onclick="btnclear_Click"></asp:LinkButton>
    <asp:LinkButton ID="btnback" runat="server" Text="Go To Back" class="button btn btn-warning btn-large" onclick="btnback_Click"></asp:LinkButton>
    <br />

    <asp:GridView ID="gvrestoconditions" runat="server" AutoGenerateColumns="False" 
        onrowcommand="gvrestoconditions_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="RestoConditionId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" 
                        Text='<%# Bind("RestoConditionId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestoConditionId" runat="server" Text='<%# Bind("RestoConditionId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RestaurantId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantId" runat="server" Text='<%# Bind("RestaurantId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
          <%--  <asp:TemplateField HeaderText="RestaurantName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblRestaurantName" runat="server" Text='<%# Bind("RestaurantName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="MinimumOrders">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MinimumOrders") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMinimumOrders" runat="server" Text='<%# Bind("MinimumOrders") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DeliveryCharges">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("DeliveryCharges") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDeliveryCharges" runat="server" Text='<%# Bind("DeliveryCharges") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PickupTime">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PickupTime") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPickupTime" runat="server" Text='<%# Bind("PickupTime") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DeliveryTime">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DeliveryTime") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDeliveryTime" runat="server" Text='<%# Bind("DeliveryTime") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Bind("RestoConditionId") %>'  CommandName="EditRow">Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%# Bind("RestoConditionId") %>' CommandName="DeleteRow" OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

