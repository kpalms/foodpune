﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class WF_SelectRestoCategory : System.Web.UI.Page
{
    BL_Restaurants bl_restaurants = new BL_Restaurants();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRestoCatDDL();
            BindRestoCategoryByRestoId();
        }
    }


   
    private void BindRestoCatDDL()
    {
        DataTable dt = bl_restaurants.BindRestoCatDDL(bl_restaurants);
        ddlRestaurantCategory.DataSource = dt;
        ddlRestaurantCategory.DataValueField = "RestoCategoryId";
        ddlRestaurantCategory.DataTextField = "RestoCategory";
        ddlRestaurantCategory.DataBind();
        //ddlRestaurantCategory.Items.Insert(0, "-- Select RestoCategory --");
        ddlRestaurantCategory.Items.Insert(0, new ListItem("-- Select RestoCategory --", "0"));

        ddlRestaurantCategory.SelectedIndex = 0;
    }

    private void BindRestoCategoryByRestoId()
    {
        lblrestoname.Text = Request.QueryString["rn"].ToString();
        bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
        DataSet ds = bl_restaurants.BindRestoCategoryByRestoId(bl_restaurants);
        gdvselectrestocategory.DataSource = ds.Tables[0];
        gdvselectrestocategory.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (ddlRestaurantCategory.SelectedItem.Text == "-- Select RestoCategory --")
        {
            lblmsg.Text = "please select resturant category";
        }
        else
        {
            int bindcatid = 0;
            int selectedcatid = Convert.ToInt32(ddlRestaurantCategory.SelectedValue);
            bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
            DataSet ds = bl_restaurants.BindRestoCategoryByRestoId(bl_restaurants);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                     bindcatid = Convert.ToInt32(ds.Tables[0].Rows[i]["RestoCategoryId"]);
                    if (selectedcatid == bindcatid)
                    {
                        break;
                    }
                }
            }
            if (selectedcatid == bindcatid)
            {
                lblmsg.Text = "This RestoCategory is already Exiting";
            }
            else
            {

                SetFill();
                bl_restaurants.Mode = "Insert";
                bl_restaurants.SelectRestoCatId = 0;
                bl_restaurants.InsertRestaurantCategoryEntry(bl_restaurants);
                BindRestoCategoryByRestoId();
                lblmsg.Visible = false;
            }
        }

    }

    private void SetFill()
    {
     bl_restaurants.RestoCategoryId= Convert.ToInt32(ddlRestaurantCategory.SelectedValue);
     bl_restaurants.RestaurantId = Convert.ToInt32(Request.QueryString["key"].ToString());
    }
    protected void gdvselectrestocategory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        if (e.CommandName == "DeleteRow")
        {
            bl_restaurants.Mode = "Delete";
            bl_restaurants.RestoCategoryId = 0;
            bl_restaurants.RestaurantId = 0;
            bl_restaurants.SelectRestoCatId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_restaurants.InsertRestaurantCategoryEntry(bl_restaurants);
            BindRestoCategoryByRestoId();
            lblmsg.Visible = false;
        }
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestaurantDetails.aspx?key=" + Request.QueryString["key"]);
    }
}