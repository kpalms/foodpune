﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="EditRestaurant.aspx.cs" Inherits="fp_EditRestaurant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="../Search/jquerysctipttop.css" rel="stylesheet" type="text/css">

<script src="../Search/jquery-1.11.3.min.js"></script>

<script src="../Search/mv-autocomplete.js" type="text/javascript"></script>

<link href="../Search/mv-autocomplete.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="account-container register">
<div class="content clearfix">	
<h1><asp:Label ID="lblrestoname" runat="server" Text="Label"></asp:Label></h1>			
    <asp:Image ID="imgresto" runat="server" Height="100px" Width="100px"/>
<div class="login-fields">
<div class="span5" >
<div class="field">
Select City :<br />
 <asp:DropDownList ID="ddlcity" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlcity_SelectedIndexChanged">
    </asp:DropDownList>

     <asp:RequiredFieldValidator ControlToValidate="ddlcity" ID="RequiredFieldValidator1"
ValidationGroup="new1" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a City"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
</div> 


<div class="field">
Restaurant Name :<br />
 <asp:TextBox  placeholder="Restaurant Name" ID="txtrestaurantname" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Address :<br />
<asp:TextBox ID="txtadd" placeholder="Restaurant Name" runat="server" TextMode="MultiLine" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Mobile No1 :<br />
 <asp:TextBox placeholder="Mobile No 1" ID="txtmobileno1" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
ErrorMessage="Please enter mobile no" ForeColor="Red" ControlToValidate="txtmobileno1" 
ValidationGroup="new1" ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtmobileno1" ValidationGroup="new1"></asp:RegularExpressionValidator>
  </div> 

<div class="field">
Contact Person1 : <br />
 <asp:TextBox placeholder="Contact Person1" ID="txtcontactperson1" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
</div> 

<div class="field">
Email : <br />
 <asp:TextBox ID="txtemail" placeholder="Email" runat="server" CssClass="textboxes" required="required"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
ErrorMessage="enter email address" ControlToValidate="txtemail" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" 
runat="server" ErrorMessage="Envalid email" ControlToValidate="txtemail" 
ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
 
                    
</div> 
</div>


<div class="span5" >

<div class="field">
Select Area :<br />
    <asp:DropDownList ID="ddlarea" runat="server">
    </asp:DropDownList>
    <asp:RequiredFieldValidator ControlToValidate="ddlarea" ID="RequiredFieldValidator3"
ValidationGroup="new1" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Area"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>

</div> 


<div class="field">
Description :<br />
<asp:TextBox ID="txtdes"  placeholder="Description" runat="server" TextMode="MultiLine" CssClass="textboxes" required="required"></asp:TextBox>
</div> 


<div class="field">
Phone No : <br />
 <asp:TextBox placeholder="Phone No" ID="txtphoneno" runat="server" CssClass="textboxes"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="please enter correct phone number"   ValidationExpression="\d{5}([- ]*)\d{6}" ControlToValidate="txtphoneno" ValidationGroup="new1" ForeColor="Red"></asp:RegularExpressionValidator>
</div> 

<div class="field">
Mobile No2 :<br />
 <asp:TextBox placeholder="Mobile No 2" ID="txtmobileno2" runat="server" CssClass="textboxes"></asp:TextBox>

<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtmobileno2" ValidationGroup="new1"></asp:RegularExpressionValidator>
  </div> 

<div class="field">
Contact Person2 : <br />
 <asp:TextBox  placeholder="Contact Person2"  ID="txtcontactperson2" runat="server" CssClass="textboxes"></asp:TextBox>
</div> 


<div class="field">
Image :<br />
 <asp:FileUpload ID="imageuploder" runat="server" />
</div>

</div>
 
</div>
</div> 
</div> 
 <asp:Button ID="btnupdate" runat="server"  ValidationGroup="new1" Text="Update"  OnClick="btnupdate_Click"/>
   <asp:Button ID="btnback" runat="server" Text="Go To Back" class="button btn btn-warning btn-large" onclick="btnback_Click"/>
</asp:Content>

