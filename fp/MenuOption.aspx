﻿<%@ Page Title="" Language="C#" MasterPageFile="~/fp/AdminMasterPage.master" AutoEventWireup="true" CodeFile="MenuOption.aspx.cs" Inherits="WF_MenuOption" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <asp:DropDownList ID="ddlsubname" runat="server"></asp:DropDownList><br />
    <asp:TextBox ID="txtname" runat="server"></asp:TextBox><br />
    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" />
    <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnupdate_Click" />
    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" />
    <br />
    <asp:GridView ID="dgvmenuoption" runat="server" AutoGenerateColumns="False" OnRowCommand="dgvmenuoption_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="MenuExtraId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("MenuOptionId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMenuOptionId" runat="server" Text='<%# Bind("MenuOptionId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="MenuSubItemId" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("MenuSubItemId") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMenuSubItemId" runat="server" Text='<%# Bind("MenuSubItemId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="SubItemName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("SubItemName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblsubname" runat="server" Text='<%# Bind("SubItemName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Active" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Active") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkactive" runat="server" CommandName="Active" CommandArgument='<%# Bind("MenuOptionId") %>' Text='<%# Bind("Active") %>'></asp:LinkButton>
                    <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Bind("MenuOptionId") %>' CommandName="EditRow">Edit</asp:LinkButton>
                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%# Bind("MenuOptionId") %>' CommandName="DeleteRow" OnClientClick="return conformbox();">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>

