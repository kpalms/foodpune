﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Enquiry.aspx.cs" Inherits="Enquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
 Name <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
  <br />
  
 Email <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
  <br />
  Phone No : <asp:TextBox ID="txtphone" runat="server"></asp:TextBox>
  <br />

  Mobile No:<asp:TextBox ID="Txtmobileno" runat="server"></asp:TextBox>
  <br/>

  Restaurant Name<asp:TextBox ID="Txtrestaurant" runat="server"></asp:TextBox>
  <br/>

  Select City<asp:DropDownList ID="DdlselectCity" runat="server" 
        AutoPostBack="True" onselectedindexchanged="DdlselectCity_SelectedIndexChanged"></asp:DropDownList>
  <br/>

  Select Area<asp:DropDownList ID="ddlselectarea" runat="server" 
        onselectedindexchanged="ddlselectarea_SelectedIndexChanged"></asp:DropDownList>
  <br/>

  Comments<asp:TextBox ID="txtcomments" runat="server" TextMode="MultiLine"></asp:TextBox>
  <br/>

  <asp:Button ID="ButSend" runat="server" Text="Send" onclick="ButSend_Click" />
  

  <asp:Button ID="ButClear" runat="server" Text="Clear" onclick="ButClear_Click" />

</asp:Content>

