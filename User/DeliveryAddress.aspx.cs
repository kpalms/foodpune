﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_DeliveryAddress : System.Web.UI.Page
{
    BL_DeliveryAddress bl_deliveryAddress = new BL_DeliveryAddress();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        setField();
        bl_deliveryAddress.Mode = "Insert";
        bl_deliveryAddress.CityId = 1;
        bl_deliveryAddress.AreaId = 1;
        bl_deliveryAddress.UserId = 1;
        bl_deliveryAddress.Address = "Address Temp";
        bl_deliveryAddress.InsertDeliveryAddress(bl_deliveryAddress);
        Clear();
    }
    public void setField()
    {
        Common common = new Common();
        bl_deliveryAddress.Date = common.GetDate();
        bl_deliveryAddress.Company = txtCompany.Text;
        bl_deliveryAddress.FlatNo = txtFlat.Text;
        bl_deliveryAddress.Appartment = txtAppartment.Text;
        bl_deliveryAddress.AlternateContactNo = txtAlternateContact.Text;
        bl_deliveryAddress.Landmark = txtLandmark.Text;
        
       
    }
    public void Clear()
    {
        txtFlat.Text = "";
        txtAppartment.Text = "";
        txtCompany.Text = "";
        txtAlternateContact.Text = "";
        txtLandmark.Text="";

    }
}