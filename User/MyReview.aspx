﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyReview.aspx.cs" Inherits="User_MyReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  

<br />

<div  class="widget">
<div class="widget-content">





<%--tab3--%>					
<div class="tab-pane " id="Review">
<div class="span10 review" >
<fieldset>

    <div class="widget">
            <div class="widget-header"> <i class="icon-file"></i>
              <h3> Review</h3>
            </div>
           
            <div class="widget-content">
            
              <ul class="messages_layout">
              <table>
              <tr><td>
              <div id="dvReviews">
                <asp:Repeater ID="rptMyReview" runat="server">
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar">
                
                <img src="../<%#Eval("Image") %>" height="50" width="50"/></a>
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name reviewId"><asp:Label ID="lblreviewid"  runat="server" Text='<%#Eval("ReviewId") %>' Visible="false"></asp:Label></a>
<a class="name"><asp:Label ID="lblUserId" runat="server" Text='<%#Eval("RestaurantName") %>'></asp:Label></a> 

                    </div>
<span class="time date"> <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date") %>' Font-Size="Small"></asp:Label></span>
<div class="text comments"> <asp:Label ID="lblComments" runat="server" Text='<%#Eval("Comments") %>'></asp:Label></div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               
                </td>
<td valign="bottom">
    <img id="loader" alt="" src="loading.gif" style="display: none" />
</td>
</tr>
</table>
              </ul>
             
            </div>
          
          </div>

</fieldset>


</div>
</div>








</div>
</div>

</div>
</div>
</div>
</div>
</div>











<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>
</asp:Content>

