﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="User_ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../css/pages/signin.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 

<div  class="widget">
<div class="widget-content">



<div class="account-container">
	
	<div class="content clearfix">
		
		
		<h1>Forgot Password</h1>	
			
			<div class="login-fields">
				
				<p>    <asp:Label ID="lblmessage" runat="server" Text="Label" Visible="false" ForeColor="Red"></asp:Label></p>

                <asp:Label ID="lblInvalid" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
				
				<div class="field">
                    <label for="firstname">Email Id:</label>
                    <asp:TextBox ID="txtemail" runat="server" placeholder="Enter Email Id"  class="textboxes" required></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
ErrorMessage="enter email address" ControlToValidate="txtemail" 
ValidationGroup="new1" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
runat="server" ErrorMessage="Envalid email" ControlToValidate="txtemail" 
ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
ForeColor="Red" ValidationGroup="new1"></asp:RegularExpressionValidator>
				</div> 
				
			</div> 
			
			<div class="login-actions">
				
 <asp:Button ID="btnsend" runat="server" Text="Send" 
          class="button btn btn-success btn-large" onclick="btnsend_Click" ValidationGroup="new1"/>

			</div> 
				
	</div> 
	
</div>


</div>
</div>


</div>
</div>
</div>
</div>
</div>

<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>

</asp:Content>

