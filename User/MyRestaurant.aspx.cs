﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_MyRestaurant : System.Web.UI.Page
{
    BL_Restaurants bl_restaurants = new BL_Restaurants();
    BL_Favorites bl_favorites = new BL_Favorites();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"]== null)
        {
            Response.Redirect("../Login.aspx");
           
        }
        else
        {
            RestoBind();
        }
      
      
    }



    private void RestoBind()
    {
        bl_restaurants.UserId = Convert.ToInt32(Session["UserId"]);
        DataTable dt = bl_restaurants.SelectFavorateRestaurant(bl_restaurants);
        rptrestaurant.DataSource = dt;
        rptrestaurant.DataBind();
    }

    protected void rptrestaurant_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
     if (e.CommandName.ToString() == "UnFavorites")
        {
            bl_favorites.UserId = Convert.ToInt32(Session["UserId"]);
            bl_favorites.RestaurantId = Convert.ToInt32(e.CommandArgument.ToString());
            bl_favorites.DeleteFavourite(bl_favorites);

        }

    }




    protected void rptrestaurant_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblrestoid = (Label)e.Item.FindControl("lblrestoid");
        Label lblfavcount = (Label)e.Item.FindControl("lblfavcount");
        Label lbldiscount = (Label)e.Item.FindControl("lbldiscount");
        Label lblcoupncode = (Label)e.Item.FindControl("lblcoupncode");
        Label kkkk = (Label)e.Item.FindControl("kkkk");
        ImageButton imgfave = (ImageButton)e.Item.FindControl("imgfave");

        bl_restaurants.UserId = Convert.ToInt32(Session["UserId"]);

        bl_restaurants.RestaurantId = Convert.ToInt32(lblrestoid.Text);

        DataSet ds = bl_restaurants.SelectRestoCatagorybyId(bl_restaurants);
        Repeater rptcategory = e.Item.FindControl("rptcategory") as Repeater;

        rptcategory.DataSource = ds.Tables[0];
        rptcategory.DataBind();



        DataSet dsdiscount = bl_restaurants.SelectRestoConditionbyId(bl_restaurants);
        if (dsdiscount.Tables[1].Rows.Count > 0)
        {
            lbldiscount.Text = dsdiscount.Tables[1].Rows[0]["DiscountAmount"].ToString() + ' ' + "Discount";
            lblcoupncode.Text = "Use Code " + dsdiscount.Tables[1].Rows[0]["Code"].ToString();
        }

        //lblfavcount.Text = ds.Tables[1].Rows[0][0].ToString();
        //imgfave.ImageUrl = "../img/fave.png";
        //imgfave.ToolTip = "Make Unfavourite";
        //imgfave.Enabled = true;
        //imgfave.CommandName = "UnFavorites";
       


            //if (dtfavorites.Rows.Count > 0)
            //{



            //    for (int i = 0; i < dtfavorites.Rows.Count; i++)
            //    {
            //        if (lblrestoid.Text == dtfavorites.Rows[i]["RestaurantId"].ToString())
            //        {
            //            imgfave.ImageUrl = "img/fave.png";
            //            imgfave.ToolTip = "Make Unfavourite";
            //            imgfave.Enabled = true;
            //            imgfave.CommandName = "UnFavorites";
            //            break;

            //        }
            //        else
            //        {
            //            imgfave.ImageUrl = "img/FavBlack.png";
            //            imgfave.ToolTip = "Make Favourite";
            //            imgfave.Enabled = true;
            //            imgfave.CommandName = "Favorites";

            //        }


            //    }


            //}




    }
}