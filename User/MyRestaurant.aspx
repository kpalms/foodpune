﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyRestaurant.aspx.cs" Inherits="User_MyRestaurant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="../css/pages/Favourites.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  

<br />

<div  class="widget">
<div class="widget-content">
        
        <asp:Repeater ID="rptrestaurant" runat="server" 
        onitemdatabound="rptrestaurant_ItemDataBound" OnItemCommand="rptrestaurant_ItemCommand"
        >

<ItemTemplate>

  <div class="restopanal span8">
  <div class="ownerimage span2 ">
  <img alt='<%#Eval("Image")  %>' src="../<%#Eval("Image")  %>" height="150px" width="150px"  />
  </div>

   <div class="span3">
         <asp:Label ID="Label1" runat="server" Text='<%#Eval("RestaurantName")  %>' CssClass="ownertitle restaurantname"></asp:Label>
       <asp:Label ID="lblrestoid" runat="server" Text='<%#Eval("RestaurantId")  %>' Visible="false"></asp:Label>
        <asp:Repeater ID="rptcategory" runat="server" >
        <ItemTemplate>
        <div class="infohead"><li> <asp:Label ID="lblRestoCategory" runat="server" Text='<%#Eval("RestoCategory")  %>'></asp:Label> </li></div>
        </ItemTemplate>
        </asp:Repeater>

   </div>



    <div class="menubutton span2">
        <asp:Label ID="lbldiscount" runat="server" ></asp:Label>
        <br />
        <asp:Label ID="lblcoupncode" runat="server" ></asp:Label>
    <br /> <a href="../Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>&or=<%#Eval("Orders")%>" class="btn btn-success">
                        Menu
                        </a>
    </div>

  </div>




</ItemTemplate>
</asp:Repeater>
</div>
</div>

</div>
</div>
</div>
</div>
</div>





    <script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>


</asp:Content>

