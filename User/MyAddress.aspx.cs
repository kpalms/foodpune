﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_MyAddress : System.Web.UI.Page
{
    BL_DeliveryAddress bl_deliveryaddress = new BL_DeliveryAddress();
    protected void Page_Load(object sender, EventArgs e)
    {
       
          if (Session["UserId"] == null)
          {
              Response.Redirect("../Login.aspx");
         }
        else
        {
            SelectMyAddress();
        }
       
    }

    public void SelectMyAddress()
    {
        bl_deliveryaddress.UserId = Convert.ToInt32(Session["UserId"].ToString());
        DataTable dt = bl_deliveryaddress.SelectMyAddress(bl_deliveryaddress);
        rptMyAddress.DataSource = dt;
        rptMyAddress.DataBind(); 
    }
}