﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_MyOrders : System.Web.UI.Page
{
    BL_Order bl_order = new BL_Order();
    string Orderid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"]== null)
        {
            Response.Redirect("../Login.aspx");
        }
        else
        {
            GetOrderDetails();
            
        }
   

    }

    private void GetOrderDetails()
    {

        bl_order.UserId = Convert.ToInt16(Session["UserId"].ToString());
        DataTable dt = bl_order.GetRestoDetailsByUserId(bl_order);
        rptrestodetails.DataSource = dt;
        rptrestodetails.DataBind();
 
    }

    protected void rptMyOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Label lblRestaurantId = (Label)e.Item.FindControl("lblRestaurantId");
        Label lblorderid = (Label)e.Item.FindControl("lblorderid");
        LinkButton lnkpostreview = (LinkButton)e.Item.FindControl("lnkpostreview");

        lnkpostreview.PostBackUrl = "PostReview.aspx?key=" + lblRestaurantId.Text + "&orid=" + lblorderid.Text + "&pg=user";
     
        Repeater rptorderdetails = e.Item.FindControl("rptorderdetails") as Repeater;
        bl_order.OrderId = Convert.ToInt32(lblorderid.Text);
        DataSet ds= bl_order.GetOrderDetails(bl_order);
        rptorderdetails.DataSource = ds.Tables[0];
        rptorderdetails.DataBind();

        if (ds.Tables[1].Rows.Count > 0)
        {
            lnkpostreview.Visible = false;
        }
        else
        {
            lnkpostreview.Visible = true;
        }
      

    }

    protected void lnkpostreview_Click(object sender, EventArgs e)
    {
        //Response.Redirect("PostReview.aspx");

    }

}