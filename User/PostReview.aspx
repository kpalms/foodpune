﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PostReview.aspx.cs" Inherits="User_PostReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div  class="widget">
<div class="widget-content">


<div class="account-container register">
<div class="content clearfix">

    <asp:Label ID="lblmessage" runat="server" Text="Label" Visible="false"></asp:Label>
		
<div class="login-fields">

<div class="field">
<label for="firstname">Post Review</label>
<asp:TextBox ID="txtreview" runat="server" placeholder="Enter Your Review"  class="textboxes"  required TextMode="MultiLine"></asp:TextBox>
</div> 

</div>
<div class="login-actions">
    <asp:Button ID="btnsend" runat="server" Text="Post Review" 
          class="button btn btn-success btn-large" onclick="btnsend_Click" 
        />
  <a href="MyOrders.aspx"  class="button btn btn-warning btn-large"> Go To Back</a>
</div>

</div> 
</div> 


</div>
</div>


    <script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>
</asp:Content>

