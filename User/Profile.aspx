﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="User_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../css/pages/signin.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div  class="widget">
<div class="widget-content">


<div class="account-container register">
<div class="content clearfix">
<h1>Profile</h1>			
<div class="login-fields">

<div class="field">
<label for="firstname">First Name:</label>
<asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" class="textboxes" required></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">Middle Name:</label>
<asp:TextBox ID="txtMiddleName" runat="server" class="signup" placeholder="Middle Name"></asp:TextBox>
</div> 

<div class="field">
<label for="firstname">Last Name:</label>
<asp:TextBox ID="txtLastName" runat="server" class="signup" required placeholder="Last Name"></asp:TextBox>

</div> 

<div class="field">
<label for="firstname">Mobile :</label>
<asp:TextBox ID="txtMobileNo" runat="server" class="signup" required placeholder="Mobile"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
ErrorMessage="Please enter mobile no" ForeColor="Red" ControlToValidate="txtMobileNo" 
ValidationGroup="new" ></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtMobileNo" ValidationGroup="new"></asp:RegularExpressionValidator>
  
</div> 

<div class="field">
<label for="firstname">Email:</label>
<asp:TextBox ID="txtEmail" runat="server" class="signup" ReadOnly="true" placeholder="Email"></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">City</label>
<asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlCity_SelectedIndexChanged"></asp:DropDownList>
        
         <asp:RequiredFieldValidator ControlToValidate="ddlCity" ID="RequiredFieldValidator1"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a City"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
</div> 


<div class="field">
<label for="firstname">Area</label>
<asp:DropDownList ID="ddlArea" runat="server"></asp:DropDownList>
                
                <asp:RequiredFieldValidator ControlToValidate="ddlArea" ID="RequiredFieldValidator2"
ValidationGroup="new" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a Area"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
</div> 

<div class="field">
<label for="firstname">Address</label>
<asp:TextBox ID="txtAddress" runat="server" class="signup" required placeholder="Address"></asp:TextBox>
</div> 


</div>
<div class="login-actions">
    <asp:Button ID="btnSubmit" runat="server" Text="Update" ValidationGroup="new" 
        onclick="btnSubmit_Click"  class="button btn btn-success btn-large"/>
</div>
</div> 
</div> 


</div>
</div>



<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>

</asp:Content>

