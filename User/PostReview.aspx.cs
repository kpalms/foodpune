﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_PostReview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("../Login.aspx");

            }
            else
            {
                string a = Request.QueryString["key"].ToString();
            }

        }
        
    }
    BL_Order bl_order = new BL_Order();

    protected void btnsend_Click(object sender, EventArgs e)
    {
        bl_order.RestaurantId =Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_order.UserId = Convert.ToInt32(Session["UserId"].ToString());
        Common common = new Common();
        bl_order.Date = common.GetDate();
        bl_order.Active = 0;
        bl_order.Comments = txtreview.Text;
        bl_order.OrderId = Convert.ToInt32(Request.QueryString["orid"].ToString());
        bl_order.InsertReviewsByRestoId(bl_order);
        txtreview.Text = "";
        Response.Redirect("MyOrders.aspx?pg=user");
    }


   
}