﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_MyActivity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserId"].ToString() != null)
        //{
          
        //}
        //else
        //{
        //    Response.Redirect("Login.aspx");
        //}

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
    }

    protected void lbtnOrders_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/User/MyOrders.aspx");
    }
    protected void lbtnAddress_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/User/MyAddress.aspx");
    }
    protected void lbtnReviews_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/User/MyReview.aspx");
    }
    protected void lbtnFavorites_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/User/MyRestaurant.aspx");
    }


}