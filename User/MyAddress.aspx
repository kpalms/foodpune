﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyAddress.aspx.cs" Inherits="User_MyAddress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="../css/pages/Address.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  

<br />

<div  class="widget">
<div class="widget-content">

    <asp:Repeater ID="rptMyAddress" runat="server">
    <ItemTemplate>

        <div class="restopanal span8">
  
        <asp:Label ID="Label1" runat="server" Text='<%# Eval("DeliveryId") %>' Visible="false"></asp:Label>
        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Date") %>'  Visible="false"></asp:Label>
                
                <table border="0" width="100%">

                    <tr>
                        <td class="head">City</td>
                         <td class="normaltext"><asp:Label ID="Label5" runat="server" Text='<%# Eval("CityName") %>'></asp:Label>
                               ,<asp:Label ID="Label9" runat="server" Text='<%# Eval("AreaName") %>'></asp:Label>
                         </td>
                    </tr>

                    <tr>
                        <td class="head">FlatNo</td>
                         <td class="normaltext">  <asp:Label ID="Label3" runat="server" Text='<%# Eval("FlatNo") %>'></asp:Label></td>
                    </tr>

                     <tr>
                        <td class="head">Appartment</td>
                         <td class="normaltext">  <asp:Label ID="Label4" runat="server" Text='<%# Eval("Appartment") %>'></asp:Label></td>
                    </tr>

                     <tr>
                        <td class="head">Company</td>
                         <td class="normaltext"><asp:Label ID="Label7" runat="server" Text='<%# Eval("Company") %>'></asp:Label></td>
                    </tr>

                    <tr>
                        <td class="head">AlternateContactNo</td>
                         <td class="normaltext"><asp:Label ID="Label6" runat="server" Text='<%# Eval("AlternateContactNo") %>'></asp:Label></td>
                    </tr>


                     <tr>
                        <td class="head">Landmark</td>
                         <td class="normaltext"><asp:Label ID="Label8" runat="server" Text='<%# Eval("Landmark") %>'></asp:Label></td>
                    </tr>

                    <tr>
                    <td class="head">Address</td>
                    <td class="normaltext"><asp:Label ID="Label12" runat="server" Text='<%# Eval("Address") %>'></asp:Label></td>
                    </tr>


                </table>


        
       </div>

    </ItemTemplate>
    </asp:Repeater>


</div>
</div>


</div>
</div>
</div>
</div>
</div>   
    
    
    




<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>
</asp:Content>

