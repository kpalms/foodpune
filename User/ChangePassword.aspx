﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="User_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../css/pages/signin.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />


<div  class="widget">
<div class="widget-content">

    
<div class="account-container register">
<asp:Label ID="lblmsg" runat="server"></asp:Label>
<div class="content clearfix">
<h1>Change Password</h1>			
<div class="login-fields">

<div class="field">
<label for="firstname">Existing Password:</label>
<asp:TextBox ID="txtExistingPassword" runat="server" placeholder="Existing Password"  class="textboxes" required TextMode="Password" ValidationGroup="changepass"></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">New Password:</label>
<asp:TextBox ID="txtNewPassword" runat="server" class="signup" required="required" placeholder="New Password" TextMode="Password" ValidationGroup="changepass"></asp:TextBox>
</div> 

<div class="field">
<label for="firstname">Confirm Password:</label>
<asp:TextBox ID="txtConfirmPassword" runat="server" class="signup" required="required" placeholder="Confirm Password" TextMode="Password"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" 
    ErrorMessage="password does not match" ControlToCompare="txtConfirmPassword" ControlToValidate="txtNewPassword"
    ForeColor="Red" ValidationGroup="changepass"></asp:CompareValidator>
</div> 

</div>
<div class="login-actions">
    <asp:Button ID="btnchangepassword" runat="server" Text="Change Password" 
          class="button btn btn-success btn-large" 
        onclick="btnchangepassword_Click" ValidationGroup="changepass"/>
</div>
</div> 
</div> 


</div>
</div>



<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>

</asp:Content>

