﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyOrders.aspx.cs" Inherits="User_MyOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="../css/pages/MyOrders.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  

<br />

<div  class="widget">
<div class="widget-content">
                        <asp:Repeater ID="rptrestodetails" runat="server" onitemdatabound="rptMyOrders_ItemDataBound" >
                       
                        <ItemTemplate>
                        

                        <div class="restopanal span8">

                        <table border="0" cellpadding="2" cellspacing="2" width="100%">
                        <tr>
                        

                        <td>
                        <div class="ownerimage span2 ">
                        <img alt='<%#Eval("Image")  %>' src='../<%#Eval("Image")  %>'  class="image"/>
                        </div>
                        </td>

                        <td>
                        <div class="span3">
                         <asp:Label ID="lblrestoname" runat="server" Text='<%#Eval("RestaurantName")  %>' CssClass="ownertitle restaurantname"></asp:Label>
                         <asp:Label ID="lblorderid" runat="server" Text='<%#Eval("OrderId")  %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblRestaurantId" runat="server" Text='<%#Eval("RestaurantId")  %>' Visible="false"></asp:Label>
                            <asp:Repeater ID="rptorderdetails" runat="server">
                                <ItemTemplate>
                                    <li>
                                    <span class="infohead"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Item") %>'></asp:Label></span>
                                    </li>
                                                                   

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        </td>

                        <td>
                        <div class="menubutton span2">
                        <a href="../Menu.aspx?key=<%#Eval("RestaurantId")%>&rn=<%#Eval("RestaurantName")%>&city=<%#Eval("CityName")%>&area=<%#Eval("AreaName")%>&ct=<%#Eval("CityId")%>&at=<%#Eval("AreaId")%>&or=<%#Eval("Orders")%>" class="btn btn-success">Menu
                         </a>
                         <br />
                         <br />
                            <asp:LinkButton ID="lnkpostreview" runat="server" CssClass="btn btn-success">Post Review</asp:LinkButton>
<%--                          <a href="PostReview.aspx?key=<%#Eval("RestaurantId")%>&orid=<%#Eval("OrderId")%>" class="btn btn-success">Post Review</a>--%>
                    
                        </div>
                        </td>
                      
                         </tr>
                        </table>
                        </div>


                        </ItemTemplate>
                        </asp:Repeater>
</div>
</div>



</div>
</div>
</div>
</div>
</div>


<%--<asp:Repeater ID="rptrestodetails" runat="server" onitemdatabound="rptMyOrders_ItemDataBound">
                       
<ItemTemplate>
                       

<div class="restopanal span8">
<div class="span3">
<div id="dvCategory" runat="server">
<div class="ownerimage span2 ">
<img alt='<%#Eval("Image")  %>' src='../WF/images/Resto/<%#Eval("Image")  %>' />
</div>
<b>
<a href="../Menu.aspx?key=<%# Eval("RestaurantId")%>&rn=<%# Eval("RestaurantName")%>&cn=<%# Eval("CityId")%>&an=<%# Eval("AreaId")%> ">
<asp:Label ID="lblrestoname" runat="server" Text='<%# Eval("RestaurantName") %>' CssClass="ownertitle"></asp:Label></b>
</a>
</div>

<ul>
<li>
<span class="infohead">
<asp:Label ID="Label5" runat="server" Text='<%# Eval("Item") %>'></asp:Label>
<asp:Label ID="Label1" runat="server" Visible="false" Text='<%# Eval("OrderId") %>'></asp:Label>
<asp:Label ID="Label3" runat="server" Visible="false" Text='<%# Eval("UserId") %>'></asp:Label>
<asp:Label ID="Label4" runat="server" Visible="false" Text='<%# Eval("RestaurantId") %>'></asp:Label>
</span>
</li>
</ul>
</div>


</div>

</ItemTemplate>
</asp:Repeater> --%>











<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>



</asp:Content>

