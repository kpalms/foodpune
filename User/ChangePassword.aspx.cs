﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class User_ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
        }
    }


    BL_UserDetails bl_userdetails = new BL_UserDetails();

      
       protected void btnchangepassword_Click(object sender, EventArgs e)
       {
           bl_userdetails.UserId = Convert.ToInt32(Session["UserId"].ToString());
           DataTable dt = bl_userdetails.SelectMyProfile(bl_userdetails);
          string password  = dt.Rows[0]["Password"].ToString();
          if (txtExistingPassword.Text == password)
          {
              SetField();
              bl_userdetails.Mode = "Update";
              bl_userdetails.ChangeUserPassword(bl_userdetails);
              lblmsg.Text = "Password Change Sussfully !";
              lblmsg.ForeColor = System.Drawing.Color.Green;
              //EmailSms email = new EmailSms();
              //string body = "<table><tr><td>User Name :</td><td>'" + Session["username"].ToString()+ "'</td></tr><tr><td>Password :</td><td>'" + txtNewPassword.Text + "'</td></tr></table>";
              //email.SendMail("Change Password Sussfully", body, Session["email"].ToString());
          }
          else
          {
              lblmsg.Text = "Existing Password is Wrong !";
              lblmsg.ForeColor = System.Drawing.Color.Red;
          }
           clear();
       }

       private void SetField()
       {
           bl_userdetails.UserId = Convert.ToInt32(Session["UserId"].ToString());
           bl_userdetails.Password = txtNewPassword.Text;
       }

       private void clear()
       {
           txtExistingPassword.Text = "";
           txtNewPassword.Text = "";
           txtConfirmPassword.Text = "";
       }
}