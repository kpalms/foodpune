﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DeliveryAddress.aspx.cs" Inherits="User_DeliveryAddress" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link href="../css/pages/signin.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div  class="widget">
<div class="widget-content">


<div class="account-container register">
<div class="content clearfix">
<h1>Delivery Address</h1>			
<div class="login-fields">

<div class="field">
<label for="firstname">Flat No:</label>
<asp:TextBox ID="txtFlat" runat="server" placeholder="Flat No" class="signup" required></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">Appartment:</label>
<asp:TextBox ID="txtAppartment" runat="server" class="signup" placeholder="Appartment"></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">Company :</label>
 <asp:TextBox ID="txtCompany" runat="server" class="signup" required placeholder="Company"></asp:TextBox>
</div> 

<div class="field">
<label for="firstname">Alternate contact no.:</label>
<asp:TextBox ID="txtAlternateContact" runat="server" class="signup" required placeholder="Email"></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">Landmark</label>
<asp:TextBox ID="txtLandmark" runat="server"  class="signup" required placeholder="Landmark"></asp:TextBox>
</div> 


<div class="field">
<label for="firstname">Area</label>
<asp:TextBox ID="txtarea" runat="server" class="signup" required placeholder="Area"></asp:TextBox>
</div> 

<div class="field">
<label for="firstname">Address</label>
<asp:TextBox ID="txtAddress" runat="server" class="signup" required placeholder="Address"></asp:TextBox>
</div> 


</div>
<div class="login-actions">
    <asp:Button ID="Button1" runat="server" Text="Submit" 
        onclick="btnSubmit_Click"  class="button btn btn-success btn-large"/>
</div>
</div> 
</div> 


</div>
</div>



<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/base.js"></script>


    </asp:Content>

