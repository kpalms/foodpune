﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_Profile : System.Web.UI.Page
{
    BL_UserDetails bl_userdetails = new BL_UserDetails();
    Common common = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"]== null)
        {
            Response.Redirect("../Login.aspx");

        }
        else
        {
            
            if(!IsPostBack)
            {
                common.bindCity(ddlCity);
            SelectMyProfile();
            }
            
        }
     
    }
    public void SelectMyProfile()
    {
        bl_userdetails.UserId = Convert.ToInt16(Session["UserId"].ToString());
        DataTable dt = bl_userdetails.SelectMyProfile(bl_userdetails);
        txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
        txtMiddleName.Text = dt.Rows[0]["MiddleName"].ToString();
        txtLastName.Text = dt.Rows[0]["LastName"].ToString();
        txtMobileNo.Text = dt.Rows[0]["MobileNo"].ToString();
        txtEmail.Text = dt.Rows[0]["Email"].ToString();
        ddlCity.SelectedValue = (dt.Rows[0]["CityId"].ToString());

        if (0 != Convert.ToInt32(dt.Rows[0]["CityId"]))
        {
            common.bindAreabycityAdmin(ddlArea, Convert.ToInt32(dt.Rows[0]["CityId"]));
            ddlArea.SelectedValue = (dt.Rows[0]["AreaId"].ToString());
        }
        txtAddress.Text = dt.Rows[0]["Address"].ToString();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SetField();
        bl_userdetails.Mode = "Update";
        bl_userdetails.ModifyUserDetails(bl_userdetails);
    }

    private void SetField()
    {
        bl_userdetails.UserId = Convert.ToInt32(Session["UserId"]);
        bl_userdetails.FirstName = txtFirstName.Text;
        bl_userdetails.MiddleName = txtMiddleName.Text;
        bl_userdetails.LastName = txtLastName.Text;
        bl_userdetails.Mobile = txtMobileNo.Text;
        bl_userdetails.Email = txtEmail.Text;
        bl_userdetails.CityId = Convert.ToInt32(ddlCity.SelectedItem.Value);
        bl_userdetails.AreaId = Convert.ToInt32(ddlArea.SelectedItem.Value);
        bl_userdetails.Address = txtAddress.Text;
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(ddlCity.SelectedItem.Value);
        common.bindAreabycityAdmin(ddlArea, id);
    }
}