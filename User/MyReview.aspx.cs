﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_MyReview : System.Web.UI.Page
{
    BL_Review bl_review = new BL_Review();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("../Login.aspx");
               
            }
            else
            {
                SelectMyReview();
            }
         
        }
    }

    public void SelectMyReview()
    {
        //bl_review.UserId = 1;
        bl_review.UserId = Convert.ToInt16(Session["UserId"].ToString());
        DataTable dt = bl_review.SelectMyReview(bl_review);
        rptMyReview.DataSource = dt;
        rptMyReview.DataBind();
    }
}