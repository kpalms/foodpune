﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

using System.Web.Services;
using System.Configuration;

public partial class demo : System.Web.UI.Page
{
    int totalCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            SqlConnection connection = new SqlConnection(connectionString);
            DataSet ds = new DataSet();
            String sql = "select RestaurantId,RestaurantName from RestaurantDetails";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            adapter.Fill(ds);
            totalCount = ds.Tables[0].Rows.Count;

            bindData();
        }
    }

    public void bindData()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        SqlConnection connection = new SqlConnection(connectionString);
        DataSet ds = new DataSet();
        String sql = "select RestaurantId,RestaurantName from RestaurantDetails";
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;
        connection.Open();
        SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
        adapter.Fill(ds, val, 5, "authors");
        connection.Close();
        repeater1.DataSource = ds;
        repeater1.DataBind();

        if (val <= 0)
        {
            lnkBtnPrev.Visible = false;
            lnkBtnNext.Visible = true;
        }

        if (val >= 5)
        {
            lnkBtnPrev.Visible = true;
            lnkBtnNext.Visible = true;
        }

        if ((val + 5) >= totalCount)
        {
            lnkBtnNext.Visible = false;
        }
    }




    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 5);
        bindData();
    }
    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 5);
        bindData();
    }
}