﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Menu.aspx.cs" Inherits="Menu"  %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="Stylesheet" type="text/css" href="css/pages/Menu.css" />

<%--
<link href="Search/jquerysctipttop.css" rel="stylesheet" type="text/css"/>
<script src="Search/jquery-1.11.3.min.js"></script>
<script src="Search/mv-autocomplete.js" type="text/javascript"></script>
<link href="Search/mv-autocomplete.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
--%>

<script type="text/javascript">
function Go() {
window.location = "Restaurants.aspx?ct=" + hdnSelectedCity.value + '&city=' + txtCity.value + '&at=' + hdnSelectedCityArea.value + '&area=' + txtArea.value;
}
</script> 


<script type="text/javascript">
    var pageIndex = 1;
    var pageCount;
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            GetRecords();
        }
    });
    function GetRecords() {
        pageIndex++;
        if (pageIndex == 2 || pageIndex <= pageCount) {
            $("#loader").show();
            $.ajax({
                type: "POST",
                url: "Menu.aspx/GetReview",
                data: '{pageIndex: ' + pageIndex + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }
    }
    function OnSuccess(response) {
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
        var reviews = xml.find("dsname");
        reviews.each(function () {
            var customer = $(this);
            var table = $("#dvReviews table").eq(0).clone(true);
            $(".name", table).html(customer.find("Name").text());
            $(".date", table).html(customer.find("Date").text());
            $(".comments", table).html(customer.find("Comments").text());
            $(".reviewId", table).html(customer.find("ReviewId").text());
            $("#dvReviews").append(table).append("<br />");
        });
        $("#loader").hide();
    }
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />

<div class="subnavbar2">
  <div class="subnavbar-inner2">
    <div class="container2">
      <div class="middlebox">
     
      <input id="txtCity" type="text" class="textboxes" placeholder="Enter Your City..." data-object />
       <input id="txtArea" type="text" class="textboxes" placeholder="Enter your Area..." data-object />
       <input type="button" value="Find Restaurant" id="btnCheckID"  disabled="disabled" onclick="Go()" class="button btn btn-success btn-large"/>
       
        </div>
    


</div> 
<div class="widget-content">
</div>
  </div>
</div>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  

<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="Default.aspx">Home</a></li>
        <li><a href="Restaurants.aspx" onclick="Go()"><asp:LinkButton ID="lnkcityname" runat="server" onclick="lnkcityname_Click"></asp:LinkButton></a></li>
          <li><asp:Label ID="lblrestoname" runat="server" Text=''></asp:Label></li>
    </ul>
    <asp:Label ID="lblctid" runat="server" Text='' Visible="false"></asp:Label>
    <asp:Label ID="lblarid" runat="server" Text='' Visible="false"></asp:Label>
        <asp:Label ID="lblrestoid" runat="server" Text='' Visible="false"></asp:Label>

</div>


	

<%--<div  class="widget">
<div class="widget-content">
<p>

<h1>
<asp:Image ID="Restoimage" runat="server" Width="100px" Height="100px"/><asp:Label ID="lblrestonamehead" runat="server" Text="Label"></asp:Label>
</h1>
<asp:Repeater ID="rptrestocathead" runat="server">
<ItemTemplate>
<asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>'></asp:Label>,
</ItemTemplate>
</asp:Repeater>

</p>	
</div> 
</div>--%> 




                <div class="info-box">
                <div class="row-fluid stats-box">

  

						
<div class="tabbable">
<div >

<div class="ownerimage">
<asp:Image ID="Restoimage" runat="server" Width="100px" Height="100px"/>
<asp:Label ID="lblrestonamehead" runat="server" Text="Label" CssClass="ownertitle"></asp:Label>
</div>

<div >
<asp:Repeater ID="rptrestocathead" runat="server">
<ItemTemplate>
<asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>' CssClass="ownerCategory"></asp:Label>,
</ItemTemplate>
</asp:Repeater>
</div>

</div>

<ul class="nav nav-tabs">
<li class="active">
<a href="#Menu" data-toggle="tab">Menu</a>
</li>
<li>
<a href="#Review" data-toggle="tab">Review</a>
</li>
<li>
<a href="#Info" data-toggle="tab">Info</a>
</li>
</ul>
						

<%--endtab3--%>					
<div class="tab-content">
<div class="tab-pane active" id="Menu">

<fieldset>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    <span class="span2" >
    <asp:Repeater ID="rptcategory" runat="server">
    <HeaderTemplate><ul  class="menucategory" ></HeaderTemplate>
        <ItemTemplate>
          <li>
        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
          </li>
        </ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
    </span>


<span class="span6" >

<asp:Repeater ID="rptitem" runat="server" onitemdatabound="rptitem_ItemDataBound" onitemcommand="rptitem_ItemCommand">
<ItemTemplate > 
<div id="dvCategory" runat="server" class="menuItemCatrow ">
    <div class="menuItemCatHead">
        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
    </div>
    <div class="span6">

        <img src='<%#Eval("Image") %>'  >
        
    </div>

</div>

<div>

</div>

<div class="span6" >

<span class="span3 menuItemName" >
<asp:Label ID="lblitemid" runat="server" Text='<%#Eval("ItemId") %>' Visible="false"></asp:Label>
<asp:Label ID="lblCustomisation" runat="server" Text='<%#Eval("Customisation") %>' Visible="false"></asp:Label>
<asp:Label ID="lblItem" CssClass="item_name" runat="server" Text='<%#Eval("Item") %>'></asp:Label>
<asp:Label ID="lblitemitemdescripation" CssClass="item_name" runat="server" Text='<%#Eval("ItemDescription") %>'></asp:Label>


</span>

<div class="span1 menuItemPrice" >
<asp:Label ID="lblPrice" CssClass="item_price" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
</div>

<span class="span1 addtocartbut" >
<asp:ImageButton ID="imgadd" runat="server" ImageUrl="~/img/add.png"/>
</span>

    <span class="span6" >
    <asp:Repeater ID="rptmenusubitem" runat="server" Visible="false"  onitemcommand="rptmenusubitem_ItemCommand">
    <ItemTemplate>
        <asp:Label ID="lblMenuSubItemId" runat="server" Text='<%#Eval("MenuSubItemId") %>' Visible="false"></asp:Label>
        <span class="span3 menuItemName" ><asp:Label ID="lblName" CssClass="item_name" Font-Size="Small" runat="server" Text='<%#Eval("Name") %>'></asp:Label></span>
        <div class="span1 menuItemPrice" style="width:9%"><asp:Label ID="lblPrice" CssClass="item_name" Font-Size="Small" runat="server" Text='<%#Eval("Price") %>'></asp:Label></div>
        <span class="span1 addtocartbut" style="width:9%"><asp:ImageButton ID="imgadd" runat="server" ImageUrl="~/img/add.png"/></span>
    </ItemTemplate>
    </asp:Repeater>
    </span>

</div>
</ItemTemplate>
</asp:Repeater>

</span>

<div class="span3" style="padding:10px;">
<div>
    <span class="minimumorder">Minimum Order Rs:<asp:Label ID="lblminimumorder" runat="server" Text="0"></asp:Label></span>
    
    <asp:GridView ID="grdcart" runat="server" AutoGenerateColumns="False" 
        onrowcommand="grdcart_RowCommand" Width="100%" CssClass="Grid">
            <headerstyle CssClass="addtocartheaders"/>
            <rowstyle  CssClass="addtocartRow"  />
            <alternatingrowstyle CssClass="addtocartAlternateRow"/>

        <Columns>
            <asp:TemplateField HeaderText="id" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="itemname">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("itemname") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblitemname" runat="server" Text='<%# Bind("itemname") %>' CssClass="addtocarItemname"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Qty") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblQty" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Total") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <ItemTemplate>
                    <asp:LinkButton ID="lnkplus" runat="server" OnClientClick="ShowProgress();" Font-Size="20pt" CommandName="plus" CommandArgument='<%# Bind("id") %>'>+</asp:LinkButton>
                    <asp:LinkButton ID="lnkminus" runat="server" Font-Size="20pt" CommandName="minus" CommandArgument='<%# Bind("id") %>'>-</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
     
    </asp:GridView>
   
    <span class="minimumorder" >Total: <asp:Label ID="lbltotal" runat="server" Text=""></asp:Label></span>
    </div>
    <div style="float:right;padding:10px;">
        <asp:Button ID="btncheckout" runat="server" Text="CheckOut" 
            class="button btn btn-success btn-large" Enabled="false" 
            onclick="btncheckout_Click1"/>
    </div>
</div>
    </ContentTemplate>
    </asp:UpdatePanel>

 </fieldset>
</div>

<%--tab3--%>					
<div class="tab-pane " id="Review">
<div class="span10 review" >
<fieldset>

    <div class="widget">
            <div class="widget-header"> <i class="icon-file"></i>
              <h3> Review</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
            
              <ul class="messages_layout">
              <table>
              <tr><td>
              <div id="dvReviews">
                <asp:Repeater ID="rptreview" runat="server">
                <ItemTemplate>
                
                 <table>
                <tr>
                    <td>
             
                <li class="from_user left"> <a href="#" class="avatar"><img src="img/user.png"/></a>
                  <div class="message_wrap"> <span class="arrow"></span>
                    <div class="info">

<a class="name reviewId"><asp:Label ID="lblreviewid"  runat="server" Text='<%#Eval("ReviewId") %>' Visible="false"></asp:Label></a>

                    </div>
                    <a class="name"><asp:Label ID="lblUserId" runat="server" Text='<%#Eval("Name") %>'></asp:Label></a> 
<span class="time date"> <asp:Label ID="lblDate" runat="server" Text='<%#Eval("Date") %>' Font-Size="Small"></asp:Label></span>
<div class="text comments"> <asp:Label ID="lblComments" runat="server" Text='<%#Eval("Comments") %>'></asp:Label></div>


                  </div>
                </li>
                  
                     </td>
                </tr>
            </table>
          
                </ItemTemplate>
                
                </asp:Repeater>
                </div>
               
                </td>
<td valign="bottom">
    <img id="loader" alt="" src="loading.gif" style="display: none" />
</td>
</tr>
</table>
              </ul>
             
            </div>
            <!-- /widget-content --> 
          </div>

</fieldset>


</div>
</div>
    <%--endtab3--%>	


<%--tab3--%>					
<div class="tab-pane" id="Info">
<div class="span10" >
<fieldset>




        <div class="widget">
            <div class="widget-header2"> <i class="icon-file"></i>
                
                Info
              
            </div>

            <div class="widget-content">
              <ul class="messages_layout">

                     
<asp:Label ID="lblRestaurantName" runat="server" Text='' CssClass="ownertitle"></asp:Label>
<br />
<asp:Label ID="lblDescription" runat="server" Text='' CssClass="owneraddress"></asp:Label>
<br />
<asp:Label ID="lblAddress" runat="server" Text='' CssClass="owneraddress"></asp:Label>

 

                        <li class="from_user left"> <a href="#" class="avatar"><img src="img/cuisine.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info"> <a class="head">Cuisines</a>
                        </div>
                        <div class="text">
                        <asp:Repeater ID="rptrestocat" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblrestocat" runat="server" Text='<%#Eval("RestoCategory") %>' ></asp:Label>,
                        </ItemTemplate>
                        </asp:Repeater>
                        </div>
                        </div>
                        </li>


                        <li class="from_user left  span10"> <a href="#" class="avatar"><img src="img/condition.png"/></a>
                        <div class="message_wrap span8"> <span class="arrow"></span>
                        <div class="info"> <a class="head">Restaurant</a>
                        </div>
                        <div class="text">
                       MinimumOrders : <asp:Label ID="lblMinimumOrders" runat="server" Text="Label"></asp:Label><br />
                        DeliveryCharges : <asp:Label ID="lblDeliveryCharges" runat="server" Text="Label"></asp:Label><br />
                         PickupTime : <asp:Label ID="lblPickupTime" runat="server" Text="Label"></asp:Label> Min<br />
                         DeliveryTime : <asp:Label ID="lblDeliveryTime" runat="server" Text="Label"></asp:Label> Min<br />
                        </div>
                        </div>
                        </li>

                        <li class="by_myself right"> <a href="#" class="avatar"><img src="img/payment.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info"> 
                        <a class="head"> Payment Type </a> 
                        </div>
                        <div class="text">
                        <asp:Repeater ID="rptpaymenttype" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblPaymentTypeId" runat="server" Visible="false" Text='<%#Eval("PaymentTypeId") %>'>'></asp:Label>
                        <asp:Label ID="lblPaymentType" runat="server" Text='<%#Eval("PaymentType") %>'>'></asp:Label>
                        </ItemTemplate>
                        </asp:Repeater>
                        </div>
                        </div>
                        </li>



                        <li class="from_user left"><a href="#" class="avatar"><img src="img/location.png"/></a>
                        <div class="message_wrap"> <span class="arrow"></span>
                        <div class="info">
                        <a class="head">Delivery locations</a>
                        </div>
                        <div class="text">
                        <asp:ListView ID="lstdeliverylocation" runat="server">
                        <ItemTemplate>
                        <asp:Label ID="lblAreaName" runat="server" Text='<%#Eval("AreaName") %>'></asp:Label>,
                        </ItemTemplate>
                        </asp:ListView>
                        </div>
                        </div>
                        </li>

              </ul>
            </div>
          </div>

</fieldset>
</div>
</div>
<%--endtab3--%>	

							
</div>
</div>		  
	
   














	                  </div>
                  </div>
						

</div>
</div>
</div>
</div>
</div>


<script>

    var countries = "Manoj";


    $('#txtCity').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyCity',
        loading_html: 'Loading...',
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCity').value = selected.split("|")[1];
        }
    });

    $('#txtArea').mvAutocomplete({
        data: countries,
        container_class: 'results',
        result_class: 'result',
        url: 'WebService.asmx/GetMyArea',
        loading_html: 'Loading...',
        post_data: {
            cityid: function () { return document.getElementById('hdnSelectedCity').value; }
        },
        callback: function (el, selected) {
            console.log('Click Back!');
            document.getElementById('hdnSelectedCityArea').value = selected.split("|")[1];
            document.getElementById('btnCheckID').disabled = false
        }
    });


</script>








</asp:Content>

