﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Review : System.Web.UI.Page
{
    BL_Review bl_review = new BL_Review();

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    private void setfill()
    {
        Common common = new Common();
        bl_review.Date = common.GetDate();
        bl_review.UserId = 1;
        bl_review.RestaurantId = 1;
        bl_review.Comments = txtcomment.Text;
    }


    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        try
        {
            setfill();
            bl_review.Review(bl_review);
            txtcomment.Text = "";
        }
        catch
        {

        }
    }
}