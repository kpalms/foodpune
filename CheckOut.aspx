﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CheckOut.aspx.cs" Inherits="CheckOut" Culture="hi-IN"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



<link href="css/pages/CheckOut.css" rel='stylesheet' type='text/css' />

<style>
.radioboxlist radioboxlistStyle
{
font-size:x-large;
padding-right: 20px;
}
.radioboxlist label {
color: #3E3928;
background-color:#E8E5D4;
padding-left: 6px;
padding-right: 16px;
padding-top: 2px;
padding-bottom: 2px;
border: 1px solid #AAAAAA;
white-space: nowrap;
clear: left;
margin-right: 10px;
margin-left: 10px;
}
.radioboxlist label:hover
{
color: #CC3300;
background: #D1CFC2;
}
input:checked + label {
color: #CC3300;
background: #D1CFC2;
}
</style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">





<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<input id="hdnSelectedCity" class="form-control" type="hidden" />
<input id="hdnSelectedCityArea" class="form-control" type="hidden" />


<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12">  


<div class="row">&nbsp</div>
<div class="bs-example">
    <ul class="breadcrumb">
        <li><a href="Default.aspx">Home</a></li>
        <li><a href="#"><asp:LinkButton ID="lnkcityname" runat="server" onclick="lnkcityname_Click"></asp:LinkButton></a></li>
          <li><a href="#">
              <asp:LinkButton ID="lnkresto" runat="server" OnClick="lnkresto_Click"></asp:LinkButton></a></li>
    </ul>
</div>


<div  class="widget">
<div class="widget-content">

<p>

<h1>
<asp:Image ID="Restoimage" runat="server" Width="100px" Height="100px"/>
<asp:Label ID="lblrestonamehead" runat="server" Text="Label" CssClass="ownertitle"></asp:Label>
<asp:Label ID="lblrestoid" runat="server" Text="" Visible="false"></asp:Label>
</h1>

<asp:Repeater ID="rptrestocathead" runat="server">
<ItemTemplate>
</ItemTemplate>
</asp:Repeater>
	
</div> 
</div> 

<div class="info-box">
<div class="row-fluid stats-box">



  <div class="container">
		  <div class="check_box">	 
		<div class="col-md-9 cart-items" >
			 

			 <div class="cart-header">
				
				 <div class="cart-sec simpleCart_shelfItem">

  		 <div class="registration_form">
	            <div class="middle">
                <asp:Panel ID="pnldeliveryaddress" runat="server">
                <a class="deliveryaddress" href="#">
                Select your Delivery Address
                                    <asp:RadioButton ID="rdoaddress" runat="server" GroupName="address" 
                        oncheckedchanged="rdoaddress_CheckedChanged" AutoPostBack="true"  />
                </a>
                  </asp:Panel>


                    <asp:Panel ID="pnlselectaddress" runat="server">
                     
                        <asp:RadioButtonList ID="rdoselectaddress" runat="server" 
                            RepeatDirection="Vertical" CssClass="radioboxlist" RepeatLayout="Flow"
                            >
                        
                        </asp:RadioButtonList>
                        				
                                        
                        <asp:Label ID="lblmsgdeliveryaddress" runat="server" Text="No Delivery Address Found" Visible="false" ForeColor="Red"></asp:Label>
                    </asp:Panel>

                   
               <a class="deliveryaddress" href="#">Enter your Delivery Address
                <asp:RadioButton ID="rdonewaddress" runat="server" GroupName="address" 
                        oncheckedchanged="rdonewaddress_CheckedChanged" AutoPostBack="true" />
               </a>

			 <asp:Panel ID="pnlnewaddress" runat="server" DefaultButton="btnSubmit">
             
       		    <div>
					<label>
                        <asp:TextBox ID="txtbuildingname" runat="server" placeholder="Building Name/Flat No" class="textboxcheckout" required></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="please enter Building Name/Flat No" ControlToValidate="txtbuildingname" ForeColor="Red" ValidationGroup="placeorder"></asp:RequiredFieldValidator>
                    </label>
				</div>

				<div>
					<label>
                        <asp:TextBox ID="txtapartment" runat="server" placeholder="Apartment" class="textboxcheckout" required></asp:TextBox>
					</label>
				</div>
				<div>
					<label>
						
                        <asp:TextBox ID="txtaddress" runat="server" class="textboxcheckout" placeholder="Address" required></asp:TextBox>
					 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="please enter address" ControlToValidate="txtaddress" ForeColor="Red" ValidationGroup="placeorder" ></asp:RequiredFieldValidator>
                    </label>
				</div>
				<div>
					<label>
						
                        <asp:TextBox ID="txtcompany" runat="server" class="textboxcheckout" placeholder="Company"></asp:TextBox>
					</label>
				</div>
                <div>
					<label>
						
                       <asp:TextBox ID="txtalternetcontact" runat="server" class="textboxcheckout" placeholder="Alternate Contact No"></asp:TextBox>
					<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="please enter 10 Digit mobile no" ForeColor="Red"  ValidationExpression="[0-9]{10}" ControlToValidate="txtalternetcontact" ValidationGroup="placeorder"></asp:RegularExpressionValidator>
                    </label>
				</div>
                <div>
					<label>
						
                        <asp:TextBox ID="txtlandmark" runat="server" class="textboxcheckout" placeholder="Landmark"></asp:TextBox>
					</label>
				</div>
                <div>
					<label>
                <asp:TextBox ID="txtmessage" runat="server" TextMode="MultiLine" class="textboxcheckout" placeholder="If you want to add any comment,  about  delivery instructions, this is the right place"></asp:TextBox>
                </label>
				</div>
           
			</asp:Panel>
	      </div>
		</div>





				<div class="clearfix"></div>
											
				  </div>
			 </div>
		 </div>




		 <div class="col-md-3 cart-total">
			 <a class="continue" href="#">Delivery Details</a>
			 <div class="price-details">
			
                <asp:RadioButton ID="rdodelivery" runat="server" GroupName="DP" /> Delivery
                <br />
                Your order will be delivered to your address.
                <br />
                <br />

                <asp:RadioButton ID="rdopickup" runat="server" GroupName="DP" AutoPostBack="true"/> PickUp
                <br />
                You will pick up the order yourself at restaurant.
                <br />
                <div><asp:Label ID="lblrestoaddress" runat="server" Text="Label"></asp:Label></div>
                <br />
                 <br />
                <asp:RadioButton ID="rbtassoonaspossible" runat="server" GroupName="checkout"  AutoPostBack="true"
                oncheckedchanged="rbtassoonaspossible_CheckedChanged" Text="As Soon As Possible"/>
               <div> Estimated <asp:Label ID="lbldeliverytext" runat="server" Text="Label"></asp:Label> time:<asp:Label ID="lbldeliverytime" runat="server" Text="Label"></asp:Label></div>
			 </div>	
			 <div class="clearfix"></div>	 
             <div class="total-item">
                <asp:RadioButton ID="rbtlater" runat="server" 
                GroupName="checkout" oncheckedchanged="rbtlater_CheckedChanged" AutoPostBack="true"
                />Later</b><br />
                <asp:DropDownList ID="ddldate" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddldate_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ControlToValidate="ddldate" ID="RequiredFieldValidator3"
ValidationGroup="placeorder" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a date"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddltime" runat="server">
                </asp:DropDownList>
                 <asp:RequiredFieldValidator ControlToValidate="ddltime" ID="RequiredFieldValidator4"
ValidationGroup="placeorder" CssClass="errormesg" ForeColor="Red" ErrorMessage="Please select a date"
InitialValue="0" runat="server"  Display="Dynamic">
</asp:RequiredFieldValidator>
                <%--(If you want to add any comment, e.g. about allergies or delivery<br />
                instructions, this is the right place)<br />
                <asp:LinkButton ID="lnkaddmessage" runat="server" onclick="lnkaddmessage_Click">Add Message To Your Order</asp:LinkButton>
                <br />--%>
			 </div>

			</div>
			<div class="clearfix"></div>
	       </div>
	     </div>





  <div class="container">
		  <div class="check_box">	 
		<div class="col-md-9 cart-items">
	
			 <div class="cart-header">
				
				 <div class="cart-sec simpleCart_shelfItem">

    <div class="middle">
                <span class="minimumorder">
                Minimum Order Rs:<asp:Label ID="lblminimumorder" runat="server" Text="0"></asp:Label>
                </span>
    
    <asp:GridView ID="grdcart" runat="server" AutoGenerateColumns="False" 
        onrowcommand="grdcart_RowCommand" CssClass="Grid" >
                <headerstyle CssClass="addtocartheaders"/>
                <rowstyle  CssClass="addtocartRow"  />
                <alternatingrowstyle CssClass="addtocartAlternateRow"/>
        <Columns>
            <asp:TemplateField HeaderText="id" Visible="false">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="itemname">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("itemname") %>' ></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblitemname" runat="server" Text='<%# Bind("itemname") %>' CssClass="addtocarItemname"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Qty") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblQty" runat="server" Text='<%# Bind("Qty") %>' CssClass="addtocarItemname"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Total") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("Total") %>' CssClass="addtocarItemname"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <ItemTemplate>
                    <asp:LinkButton ID="lnkplus" runat="server" Font-Size="20pt" CommandName="plus" CommandArgument='<%# Bind("id") %>'>+</asp:LinkButton>
                    <asp:LinkButton ID="lnkminus" runat="server" Font-Size="20pt" CommandName="minus" CommandArgument='<%# Bind("id") %>'>-</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    
</div>



					   <div class="clearfix"></div>
											
				  </div>
			 </div>
		 </div>




		 <div class="col-md-3 cart-total">
             <asp:LinkButton ID="lnkcontinuetocart" class="continue" runat="server" OnClick="lnkcontinuetocart_Click">Continue to Cart</asp:LinkButton>
			
			 <div class="price-details">
				 <h3>Price Details</h3>
<%--				 <span>Total</span>
				 <span class="total1">6200.00</span>--%>
                  <span class="minimumorder" >SubTotal: <asp:Label ID="lbltotal" runat="server" Text="" ></asp:Label></span><br />
<%--				 <span></span><span class="total1"><asp:Label ID="lbldiscount" runat="server" Visible="false"></asp:Label></span><br />

--%>
 <span class="total1"><asp:Label ID="lbldiscountcoupn" runat="server"></asp:Label></span>
 <br />				 
<span>Delivery Charges</span><span class="total1"> : <asp:Label ID="lbldeliverycharges" runat="server"
                    ></asp:Label></span>
                    
				 <div class="clearfix"></div>				 
			 </div>	
			 <ul class="total_price">
			   <li class="last_price">  Final Total</li>	
			   <li class="last_price"><span>
                   <asp:Label ID="lblfinaltotal" runat="server" Text="Label"></asp:Label></span></li>

			   <div class="clearfix"> </div>

			 </ul>
			
			 
			 <div class="clearfix"></div>
<%--             <asp:Button ID="btncheckout" runat="server" Text="CheckOut" onclick="btncheckout_Click" class="order"/>--%>
               <asp:Button ID="btncheckout"  runat="server" Text="Place Order" class="order" onclick="btncheckout_Click" ValidationGroup="placeorder"/>
             <div class="total-item">
                 <asp:TextBox ID="txtvouchercode" runat="server"></asp:TextBox>
                 <asp:LinkButton ID="btnsubmit"  runat="server"  class="cpns" 
                     onclick="btnsubmit_Click">Apply Coupons</asp:LinkButton>
                 
                 <asp:LinkButton ID="lnkcancel" runat="server" onclick="lnkcancel_Click" Visible="false">X</asp:LinkButton>
                     <br />
                 <asp:Label ID="lblmessage" runat="server" ></asp:Label>
			 </div>
			</div>
			<div class="clearfix"></div>
	 </div>
	     </div>




            </div>
            </div>



</div>
</div>
</div>
</div>
</div>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

