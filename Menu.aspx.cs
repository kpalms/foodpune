﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Services;
using System.Configuration;
public partial class Menu : System.Web.UI.Page
{
    static int restoid;
    BL_Restaurants bl_restaurant = new BL_Restaurants();
    string Category = "";
   // string customizationavailable = "";
    DataTable Basket_DataTable = null;
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["restoid"] == null)
        {
            Session["restoid"] = 0;
        }

        if (Convert.ToInt32(Session["restoid"].ToString()) != Convert.ToInt32(Request.QueryString["key"].ToString()))
        {
            Session["basket"] = null;
        }

        if (!Page.IsPostBack)
        {
            BindAllRestaurantDetails();
            rptreview.DataSource = GetReviewData(1);
            rptreview.DataBind();
        }

        if (Session["basket"] != null)
        {
            Basket_DataTable = (DataTable)Session["basket"];
            grdcart.DataSource = Basket_DataTable;
            GetTotal(Basket_DataTable);
            grdcart.DataBind();
        }
        else
        {
            Basket_DataTable = new DataTable();
            Basket_DataTable.Columns.Add("id");
            Basket_DataTable.Columns.Add("itemname");
            Basket_DataTable.Columns.Add("Qty");
            Basket_DataTable.Columns.Add("Total");
        }

        

       

    }

    private void BindAllRestaurantDetails()
    {
        //Label cn = new Label("lblcity");
       lblrestoid.Text = Request.QueryString["key"].ToString();
       lnkcityname.Text = "Restaurants in " + Request.QueryString["area"].ToString();
        lblrestoname.Text = Request.QueryString["rn"].ToString();
       // lblarea.Text = Request.QueryString["area"].ToString();
        lblctid.Text = Request.QueryString["ct"].ToString();
        lblarid.Text=Request.QueryString["at"].ToString();
        string or = Request.QueryString["or"].ToString();
        if (or == "Preorder Now")
        {
            btncheckout.Text = "Preorder";
        }
        else
        {
            btncheckout.Text = "CheckOut";
        }
        restoid= Convert.ToInt32(Request.QueryString["key"].ToString());
        bl_restaurant.RestaurantId = restoid;
       // bl_restaurant.RestaurantId = 1;
        DataSet ds = bl_restaurant.SelectAllRestaurantDetails(bl_restaurant);
        rptcategory.DataSource = ds.Tables[0];
        rptcategory.DataBind();

        rptitem.DataSource = ds.Tables[1];
        rptitem.DataBind();


        //lstresto.DataSource = ds.Tables[2];      
        //lstresto.DataBind();

        lblrestonamehead.Text = ds.Tables[2].Rows[0]["RestaurantName"].ToString();
        Restoimage.ImageUrl = ds.Tables[2].Rows[0]["Image"].ToString();
        lblRestaurantName.Text = ds.Tables[2].Rows[0]["RestaurantName"].ToString();
        lblDescription.Text = ds.Tables[2].Rows[0]["Description"].ToString();
        lblAddress.Text = ds.Tables[2].Rows[0]["Address"].ToString();




        lstdeliverylocation.DataSource = ds.Tables[4];
        lstdeliverylocation.DataBind();

        rptpaymenttype.DataSource = ds.Tables[5];
        rptpaymenttype.DataBind();  

        rptrestocat.DataSource=ds.Tables[6];
        rptrestocat.DataBind();

        rptrestocathead.DataSource = ds.Tables[6];
        rptrestocathead.DataBind();

        //rptrestocondtions.DataSource = ds.Tables[7];
        //rptrestocondtions.DataBind();
        if (ds.Tables[7].Rows.Count > 0)
        {
            lblMinimumOrders.Text = ds.Tables[7].Rows[0]["MinimumOrders"].ToString();
            lblDeliveryCharges.Text = ds.Tables[7].Rows[0]["DeliveryCharges"].ToString();
            lblPickupTime.Text = ds.Tables[7].Rows[0]["PickupTime"].ToString();
            lblDeliveryTime.Text = ds.Tables[7].Rows[0]["DeliveryTime"].ToString();
            lblminimumorder.Text = ds.Tables[7].Rows[0]["MinimumOrders"].ToString();
        }

       
     
    }

    protected void rptitem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    { 
        //Label lblCityName = (Label)row.FindControl("lblCityName");
        Control divHeader = e.Item.FindControl("dvCategory");

        Repeater rptmenusubitem = e.Item.FindControl("rptmenusubitem") as Repeater;
        Label lblCustomisation = e.Item.FindControl("lblCustomisation") as Label;
        Label lblitemid = e.Item.FindControl("lblitemid") as Label;
        Label lblPrice = e.Item.FindControl("lblPrice") as Label;
        ImageButton imgadd = e.Item.FindControl("imgadd") as ImageButton;
        
        
        if (divHeader != null)
        {
            Label lblCategory = (Label)divHeader.FindControl("lblCategory");
            if (lblCategory != null)
            {
                if (Category == lblCategory.Text)
                {
                    divHeader.Visible = false;
                }
                else
                {
                    Category = lblCategory.Text;
                }
            }
        }

        if (lblCustomisation.Text == "1")
        {
            rptmenusubitem.Visible = true;
            lblPrice.Visible = false;
            imgadd.Visible = false;
            BL_MenuSubItem bl_MenuSubItem = new BL_MenuSubItem();
            bl_MenuSubItem.ItemId = Convert.ToInt32(lblitemid.Text);
            DataTable dt = bl_MenuSubItem.SelectMenuSubItem(bl_MenuSubItem);
            rptmenusubitem.DataSource = dt;
            rptmenusubitem.DataBind();
        }




    }


    protected void rptitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        Session["restoid"] = Convert.ToInt32(Request.QueryString["key"].ToString());
      
            Label lblitemid = e.Item.FindControl("lblitemid") as Label;
            Label lblItem = e.Item.FindControl("lblItem") as Label;
            Label lblPrice = e.Item.FindControl("lblPrice") as Label;
            string id = lblitemid.Text;
            string name = lblItem.Text;
            string price = lblPrice.Text;


            //search item in DataTable
            bool Found = false;
            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
                if (Basket_DataTable.Rows[i][0].ToString() == lblitemid.Text)
                    Found = true;

            //add to basket
            if (Found == false)
            {

                Basket_DataTable.Rows.Add(new object[] { lblitemid.Text, lblItem.Text, "1", lblPrice.Text });

            }
            if (Found == true)
            {

                for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
                {
                    if (Basket_DataTable.Rows[i]["id"].ToString() == lblitemid.Text)
                    {
                        int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) + 1;
                        Basket_DataTable.Rows[i]["Qty"] = newqty;
                        Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString()) ;
                    }
                }

            }

            grdcart.DataSource = Basket_DataTable;
            grdcart.DataBind();
            GetTotal(Basket_DataTable);
            Session["basket"] = Basket_DataTable;
        
    }

    protected void rptmenusubitem_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        Session["restoid"] = Convert.ToInt32(Request.QueryString["key"].ToString());

        Label lblMenuSubItemId = e.Item.FindControl("lblMenuSubItemId") as Label;
        Label lblName = e.Item.FindControl("lblName") as Label;
        Label lblPrice = e.Item.FindControl("lblPrice") as Label;
        string id = lblMenuSubItemId.Text;
        string name = lblName.Text;
        string price = lblPrice.Text;


        //search item in DataTable
        bool Found = false;
        for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            if (Basket_DataTable.Rows[i][0].ToString() == lblMenuSubItemId.Text)
                Found = true;

        //add to basket
        if (Found == false)
        {

            Basket_DataTable.Rows.Add(new object[] { lblMenuSubItemId.Text, lblName.Text, "1", lblPrice.Text });

        }
        if (Found == true)
        {

            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            {
                if (Basket_DataTable.Rows[i]["id"].ToString() == lblMenuSubItemId.Text)
                {
                    int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) + 1;
                    Basket_DataTable.Rows[i]["Qty"] = newqty;
                    Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString()) ;
                }
            }

        }

        grdcart.DataSource = Basket_DataTable;
        grdcart.DataBind();
        GetTotal(Basket_DataTable);
        Session["basket"] = Basket_DataTable;
    }

    protected void grdcart_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        Label lblid = (Label)row.FindControl("lblid");
        Label lblitemname = (Label)row.FindControl("lblitemname");
        Label lblQty = (Label)row.FindControl("lblQty");
        Label lblTotal = (Label)row.FindControl("lblTotal");

        if (e.CommandName.ToString() == "plus")
        {
            string id = lblid.Text;
            string name = lblitemname.Text;
            string price = Convert.ToString(Convert.ToDecimal(lblTotal.Text));

        

                for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
                {
                    if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text)
                    {
                        int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) + 1;
                        Basket_DataTable.Rows[i]["Qty"] = newqty;
                        Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString()) ;
                    }
                }
            grdcart.DataSource = Basket_DataTable;
            grdcart.DataBind();
            GetTotal(Basket_DataTable);
            Session["basket"] = Basket_DataTable;

        }
        if (e.CommandName.ToString() == "minus")
        {
            string id = lblid.Text;
            string name = lblitemname.Text;
            string price = Convert.ToString(Convert.ToDecimal(lblTotal.Text));
            for (int i = 0; i < Basket_DataTable.Rows.Count; i++)
            {
                if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text && Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"].ToString()) > 1)
                {
                    int newqty = Convert.ToInt32(Basket_DataTable.Rows[i]["Qty"]) - 1;
                    Basket_DataTable.Rows[i]["Qty"] = newqty;
                    Basket_DataTable.Rows[i]["Total"] = Convert.ToDecimal(Basket_DataTable.Rows[i]["Total"].ToString()) ;
                    lbltotal.Text = Convert.ToString(Convert.ToDecimal(lbltotal.Text) - Convert.ToDecimal(price));
                }
                else
                {
                    if (Basket_DataTable.Rows[i]["id"].ToString() == lblid.Text)
                        Basket_DataTable.Rows.Remove(Basket_DataTable.Rows[i]);
                }
            }
            grdcart.DataSource = Basket_DataTable;
            grdcart.DataBind();
            GetTotal(Basket_DataTable);
            Session["basket"] = Basket_DataTable;
        }
    }

    private void GetTotal(DataTable dt)
    {
        decimal tot = 0, min;


        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tot = tot + Convert.ToDecimal(dt.Rows[i]["Total"].ToString()) * Convert.ToDecimal(dt.Rows[i]["Qty"].ToString());
            }
            min = Convert.ToDecimal(lblminimumorder.Text);
            if (tot >= min)
            {
                btncheckout.Enabled = true;
            }
            else
            {
                btncheckout.Enabled = false;
            }
            lbltotal.Text = Convert.ToString(tot);
        }
        else
        {
            lbltotal.Text = "";
            btncheckout.Enabled = false;
        }
    }
    protected void btncheckout_Click1(object sender, EventArgs e)
    {
      

        if (Session["UserId"] == null)
        {
            //Response.Redirect("Register.aspx?pg=checkout&key=" + Request.QueryString["key"].ToString()+"&rn="+ Request.QueryString["rn"].ToString()+"&city=" + Request.QueryString["city"].ToString() +"&area=" + Request.QueryString["area"].ToString()+"&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");
            Response.Redirect("Register.aspx?key=" + Request.QueryString["key"].ToString() + "&rn=" + Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");

        }
        else
        {
            Response.Redirect("CheckOut.aspx?key=" + Request.QueryString["key"].ToString() +"&rn="+ Request.QueryString["rn"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "&ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&or=" + Request.QueryString["or"].ToString() + "");

        }
    }

    [WebMethod]
    public static string GetReview(int pageIndex)
    {
        return GetReviewData(pageIndex).GetXml();
    }

    public static DataSet GetReviewData(int pageIndex)
    {
        BL_Restaurants bl_restaurant = new BL_Restaurants();

        DataSet ds = bl_restaurant.GetReviewData(bl_restaurant, pageIndex , restoid);
        return ds;
    }

    protected void lnkcityname_Click(object sender, EventArgs e)
    {

        Response.Redirect("Restaurants.aspx?ct=" + Request.QueryString["ct"].ToString() + "&at=" + Request.QueryString["at"].ToString() + "&city=" + Request.QueryString["city"].ToString() + "&area=" + Request.QueryString["area"].ToString() + "");
        
    }

    
}

