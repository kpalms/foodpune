﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using System.Collections.Concurrent;
[HubName("OrderNotificationHub")]



/// <summary>
/// Summary description for OrderNotification
/// </summary>
public class OrderNotificationHub : Hub
{
    private static readonly ConcurrentDictionary<string, User> Users
     = new ConcurrentDictionary<string, User>(StringComparer.InvariantCultureIgnoreCase);
    public void send(string message)
    {
        Clients.All.addMessage(message);
    }

    public void Send(string to, string message)
    {

        User receiver;
        if (Users.TryGetValue(to.Trim(), out receiver))
        {

            User sender = GetUser(Context.RequestCookies["ASP.NET_SessionId"].Value);

            IEnumerable<string> allReceivers;
            lock (receiver.ConnectionIds)
            {
                lock (sender.ConnectionIds)
                {

                    //  allReceivers = receiver.ConnectionIds.Concat(sender.ConnectionIds);
                    allReceivers = receiver.ConnectionIds;
                }
            }

            foreach (var cid in allReceivers)
            {

                Clients.Client(cid).received(new { sender = sender.Name, message = message, isPrivate = true });
            }
        }
    }

    public override Task OnConnected()
    {
        //  Program.MainForm.WriteToConsole("Client connected: " + Context.ConnectionId);

        string userName = Context.RequestCookies["ASP.NET_SessionId"].Value;
        string connectionId = Context.ConnectionId;
        var user = Users.GetOrAdd(userName, _ => new User
        {
            Name = userName,
            ConnectionIds = new HashSet<string>()
        });

        lock (user.ConnectionIds)
        {

            user.ConnectionIds.Add(connectionId);
            // // broadcast this to all clients other than the caller
            // Clients.AllExcept(user.ConnectionIds.ToArray()).userConnected(userName);
            // Or you might want to only broadcast this info if this 
            // is the first connection of the user
            if (user.ConnectionIds.Count == 1)
            {
                Clients.Others.userConnected(userName);
            }
        }
        return base.OnConnected();
    }
    public override Task OnDisconnected()
    {
        //  Program.MainForm.WriteToConsole("Client disconnected: " + Context.ConnectionId);
        return base.OnDisconnected();
    }


    public List<string> GetConnectedUsers()
    {
        List<string> loggedinUsers = new List<string>();

        foreach (KeyValuePair<string, User> u in Users)
        {

            loggedinUsers.Add(u.Key);

        }


        return loggedinUsers;
    }


    private User GetUser(string username)
    {

        User user;
        Users.TryGetValue(username, out user);

        return user;
    }

    public static void SendOrderNotification(string user, string orderNotification, int orderid)
    {
        var hubContext = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<OrderNotificationHub>();
        User receiver;
        if (Users.TryGetValue(user.Trim(), out receiver))
        {
            IEnumerable<string> receivers = receiver.ConnectionIds;
            foreach (var cid in receivers)
            {
                hubContext.Clients.Client(cid).received(new { sender = user, message = orderNotification, orderid = orderid, isPrivate = true });
            }
        }
    }
}


public class User
{
    public string Name { get; set; }
    public HashSet<string> ConnectionIds { get; set; }
    public string LoginName { get; set; }
}