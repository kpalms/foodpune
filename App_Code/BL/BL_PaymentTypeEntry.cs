﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_PaymentTypeEntry
/// </summary>
public class BL_PaymentTypeEntry
{
	public BL_PaymentTypeEntry()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_PaymentTypeEntry dl_PaymentTypeEntry = new DL_PaymentTypeEntry();

    private string mode;
    private int paymentteid;
    private int paymenttypeid;
    private int restaurantid;


    public int PaymentTEId
    {
        get { return paymentteid; }
        set { paymentteid = value; }
    }

    public int PaymentTypeId
    {
        get { return paymenttypeid; }
        set { paymenttypeid = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

   
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public void PaymentTypeEntry(BL_PaymentTypeEntry bl_PaymentTypeEntry)
    {
        dl_PaymentTypeEntry.PaymentTypeEntry(bl_PaymentTypeEntry);

    }

    public DataTable SelectPaymentTypeEntry(BL_PaymentTypeEntry bl_PaymentTypeEntry)
    {
        DataTable dt = dl_PaymentTypeEntry.SelectPaymentTypeEntry(bl_PaymentTypeEntry);
        return dt;
    }
}