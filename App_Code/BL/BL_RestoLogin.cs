﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for BL_RestoLogin
/// </summary>
public class BL_RestoLogin
{
	public BL_RestoLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_RestoLogin dl_restologin = new DL_RestoLogin();

    private string username;
    private string password;
    private int restaurantid;
    private int active;
    private int status;
    private int itemid;


    public string UserName
    {
        get { return username; }
        set { username = value; }

    }
    public string Password
    {
        get { return password; }
        set { password = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public int Status
    {
        get { return status; }
        set { status = value; }
    }


    public int Active
    {
        get { return active; }
        set { active = value; }

    }
    public int ItemId
    {
        get { return itemid; }
        set { itemid = value; }
    }

    public string OwnerLogin(BL_RestoLogin bl_restologin)
    {
        string RestaurantId = dl_restologin.OwnerLogin(bl_restologin);
        return RestaurantId;
    }

    public DataTable SelectOwnerCatItem(BL_RestoLogin bl_restologin)
    {
        DataTable dt = dl_restologin.SelectOwnerCatItem(bl_restologin);
        return dt;
    }




    public DataSet SelectAllRestaurantDetails(BL_RestoLogin bl_restologin)
    {
        DataSet ds = dl_restologin.SelectAllRestaurantDetails(bl_restologin);
        return ds;
    }

    public void OwnerItemActiveDeactive(BL_RestoLogin bl_restologin)
    {

        dl_restologin.OwnerItemActiveDeactive(bl_restologin);
    }

    public void OwnerStatus(BL_RestoLogin bl_restologin)
    {
        dl_restologin.OwnerStatus(bl_restologin);
    }



}