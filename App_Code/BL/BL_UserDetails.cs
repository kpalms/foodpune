﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BL_UserDetails-
/// </summary>
public class BL_UserDetails
{
    DL_UserDetails dl_userdetails = new DL_UserDetails();

    private int userid;
    private int gender;
    private DateTime date;
    private string firstname;
    private string middlename;
    private string lastname;
    private string email;
    private string password;
    private string mobile;
    private string mode;
    private int areaid;
    private int cityid;
    private string newpassword;
    private string address;


    public string NewPassword
    {
        get { return newpassword; }
        set { newpassword = value; }
    }

    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }

    public int Gender
    {
        get { return gender; }
        set { gender = value; }
    }


    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }
    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }
    public string FirstName
    {
        get { return firstname; }
        set { firstname = value; }
    }

    public string MiddleName
    {
        get { return middlename; }
        set { middlename = value; }
    }
    public string LastName
    {
        get { return lastname; }
        set { lastname = value; }
    }

    public string Mobile
    {
        get { return mobile; }
        set { mobile = value; }
    }

    public string Email
    {
        get { return email; }
        set { email = value; }
    }

    public string Password
    {
        get { return password; }
        set { password = value; }
    }

	public BL_UserDetails()
	{
		
	}

    public int InsertUserDetails(BL_UserDetails bl_userdetails)
    {
      int userid=  dl_userdetails.InsertUserDetails(bl_userdetails);
      return userid;
    }
    public void ModifyUserDetails(BL_UserDetails bl_userdetails)
    {
        dl_userdetails.ModifyUserDetails(bl_userdetails);
    }

    

    public DataTable SelectMyProfile(BL_UserDetails bl_userdetails)
    {
        DataTable dt = dl_userdetails.SelectMyProfile(bl_userdetails);
        return dt;
    }

    public DataTable CheckUserLogin(BL_UserDetails bl_userdetails)
    {
        DataTable dt = dl_userdetails.CheckUserLogin(bl_userdetails);
        return dt;
    }
    public DataTable CheckUserEmailValidation(BL_UserDetails bl_userdetails)
    {
        DataTable dt = dl_userdetails.CheckUserEmailValidation(bl_userdetails);
        return dt;
    }
    
    public void ChangeUserPassword(BL_UserDetails bl_userdetails)
    {
         dl_userdetails.ChangeUserPassword(bl_userdetails);
      
    }


    public DataTable CheckUserPassword(BL_UserDetails bl_userdetails)
    {
        DataTable dt = dl_userdetails.CheckUserPassword(bl_userdetails);
        return dt;
    }

}