﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_PaymentType
/// </summary>
public class BL_PaymentType
{
	public BL_PaymentType()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_PaymentType dl_PaymentType = new DL_PaymentType(); 

    private string mode;
    private string paymenttype;
    private int paymenttypeid;

    public int PaymentTypeId
    {
        get { return paymenttypeid; }
        set { paymenttypeid = value; }
    }

    public string PaymentType
    {
        get { return paymenttype; }
        set { paymenttype = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }


    public void PaymentTypes(BL_PaymentType bl_payment)
    {
        dl_PaymentType.PaymentTypes(bl_payment);

    }

    public DataTable SelectPaymentType()
    {
        DataTable dt = dl_PaymentType.SelectPaymentType();
        return dt;
    }


   
}