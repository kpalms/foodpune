﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_MenuItem
/// </summary>
public class BL_MenuItem
{
	public BL_MenuItem()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_MenuItem dl_MenuItem = new DL_MenuItem();

    private string item;
    private string mode;
    private string itemdescription;
    private int categoryid;
    private int active;
    private int itemid;

    private int customisation;
    private string image;
    private decimal price;
    private int type;
    private int restaurantid;
    public int Type
    {
        get { return type; }
        set { type = value; }
    }
    //menusubitems
    private int mensubitemid;
    private string name;

    public int MenuSubItemId
    {
        get { return mensubitemid; }
        set { mensubitemid = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }


    //menuoption

    private int menuoptionid;


    public int MenuOptionId
    {
        get { return menuoptionid; }
        set { menuoptionid = value; }
    }

    //menuextra

    private int menuextraid;


    public int MenuExtraId
    {
        get { return menuextraid; }
        set { menuextraid = value; }
    }


    public int CategoryId
    {
        get { return categoryid; }
        set { categoryid = value; }
    }

    public decimal Price
    {
        get { return price; }
        set { price = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public int ItemId
    {
        get { return itemid; }
        set { itemid = value; }
    }

    public string Image
    {
        get { return image; }
        set { image = value; }
    }

    public string ItemDescription
    {
        get { return itemdescription; }
        set { itemdescription = value; }
    }
    public string Item
    {
        get { return item; }
        set { item = value; }
    }
    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public int Customisation
    {
        get { return customisation; }
        set { customisation = value; }
    }
    
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public void MenuItem(BL_MenuItem bl_MenuItem)
    {
        dl_MenuItem.MenuItem(bl_MenuItem);

    }



    public DataTable SelectMenuItem(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenuItem(bl_MenuItem);
        return dt;
    }


    //menusubitems

    public void MenusubItem(BL_MenuItem bl_MenuItem)
    {
        dl_MenuItem.MenusubItem(bl_MenuItem);

    }

    public DataTable SelectMenusubItem(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenusubItem(bl_MenuItem);
        return dt;
    }

    //public DataTable SelectItemIdItem(BL_MenuItem bl_MenuItem)
    //{
    //    DataTable dt = dl_MenuItem.SelectItemIdItem(bl_MenuItem);
    //    return dt;
    //}

    //menuoption

    public void AddMenuOption(BL_MenuItem bl_MenuItem)
    {
        dl_MenuItem.AddMenuOption(bl_MenuItem);

    }

    public DataTable SelectMenuOption(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenuOption(bl_MenuItem);
        return dt;
    }


    public DataTable SelectMenuSubItemIdName(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenuSubItemIdName(bl_MenuItem);
        return dt;
    }

    //menuextra
    public void AddMenuExtra(BL_MenuItem bl_MenuItem)
    {
        dl_MenuItem.AddMenuExtra(bl_MenuItem);

    }

    public DataTable SelectMenuExtra(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenuExtra(bl_MenuItem);
        return dt;
    }

    public DataTable SelectMenuSubItemByItemId(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_MenuItem.SelectMenuSubItemByItemId(bl_MenuItem);
        return dt;
    }

   }