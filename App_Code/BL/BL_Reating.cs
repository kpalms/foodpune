﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Reating
/// </summary>
public class BL_Reating
{

    DL_Reating dl_reating = new DL_Reating();
	public BL_Reating()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private int ratingid;
    private int userid;
    private int restaurantid;
    private int rating;


    public int Rating
    {
        get { return rating; }
        set { rating=value;}
    }

    public int RatingId
    {
        get { return ratingid; }
        set { ratingid = value; }
    }

    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public void Reating(BL_Reating bl_reating)
    {
        dl_reating.Reating(bl_reating);
    }
}