﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BL_Review
/// </summary>
public class BL_Review
{
    DL_Review dl_review = new DL_Review();


	public BL_Review()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private DateTime date;
    private int reviewid;
    private int userid;
    private string comments;
    private int active;
    private int restaurantid;

    public DateTime Date
    {
        get { return date;}
        set { date = value; }
    }
    public int ReviewId
    {
        get { return reviewid; }
        set { reviewid = value; }
    }
    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }
     public string Comments
    {
        get { return comments; }
        set { comments = value; }
    }
     public int Active
     {
         get { return active; }
         set { active = value; }
     }


     public int RestaurantId
     {
         get { return restaurantid; }
         set { restaurantid = value; }
     }


    public void Review(BL_Review bl_review)
    {
        dl_review.Review(bl_review);
    }


    public DataTable SelectMyReview(BL_Review bl_review)

    {
        DataTable dt = dl_review.SelectMyReview(bl_review);
        return dt;
    }


    //public DataTable SelectReviewByRestoId(BL_Review bl_review)
    //{
    //    DataTable dt = dl_review.SelectReviewByRestoId(bl_review);
    //    return dt;
    //}


}