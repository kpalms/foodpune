﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BL_DeliveryAddress
/// </summary>
public class BL_DeliveryAddress
{
    DL_DeliveryAddress dl_deliveryaddress = new DL_DeliveryAddress();

  

    private string mode;
    private DateTime date;
    private string landmark;
    private string deliveryinstruction;
    private int deliveryid;
    private int userid;
    private string company;
    private string flatno;
    private string appartment;
    private int cityid;
    private int areaid;
    private string address;
    private string alternatecontactno;
    private string deliverynote;
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }

    public string Landmark
    {
        get { return landmark; }
        set { landmark = value; }
    }

    public string DeliveryInstruction
    {
        get { return deliveryinstruction; }
        set { deliveryinstruction = value; }
    }

    public int DeliveryId
    {
        get { return deliveryid; }
        set { deliveryid = value; }
    }
     public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }
      public string Company
    {
        get { return company; }
        set { company = value; }
    }
     public string FlatNo
    {
        get { return flatno; }
        set { flatno = value; }
    }
       public string Appartment
    {
        get { return appartment; }
        set { appartment = value; }
    }
     public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }
    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }
     public string Address
    {
        get { return address; }
        set { address = value; }
    }

     public string AlternateContactNo
     {
         get { return alternatecontactno; }
         set { alternatecontactno = value; }
     }

     public string DeliveryNote
    {
        get { return deliverynote; }
        set { deliverynote = value; }
    }


    public int InsertDeliveryAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
       int deliveryid=dl_deliveryaddress.InsertDeliveryAddress(bl_deliveryaddress);
       return deliveryid;
    }
    public DataTable SelectMyAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
        DataTable dt = dl_deliveryaddress.SelectMyAddress(bl_deliveryaddress);
        return dt;
    }
    public DataTable SelectAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
        DataTable dt = dl_deliveryaddress.SelectAddress(bl_deliveryaddress);
        return dt;
    }

}