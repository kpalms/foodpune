﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BL_Coupen
/// </summary>
public class BL_Coupen
{
    DL_Coupen dl_coupen = new DL_Coupen();

    private string code;
    private int discountid;
    private int coupenid;
    private string mode;
    private int restaurantid;
    public string Code
    {
        get { return code; }
        set { code = value; }
    }

    public int DiscountId
    {
        get { return discountid; }
        set { discountid = value; }
    }
    public int CoupenId
    {
        get { return coupenid; }
        set { coupenid = value; }
    }
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

	public BL_Coupen()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void InsertCoupen(BL_Coupen bl_coupen)
    {
        dl_coupen.InsertCoupen(bl_coupen);
        
    }
    public DataTable BindCoupanGRD(BL_Coupen bl_coupen)
    {
        DataTable dt = dl_coupen.BindCoupanGRD(bl_coupen);
        return dt;
    }


    public DataTable SelectDiscountOffersNameId(BL_Coupen bl_coupen)
    {
        DataTable dt = dl_coupen.SelectDiscountOffersNameId(bl_coupen);
        return dt;
    }
}