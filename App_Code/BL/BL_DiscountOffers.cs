﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_DiscountOffers
/// </summary>
public class BL_DiscountOffers
{
	public BL_DiscountOffers()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_DiscountOffers dl_disountoffers = new DL_DiscountOffers();

    private DateTime date;
    private int discounttype;
    private DateTime discountstart;
    private DateTime discountend;
    private string mode;
    private string offername;
    private int discountid;
    private int restaurantid;
    private int discountoccurance;
    private decimal discountamount;
    
    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }

    

    public int DiscountType
    {
        get { return discounttype; }
        set { discounttype = value; }
    }

    public DateTime DiscountStart
    {
        get { return discountstart; }
        set { discountstart = value; }
    }

    public DateTime DiscountEnd
    {
        get { return discountend; }
        set { discountend = value; }
    }

    public int DiscountId
    {
        get { return discountid; }
        set { discountid = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public int DiscountOccurance
    {
        get { return discountoccurance; }
        set { discountoccurance = value; }
    }

    public decimal DiscountAmount
    {
        get { return discountamount; }
        set { discountamount = value; }
    }


    public string OfferName
    {
        get { return offername; }
        set { offername = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public void DiscountOffer(BL_DiscountOffers bl_discountoffer)
    {
        dl_disountoffers.DiscountOffer(bl_discountoffer);

    }

    public DataTable SelectDiscountOffer(BL_DiscountOffers bl_discountoffer)
    {
        DataTable dt = dl_disountoffers.SelectDiscountOffer(bl_discountoffer);
        return dt;
    }


    public DataTable SelectRestoIdName()
    {
        DataTable dt = dl_disountoffers.SelectRestoIdName();
        return dt;
    }
   
}