﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_DeliveryLocation
/// </summary>
public class BL_DeliveryLocation
{
	public BL_DeliveryLocation()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_DeliveryLocation dl_DeliveryLocation = new DL_DeliveryLocation();

    private int deliveryid;
    private int restaurantid;
    private int areaid;
    private string mode;
    private int cityid;

    public int DeliveryId
    {
        get { return deliveryid; }
        set { deliveryid = value; }
    }
    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }
    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }

    public string Mode
    {
        get { return mode ;}
        set { mode=value ;}
    }

    public int CityId
    {
        get {return cityid ;}
        set { cityid = value;}
    }

    public void DeliveryLocation(BL_DeliveryLocation bl_DeliveryLocation)
    {
        dl_DeliveryLocation.DeliveryLocation(bl_DeliveryLocation);

    }

    public DataTable SelectDeliveryLocation(BL_DeliveryLocation bl_DeliveryLocation)
    {
        DataTable dt = dl_DeliveryLocation.SelectDeliveryLocation(bl_DeliveryLocation);
        return dt;
    }

}