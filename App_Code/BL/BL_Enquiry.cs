﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for BL_Enquiry
/// </summary>
public class BL_Enquiry
{
    DL_Enquiry dl_enquiry = new DL_Enquiry();
	public BL_Enquiry()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private DateTime date;
    private int enquiryid;
    private string name;
    private string mode;
    private string email;
    private string phone;
    private string mobile;
    private string restaurantname;
    private int cityid;
    private int areaid;
    private string comments;

    public DateTime Date
    {
        get { return date; }
        set { date = value; }

    }
    public int EnquiryId
    {
        get { return enquiryid; }
        set { enquiryid = value; }
    }
    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string Phone
    {
        get { return phone; }
        set { phone = value; }
    }
    public string Mobile
    {
        get { return mobile; }
        set { mobile = value; }
    }
    public string RestaurantName
    {
        get { return restaurantname; }
        set { restaurantname = value; }
    }
    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }
    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }
    public string Comments
    {
        get { return comments; }
        set { comments = value; }
    }
    
        
        public void insertdata(BL_Enquiry bl_enquiry)
        {
            dl_enquiry.insertdata(bl_enquiry);
        }

}