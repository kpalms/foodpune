﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_RestaurantCondition
/// </summary>
public class BL_RestaurantCondition
{
	public BL_RestaurantCondition()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_RestaurantCondition dl_restaurantconditions = new DL_RestaurantCondition();

    private int restoconditionid;
    private int restaurantid;
    private decimal minimumorders;
    private decimal deliverycharges;
    private int pickuptime;
    private int deliverytime;
    private string mode;


    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }
    public int RestoConditionId
    {
        get { return restoconditionid; }
        set { restoconditionid = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public decimal MinimumOrders
    {
        get { return minimumorders; }
        set { minimumorders = value; }
    }

    public decimal DeliveryCharges
    {
        get { return deliverycharges; }
        set { deliverycharges = value; }
    }

    public int PickupTime
    {
        get { return pickuptime; }
        set { pickuptime = value; }
    }

    public int DeliveryTime
    {
        get { return deliverytime; }
        set { deliverytime = value; }
    }


    public void InsertRestoConditions(BL_RestaurantCondition bl_restaurantconditions)
    {
        dl_restaurantconditions.InsertRestoConditions(bl_restaurantconditions);
    }

    public DataTable SelectRestoConditions(BL_RestaurantCondition bl_restaurantconditions)
    {
        DataTable dt = dl_restaurantconditions.SelectRestoConditions(bl_restaurantconditions);

        return dt;
    }
}