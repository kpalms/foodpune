﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Order
/// </summary>
public class BL_Order
{
	public BL_Order()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Order dl_order = new DL_Order();


    private int orderid;
    private int userid;
    private int restaurantid;
    private int billno;
    private int discountid;
    private int orderdetailsid;

    private int active;
    private string comments;




    private int qty;
    private decimal price;
    private decimal deliverycharges;
    private string item;
    private string orderstatus;
    private int deliverytype;
    private DateTime date;
    private DateTime todate;
    private DateTime formdate;
    private DateTime deliveryttime;
    private int deliveryid;

    public int OrderId
    {
        get { return orderid ;}
        set { orderid = value; }
    }

    public string Comments
    {
        get { return comments; }
        set { comments = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public int BillNo
    {
        get { return billno; }
        set { billno = value; }
    }

    public int DiscountId
    {
        get { return discountid; }
        set { discountid = value; }
    }

    public int Qty
    {
        get { return qty; }
        set { qty = value; }
    }

    
    public int OrderDetailsId
    {
        get { return orderdetailsid; }
        set { orderdetailsid = value; }
    }


    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }

    public DateTime ToDate
    {
        get { return todate; }
        set { todate = value; }
    }

    public DateTime FromDate
    {
        get { return formdate; }
        set { formdate = value; }
    }

    public int DeliveryType
    {
        get { return deliverytype; }
        set { deliverytype = value; }
    }
    public int DeliveryId
    {
        get { return deliveryid; }
        set { deliveryid = value; }
    }
    public DateTime DeliveryTime
    {
        get { return deliveryttime; }
        set { deliveryttime = value; }
    }
    public string OrderStatus
    {
        get { return orderstatus; }
        set { orderstatus = value; }
    }

    public string Item
    {
        get { return item; }
        set { item = value; }
    }

    public decimal Price
    {
        get { return price; }
        set { price = value; }
    }
    public decimal DeliveryCharges
    {
        get { return deliverycharges; }
        set { deliverycharges = value; }
    }

    public int Orders(BL_Order bl_order)
    {
       int orderid= dl_order.Orders(bl_order);
       return orderid;
    }

    public void OrderDetails(BL_Order bl_order)
    {
        dl_order.OrderDetails(bl_order);
    }

    public void UpdateNewOrder(BL_Order bl_order)
    {
        dl_order.UpdateNewOrder(bl_order);
    }

    public DataSet SelectAllOrderStatus(BL_Order bl_order)
    {

        DataSet ds = dl_order.SelectAllOrderStatus(bl_order);
        return ds;
    }

    public void InsertReviewsByRestoId(BL_Order bl_order)
    {
        dl_order.InsertReviewsByRestoId(bl_order);
    }

    public DataTable GetRestoDetailsByUserId(BL_Order bl_order)
    {
        DataTable dt = dl_order.GetRestoDetailsByUserId(bl_order);
        return dt;
    }

    public DataSet GetOrderDetails(BL_Order bl_order)
    {
        DataSet ds = dl_order.GetOrderDetails(bl_order);
        return ds;
    }


    public DataTable SelectRecentOrder(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectRecentOrder(bl_order);
        return dt;
    }

    public DataTable SelectNewOrder(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectNewOrder(bl_order);
        return dt;
    }

    public DataTable SelectLaterOrderById(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectLaterOrderById(bl_order);
        return dt;
    }
    
    public string GetNewOrderCount(BL_Order bl_order)
    {
       string count= dl_order.GetNewOrderCount(bl_order);
       return count;
    }

    public DataTable SelectResturantOrderDetailsByAccept(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectResturantOrderDetailsByAccept(bl_order);
        return dt;
    }
    

    public DataSet GetOrderDetailsByOrderId(BL_Order blorder)
    {
        DataSet ds = dl_order.GetOrderDetailsByOrderId(blorder);
        return ds;
    }

    public DataTable SelectOrderHistory(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectOrderHistory(bl_order);
        return dt;
    }

    public void UpdateDiscountOffers(BL_Order bl_order)
    {
        dl_order.UpdateDiscountOffers(bl_order);
    }

    public DataTable GetDiscountById(BL_Order bl_order)
    {
        DataTable dt = dl_order.GetDiscountById(bl_order);
        return dt;
    }


    public DataSet SelectTodayOrderByAccept(BL_Order blorder)
    {
        DataSet ds = dl_order.SelectTodayOrderByAccept(blorder);
        return ds;
    }


    public DataSet SelectTodayOrderByPlaceOrder(BL_Order blorder)
    {
        DataSet ds = dl_order.SelectTodayOrderByPlaceOrder(blorder);
        return ds;
    }

    public DataSet SelectTodayOrderByDisaccept(BL_Order blorder)
    {
        DataSet ds = dl_order.SelectTodayOrderByDisaccept(blorder);
        return ds;
    }

    public DataTable SelectResturantOrderDetailsByDisaccept(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectResturantOrderDetailsByDisaccept(bl_order);
        return dt;
    }


    public DataTable SelectResturantOrderDetailsByPlaceOrder(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectResturantOrderDetailsByPlaceOrder(bl_order);
        return dt;
    }



    public DataSet SelectTodayOrderUserCancel(BL_Order blorder)
    {
        DataSet ds = dl_order.SelectTodayOrderUserCancel(blorder);
        return ds;
    }

    public DataSet SelectLaterOrder(BL_Order blorder)
    {
        DataSet ds = dl_order.SelectLaterOrder(blorder);
        return ds;
    }
    
    public DataTable SelectResturantOrderDetailsByUserCancel(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectResturantOrderDetailsByUserCancel(bl_order);
        return dt;
    }

    public DataTable SelectResturantOrderDetailsByLater(BL_Order bl_order)
    {
        DataTable dt = dl_order.SelectResturantOrderDetailsByLater(bl_order);
        return dt;
    }
    
    public void UpdateCall(string status,string orderid)
    {
        dl_order.UpdateCall(status, orderid);
    }

    public void RejectCall(string status, string orderid,string resion)
    {
        dl_order.RejectCall(status, orderid, resion);
    }

}