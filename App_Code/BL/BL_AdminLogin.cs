﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_AdminLogin
/// </summary>
public class BL_AdminLogin
{
	public BL_AdminLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_AdminLogin dl_adminlogin = new DL_AdminLogin();

    private int loginid;
    private string username;
    private string password;

   public int LoginId
   {
       get { return loginid; }
       set { loginid = value; }
   }


   public string UserName
   {
       get { return username; }
       set { username = value; }
   }

   public string Password
   {
       get { return password; }
       set { password = value; }
   }


   public DataTable AdminLogin(BL_AdminLogin bl_AdminLogin)
   {
       DataTable dt = dl_adminlogin.AdminLogin(bl_AdminLogin);
       return dt;

   }


   public DataTable SelectAdminProfile(BL_AdminLogin bl_AdminLogin)
   {
       DataTable dt = dl_adminlogin.SelectAdminProfile(bl_AdminLogin);
       return dt;
   }

}