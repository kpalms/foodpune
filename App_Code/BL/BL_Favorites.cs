﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for BL_Favorites
/// </summary>
public class BL_Favorites
{
    DL_Favorites dl_favorites = new DL_Favorites();
	public BL_Favorites()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Favorites dl_favourites = new DL_Favorites();

    private DateTime date;
    private int favoriteid;
    private int restaurantid;
    private int userid;
   

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }
    public int FavoriteId
    {
        get { return favoriteid; }
        set { favoriteid = value; }
    }
    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }
    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }


    public DataTable GetFavouriteByUserId(BL_Favorites bl_favorites)
    {
        DataTable dt = dl_favourites.GetFavouriteByUserId(bl_favorites);
        return dt;
    }

    public void DeleteFavourite(BL_Favorites bl_favorites)
    {
        dl_favourites.DeleteFavourite(bl_favorites);
        
    }
    public void InserFavourite(BL_Favorites bl_favorites)
    {
        dl_favorites.InserFavourite(bl_favorites);

    }



    
    


}