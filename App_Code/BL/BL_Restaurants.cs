﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Restaurants
/// </summary>
public class BL_Restaurants
{
	public BL_Restaurants()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    DL_Restaurants dl_restaurants = new DL_Restaurants();

    private string mode;
    private DateTime date;
    private string restaurantname;
    private int restaurantid;
    private int cityid;
    private int areaid;
    private int ownerid;
    private string description;
    private string address;
    private string phoneno;
    private string mobileno1;
    private string mobileno2;
    private string contactperson1;
    private string contactperson2;
    private string email;
    private string image;
    private string password;
    private int userid;
    private string nametext;
    private int sortmode;
    private string colname;
    private int restocategoryid;
    private int selectrestocatid;
    private string cuisines;
    private string facility;
    private string resturantcategory;
    private string paymenttypeslist;
    private string locationlist;
    private int active;
    public string ResturantCategory
    {
        get { return resturantcategory; }
        set { resturantcategory = value; }
    }
    public string PaymentTypesList
    {
        get { return paymenttypeslist; }
        set { paymenttypeslist = value; }
    }
    public string LocationList
    {
        get { return locationlist; }
        set { locationlist = value; }
    }
    public string Facility
    {
        get { return facility; }
        set { facility = value; }
    }

    public string Cuisines
    {
        get { return cuisines; }
        set { cuisines = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public int SelectRestoCatId
    {
        get { return selectrestocatid; }
        set { selectrestocatid = value; }
    }

    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }

    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }


    public int UserId
    {
        get { return userid; }
        set { userid = value; }
    }


    public int RestoCategoryId
    {
        get { return restocategoryid; }
        set { restocategoryid = value; }
    }

    public int OwnerId
    {
        get { return ownerid; }
        set { ownerid = value; }
    }
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }
    public string RestaurantName
    {
        get { return restaurantname; }
        set { restaurantname = value; }
    }
    public string Description
    {
        get { return description; }
        set { description = value; }
    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public string Phoneno
    {
        get { return phoneno; }
        set { phoneno = value; }
    }
    public string MobileNo1
    {
        get { return mobileno1; }
        set { mobileno1 = value; }
    }

    public string MobileNo2
    {
        get { return mobileno2; }
        set { mobileno2 = value; }
    }

    public string ContactPerson1
    {
        get { return contactperson1; }
        set { contactperson1 = value; }
    }
    public string ContactPerson2
    {
        get { return contactperson2; }
        set { contactperson2 = value; }
    }
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string Image
    {
        get { return image; }
        set { image = value; }
    }
   
    public string Password
    {
        get { return password; }
        set { password = value; }
    }
    public string NameText
    {
        get { return nametext; }
        set { nametext = value; }
    }
     public int SortMode
    {
        get { return sortmode; }
        set { sortmode = value; }
    }

     public string ColName
     {
         get { return colname; }
         set { colname = value; }
     }


     public int Active
     {
         get { return active; }
         set { active = value; }
     }


    public void Restaurants(BL_Restaurants bl_restaurants)
    {
       dl_restaurants.Restaurants(bl_restaurants);
    }

    public void InsertRestaurantCategoryEntry(BL_Restaurants bl_restaurants)
    {
        dl_restaurants.InsertRestaurantCategoryEntry(bl_restaurants);
    }

    public DataTable SelectRestaurants(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.SelectRestaurants(bl_restaurants);
        return dt;
    }


    public DataTable SelectDeactiveRestaurantDetails(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.SelectDeactiveRestaurantDetails(bl_restaurants);
        return dt;
    }
    public DataTable SelectRestaurantDetailsByRestoId(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.SelectRestaurantDetailsByRestoId(bl_restaurants);
        return dt;
    }
    public DataTable SelectCityAreaWiseResto(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.SelectCityAreaWiseResto(bl_restaurants);
        return dt;
    }


    public DataTable SelectFavorateRestaurant(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.SelectFavorateRestaurant(bl_restaurants);
        return dt;
    }

      public DataTable BindRestoCategory(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.BindRestoCategory(bl_restaurants);
        return dt;
    }

      public DataTable BindRestaurantDDL(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.BindRestaurantDDL(bl_restaurants);
        return dt;
    }

      public DataTable BindRestoCatDDL(BL_Restaurants bl_restaurants)
      {
          DataTable dt = dl_restaurants.BindRestoCatDDL(bl_restaurants);
          return dt;
      }

      public DataSet  BindRestoCategoryByRestoId(BL_Restaurants bl_restaurants)
      {
          DataSet ds = dl_restaurants.BindRestoCategoryByRestoId(bl_restaurants);
          return  ds;
      }

      public DataTable BindRestoFacilities(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_restaurants.BindRestoFacilities(bl_restaurants);
        return dt;
    }

     

      public DataTable SearchRestaurantsBySortMode(BL_Restaurants bl_restaurants)
      {
          DataTable dt = dl_restaurants.SearchRestaurantsBySortMode(bl_restaurants);
          return dt;
      }
      

      public DataTable FilterRestaurants(BL_Restaurants bl_restaurants)
      {
          DataTable dt = dl_restaurants.FilterRestaurants(bl_restaurants);
          return dt;
      }
    
    

     public DataSet SelectAllRestaurantDetails(BL_Restaurants bl_restaurants)

    {
        DataSet ds = dl_restaurants.SelectAllRestaurantDetails(bl_restaurants);
        return ds;
    }


      public DataSet SelectRestoConditionbyId(BL_Restaurants bl_restaurants)
    {
        DataSet dt = dl_restaurants.SelectRestoConditionbyId(bl_restaurants);
        return dt;
    }


      public DataSet GetReviewData(BL_Restaurants bl_restaurants, int pageindex, int restoid)
      {
          DataSet ds = dl_restaurants.GetReviewData(bl_restaurants, pageindex, restoid);
          return ds;
      }



      public DataSet GetRestaurantData(BL_Restaurants bl_restaurants, int val, int cityid, int areaid)
      {
          DataSet ds = dl_restaurants.GetRestaurantData(bl_restaurants, val, cityid, areaid);
          return ds;
      }


      public DataSet SelectRestoCatagorybyId(BL_Restaurants bl_restaurants)
      {
          DataSet dt = dl_restaurants.SelectRestoCatagorybyId(bl_restaurants);
          return dt;
      }

      public void DeleteRestaurant(BL_Restaurants bl_restaurants)
      {
          dl_restaurants.DeleteRestaurant(bl_restaurants);
         
      }
    

}