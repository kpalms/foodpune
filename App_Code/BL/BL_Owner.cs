﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BL_Owner
/// </summary>
public class BL_Owner
{
    DL_Owner dl_owner = new DL_Owner();
 
    private string firstname;
    private string middlename;
    private string lastname;
    private string phoneno;
    private string mobile1;
    private string mobile2;
    private string mobile3;
    private string email;
    private int areaid;
    private int cityid;
    private string address;
    private string image;
    private string password;
    private DateTime date;
    private string mode;
    private int ownerid;


    public int OwnerId
    {
        get { return ownerid; }
        set { ownerid = value; }
    }
    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }
    public string FirstName
    {
        get { return firstname; }
        set { firstname = value; }
    }

    public string MiddleName
    {
        get { return middlename; }
        set { middlename = value; }
    }
    public string LastName
    {
        get { return lastname; }
        set { lastname = value; }
    }

    public string PhoneNo
    {
        get { return phoneno; }
        set { phoneno = value; }
    }
    public string Mobile1
    {
        get { return mobile1; }
        set { mobile1 = value; }
    }

    public string Mobile2
    {
        get { return mobile2; }
        set { mobile2 = value; }
    }
    public string Mobile3
    {
        get { return mobile3; }
        set { mobile3 = value; }
    }

    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }

    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }

    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }

    public string Image
    {
        get { return image; }
        set { image = value; }
    }
   
    public string Password
    {
        get { return password; }
        set { password = value; }
    }

	public BL_Owner()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void InsertOwner(BL_Owner bl_owner)
    {
         dl_owner.InsertOwner(bl_owner);
        
    }
    public DataTable BindOwner()
    {
        DataTable dt = dl_owner.BindOwner();
        return dt;
    }

    //public DataTable SelectCity()
    //{
    //    DataTable dt = dl_owner.SelectCity();
    //    return dt;
    //}
    //public DataTable SelectArea(BL_Owner bl_owner)
    //{
    //    DataTable dt = dl_owner.SelectArea(bl_owner);
    //    return dt;
    //}


    public DataSet SelectOwnerRestoDetailsById(BL_Owner bl_owner)
    {
        DataSet ds = dl_owner.SelectOwnerRestoDetailsById(bl_owner);
        return ds;
    }


    public DataSet getcustomer(BL_Owner bl_owner,int pageindex)
    {
        DataSet ds = dl_owner.getcustomer(bl_owner, pageindex);
        return ds;
    }

    public DataTable SelectOwner(BL_Owner bl_owner)
    {
        DataTable dt = dl_owner.SelectOwner(bl_owner);
        return dt;
    }

    //public DataSet BindOwner(BL_Owner bl_owner, int pageindex)
    //{
    //    DataSet ds = dl_owner.BindOwner(bl_owner, pageindex);
    //    return ds;
    //}

}