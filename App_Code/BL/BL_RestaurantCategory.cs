﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_RestaurantCategory
/// </summary>
public class BL_RestaurantCategory
{
	public BL_RestaurantCategory()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_RestaurantCategory dl_RestaurantCategory = new DL_RestaurantCategory();

    private string mode;
    private string restocategory;
    private int restocategoryid;

    public int RestoCategoryId
    {
        get { return restocategoryid; }
        set { restocategoryid = value; }
    }

    public string RestoCategory
    {
        get { return restocategory; }
        set { restocategory = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }


    public void RestaurantCategory(BL_RestaurantCategory bl_RestaurantCategory)
    {
        dl_RestaurantCategory.RestaurantCategory(bl_RestaurantCategory);

    }

    public DataTable SelectRestaurantCategory(BL_RestaurantCategory bl_RestaurantCategory)
    {
        DataTable dt = dl_RestaurantCategory.SelectRestaurantCategory(bl_RestaurantCategory);
        return dt;
    }
}