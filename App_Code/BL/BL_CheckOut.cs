﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_CheckOut
/// </summary>
public class BL_CheckOut
{
	public BL_CheckOut()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_CheckOut dl_checkout = new DL_CheckOut();
    private int restaurantid;
    private int discountid;
    private string code;
    private DateTime date;

    public string Code
    {
        get { return code; }
        set { code = value; }
    }

    public DateTime Date
    {
        get { return date; }
        set { date = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }


    public int DiscountId
    {
        get { return discountid; }
        set { discountid = value; }
    }


    public DataSet SelectcheckoutinfoByRestoId(BL_CheckOut bl_checkout)
    {
        DataSet ds = dl_checkout.SelectcheckoutinfoByRestoId(bl_checkout);
        return ds;
    }


    public DataTable checkcoupcode(BL_CheckOut bl_checkout)
    {
        DataTable dt = dl_checkout.checkcoupcode(bl_checkout);
        return dt;
    }



  
}