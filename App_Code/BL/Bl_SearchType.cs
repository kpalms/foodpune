﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Bl_SearchType
/// </summary>
public class Bl_SearchType
{
	public Bl_SearchType()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    Dl_SearchType dl_searchtype = new Dl_SearchType();

    private int searchid;
    private int restaurantid;
    private string searchtext;
    private string mode;

    public int SearchId
    {
        get{return searchid;}
        set{searchid=value;}
    }
     public int RestaurantId
    {
        get{return restaurantid;}
        set{restaurantid=value;}
    }
      public string SearchText
    {
        get{return searchtext;}
        set{searchtext=value;}
    }

      public string Mode
      {
          get { return mode; }
          set { mode = value; }
      }


      public void SearchType(Bl_SearchType bl_searchtype)
      {
          dl_searchtype.SearchType(bl_searchtype);

      }

      public DataTable SelectSearchType(Bl_SearchType bl_searchtype)
      {
          DataTable dt = dl_searchtype.SelectSearchType(bl_searchtype);
          return dt;
      }
    
}