﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_City
/// </summary>
public class BL_City
{
	public BL_City()
	{
		//
		// TODO: Add constructor logic here
		//

	}

    DL_City dl_city = new DL_City();

    private string cityname;
    private string mode;
    private int cityid;
    private int active;
    private string textsearch;

    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public string CityName
    {
        get { return cityname; }
        set { cityname = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }
    public string TextSearch
    {
        get { return textsearch; }
        set { textsearch = value; }
    }


    public void City(BL_City bl_city)
    {
        dl_city.City(bl_city);

    }

    public DataTable SelectCity(BL_City bl_city)
    {
        DataTable dt = dl_city.SelectCity(bl_city);
        return dt;
    }

    public DataTable BindCity(BL_City bl_city)
    {
        DataTable dt = dl_city.BindCity(bl_city);
        return dt;
    }
    

}