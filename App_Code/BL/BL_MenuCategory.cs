﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_MenuCategory
/// </summary>
public class BL_MenuCategory
{
	public BL_MenuCategory()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_MenuCategory dl_MenuCategory = new DL_MenuCategory();
    private string category;
    private string mode;
    private int categoryid;
    private int active;
    private int restaurantid;
    private string image;

    public int CategoryId
    {
        get { return categoryid; }
        set { categoryid = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

    public string Image
    {
        get { return image; }
        set { image = value; }
    }

    public string Category
    {
        get { return category; }
        set { category = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public void MenuCategory(BL_MenuCategory bl_MenuCategory)
    {
        dl_MenuCategory.MenuCategory(bl_MenuCategory);

    }

    public DataTable SelectMenuCategory(BL_MenuCategory bl_MenuCategory)
    {
        DataTable dt = dl_MenuCategory.SelectMenuCategory(bl_MenuCategory);
        return dt;
    }

 
}