﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Area
/// </summary>
public class BL_Area
{
	public BL_Area()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Area dl_area = new DL_Area();

    private string areaname;
    private string mode;
    private int cityid;
    private int active;
    private int areaid;
    private string pincode;
    private string textsearch;

    public int CityId
    {
        get { return cityid; }
        set { cityid = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public int AreaId
    {
        get { return areaid; }
        set { areaid = value; }
    }

    public string Pincode
    {
        get { return pincode; }
        set { pincode = value; }
    }

    public string AreaName
    {
        get { return areaname; }
        set { areaname = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }
    public string TextSearch
    {
        get { return textsearch; }
        set { textsearch = value; }
    }
    public void Area(BL_Area bl_area)
    {
        dl_area.Area(bl_area);

    }

    public DataTable SelectArea(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectArea(bl_area);
        return dt;
    }

    public DataTable SelectAreaByCity(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectAreaByCity(bl_area);
        return dt;
    }

    public DataTable bindAreabycityAdmin(BL_Area bl_area)
    {
        DataTable dt = dl_area.bindAreabycityAdmin(bl_area);
        return dt;
    }
    
    public DataTable SelectAreaByCityId(BL_Area bl_area)
    {
        DataTable dt = dl_area.SelectAreaByCityId(bl_area);
        return dt;
    }

    public DataTable GetArea(BL_Area bl_area)
    {
        DataTable dt = dl_area.GetArea(bl_area);
        return dt;
    }
    
}