﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Bl_SelectRestoCategory
/// </summary>
public class Bl_SelectRestoCategory
{
	public Bl_SelectRestoCategory()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int restocategoryid;
    private int restaurantid;

    public int RestoCategoryId
    {
        get{return restocategoryid;}
        set{restocategoryid=value;}
    }
    public int RestaurantId
    {
        get { return restaurantid; }
        set { restaurantid = value; }
    }

}