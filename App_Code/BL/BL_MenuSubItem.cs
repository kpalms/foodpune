﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_MenuSubItem
/// </summary>
public class BL_MenuSubItem
{

    DL_MenuSubItem dl_MenuSubItem = new DL_MenuSubItem();
	public BL_MenuSubItem()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string name;
    private int menusubItemId;
    private int itemId;
    private decimal price;
    private int  active;



    public int MenuSubItemId
    {
        get { return menusubItemId; }
        set { menusubItemId = value; }
    }

    public int ItemId
    {
        get { return itemId; }
        set { itemId = value; }
    }

    public decimal Price
    {
        get { return price; }
        set { price = value; }
    }

    public int Active
    {
        get { return active; }
        set { active = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public DataTable SelectMenuSubItem(BL_MenuSubItem bl_MenuSubItem)
    {
        DataTable dt = dl_MenuSubItem.SelectMenuSubItem(bl_MenuSubItem);
        return dt;
    }

    public DataTable SelectOwnerMenuSubItemByItemId(BL_MenuSubItem bl_MenuSubItem)
    {
        DataTable dt = dl_MenuSubItem.SelectOwnerMenuSubItemByItemId(bl_MenuSubItem);
        return dt;
    }

    

    public DataTable AddOwnerMenuSubItem(BL_MenuSubItem bl_MenuSubItem)
    {
        DataTable dt = dl_MenuSubItem.AddOwnerMenuSubItem(bl_MenuSubItem);
        return dt;
    }
}