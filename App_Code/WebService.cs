﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }


    [WebMethod()]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public List<City> GetMyCity()
    {

        string query = System.Web.HttpContext.Current.Request["query"].ToString();


        List<City> listcity = new List<City>();
        BL_City bl_city = new BL_City();

        bl_city.TextSearch = query;

        DataTable dt = bl_city.SelectCity(bl_city);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            City objst = new City();

            objst.ID = dt.Rows[i]["CityId"].ToString();

            objst.Name = Convert.ToString(dt.Rows[i]["CityName"].ToString());

            listcity.Insert(i, objst);

        }
        return listcity;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public List<Area> GetMyArea()
    {


        List<Area> listarea = new List<Area>();
        BL_Area bl_area = new BL_Area();
        string query = System.Web.HttpContext.Current.Request["query"].ToString();
        string cityid = System.Web.HttpContext.Current.Request["cityid"].ToString();
            bl_area.CityId = Convert.ToInt32(cityid);
        bl_area.TextSearch = query;

        DataTable dt = bl_area.SelectAreaByCity(bl_area);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Area objst = new Area();

            objst.ID = dt.Rows[i]["AreaId"].ToString();

            objst.Name = Convert.ToString(dt.Rows[i]["AreaName"].ToString());

            listarea.Insert(i, objst);

        }

        return listarea;
    }

    [WebMethod()]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string UpdateCall(string status, string orderid)
    {
        BL_Order bl_order = new BL_Order();
        bl_order.UpdateCall(status, orderid);
        string msg = string.Empty;
        msg = "true";
        return msg;
    }

    [WebMethod()]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string RejectCall(string status, string orderid, string resion)
    {
        BL_Order bl_order = new BL_Order();
        bl_order.RejectCall(status, orderid, resion);
        string msg = string.Empty;
        msg = "true";
        return msg;
    }


}


public class City
{
    public City()
    {
    }

    public string Name;
    public string ID;
}

public class Area
{
    public Area()
    {
    }

    public string Name;
    public string ID;
}