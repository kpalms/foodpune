﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Area
/// </summary>
public class DL_Area
{
	public DL_Area()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void Area(BL_Area bl_area)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_area.Mode);
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_area.AreaId);
        SqlParameter Pincode = new SqlParameter("@Pincode", bl_area.Pincode);
        SqlParameter AreaName = new SqlParameter("@AreaName", bl_area.AreaName);
        SqlParameter Active = new SqlParameter("@Active", bl_area.Active);
        DL_Connection.UseExecuteNonQuery("AddArea", Mode, CityId,AreaId,Pincode, AreaName, Active);
    }

    public DataTable SelectArea(BL_Area bl_area)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectArea");
        return dt;
    }

    public DataTable SelectAreaByCity(BL_Area bl_area)
    {
        SqlParameter TextSearch = new SqlParameter("@TextSearch", bl_area.TextSearch);
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        DataTable dt = dl_connection.UseDataTablePro("SelectAreaByCity", CityId, TextSearch);
        return dt;
    }

    public DataTable bindAreabycityAdmin(BL_Area bl_area)
    {
       
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
        DataTable dt = dl_connection.UseDataTablePro("SelectAreaByCityIdadmin", CityId);
        return dt;
    }
    public DataTable SelectAreaByCityId(BL_Area bl_area)
    {
        
        SqlParameter CityId = new SqlParameter("@CityId", bl_area.CityId);
       // SqlParameter CityId = new SqlParameter("@CityId",5);
        DataTable dt = dl_connection.UseDataTablePro("SelectAreaByCityId", CityId);
        return dt;
    }



    public DataTable GetArea(BL_Area bl_area)
    {
        
        DataTable dt = dl_connection.UseDataTablePro("GetArea");
        return dt;
    }
    

}