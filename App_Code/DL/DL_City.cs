﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_City
/// </summary>
public class DL_City
{
    DL_Connection dl_connection = new DL_Connection();

	public DL_City()
	{
		//
		// TODO: Add constructor logic here
		//
	}

  

    public void City(BL_City bl_city)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_city.Mode);
        SqlParameter CityId = new SqlParameter("@CityId", bl_city.CityId);
        SqlParameter CityName = new SqlParameter("@CityName", bl_city.CityName);
        SqlParameter Active = new SqlParameter("@Active", bl_city.Active);
        DL_Connection.UseExecuteNonQuery("AddCity", Mode, CityId, CityName, Active);
    }

    public DataTable SelectCity(BL_City bl_city)
    {

        SqlParameter TextSearch = new SqlParameter("@TextSearch", bl_city.TextSearch);
        DataTable dt = dl_connection.UseDataTablePro("SelectCity", TextSearch);
        return dt;
    }

    public DataTable BindCity(BL_City bl_city)
    {

        DataTable dt = dl_connection.UseDataTablePro("BindCity");
        return dt;
    }
}