﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for DL_RestoLogin
/// </summary>
public class DL_RestoLogin
{
	public DL_RestoLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();


    public string OwnerLogin(BL_RestoLogin bl_restologin)
    {
        SqlParameter UserName = new SqlParameter("@UserName", bl_restologin.UserName);
        SqlParameter Password = new SqlParameter("@Password", bl_restologin.Password);
        string RestaurantId = dl_connection.UseExcuteScallerPro("OwnerLogIn", UserName, Password);
        return RestaurantId;
    }

    public DataTable SelectOwnerCatItem(BL_RestoLogin bl_restologin)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restologin.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectOwnerCatItem", RestaurantId);
        return dt;
    }

    public void OwnerItemActiveDeactive(BL_RestoLogin bl_restologin)
    {
        SqlParameter Active = new SqlParameter("@Active", bl_restologin.Active);
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_restologin.ItemId);
        DL_Connection.UseExecuteNonQuery("AddOwnerMenuItem", Active, ItemId);
    }

    public DataSet SelectAllRestaurantDetails(BL_RestoLogin bl_restologin)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restologin.RestaurantId);
        DataSet ds = dl_connection.UseDatasetPro("SelectAllRestaurantDetails", RestaurantId);
        return ds;
    }




    public void OwnerStatus(BL_RestoLogin bl_restologin)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restologin.RestaurantId);
        SqlParameter Status = new SqlParameter("@Status", bl_restologin.Status);
        DL_Connection.UseExecuteNonQuery("AddOwnerStatus", RestaurantId, Status);
    }

}