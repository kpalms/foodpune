﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DL_Favorites
/// </summary>
public class DL_Favorites
{
    DL_Connection dl_connection = new DL_Connection();
    
    //BL_Favorites bl_favorites = new BL_Favorites();
	public DL_Favorites()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    public DataTable GetFavouriteByUserId(BL_Favorites bl_favorites)
    {

        SqlParameter UserId = new SqlParameter("@UserId", bl_favorites.UserId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_favorites.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("GetFavouriteByUserId", UserId, RestaurantId);
        return dt;

    }
    public void DeleteFavourite(BL_Favorites bl_favorites)
    {

        SqlParameter UserId = new SqlParameter("@UserId", bl_favorites.UserId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_favorites.RestaurantId);
        dl_connection.UseDataTablePro("DeleteFavourite", UserId,RestaurantId);
      
    }
    public void InserFavourite(BL_Favorites bl_favorites)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_favorites.RestaurantId);
        SqlParameter UserId = new SqlParameter("@UserId", bl_favorites.UserId);
        SqlParameter Date = new SqlParameter("@Date", bl_favorites.Date);
        dl_connection.UseDataTablePro("InserFavourite", RestaurantId, UserId, Date);

    }

    
}