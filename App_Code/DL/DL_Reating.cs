﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Reating
/// </summary>
public class DL_Reating
{

    DL_Connection dl_connection = new DL_Connection();
	public DL_Reating()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void Reating(BL_Reating bl_reating)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_reating.RestaurantId);
        SqlParameter UserId = new SqlParameter("@UserId", bl_reating.UserId);
        SqlParameter Rating = new SqlParameter("@Rating", bl_reating.Rating);
        DL_Connection.UseExecuteNonQuery("AddRating", RestaurantId, UserId, Rating);
    }
}