﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_RestaurantCondition
/// </summary>
public class DL_RestaurantCondition
{
	public DL_RestaurantCondition()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void InsertRestoConditions(BL_RestaurantCondition bl_restaurantconditions)
    {
        SqlParameter RestoConditionId = new SqlParameter("@RestoConditionId", bl_restaurantconditions.RestoConditionId);
        SqlParameter Mode = new SqlParameter("@Mode", bl_restaurantconditions.Mode);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurantconditions.RestaurantId);
        SqlParameter MinimumOrders = new SqlParameter("@MinimumOrders", bl_restaurantconditions.MinimumOrders);
        SqlParameter DeliveryCharges = new SqlParameter("@DeliveryCharges", bl_restaurantconditions.DeliveryCharges);
        SqlParameter PickupTime = new SqlParameter("@PickupTime", bl_restaurantconditions.PickupTime);
        SqlParameter DeliveryTime = new SqlParameter("@DeliveryTime", bl_restaurantconditions.DeliveryTime);
        DL_Connection.UseExecuteNonQuery("AddRestaurantCondition", Mode, RestoConditionId, RestaurantId, MinimumOrders, DeliveryCharges, PickupTime, DeliveryTime);
    }

    public DataTable SelectRestoConditions(BL_RestaurantCondition bl_restaurantconditions)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurantconditions.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantCondition", RestaurantId);
        return dt;
    }

}