﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_CheckOut
/// </summary>
public class DL_CheckOut
{
	public DL_CheckOut()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();

    public DataSet SelectcheckoutinfoByRestoId(BL_CheckOut bl_checkout)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_checkout.RestaurantId);
        DataSet ds = dl_connection.UseDatasetPro("SelectcheckoutinfoByRestoId", RestaurantId);
        return ds;
    }


    public DataTable checkcoupcode(BL_CheckOut bl_checkout)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_checkout.RestaurantId);
        SqlParameter Code = new SqlParameter("@Code", bl_checkout.Code);
        DataTable dt = dl_connection.UseDataTablePro("CheckCoupCode", RestaurantId, Code);
        return dt;

    }




}