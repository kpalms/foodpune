﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

    /// <summary>
    /// Summary description for DL_Enquiry
    /// </summary>
    public class DL_Enquiry
    {
        public DL_Enquiry()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        DL_Connection dl_connection = new DL_Connection();

        public void insertdata(BL_Enquiry bl_enquiry)
        {
            SqlParameter Mode = new SqlParameter("@Mode", bl_enquiry.Mode);
            SqlParameter CityId = new SqlParameter("@CityId", bl_enquiry.CityId);
            SqlParameter AreaId = new SqlParameter("@AreaId", bl_enquiry.AreaId);
            SqlParameter Date = new SqlParameter("@Date", bl_enquiry.Date);
            SqlParameter Name = new SqlParameter("@Name", bl_enquiry.Name);
            SqlParameter Email = new SqlParameter("@Email", bl_enquiry.Email);
            SqlParameter Phone = new SqlParameter("@Phone", bl_enquiry.Phone);
            SqlParameter Mobile = new SqlParameter("@Mobile", bl_enquiry.Mobile);
            SqlParameter RestaurantName = new SqlParameter("@RestaurantName", bl_enquiry.RestaurantName);
            SqlParameter Comments = new SqlParameter("@Comments", bl_enquiry.Comments);
            DL_Connection.UseExecuteNonQuery("AddEnquiry", Mode, CityId, AreaId, Date, Name, Email, Phone, Mobile, RestaurantName, Comments);

        }
    }
