﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_DeliveryLocation
/// </summary>
public class DL_DeliveryLocation
{
	public DL_DeliveryLocation()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void DeliveryLocation(BL_DeliveryLocation bl_DeliveryLocation)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_DeliveryLocation.Mode);
        SqlParameter DeliveryId = new SqlParameter("@DeliveryId",bl_DeliveryLocation.DeliveryId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_DeliveryLocation.AreaId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_DeliveryLocation.RestaurantId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_DeliveryLocation.CityId);
        DL_Connection.UseExecuteNonQuery("AddDeliveryLocation", Mode, DeliveryId, AreaId, RestaurantId, CityId);
    }


    public DataTable SelectDeliveryLocation(BL_DeliveryLocation bl_DeliveryLocation)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectDeliveryLocation");
        return dt;
    }

   
}