﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_MenuItem
/// </summary>
public class DL_MenuItem
{
	public DL_MenuItem()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();

    public void MenuItem(BL_MenuItem bl_MenuItem)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_MenuItem.Mode);
        SqlParameter CategoryId = new SqlParameter("@CategoryId", bl_MenuItem.CategoryId);
        SqlParameter Customisation = new SqlParameter("@Customisation", bl_MenuItem.Customisation);
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuItem.ItemId);
        SqlParameter Item = new SqlParameter("@Item", bl_MenuItem.Item);
        SqlParameter ItemDescription = new SqlParameter("@ItemDescription", bl_MenuItem.ItemDescription);
        SqlParameter Image = new SqlParameter("@Image", bl_MenuItem.Image);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuItem.Active);
        SqlParameter Price = new SqlParameter("@Price", bl_MenuItem.Price);
        SqlParameter Type = new SqlParameter("@Type", bl_MenuItem.Type);
        DL_Connection.UseExecuteNonQuery("AddMenuItem", Mode,Customisation, CategoryId, ItemId, Item, ItemDescription, Image, Active, Price, Type);

    }

    public DataTable SelectMenuItem(BL_MenuItem bl_MenuItem)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_MenuItem.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuItem",RestaurantId);
        return dt;
    }


    //menusubitems

    public void MenusubItem(BL_MenuItem bl_MenuItem)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_MenuItem.Mode);
        SqlParameter Name = new SqlParameter("@Name", bl_MenuItem.Name);
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuItem.ItemId);
        SqlParameter MenuSubItemId = new SqlParameter("@MenuSubItemId", bl_MenuItem.MenuSubItemId);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuItem.Active);
        SqlParameter Price = new SqlParameter("@Price", bl_MenuItem.Price);
        DL_Connection.UseExecuteNonQuery("AddMenuSubItem", Mode, Name, ItemId, MenuSubItemId, Active, Price);
    }

    public DataTable SelectMenusubItem(BL_MenuItem bl_MenuItem)
    {
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuItem.ItemId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuSubItem", ItemId);
        return dt;
    }

    //public DataTable SelectItemIdItem(BL_MenuItem bl_MenuItem)
    //{
    //    DataTable dt = dl_connection.UseDataTablePro("SelectItemIdItem");
    //    return dt;
    //}


    //menuoption
    public void AddMenuOption(BL_MenuItem bl_MenuItem)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_MenuItem.Mode);
        SqlParameter Name = new SqlParameter("@Name", bl_MenuItem.Name);
        SqlParameter MenuOptionId = new SqlParameter("@MenuOptionId", bl_MenuItem.MenuOptionId);
        SqlParameter MenuSubItemId = new SqlParameter("@MenuSubItemId", bl_MenuItem.MenuSubItemId);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuItem.Active);
        DL_Connection.UseExecuteNonQuery("AddMenuOption", Mode, Name, MenuOptionId, MenuSubItemId, Active);
    }

    public DataTable SelectMenuOption(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuOption");
        return dt;
    }

    public DataTable SelectMenuSubItemIdName(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuSubItemIdName");
        return dt;
    }

    //menuextra

    public void AddMenuExtra(BL_MenuItem bl_MenuItem)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_MenuItem.Mode);
        SqlParameter Name = new SqlParameter("@Name", bl_MenuItem.Name);
        SqlParameter MenuExtraId = new SqlParameter("@MenuExtraId", bl_MenuItem.MenuExtraId);
        SqlParameter MenuSubItemId = new SqlParameter("@MenuSubItemId", bl_MenuItem.MenuSubItemId);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuItem.Active);
        SqlParameter Price = new SqlParameter("@Price", bl_MenuItem.Price);
        DL_Connection.UseExecuteNonQuery("AddMenuExtra", Mode, Name, MenuExtraId, MenuSubItemId, Active, Price);
    }

    public DataTable SelectMenuExtra(BL_MenuItem bl_MenuItem)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuExtra");
        return dt;
    }

    public DataTable SelectMenuSubItemByItemId(BL_MenuItem bl_MenuItem)
    {
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuItem.ItemId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuSubItemByItemId", ItemId);
        return dt;
    }
}