﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_PaymentType
/// </summary>
public class DL_PaymentType
{
	public DL_PaymentType()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void PaymentTypes(BL_PaymentType bl_payment)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_payment.Mode);
        SqlParameter PaymentTypeId = new SqlParameter("@PaymentTypeId", bl_payment.PaymentTypeId);
        SqlParameter PaymentType = new SqlParameter("@PaymentType", bl_payment.PaymentType);
        DL_Connection.UseExecuteNonQuery("AddPaymentType", Mode, PaymentTypeId, PaymentType);


    }

    public DataTable SelectPaymentType()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectPaymentType");
        return dt;
    }
}