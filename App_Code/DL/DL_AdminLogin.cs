﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_AdminLogin
/// </summary>
public class DL_AdminLogin
{
    DL_Connection dl_connection = new DL_Connection();
	public DL_AdminLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable AdminLogin(BL_AdminLogin bl_AdminLogin)
    {
        SqlParameter UserName = new SqlParameter("@UserName", bl_AdminLogin.UserName);
        SqlParameter Password = new SqlParameter("@Password", bl_AdminLogin.Password);
        DataTable dt = dl_connection.UseDataTablePro("CheckAdminLogin", UserName, Password);
        return dt;

    }
    public DataTable SelectAdminProfile(BL_AdminLogin bl_AdminLogin)
    {
        SqlParameter LoginId = new SqlParameter("@LoginId", bl_AdminLogin.LoginId);

        DataTable dt = dl_connection.UseDataTablePro("SelectAdminProfile", LoginId);
        return dt;

    }
    

}