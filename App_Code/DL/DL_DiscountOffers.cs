﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_DiscountOffers
/// </summary>
public class DL_DiscountOffers
{
	public DL_DiscountOffers()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void DiscountOffer(BL_DiscountOffers bl_discountoffer)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_discountoffer.Mode);
        SqlParameter OfferName = new SqlParameter("@OfferName", bl_discountoffer.OfferName);
        SqlParameter Date = new SqlParameter("@Date", bl_discountoffer.Date);
        SqlParameter DiscountId = new SqlParameter("@DiscountId", bl_discountoffer.DiscountId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_discountoffer.RestaurantId);
        SqlParameter DiscountType = new SqlParameter("@DiscountType", bl_discountoffer.DiscountType);
        SqlParameter DiscountStart = new SqlParameter("@DiscountStart", bl_discountoffer.DiscountStart);
        SqlParameter DiscountEnd = new SqlParameter("@DiscountEnd", bl_discountoffer.DiscountEnd);
        SqlParameter DiscountOccurance = new SqlParameter("@DiscountOccurance", bl_discountoffer.DiscountOccurance);
        SqlParameter DiscountAmount = new SqlParameter("@DiscountAmount", bl_discountoffer.DiscountAmount);
        DL_Connection.UseExecuteNonQuery("AddDiscountOffers", Mode, OfferName, Date, DiscountId, RestaurantId, DiscountType, DiscountStart, DiscountEnd, DiscountOccurance, DiscountAmount);
    }

    public DataTable SelectDiscountOffer(BL_DiscountOffers bl_discountoffer)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_discountoffer.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectDiscountOffers", RestaurantId);
        return dt;
    }


    public DataTable SelectRestoIdName()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestoIdName");
        return dt;
    }
}