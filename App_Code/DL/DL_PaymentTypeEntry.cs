﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_PaymentTypeEntry
/// </summary>
public class DL_PaymentTypeEntry
{
	public DL_PaymentTypeEntry()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();

    public void PaymentTypeEntry(BL_PaymentTypeEntry bl_PaymentTypeEntry)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_PaymentTypeEntry.Mode);
        SqlParameter PaymentTEId = new SqlParameter("@PaymentTEId", bl_PaymentTypeEntry.PaymentTEId);
        SqlParameter PaymentTypeId = new SqlParameter("@PaymentTypeId", bl_PaymentTypeEntry.PaymentTypeId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_PaymentTypeEntry.RestaurantId);
        DL_Connection.UseExecuteNonQuery("AddPaymentTypeEntry", Mode, PaymentTEId, PaymentTypeId, RestaurantId);
    }

    public DataTable SelectPaymentTypeEntry(BL_PaymentTypeEntry bl_PaymentTypeEntry)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_PaymentTypeEntry.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectPaymentTypeEntry", RestaurantId);
        return dt;
    }

}