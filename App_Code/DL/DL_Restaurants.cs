﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BL_Restaurants
/// </summary>
public class DL_Restaurants
{
	public DL_Restaurants()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();
    public void Restaurants(BL_Restaurants bl_restaurants)
    {

        SqlParameter Mode = new SqlParameter("@Mode", bl_restaurants.Mode);
         SqlParameter AreaId = new SqlParameter("@AreaId", bl_restaurants.AreaId);
         SqlParameter CityId = new SqlParameter("@CityId", bl_restaurants.CityId);
        SqlParameter OwnerId = new SqlParameter("@OwnerId", bl_restaurants.OwnerId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        SqlParameter RestaurantName = new SqlParameter("@RestaurantName", bl_restaurants.RestaurantName);
        SqlParameter Date = new SqlParameter("@Date", bl_restaurants.Date);//
        SqlParameter Description = new SqlParameter("@Description", bl_restaurants.Description);
        SqlParameter Address = new SqlParameter("@Address", bl_restaurants.Address);
        SqlParameter Phoneno = new SqlParameter("@Phoneno", bl_restaurants.Phoneno);
        SqlParameter MobileNo1 = new SqlParameter("@MobileNo1", bl_restaurants.MobileNo1);
        SqlParameter MobileNo2 = new SqlParameter("@MobileNo2", bl_restaurants.MobileNo2);
        SqlParameter ContactPerson1 = new SqlParameter("@ContactPerson1", bl_restaurants.ContactPerson1);
        SqlParameter ContactPerson2 = new SqlParameter("@ContactPerson2", bl_restaurants.ContactPerson2);
        SqlParameter Email = new SqlParameter("@Email", bl_restaurants.Email);
        SqlParameter Image = new SqlParameter("@Image", bl_restaurants.Image);
        SqlParameter Password = new SqlParameter("@Password", bl_restaurants.Password);
        SqlParameter Active = new SqlParameter("@Active", bl_restaurants.Active);
        SqlParameter ResturantCategory = new SqlParameter("@list", bl_restaurants.ResturantCategory);
        SqlParameter PaymentTypesList = new SqlParameter("@PaymentTypesList", bl_restaurants.PaymentTypesList);
        SqlParameter LocationList = new SqlParameter("@LocationList", bl_restaurants.LocationList);
        SqlParameter i = new SqlParameter("@i", "0");
        //DL_Connection.UseExecuteNonQuery("AddRestaurants2", AreaId, CityId, OwnerId, RestaurantId, RestaurantName, Date, Description, Address, Phoneno, MobileNo1, MobileNo2, ContactPerson1, ContactPerson2, Email, Image,  Password, ResturantCategory, i, PaymentTypesList, LocationList);
        DL_Connection.UseExecuteNonQuery("AddRestaurants3",Mode, AreaId, CityId, OwnerId, RestaurantId, RestaurantName, Date, Description, Address, Phoneno, MobileNo1, MobileNo2, ContactPerson1, ContactPerson2, Email, Image,  Password,Active, ResturantCategory, i, PaymentTypesList, LocationList);
    }


    public DataTable SelectRestaurants(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantDetails");
        return dt;
    }


    public DataTable SelectDeactiveRestaurantDetails(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectDeactiveRestaurantDetails");
        return dt;
    }



    public DataTable SelectRestaurantDetailsByRestoId(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantDetailsByRestoId", RestaurantId);
        return dt;
    }

    public void InsertRestaurantCategoryEntry(BL_Restaurants bl_restaurants)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_restaurants.Mode);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        SqlParameter RestoCategoryId = new SqlParameter("@RestoCategoryId", bl_restaurants.RestoCategoryId);
        SqlParameter SelectRestoCatId = new SqlParameter("@SelectRestoCatId", bl_restaurants.SelectRestoCatId);
        DL_Connection.UseExecuteNonQuery("AddRestaurantCategoryEntry",Mode,SelectRestoCatId, RestaurantId, RestoCategoryId);
    }

    public DataTable SelectCityAreaWiseResto(BL_Restaurants bl_restaurants)
    {
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_restaurants.AreaId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_restaurants.CityId);
        DataTable dt = dl_connection.UseDataTablePro("SelectCityAreaWiseResto", AreaId, CityId);
        return dt;
    }

    public DataTable SelectFavorateRestaurant(BL_Restaurants bl_restaurants)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_restaurants.UserId);
        DataTable dt = dl_connection.UseDataTablePro("SelectFavorateRestaurant",UserId);
        return dt;
    }

    public DataTable BindRestoCategory(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestoCategory");
        return dt;
    }

    public DataTable BindRestaurantDDL(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantDetails");
        return dt;
    }
    public DataTable BindRestoCatDDL(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantCategory");
        return dt;
    }

    public DataSet BindRestoCategoryByRestoId(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        DataSet ds = dl_connection.UseDatasetPro("SelectRestoCatagorybyId", RestaurantId);
        return ds;
    }

    public DataTable BindRestoFacilities(BL_Restaurants bl_restaurants)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestoFacilities");
        return dt;
    }
   
    public DataTable SearchRestaurantsBySortMode(BL_Restaurants bl_restaurants)
    {
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_restaurants.AreaId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_restaurants.CityId);
        SqlParameter ColName = new SqlParameter("@ColName", bl_restaurants.ColName);
        DataTable dt = dl_connection.UseDataTablePro("SearchRestaurantsBySortMode", AreaId, CityId, ColName);
        return dt;
    }
  
    public DataTable FilterRestaurants(BL_Restaurants bl_restaurants)
    {
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_restaurants.AreaId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_restaurants.CityId);
        SqlParameter Cuisines = new SqlParameter("@List", bl_restaurants.Cuisines);
        SqlParameter Facility = new SqlParameter("@Facility", bl_restaurants.Facility);
        SqlParameter NameText = new SqlParameter("@NameText", bl_restaurants.NameText);
        SqlParameter Mode = new SqlParameter("@Mode", bl_restaurants.Mode);
        DataTable dt = dl_connection.UseDataTablePro("FilterRestaurants", AreaId, CityId, Cuisines, Facility, NameText, Mode);
        return dt;
    }
    public DataSet SelectAllRestaurantDetails(BL_Restaurants bl_restaurants)

    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        DataSet ds = dl_connection.UseDatasetPro("SelectAllRestaurantDetails", RestaurantId);
        return ds;
    }

    public DataSet SelectRestoConditionbyId(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        DataSet dt = dl_connection.UseDatasetPro("SelectRestoConditionbyId", RestaurantId);
        return dt;
    }

    public void InserFavourite(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        SqlParameter UserId = new SqlParameter("@UserId", bl_restaurants.UserId);
        dl_connection.UseDataTablePro("InserFavourite", RestaurantId, UserId);
    }

    public DataSet GetReviewData(BL_Restaurants bl_restaurants, int pageindex, int restoid)
    {
        SqlCommand cmd = new SqlCommand("selectReviewByRestoid");
        cmd.Parameters.AddWithValue("@PageIndex", pageindex);
        cmd.Parameters.AddWithValue("@PageSize", 10);
        cmd.Parameters.AddWithValue("@RestaurantId", restoid);
        cmd.Parameters.Add("@PageCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
        DataSet ds = dl_connection.UseDatasetAutoLoadPro(cmd, pageindex);    
        return ds;
    }

    public DataSet GetRestaurantData(BL_Restaurants bl_restaurants, int val, int cityid, int areaid)
    {
        SqlCommand cmd = new SqlCommand("SelectCityAreaWiseResto");
        cmd.Parameters.AddWithValue("@CityId", cityid);
        cmd.Parameters.AddWithValue("@AreaId", areaid);
        DataSet ds = dl_connection.UseDatasetRestoPro(cmd, val);
        return ds;
    }

    public DataSet SelectRestoCatagorybyId(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        DataSet dt = dl_connection.UseDatasetPro("SelectRestoCatagorybyId", RestaurantId);
        return dt;

    }



    public void DeleteRestaurant(BL_Restaurants bl_restaurants)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_restaurants.RestaurantId);
        SqlParameter Active = new SqlParameter("@Active", bl_restaurants.Active);
        DL_Connection.UseExecuteNonQuery("DeleteRestaurant", RestaurantId, Active);
    }
}