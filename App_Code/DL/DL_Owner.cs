﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public class DL_Owner
{
    DL_Connection dl_connection = new DL_Connection();
    public static string constr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
	public DL_Owner()
	{
		
	}

    public void InsertOwner(BL_Owner bl_owner)
    {
        SqlParameter OwnerId = new SqlParameter("@OwnerId", bl_owner.OwnerId);
        SqlParameter Mode = new SqlParameter("@Mode", bl_owner.Mode);
        SqlParameter Date = new SqlParameter("@Date", bl_owner.Date);
        SqlParameter FirstName = new SqlParameter("@FirstName", bl_owner.FirstName);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_owner.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_owner.LastName);
        SqlParameter PhoneNo = new SqlParameter("@PhoneNo", bl_owner.PhoneNo);
        SqlParameter Mobile1 = new SqlParameter("@Mobile1", bl_owner.Mobile1);
        SqlParameter Mobile2 = new SqlParameter("@Mobile2", bl_owner.Mobile2);
        SqlParameter Mobile3 = new SqlParameter("@Mobile3", bl_owner.Mobile3);
        SqlParameter Email = new SqlParameter("@Email", bl_owner.Email);
        SqlParameter CityId = new SqlParameter("@CityId", bl_owner.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_owner.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_owner.Address);
        SqlParameter Image = new SqlParameter("@Image", bl_owner.Image);
        SqlParameter Password = new SqlParameter("@Password", bl_owner.Password);
        DL_Connection.UseExecuteNonQuery("Owner",OwnerId, Mode, Date, FirstName, MiddleName, LastName, PhoneNo, Mobile1, Mobile2, Mobile3, Email, CityId, AreaId, Address, Image, Password);       
    }
    public DataTable BindOwner()
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectAllOwner");
        return dt;
    }
    //public DataTable SelectCity()
    //{
    //    string str = "Select CityId, CityName from City";
    //    DataTable dt = dl_connection.UseDataTablePro(str);
    //    return dt;
    //}
    //public DataTable SelectArea(BL_Owner bl_owner)
    //{
    //    string str = "Select AreaId,AreaName from Area where CityId='"+bl_owner.City+"'";
    //    DataTable dt = dl_connection.UseDataTablePro(str);
    //    return dt;
    //}

    public DataSet SelectOwnerRestoDetailsById(BL_Owner bl_owner)
    {
        SqlParameter OwnerId = new SqlParameter("@OwnerId", bl_owner.OwnerId);
        DataSet ds = dl_connection.UseDatasetPro("SelectOwnerRestoDetailsById", OwnerId);
        return ds;
    }

    public DataSet getcustomer(BL_Owner bl_owner,int pageindex)
    {
        SqlCommand cmd = new SqlCommand();//for delete
        DataSet ds = dl_connection.UseDatasetAutoLoadPro(cmd, pageindex);
        return ds;
      
    }

    public DataTable SelectOwner(BL_Owner bl_owner)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectOwnerNameId");
         return dt;
    }

    //public DataSet BindOwner(BL_Owner bl_owner, int pageindex)
    //{
    //    DataSet ds = dl_connection.UseDatasetAutoLoadPro("SelectAllOwner", pageindex);
    //    return ds;
    //}

}