﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_Order
/// </summary>
public class DL_Order
{
	public DL_Order()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();


    public int Orders(BL_Order bl_order)
    {
        SqlParameter Date = new SqlParameter("@Date", bl_order.Date);
        SqlParameter UserId = new SqlParameter("@UserId", bl_order.UserId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        SqlParameter DeliveryType = new SqlParameter("@DeliveryType", bl_order.DeliveryType);
        SqlParameter DeliveryTime = new SqlParameter("@DeliveryTime", bl_order.DeliveryTime);
        SqlParameter OrderStatus = new SqlParameter("@OrderStatus", bl_order.OrderStatus);
        SqlParameter DiscountId = new SqlParameter("@DiscountId", bl_order.DiscountId);
        SqlParameter DeliveryId = new SqlParameter("@DeliveryId", bl_order.DeliveryId);
        SqlParameter DeliveryCharges = new SqlParameter("@DeliveryCharges", bl_order.DeliveryCharges);
        SqlParameter OrderId = new SqlParameter("@OrderId", 0);
        OrderId.Direction = ParameterDirection.Output;

        DL_Connection.UseExecuteNonQuery("AddOrder", Date, UserId, RestaurantId, OrderId, DeliveryType, OrderStatus, DiscountId, DeliveryTime, DeliveryId, DeliveryCharges);
        int orderid = Convert.ToInt32(OrderId.Value);
        return orderid;

    }

    public void OrderDetails(BL_Order bl_order)
    {
        SqlParameter OrderId = new SqlParameter("@OrderId", bl_order.OrderId);
        SqlParameter Item = new SqlParameter("@Item", bl_order.Item);
        SqlParameter Price = new SqlParameter("@Price", bl_order.Price);
        SqlParameter Qty = new SqlParameter("@Qty", bl_order.Qty);
        DL_Connection.UseExecuteNonQuery("AddOrderDetails", OrderId, Item, Price, Qty);

    }
    public void UpdateNewOrder(BL_Order bl_order)
    {
        SqlParameter OrderId = new SqlParameter("@OrderId", bl_order.OrderId);
        DL_Connection.UseExecuteNonQuery("UpdateNewOrder",OrderId);
    }
    

    public DataSet SelectAllOrderStatus(BL_Order bl_order)
    {

        DataSet ds = dl_connection.UseDatasetPro("SelectAllOrderStatus");
        return ds;
    }
    public DataTable GetRestoDetailsByUserId(BL_Order bl_order)
    {

        SqlParameter UserId = new SqlParameter("@UserId", bl_order.UserId);
        DataTable dt = dl_connection.UseDataTablePro("GetRestoDetailsByUserId", UserId);
        return dt;
    }

    public DataSet GetOrderDetails(BL_Order bl_order)
    {

        SqlParameter OrderId = new SqlParameter("@OrderId", bl_order.OrderId);
        DataSet ds = dl_connection.UseDatasetPro("GetOrderDetails", OrderId);
        return ds;
    }

    public DataTable SelectRecentOrder(BL_Order bl_order)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectRecentOrder", RestaurantId);
        return dt;
    }

    public DataTable SelectNewOrder(BL_Order bl_order)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectNewOrder", RestaurantId);
        return dt;
    }

    public DataTable SelectLaterOrderById(BL_Order bl_order)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectLaterOrderById", RestaurantId);
        return dt;
    }
    

    public string GetNewOrderCount(BL_Order bl_order)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        SqlParameter Date = new SqlParameter("@Date", bl_order.Date);
        string count = dl_connection.UseExcuteScallerPro("GetNewOrderCount", RestaurantId, Date);
        return count;
    }


    public DataSet GetOrderDetailsByOrderId(BL_Order bl_Order)
    {
        SqlParameter OrderId = new SqlParameter("@OrderId", bl_Order.OrderId);
        DataSet ds = dl_connection.UseDatasetPro("GetOrderDetailsByOrderId", OrderId);
        return ds;
    }

    public void InsertReviewsByRestoId(BL_Order bl_order)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        SqlParameter UserId = new SqlParameter("@UserId", bl_order.UserId);
        SqlParameter OrderId = new SqlParameter("@OrderId", bl_order.OrderId);
        SqlParameter Date = new SqlParameter("@Date", bl_order.Date);
        SqlParameter Active = new SqlParameter("@Active", bl_order.Active);
        SqlParameter Comments = new SqlParameter("@Comments", bl_order.Comments);
        DL_Connection.UseExecuteNonQuery("InsertReviewsByRestoId", RestaurantId, UserId, OrderId, Date, Active, Comments);
    }

    public DataTable SelectOrderHistory(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectOrderHistory", RestaurantId, FromDate, ToDate);
        return dt;
    }

    public void UpdateDiscountOffers(BL_Order bl_order)
    {
        SqlParameter DiscountId = new SqlParameter("@DiscountId", bl_order.DiscountId);
        DL_Connection.UseExecuteNonQuery("UpdateDiscountOffers", DiscountId);
    }

    public DataTable GetDiscountById(BL_Order bl_order)
    {
        SqlParameter DiscountId = new SqlParameter("@DiscountId", bl_order.DiscountId);
        DataTable dt = dl_connection.UseDataTablePro("GetDiscountById", DiscountId);
        return dt;
    }

    public DataSet SelectTodayOrderByAccept(BL_Order bl_Order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_Order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_Order.FromDate);
        DataSet ds = dl_connection.UseDatasetPro("SelectTodayOrderByAccept", ToDate, FromDate);
        return ds;
    }

    public DataTable SelectResturantOrderDetailsByAccept(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectResturantOrderDetailsByAccept", RestaurantId, ToDate, FromDate);
        return dt;
    }





    public DataSet SelectTodayOrderByDisaccept(BL_Order bl_Order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_Order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_Order.FromDate);
        DataSet ds = dl_connection.UseDatasetPro("SelectTodayOrderByDisaccept", ToDate, FromDate);
        return ds;
    }

    public DataTable SelectResturantOrderDetailsByDisaccept(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectResturantOrderDetailsByDisaccept", RestaurantId, ToDate, FromDate);
        return dt;
    }




    public DataSet SelectTodayOrderByPlaceOrder(BL_Order bl_Order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_Order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_Order.FromDate);
        DataSet ds = dl_connection.UseDatasetPro("SelectTodayOrderByPlaceOrder", ToDate, FromDate);
        return ds;
    }

    public DataTable SelectResturantOrderDetailsByPlaceOrder(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectResturantOrderDetailsByPlaceOrder", RestaurantId, ToDate, FromDate);
        return dt;
    }





    public DataSet SelectTodayOrderUserCancel(BL_Order bl_Order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_Order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_Order.FromDate);
        DataSet ds = dl_connection.UseDatasetPro("SelectTodayOrderUserCancel", ToDate, FromDate);
        return ds;
    }

    public DataSet SelectLaterOrder(BL_Order bl_Order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_Order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_Order.FromDate);
        DataSet ds = dl_connection.UseDatasetPro("SelectLaterOrder", ToDate, FromDate);
        return ds;
    }

    public DataTable SelectResturantOrderDetailsByUserCancel(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectResturantOrderDetailsByUserCancel", RestaurantId, ToDate, FromDate);
        return dt;
    }

    public DataTable SelectResturantOrderDetailsByLater(BL_Order bl_order)
    {
        SqlParameter ToDate = new SqlParameter("@ToDate", bl_order.ToDate);
        SqlParameter FromDate = new SqlParameter("@FromDate", bl_order.FromDate);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_order.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectResturantOrderDetailsByLater", RestaurantId, ToDate, FromDate);
        return dt;
    }

    public void UpdateCall(string status, string orderid)
    {
        SqlParameter Status = new SqlParameter("@Status", status);
        SqlParameter Orderid = new SqlParameter("@Orderid", orderid);
        DL_Connection.UseExecuteNonQuery("UpdateCall", Status, Orderid);

    }

    public void RejectCall(string status, string orderid, string resion)
    {
        SqlParameter Status = new SqlParameter("@Status", status);
        SqlParameter Orderid = new SqlParameter("@Orderid", orderid);
        SqlParameter Resion = new SqlParameter("@Resion", resion);
        DL_Connection.UseExecuteNonQuery("RejectCall", Status, Orderid, Resion);

    }


}