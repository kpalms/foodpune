﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DL_Coupen
/// </summary>
public class DL_Coupen
{
    DL_Connection dl_connection = new DL_Connection();
	public DL_Coupen()
	{
		
	}

    public void InsertCoupen(BL_Coupen bl_coupen)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_coupen.Mode);
        SqlParameter CoupenId = new SqlParameter("@CoupenId", bl_coupen.CoupenId);
        SqlParameter Code = new SqlParameter("@Code", bl_coupen.Code);
        SqlParameter DiscountId = new SqlParameter("@DiscountId", bl_coupen.DiscountId);
        DL_Connection.UseExecuteNonQuery("AddCoupen", Mode,CoupenId,Code,DiscountId);
    }


    public DataTable BindCoupanGRD(BL_Coupen bl_coupen)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_coupen.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectCoupan", RestaurantId);
        return dt;
    }


    public DataTable SelectDiscountOffersNameId(BL_Coupen bl_coupen)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_coupen.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectDiscountOffersNameId", RestaurantId);
        return dt;
    }
}