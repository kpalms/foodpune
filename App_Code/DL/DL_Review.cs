﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DL_Review
/// </summary>
public class DL_Review
{
    DL_Connection dl_connection = new DL_Connection();

	public DL_Review()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void Review(BL_Review bl_review)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_review.UserId);
        SqlParameter Date = new SqlParameter("@Date", bl_review.Date);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_review.RestaurantId);
        SqlParameter Comments = new SqlParameter("@Comments", bl_review.Comments);
        DL_Connection.UseExecuteNonQuery("AddReview", UserId,Date, RestaurantId,Comments);
    }


    public DataTable SelectMyReview(BL_Review bl_review)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_review.UserId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMyreview", UserId);
        return dt;
    }

}