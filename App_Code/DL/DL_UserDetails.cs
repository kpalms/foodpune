﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DL_UserDetails
/// </summary>
public class DL_UserDetails
{

    DL_Connection dl_connection = new DL_Connection();
	public DL_UserDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int InsertUserDetails(BL_UserDetails bl_userdetails)
    {
        //SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString());

        SqlParameter Mode = new SqlParameter("@Mode", bl_userdetails.Mode);
        SqlParameter UserId = new SqlParameter("@UserId", bl_userdetails.UserId);
        UserId.Direction = ParameterDirection.Output;
        SqlParameter Gender = new SqlParameter("@Gender", bl_userdetails.Gender);

        SqlParameter Date = new SqlParameter("@Date", bl_userdetails.Date);
        SqlParameter FirstName = new SqlParameter("@FirstName", bl_userdetails.FirstName);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_userdetails.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_userdetails.LastName);
        SqlParameter Mobile = new SqlParameter("@Mobile", bl_userdetails.Mobile);
        SqlParameter Email = new SqlParameter("@Email", bl_userdetails.Email);
        SqlParameter Password = new SqlParameter("@Password", bl_userdetails.Password);
        DL_Connection.UseExecuteNonQuery("AddUserDetails", Mode, UserId,Gender, Date, FirstName, MiddleName, LastName, Mobile, Email, Password);
       int  userid = Convert.ToInt32(UserId.Value);
       return userid;



        //int new_MEM_BASIC_ID = -1971;
        //SqlCommand cmd = new SqlCommand("INS_MEM_BASIC", con);

        //cmd.CommandType = CommandType.StoredProcedure;

        //SqlParameter outPutVal = new SqlParameter("@New_MEM_BASIC_ID", SqlDbType.Int);

        //outPutVal.Direction = ParameterDirection.Output;
        //cmd.Parameters.Add(outPutVal);
        //cmd.Parameters.Add("@na", SqlDbType.Int).Value = "1";
        //cmd.Parameters.Add("@occ", SqlDbType.Int).Value = "2";
        //con.Open();
        //cmd.ExecuteNonQuery();
        //con.Close();

        //if (outPutVal.Value != DBNull.Value) new_MEM_BASIC_ID = Convert.ToInt32(outPutVal.Value);
        //return new_MEM_BASIC_ID;


    }

    public void ModifyUserDetails(BL_UserDetails bl_userdetails)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_userdetails.Mode);
        SqlParameter UserId = new SqlParameter("@UserId", bl_userdetails.UserId);
        SqlParameter FirstName = new SqlParameter("@FirstName", bl_userdetails.FirstName);
        SqlParameter MiddleName = new SqlParameter("@MiddleName", bl_userdetails.MiddleName);
        SqlParameter LastName = new SqlParameter("@LastName", bl_userdetails.LastName);
        SqlParameter Mobile = new SqlParameter("@Mobile", bl_userdetails.Mobile);
        SqlParameter Email = new SqlParameter("@Email", bl_userdetails.Email);
        SqlParameter CityId = new SqlParameter("@CityId", bl_userdetails.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_userdetails.AreaId);
       SqlParameter Address = new SqlParameter("@Address", bl_userdetails.Address);
       DL_Connection.UseExecuteNonQuery("ModifyUserDetails", Mode, UserId, FirstName, MiddleName, LastName, Mobile, Email, CityId, AreaId, Address);
    }
    


    public DataTable SelectMyProfile(BL_UserDetails bl_userdetails)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_userdetails.UserId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMyprofile", UserId);
        return dt;
    }

    public DataTable CheckUserLogin(BL_UserDetails bl_userdetails)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_userdetails.Email);
        SqlParameter Password = new SqlParameter("@Password", bl_userdetails.Password);
        DataTable dt = dl_connection.UseDataTablePro("CheckUserLogin", Email, Password);
        return dt;
    }

    public DataTable CheckUserEmailValidation(BL_UserDetails bl_userdetails)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_userdetails.Email);
        DataTable dt = dl_connection.UseDataTablePro("CheckUserEmailValidation", Email);
        return dt;
    }
    

    public void ChangeUserPassword(BL_UserDetails bl_userdetails)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_userdetails.UserId);
        SqlParameter Mode = new SqlParameter("@Mode", bl_userdetails.Mode);
        SqlParameter Password = new SqlParameter("@Password", bl_userdetails.Password);

        DL_Connection.UseExecuteNonQuery("ChangeUserPassword", UserId, Mode, Password);
        
    }


    public DataTable CheckUserPassword(BL_UserDetails bl_userdetails)
    {
        SqlParameter Email = new SqlParameter("@Email", bl_userdetails.Email);
        DataTable dt = dl_connection.UseDataTablePro("CheckUserPassword", Email);
        return dt;
    }

    

}