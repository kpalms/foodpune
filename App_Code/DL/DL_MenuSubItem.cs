﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_MenuSubItem
/// </summary>
public class DL_MenuSubItem
{

    DL_Connection dl_connection = new DL_Connection();

	public DL_MenuSubItem()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public DataTable SelectMenuSubItem(BL_MenuSubItem bl_MenuSubItem)
    {
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuSubItem.ItemId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuSubItemByItemId", ItemId);
        return dt;
    }

    public DataTable SelectOwnerMenuSubItemByItemId(BL_MenuSubItem bl_MenuSubItem)
    {
        SqlParameter ItemId = new SqlParameter("@ItemId", bl_MenuSubItem.ItemId);
        DataTable dt = dl_connection.UseDataTablePro("SelectOwnerMenuSubItemByItemId", ItemId);
        return dt;
    }
    public DataTable AddOwnerMenuSubItem(BL_MenuSubItem bl_MenuSubItem)
    {
        SqlParameter MenuSubItemId = new SqlParameter("@MenuSubItemId", bl_MenuSubItem.MenuSubItemId);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuSubItem.Active);
        DataTable dt = dl_connection.UseDataTablePro("AddOwnerMenuSubItem", Active, MenuSubItemId);
        return dt;
    }

}