﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Common
/// </summary>
/// 



using System.Data.SqlClient;
using System.Collections;
using System.Drawing.Printing;
using System.Data;
using System.Web.UI.WebControls;




public class Common
{
	public Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    DL_Connection dl_connection = new DL_Connection();
    BL_Field bl_field = new BL_Field();
    BL_City bl_city = new BL_City();
    BL_Area bl_area = new BL_Area();
    BL_PaymentType bl_PaymentType = new BL_PaymentType();
    BL_DiscountOffers bl_discountoffer = new BL_DiscountOffers();
    BL_Owner bl_owner = new BL_Owner();
    BL_Coupen bl_coupen = new BL_Coupen();


    public void SelectDiscountOffersNameId(DropDownList ddl, int restoid)
    {
        bl_coupen.RestaurantId = restoid;
        DataTable dt = bl_coupen.SelectDiscountOffersNameId(bl_coupen);
        ddl.DataSource = dt;
        ddl.DataValueField = "DiscountId";
        ddl.DataTextField = "OfferName";
        ddl.DataBind();
       // ddl.Items.Insert(0, "-- Select Offer --");
        ddl.Items.Insert(0, new ListItem("--Select Offer--", "0"));
        ddl.SelectedIndex = 0;
    }

    public void bindOwner(DropDownList ddl)
    {
       
        DataTable dt = bl_owner.SelectOwner(bl_owner);
        ddl.DataSource = dt;
        ddl.DataValueField = "OwnerId";
        ddl.DataTextField = "OwnerName";
        ddl.DataBind();
       // ddl.Items.Insert(0, "-- Select Owner --");
        ddl.Items.Insert(0, new ListItem("--Select Owner--", "0"));
        ddl.SelectedIndex = 0;
    }

    public void bindCity(DropDownList ddl)
    {
        bl_city.TextSearch = "p";
        DataTable dt = bl_city.BindCity(bl_city);
        ddl.DataSource = dt;
        ddl.DataValueField = "CityId";
        ddl.DataTextField = "CityName";
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("--Select City--", "0"));
         //ddl.Items.Insert(0, "-- Select City --");
         ddl.SelectedIndex = 0;
    }


    public void bindAreabycity(DropDownList ddl, int cityid)
    {

        bl_area.CityId = Convert.ToInt32(cityid);
        DataTable dt = bl_area.SelectAreaByCityId(bl_area);
        ddl.DataSource = dt;
        ddl.DataValueField = "AreaId";
        ddl.DataTextField = "AreaName";
        ddl.DataBind();
        //ddl.Items.Insert(0, "-- Select Area --");
        ddl.Items.Insert(0, new ListItem("--Select Area--", "0"));
        ddl.SelectedIndex = 0;

    }
     public void bindAreabycityAdmin(DropDownList ddl, int cityid)
    {

        bl_area.CityId = Convert.ToInt32(cityid);
        DataTable dt = bl_area.bindAreabycityAdmin(bl_area);
        ddl.DataSource = dt;
        ddl.DataValueField = "AreaId";
        ddl.DataTextField = "AreaName";
        ddl.DataBind();
       // ddl.Items.Insert(0, "-- Select Area --");
        ddl.Items.Insert(0, new ListItem("--Select Area--", "0"));
        ddl.SelectedIndex = 0;
    }
    
    public void bindRestoIdName(DropDownList ddl)
    {
        try
        {
            DataTable dt = bl_discountoffer.SelectRestoIdName();
            ddl.DataSource = dt;
            ddl.DataValueField = "RestaurantId";
            ddl.DataTextField = "RestaurantName";
            ddl.DataBind();
            //ddl.Items.Insert(0, "-- Select Restaurant --");
            ddl.Items.Insert(0, new ListItem("--Select Restaurant--", "0"));
            ddl.SelectedIndex = 0;
        }
        catch
        {

        }

    }


    public void bindPaymentType(DropDownList ddl)
    {
        try
        {
            DataTable dt = bl_PaymentType.SelectPaymentType();
            ddl.DataSource = dt;
            ddl.DataValueField = "PaymentTypeId";
            ddl.DataTextField = "PaymentType";
            ddl.DataBind();
            ddl.Items.Insert(0, "-- Select PaymentType --");
            ddl.Items.Insert(0, new ListItem("--Select PaymentType--", "0"));
            ddl.SelectedIndex = 0;
        }
        catch
        {

        }

    }
    //public void bindDistrict(DropDownList ddl)
    //{

    //    DataTable dt = bl_country.SelectDistrict(bl_country);
    //    ddl.DataSource = dt;
    //    ddl.DataValueField = "DistrictId";
    //    ddl.DataTextField = "DistrictName";
    //    ddl.DataBind();
    //    ddl.Items.Insert(0, "-- Select DistrictName --");

    //}

    //public void bindDistrictById(DropDownList ddl, int stateid)
    //{
    //    bl_country.StateId = stateid;
    //    DataTable dt = bl_country.SelectDistrictById(bl_country);
    //    ddl.DataSource = dt;
    //    ddl.DataValueField = "DistrictId";
    //    ddl.DataTextField = "DistrictName";
    //    ddl.DataBind();
    //    ddl.Items.Insert(0, "-- Select DistrictName --");

    //}


    //public void bindTahashilById(DropDownList ddl, int districtid)
    //{
    //    bl_country.DistrictId = districtid;
    //    DataTable dt = bl_country.SelectTahashilById(bl_country);
    //    ddl.DataSource = dt;
    //    ddl.DataValueField = "TahashilId";
    //    ddl.DataTextField = "TahashilName";
    //    ddl.DataBind();
    //    ddl.Items.Insert(0, "-- Select TahashilName --");

    //}



    //public void bindTahashil(DropDownList ddl)
    //{

    //    DataTable dt = bl_country.SelectTahashil(bl_country);
    //    ddl.DataSource = dt;
    //    ddl.DataValueField = "TahashilId";
    //    ddl.DataTextField = "TahashilName";
    //    ddl.DataBind();
    //    ddl.Items.Insert(0, "-- Select TahashilName --");
    //}

    public string ReturnOneValue(string query)
    {
        string OneValue = "";
        string id = dl_connection.UseExcuteScaler(query);
        if (id == "")
        {
            OneValue = "0";

        }
        else
        {
            OneValue = id;
        }
        return OneValue;

    }

    public int ReceiptNo()
    {
        int receiptno = 0;
        //string str = "select max (ReceiptNo) from Sale where Date='"+DateTime .Now .ToString ("dd/MM/yyyy")+"'";
        string str = "select max (BillNo) from Orders";
        string id = dl_connection.UseExcuteScaler(str);
        if (id == "")
        {
            receiptno = Convert.ToInt32("1");

        }
        else
        {
            receiptno = Convert.ToInt32(id) + 1;
        }
        return receiptno;

    }

    public int PasalNo()
    {
        int pasalno = 0;
        string str = "select max (ParsalNo) from OrderDetail where Date='" + DateTime.Now.ToString("dd/MM/yyyy") + "'";
        string id = dl_connection.UseExcuteScaler(str);
        if (id == "")
        {
            pasalno = Convert.ToInt32("1");

        }
        else
        {
            pasalno = Convert.ToInt32(id) + 1;
        }
        return pasalno;

    }


    public int KotNo()
    {
        int kotno = 0;
        string str = "select max (KotOrderNo) from OrderDetail";
        string id = dl_connection.UseExcuteScaler(str);
        if (id == "")
        {
            kotno = Convert.ToInt32("1");

        }
        else
        {
            kotno = Convert.ToInt32(id) + 1;
        }
        return kotno;

    }
    public int BillNo(string query)
    {
        int kotno = 0;
        string id = dl_connection.UseExcuteScaler(query);
        if (id == "")
        {
            kotno = Convert.ToInt32("1");

        }
        else
        {
            kotno = Convert.ToInt32(id) + 1;
        }
        return kotno;

    }

    //public string ReturnOneValue(string query)
    //{
    //    string OneValue = "";
    //    string id = dl_connection.UseExcuteScaler(query);
    //    if (id == "")
    //    {
    //        OneValue = "0";

    //    }
    //    else
    //    {
    //        OneValue = id;
    //    }
    //    return OneValue;

    //}
    //public void NumberOnly(KeyPressEventArgs e)
    //{
    //    if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\b')
    //    {
    //        e.Handled = true;

    //    }

    //}


    //convert word

    public String changeToWords(String numb, bool isCurrency)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = (isCurrency) ? ("Only") : ("");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    andStr = (isCurrency) ? ("and") : ("point");// just to separate whole numbers from points/cents
                    endStr = (isCurrency) ? ("Cents " + endStr) : ("");
                    pointStr = translateCents(points);
                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch { ;}
        return val;
    }
    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))
            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                    case 6:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 7://millions' range
                    case 8:
                    case 9:
                        pos = (numDigits % 7) + 1;
                        place = " Million ";
                        break;
                    case 10://Billions's range
                        pos = (numDigits % 10) + 1;
                        place = " Billion ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch { ;}
        return word.Trim();
    }
    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }
    private String ones(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }
    private String translateCents(String cents)
    {
        String cts = "", digit = "", engOne = "";
        for (int i = 0; i < cents.Length; i++)
        {
            digit = cents[i].ToString();
            if (digit.Equals("0"))
            {
                engOne = "Zero";
            }
            else
            {
                engOne = ones(digit);
            }
            cts += " " + engOne;
        }
        return cts;
    }
    //conver word




    public string GetDefaultPrinter()
    {
        PrinterSettings settings = new PrinterSettings();
        foreach (string printer in PrinterSettings.InstalledPrinters)
        {
            settings.PrinterName = printer;
            if (settings.IsDefaultPrinter)
                return printer;
        }
        return string.Empty;
    }


    public int ChangeSize()
    {
        System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
        string pp = GetDefaultPrinter();

        doctoprint.PrinterSettings.PrinterName = pp;
        int rawKind = 0;
        for (int i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count - 1; i++)
        {

            if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == "KPALMS")
            {
                rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                break;
            }
        }


        return rawKind;
    }


    public DateTime GetDate()
    {
        TimeZoneInfo timzoe = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        DateTime indiaTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timzoe);
        return indiaTime;

    }

}