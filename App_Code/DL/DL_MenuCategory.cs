﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_MenuCategory
/// </summary>
public class DL_MenuCategory
{
	public DL_MenuCategory()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();

    public void MenuCategory(BL_MenuCategory bl_MenuCategory)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_MenuCategory.Mode);
        SqlParameter CategoryId = new SqlParameter("@CategoryId", bl_MenuCategory.CategoryId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_MenuCategory.RestaurantId);
        SqlParameter Category = new SqlParameter("@Category", bl_MenuCategory.Category);
        SqlParameter Image = new SqlParameter("@Image", bl_MenuCategory.Image);
        SqlParameter Active = new SqlParameter("@Active", bl_MenuCategory.Active);
        DL_Connection.UseExecuteNonQuery("AddMenuCategory", Mode, CategoryId, RestaurantId, Category, Image, Active);

    }

    public DataTable SelectMenuCategory(BL_MenuCategory bl_MenuCategory)
    {
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_MenuCategory.RestaurantId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMenuCategory", RestaurantId);
        return dt;
    }

}