﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Dl_SearchType
/// </summary>
public class Dl_SearchType
{
	public Dl_SearchType()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    DL_Connection dl_connection = new DL_Connection();

    public void SearchType(Bl_SearchType bl_searchtype)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_searchtype.Mode);
        SqlParameter SearchId = new SqlParameter("@SearchId", bl_searchtype.SearchId);
        SqlParameter RestaurantId = new SqlParameter("@RestaurantId", bl_searchtype.RestaurantId);
        SqlParameter SearchText = new SqlParameter("@SearchText", bl_searchtype.SearchText);
        DL_Connection.UseExecuteNonQuery("AddSearchType", Mode, SearchId, RestaurantId, SearchText);

    }

    public DataTable SelectSearchType(Bl_SearchType bl_searchtype)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectSearchType");
        return dt;
    }
}