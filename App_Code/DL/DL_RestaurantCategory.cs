﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DL_RestaurantCategory
/// </summary>
public class DL_RestaurantCategory
{
	public DL_RestaurantCategory()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DL_Connection dl_connection = new DL_Connection();

    public void RestaurantCategory(BL_RestaurantCategory bl_RestaurantCategory)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_RestaurantCategory.Mode);
        SqlParameter RestoCategoryId = new SqlParameter("@RestoCategoryId", bl_RestaurantCategory.RestoCategoryId);
        SqlParameter RestoCategory = new SqlParameter("@RestoCategory", bl_RestaurantCategory.RestoCategory);
        DL_Connection.UseExecuteNonQuery("AddRestaurantCategory", Mode, RestoCategoryId, RestoCategory);


    }

    public DataTable SelectRestaurantCategory(BL_RestaurantCategory bl_RestaurantCategory)
    {
        DataTable dt = dl_connection.UseDataTablePro("SelectRestaurantCategory");
        return dt;
    }
}