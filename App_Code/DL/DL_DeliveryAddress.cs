﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DL_DeliveryAddress
/// </summary>
public class DL_DeliveryAddress
{
    DL_Connection dl_connection = new DL_Connection();
	public DL_DeliveryAddress()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int InsertDeliveryAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
        SqlParameter Mode = new SqlParameter("@Mode", bl_deliveryaddress.Mode);
        SqlParameter Date = new SqlParameter("@Date", bl_deliveryaddress.Date);
        SqlParameter FlatNo = new SqlParameter("@FlatNo", bl_deliveryaddress.FlatNo);
        SqlParameter Appartment = new SqlParameter("@Appartment", bl_deliveryaddress.Appartment);
       // SqlParameter DeliveryInstruction = new SqlParameter("@DeliveryInstruction", bl_deliveryaddress.DeliveryInstruction);
        SqlParameter AlternateContactNo = new SqlParameter("@AlternateContactNo", bl_deliveryaddress.AlternateContactNo);
        SqlParameter Company = new SqlParameter("@Company", bl_deliveryaddress.Company);
        SqlParameter Landmark = new SqlParameter("@Landmark", bl_deliveryaddress.Landmark);
        SqlParameter UserId = new SqlParameter("@UserId", bl_deliveryaddress.UserId);
        SqlParameter CityId = new SqlParameter("@CityId", bl_deliveryaddress.CityId);
        SqlParameter AreaId = new SqlParameter("@AreaId", bl_deliveryaddress.AreaId);
        SqlParameter Address = new SqlParameter("@Address", bl_deliveryaddress.Address);
        SqlParameter DeliveryNote = new SqlParameter("@DeliveryNote", bl_deliveryaddress.DeliveryNote);
        SqlParameter DeliveryId = new SqlParameter("@DeliveryId", 0);
        DeliveryId.Direction = ParameterDirection.Output;
        DL_Connection.UseExecuteNonQuery("AddDeliveryAddress", Mode, Date, FlatNo, Appartment, AlternateContactNo, Company, Landmark, UserId, CityId, AreaId, Address, DeliveryId, DeliveryNote);
        int deliveryid = Convert.ToInt32(DeliveryId.Value);
        return deliveryid;

    }
    public DataTable SelectMyAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_deliveryaddress.UserId);
        DataTable dt = dl_connection.UseDataTablePro("SelectMyAddress",UserId);
        return dt;
    }
    public DataTable SelectAddress(BL_DeliveryAddress bl_deliveryaddress)
    {
        SqlParameter UserId = new SqlParameter("@UserId", bl_deliveryaddress.UserId);
        DataTable dt = dl_connection.UseDataTablePro("SelectAddress", UserId);
        return dt;

    }

}