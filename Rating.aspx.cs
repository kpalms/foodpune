﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Rating : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(@"Data Source=KUNDANLAP-PC\SQLEXPRESS2005;Integrated Security=true;Initial Catalog=WantFoods");
        con.Open();
        SqlCommand cmd = new SqlCommand("insert into Rating(Rating,UserId,RestaurantId) values(@rating,@UserId,@RestaurantId)", con);
        cmd.Parameters.AddWithValue("@rating", Request.Form["rating"]);
        cmd.Parameters.AddWithValue("@UserId", 1);
        cmd.Parameters.AddWithValue("@RestaurantId", 1);
        cmd.ExecuteNonQuery();
    }
}